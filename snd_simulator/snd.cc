/*
 *      gap_nd_nav.cpp
 *
 *      Copyright 2007 Joey Durham <joey@engineering.ucsb.edu>
 *      modified by:   Luca Invernizzi <invernizzi.l@gmail.com>
 *      for ROS modified by:   Petr Martinec <petr.martinec@gmail.com>
 *
 *      lp program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      lp program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with lp program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

//#include <libplayercore/playercore.h>
//#include <replace/replace.h> /* for round() that is not available everywhere */
#include <iostream>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <vector>
#if defined (WIN32)
#if defined (min)
#undef min
#endif
#if defined (max)
#undef max
#endif
#else
#include <sys/times.h>
#endif

#include <sys/time.h>
#include <cstdio>

#include <logging.h>

#include "dllist.h"
#include "gap_and_valley.h"
#include "snd.h"

using namespace mre;

// Use gDebug to set verbosity of output, -1 for silent, 0 for hardly any output, 5 for normal debug
//int gDebug=5;
//
using imr::logger;
imr::CLoggerPtr sndLogger = imr::getLogger("snd");

#define TRACE(x) std::cout << x << std::endl;

/// Convert radians to degrees
inline double rtod(double r)
{
   return r * 180.0 / M_PI;
}

/// Convert degrees to radians
inline double dtor(double r)
{
   return r * M_PI / 180.0;
}


/// Limit a value to the range of min, max
   template<typename T>
inline T limit(T a, T min, T max)
{
   if (a < min)
      return min;
   else if (a > max)
      return max;
   else
      return a;
}


/// - static -------------------------------------------------------------------
imr::CConfig& CSnd::getConfig(imr::CConfig& config) {
   //   config.add<double>("x", "help", 0.0);
   double R = 0.25;
   config.add<double>("snd-robot-radius", "The radius of the minimum circle  which contains the robot", R);
   config.add<double>("snd-min-gap-width", "Minimum passage width the driver will try to exploit" , 2*R);
   config.add<double>("snd-obstacle-avoid-dist", "Maximum distance allowed from an obstacle", 4*R);
   config.add<double>("snd-max-speed", "Maximum speed allowed", 0.5);
   config.add<double>("snd-max-turn-rate", "Maximum angular speed allowed", dtor(60.0));
   config.add<double>("snd-goal-position-tol", "Maximum distance allowed from the final goal for the algorithm to stop", R/2);
   config.add<double>("snd-goal-angle-tol", "Maximum angular error from the final goal position for the algorithm to stop", dtor(360.0));

   return config;
}

/// - constructor --------------------------------------------------------------
CSnd::CSnd(SLaserConfig lconf, imr::CConfig& config) :
   cfg(config)
{
   R = cfg.get<double>("snd-robot-radius");
   minGapWidth = cfg.get<double>("snd-min-gap-width");
   safetyDistMax = cfg.get<double>("snd-obstacle-avoid-dist");
   maxSpeed = cfg.get<double>("snd-max-speed");
   maxTurnRate = cfg.get<double>("snd-max-turn-rate");
   goalPositionTol = cfg.get<double>("snd-goal-position-tol");
   goalAngleTol = cfg.get<double>("snd-goal-angle-tol");


   maxRange = lconf.maxRange;
   scanResolution = lconf.resolution;
   numScanRays = lconf.count;
}

double CSnd::timeval_subtract( timeval *end, timeval *start )
{
   if( end->tv_usec < start->tv_usec )
   {
      int nsec = (start->tv_usec - end->tv_usec)/1000000 + 1;
      start->tv_usec -= 1000000*nsec;
      start->tv_sec  += nsec;
   }
   if( end->tv_usec - start->tv_usec > 1000000 )
   {
      int nsec = (end->tv_usec - start->tv_usec)/1000000;
      start->tv_usec += 1000000*nsec;
      start->tv_sec  -= nsec;
   }

   return (end->tv_sec - start->tv_sec) + (end->tv_usec - start->tv_usec)/1000000.0;
}

bool CSnd::isRisingGapSafe( Gap* pRisingGap, int iValleyDir, std::vector<double> fullLP, double scanResolution, double maxRange, double R )
{
   // TODO: only checks if point creating gap is to close to obstacle on other side ...
   // does not guarantee safe passage through gap

   //double scanResolution = lp.GetScanRes();
   //double maxRange = lp.GetMaxRange();

   int iNumSectors = static_cast<int> (fullLP.size());
   int iRisingGap = pRisingGap->m_iSector;
   double gapDistance = pRisingGap->m_dist;

   TRACE("Distance to gap at " << iRisingGap << ": " << gapDistance << ", " << fullLP[iRisingGap]);

   double xGap = gapDistance*cos(scanResolution*(iRisingGap - iNumSectors/2));
   double yGap = gapDistance*sin(scanResolution*(iRisingGap - iNumSectors/2));

   for( int i = 1; i < iNumSectors/4; i++ )
   {
      int iTestSector = getIndex(iRisingGap + iValleyDir*i, iNumSectors);

      if( fullLP[iTestSector] < maxRange - 0.01 )
      {

	 double xI = fullLP[iTestSector]*cos(scanResolution*(iTestSector - iNumSectors/2));
	 double yI = fullLP[iTestSector]*sin(scanResolution*(iTestSector - iNumSectors/2));

	 double dist = sqrt( pow(xGap - xI, 2) + pow(yGap - yI, 2));

	 if( dist < 2.2*R )
	 {
	    TRACE("Gap at " << iRisingGap << " ruled out by proximity to obstacle at sector " << iTestSector);
	    return false;
	 }
      }
   }

   return true;
}


bool CSnd::isFilterClear( int iCenterSector, double width, double forwardLength, bool bDoRearCheck, std::vector<double> fullLP, double angRes )
{
   int iCount = static_cast<int> (fullLP.size());
   //double angRes = lp.GetScanRes();
   for( int i = 0; i < iCount; i++ )
   {
      int iDeltaSec = fabs(getSectorsBetween(i,iCenterSector,iCount));

      if( iDeltaSec > iCount/4 )
      {
	 // Semi-circle behind sensor
	 if( bDoRearCheck && (fullLP[i] < width/2.0) )
	 {
	    TRACE("Filter:  obstacle at sector " << i << " in rear semi-circle");
	    return false;
	 }
      }
      else
      {
	 // Rectangle in front of robot
	 double deltaAngle = iDeltaSec*angRes;
	 double d1 = (width/2.0)/(sin(deltaAngle));
	 double d2 = (forwardLength)/(cos(deltaAngle));

	 if( fullLP[i] < std::min(d1,d2) && forwardLength < width) //MK: added fullLP[i] << width to not care about far goals
	 {
	    TRACE("Filter: obstacle at sector " << i << " in front rectangle "  << fullLP[i] << " <" << d1 << " " << d2 << "deltaAngle: " << deltaAngle);
	    return false;
	 }
      }
   }


   return true;
}


/// - public method ---------------------------------------------------------------------------
void CSnd::setSpeed(double driveSpeed, double turnSpeed) {
   this->turnSpeed = turnSpeed;
   this->driveSpeed = driveSpeed;
}

/// - public method ---------------------------------------------------------------------------
double CSnd::getTurnSpeed(void) {
   return turnSpeed;
}

/// - public method ---------------------------------------------------------------------------
double CSnd::getDriveSpeed(void) {
   return driveSpeed;
}


void CSnd::oneStep(const SPosition &pos, CPath &path, const SScan& scan)
{
   try
   {				
      TRACE("Starting SND driver");
      TRACE("Robot radius: " << R << "; obstacle_avoid_dist " << safetyDistMax);
      TRACE("Pos tol: " << goalPositionTol << "; angle tol ");
      TRACE("minGapWidth" << minGapWidth);
      while( numScanRays <= 0 || numScanRays > 100000 || scanResolution <= 0.0 || scanResolution > 1.0 )
      {
	 TRACE("No real data");
	 /*
	    maxRange = robot->GetMaxRange();
	    scanResolution = robot->GetScanRes();
	    numScanRays = robot->GetCount();
	    */	
	 return;
      }


      int iNumSectors = (int)round( 2*M_PI/scanResolution +0.5 );

      TRACE("numScanRays: " << numScanRays << ", iNumSectors: " << iNumSectors);

      if( iNumSectors <= 0 || iNumSectors > 100000 || iNumSectors < numScanRays )
      {
	 ERROR("SND: Invalid number of sectors ");
	 return;
      }

      // Variables that get overwritten in loop
      //player_localize_hypoth_t hypo = localp.GetHypoth(0);
      //player_pose_t pose = hypo.mean;
      /*
	 pp.RequestGeom();
	 */		
      timeval startTimeval, loopTimeval, endTimeval;
      double diffTime, totalTime;

      int loopCount = 0;

      double distToGoal;
      double radToGoal;
      double SGoal;
      int iSGoal;

      double safetyDist;
      double minObsDist;
      int iSMinObs;
      double di;
      std::vector<double> fullLP(iNumSectors, 0.0);
      std::vector<double> PND(iNumSectors, 0.0);

      DLList<Gap*> gapList;

      DLLNode<Gap*>* pLoopNode = NULL;
      DLLNode<Gap*>* pNextNode = NULL;
      int iLoop, iNext;

      int iDeltaLoop, iDeltaNext;
      DLList<Valley*> valleyList;
      Valley* pValley = NULL;
      Valley* pBestValley = NULL;

      int iSTheta;
      double theta, newTurnRate, newSpeed;
      double fullTheta;
      double thetaDes = -99;
      double thetaAvoid = -99;

      gettimeofday( &endTimeval, NULL );
      gettimeofday( &startTimeval, NULL );

      //		for(;;) {

      //if( gDebug > 0 ) PLAYER_MSG0(1,"LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOP");
      gapList.clear();
      valleyList.clear();

      pLoopNode = NULL;
      pNextNode = NULL;

      /*			
      // lp blocks until new data comes; 10Hz by default
      robot.Read();
      */			
      gettimeofday( &loopTimeval, NULL);
      diffTime = timeval_subtract( &loopTimeval, &endTimeval );

      TRACE("Waited "<< diffTime << " for data");
      /*			
      // Compute which sector goal is in
      pthread_mutex_lock(&(pp.goal_mutex));
      double goalX = pp.goalX;
      double goalY = pp.goalY;
      double goalA = normalize(pp.goalA);
      pthread_mutex_unlock(&(pp.goal_mutex));
      */
	 SPosition goal = path.getGoal();

	 TRACE("Goal at: " << goal.x << "," << goal.y);
	 /*            
		       distToGoal = std::max(0.01,sqrt( pow(goalX - pp.GetXPos(),2) + pow(goalY-pp.GetYPos(),2)));
		       radToGoal = normalize(atan2( (goalY-pp.GetYPos())/distToGoal, (goalX-pp.GetXPos())/distToGoal ) - pp.GetYaw());
		       */

      distToGoal = std::max(0.01,sqrt( pos.squared_distance(goal)));
      radToGoal = normalize(pos.azimuth(goal)-pos.yaw);

      if( distToGoal < 1000 )
      {
	 TRACE("Goal " << distToGoal << "m away at " << rtod(radToGoal));
	 SGoal = iNumSectors/2.0 + radToGoal/scanResolution;
	 iSGoal = (int)round(SGoal);
      }
      else
      {
	 TRACE("Using direction mode");
	 SGoal = iNumSectors/2.0 + goal.yaw/scanResolution;
	 iSGoal = (int)round(SGoal);
      }

      TRACE("Goal sector = " << iSGoal << "  angle " << rtod(M_PI - 2*M_PI*iSGoal/(1.0*iNumSectors)));


      // Goal position fulfilled, no need to continue
      double lastTol = path.shouldSlowDown() ? 1 : 0;
      if( distToGoal < (goalPositionTol + lastTol)) // do not care about angle 
      {
	 //				if( fabs(normalize(pos.yaw - goal.yaw)) < goalAngleTol )
	 //				{
	 //					pp.SetSpeed(0.0, 0.0);
	 TRACE("=========================Reached goal location");
	 path.nextGoal();
	 if (!path.hasNextGoal()) {
	    setSpeed(0.0, 0.0);            
	 }

	 return;
	 // 				}
	 // 				else
	 // 				{
	 // 					newTurnRate = limit((goal.yaw - pos.yaw)/3, -maxTurnRate, maxTurnRate);
	 // 					TRACE("Spinning to goal angle " << goal.yaw << " from " << pos.yaw << ", tolerance " << goalAngleTol << ", turn rate " << newTurnRate);
	 // 					setSpeed(0.0, 0.0);
	 // 					
	 // 					return;
	 // 				}
      }

      // fill out fullLP, like lp but covers 2*M_PI with short distances where there is no data
      for( int i = 0; i < iNumSectors; i++ )
      {
	 int lpIdx = i - iNumSectors/2 + numScanRays/2;
	 if( lpIdx >= 0 && lpIdx < numScanRays )
	 {
	    /*					
						fullLP[i] = lp.range(lpIdx);
						*/
	    fullLP[i] = scan[lpIdx];
	 }
	 else 
	 {
	    fullLP[i] = maxRange;//R + safetyDistMax;
	 }
      }

      // Compute PND
      minObsDist = maxRange;
      iSMinObs = iNumSectors/2;
      for (int i = 0; i < iNumSectors; i++)
      {
	 if( fullLP[getIndex(i,iNumSectors)] >= maxRange )
	 {
	    PND[i] = 0;
	 }
	 else 
	 {
	    PND[i] = maxRange + 2*R - fullLP[getIndex(i,iNumSectors)];
	    if( fullLP[getIndex(i,iNumSectors)] < minObsDist )
	    {
	       minObsDist = fullLP[getIndex(i,iNumSectors)];
	       iSMinObs = getIndex(i,iNumSectors);
	    }
	 }
      }

      // Actual safety distance shrinks with proximity of closest obstacle point
      safetyDist = limit( 5*(minObsDist-R), 0.0, safetyDistMax );

      if( numScanRays < iNumSectors )
      {
	 // Force right edge of laser to be a left gap
	 int idx = iNumSectors/2 - numScanRays/2;
	 TRACE("Forcing left gap at right edge of laser scan: " << idx);
	 gapList.insertAtEnd( new Gap(getIndex(idx-1,iNumSectors), fullLP[getIndex(idx,iNumSectors)], 1) );
      }

      // Find discontinuties, should always be 'located' at the smaller PND, so that valley def is straightforward
      for( int i = 1; i < numScanRays; i++ )
      {
	 int PNDIdx = i + iNumSectors/2 - numScanRays/2;

	 di = PND[getIndex(PNDIdx,iNumSectors)] - PND[getIndex(PNDIdx-1,iNumSectors)];
	 if( di > minGapWidth )
	 {
	    TRACE("Left gap before " << PNDIdx << ", di " << di << ", pairs " << fullLP[getIndex(PNDIdx-1,iNumSectors)] << ", " << PND[getIndex(PNDIdx-1,iNumSectors)] << "; " << fullLP[getIndex(PNDIdx,iNumSectors)] << ", " << PND[getIndex(PNDIdx,iNumSectors)]);

	    gapList.insertAtEnd( new Gap(getIndex(PNDIdx-1,iNumSectors), fullLP[PNDIdx], 1) );
	 }
	 else if( di < -minGapWidth )
	 {
	    TRACE("Right gap at " << PNDIdx << ", di " << di << ", pairs " << fullLP[getIndex(PNDIdx-1,iNumSectors)] << ", " << PND[getIndex(PNDIdx-1,iNumSectors)] << "; " << fullLP[getIndex(PNDIdx,iNumSectors)] << ", " << PND[getIndex(PNDIdx,iNumSectors)]);
	    gapList.insertAtEnd( new Gap(getIndex(PNDIdx,iNumSectors), fullLP[PNDIdx-1], -1) );
	 }
      }

      if( numScanRays < iNumSectors )
      {
	 // Force left edge of laser to be a right gap
	 int idx = iNumSectors/2 - numScanRays/2 + numScanRays;
	 TRACE("Forcing right gap at left edge of laser scan: " << idx);
	 gapList.insertAtEnd( new Gap(getIndex(idx,iNumSectors), fullLP[getIndex(idx-1,iNumSectors)], -1) );
      }

      pLoopNode = gapList.head();
      pNextNode = NULL;
      while ( pLoopNode != NULL )
      {
	 pNextNode = gapList.next(pLoopNode);
	 if( pNextNode == NULL )
	    pNextNode = gapList.head();

	 iLoop = pLoopNode->m_data->m_iSector;
	 iNext = pNextNode->m_data->m_iSector;

	 if( iLoop == getIndex(iNext - 1,iNumSectors) )
	 {
	    // Combine any gaps that fall next to each other, going in same direction
	    if( pLoopNode->m_data->m_iDir == pNextNode->m_data->m_iDir )
	    {
	       if( pLoopNode->m_data->m_iDir > 0 )
	       {
		  // TODO:  Left gap removal should be done left to right to allow > 2 gaps to be combined
		  // Keep the right most of the two left gaps
		  TRACE("Removed duplicate left gap at " << iLoop);
		  gapList.deleteNode(pNextNode);
	       }
	       else if( pLoopNode->m_data->m_iDir < 0 )
	       {
		  // Keep the left most of the two right gaps
		  TRACE("Removed duplicate right gap at " << iNext);
		  pLoopNode->m_data->m_iSector = iNext;
		  gapList.deleteNode(pNextNode);
	       }
	    }
	 }

	 pLoopNode = gapList.next(pLoopNode);
      }


      pLoopNode = NULL;
      pNextNode = NULL;

      TRACE("Searching for valleys");

      // Find valleys, gaps must be in angle order with lowest (rightmost) first

      pLoopNode = gapList.head();
      while ( pLoopNode != NULL )
      {				
	 pNextNode = gapList.next(pLoopNode);
	 if( pNextNode == NULL )
	 {
	    pNextNode = gapList.head();
	 }

	 iLoop = pLoopNode->m_data->m_iSector;
	 iNext = pNextNode->m_data->m_iSector;

	 TRACE("Considering valley between " << iLoop << ", " << iNext);

	 pValley = NULL;
	 if( pLoopNode->m_data->m_iDir < 0 )
	 {
	    if ( pNextNode->m_data->m_iDir > 0 )
	    {
	       TRACE("Both disc. are rising");
	       // Both rising, pick one closest to direction of goal
	       iDeltaLoop = fabs(getSectorsBetween(iLoop, iSGoal, iNumSectors));
	       iDeltaNext = fabs(getSectorsBetween(iNext, iSGoal, iNumSectors));

	       if( iDeltaLoop <= iDeltaNext )
	       {
		  // Rising gap on the right
		  if( isRisingGapSafe( pLoopNode->m_data, 1, fullLP, scanResolution, maxRange, R ) )
		  {
		     pValley = new Valley( pLoopNode->m_data, pNextNode->m_data, 1 );
		  }
	       }
	       else
	       {
		  // Rising gap on the left
		  if( isRisingGapSafe( pNextNode->m_data, -1, fullLP, scanResolution, maxRange, R ) )
		  {
		     pValley = new Valley( pNextNode->m_data, pLoopNode->m_data, -1 );
		  }
	       }

	    }
	    else 
	    {
	       TRACE("Right is rising");
	       if( isRisingGapSafe( pLoopNode->m_data, 1, fullLP, scanResolution, maxRange, R ) )
	       {
		  pValley = new Valley( pLoopNode->m_data, pNextNode->m_data, 1 );
	       };
	    }
	 }
	 else 
	 {
	    if ( pNextNode->m_data->m_iDir > 0 )
	    {
	       TRACE("Left is rising");
	       if( isRisingGapSafe( pNextNode->m_data, -1, fullLP, scanResolution, maxRange, R ) )
	       {
		  pValley = new Valley( pNextNode->m_data, pLoopNode->m_data, -1 );
	       }
	    }
	 }

	 if( pValley != NULL )
	 {
	    TRACE("Found valley between " << iLoop << ", " << iNext << " with rising gap at " << pValley->m_pRisingDisc->m_iSector << " dir " << pValley->m_iRisingToOther);


	    if( false )
	    {
	       // See if lp valley should be combined with a neighbor
	       DLLNode<Valley*>* pValleyLoopNode = valleyList.head();
	       Gap* newRight = NULL;
	       Gap* newLeft  = NULL;

	       while ( pValleyLoopNode != NULL )
	       {
		  Gap* pValleyLeft = pValleyLoopNode->m_data->m_pRisingDisc;
		  Gap* pValleyRight = pValleyLoopNode->m_data->m_pOtherDisc;
		  if (pValleyLoopNode->m_data->m_iRisingToOther > 0)
		  {
		     pValleyLeft = pValleyLoopNode->m_data->m_pOtherDisc;
		     pValleyRight = pValleyLoopNode->m_data->m_pRisingDisc;
		  }

		  if (pValleyRight->m_iSector == iNext)
		  {
		     newRight = new Gap(pLoopNode->m_data);
		     newLeft  = new Gap(pValleyLeft);

		     TRACE("Left matches right of prior valley!");

		     break;
		  }
		  else if( pValleyLeft->m_iSector == iLoop )
		  {
		     newRight = new Gap(pValleyRight);
		     newLeft  = new Gap(pNextNode->m_data);

		     TRACE("Right matches left of prior valley!");

		     break;
		  }

		  pValleyLoopNode = valleyList.next(pValleyLoopNode);
	       }

	       if( newRight != NULL && newLeft != NULL )
	       {
		  if( newRight->m_iDir < 0 )		
		  {
		     if ( newLeft->m_iDir > 0 )
		     {
			TRACE("Both disc. are rising");
			// Both rising, pick one closest to direction of goal
			iDeltaLoop = fabs(getSectorsBetween(newRight->m_iSector, iSGoal, iNumSectors));
			iDeltaNext = fabs(getSectorsBetween(newLeft->m_iSector, iSGoal, iNumSectors));

			if( iDeltaLoop <= iDeltaNext )
			{
			   pValleyLoopNode->m_data->overwrite( new Gap(newRight), new Gap(newLeft), 1 );
			}
			else
			{
			   pValleyLoopNode->m_data->overwrite( new Gap(newLeft), new Gap(newRight), -1 );
			}
		     }
		     else 
		     {
			TRACE("Right is rising");
			pValleyLoopNode->m_data->overwrite( new Gap(newRight), new Gap(newLeft), 1 );
		     }
		  }
		  else 
		  {
		     if ( newLeft->m_iDir > 0 )
		     {
			TRACE("Left is rising");
			pValleyLoopNode->m_data->overwrite( new Gap(newLeft), new Gap(newRight), -1 );
		     }
		  }

		  TRACE("Merging valleys, valley now spans " << newRight->m_iSector << ", " << newLeft->m_iSector << " with rising gap at " << pValleyLoopNode->m_data->m_pRisingDisc->m_iSector << " dir " << pValleyLoopNode->m_data->m_iRisingToOther );

		  delete newRight;
		  delete newLeft;
		  delete pValley;
	       }
	       else
	       {
		  valleyList.insertAtEnd( pValley );
	       }
	    }
	    else
	    {
	       valleyList.insertAtEnd( pValley );	
	    }
	 }

	 pLoopNode = gapList.next(pLoopNode);
      }



      // Pick best valley
      DLLNode<Valley*>* pValleyLoopNode = valleyList.head();
      int iPass = 1;
      pBestValley = NULL;
      int iBestStoGoal = iNumSectors;
      while ( pValleyLoopNode != NULL )
      {

	 if( iPass == 1 )
	 {
	    if( numScanRays >= iNumSectors || !(pValleyLoopNode->m_data->isSectorInValley( 0, iNumSectors )) )
	    {
	       // Ignore the non-visible valley behind robot on first pass
	       int iStoGoal = fabs(getSectorsBetween(pValleyLoopNode->m_data->m_pRisingDisc->m_iSector, iSGoal, iNumSectors));

	       if( iStoGoal < iBestStoGoal )
	       {
		  iBestStoGoal = iStoGoal;
		  pBestValley = pValleyLoopNode->m_data;

		  TRACE("Pass " << iPass << ": considering valley " <<  pBestValley->m_pRisingDisc->m_iSector << ", " << pBestValley->m_pOtherDisc->m_iSector);
	       }
	    }
	 }
	 else if( iPass == 2 )
	 {
	    if( pBestValley == NULL || iBestStoGoal > (1 + iNumSectors/4) )
	    {
	       /*if( pValleyLoopNode->m_data->isSectorInValley( iSGoal, iNumSectors ) )
		 {
			   // Goal in valley, we're done
			   pBestValley = pValleyLoopNode->m_data;

			   if( gDebug > 5 ) 
			   {
			   cout<< "  Pass " << iPass << ": valley ";
			   cout<< pBestValley->m_pRisingDisc->m_iSector << ", " << pBestValley->m_pOtherDisc->m_iSector;
			   cout<< " contains goal" << endl;
			   }
			   break;
			   }*/

	       // Pick new best if prior best has dot-product with iSGoal < 0
	       int iStoGoal = fabs(getSectorsBetween(pValleyLoopNode->m_data->m_pRisingDisc->m_iSector, iSGoal, iNumSectors));

	       if( iStoGoal < iBestStoGoal )
	       {
		  iBestStoGoal = iStoGoal;
		  pBestValley = pValleyLoopNode->m_data;

		  TRACE("Pass " << iPass << ": considering valley " << pBestValley->m_pRisingDisc->m_iSector << ", " << pBestValley->m_pOtherDisc->m_iSector);
	       }
	    }
	 }

	 pValleyLoopNode = valleyList.next(pValleyLoopNode);
	 if( pValleyLoopNode == NULL && numScanRays < iNumSectors && iPass == 1 )
	 {
	    pValleyLoopNode = valleyList.head();
	    iPass = 2;
	 }
      }

      distToGoal = sqrt(pos.squared_distance(goal));

      if( minObsDist < R )
      {
	 TRACE("!!! Obstacle inside robot radius !!! Stopping. minObsDist: " << minObsDist << " R: " << R);
	 iSTheta = static_cast<int> (fullLP.size()/2);

	 //robot->exit();
	 return;
      }
      else if( pBestValley == NULL )
      {
	 // No gaps
	 TRACE("No gaps to follow ... ");

	 // Check if goal is clear (covers the larger empty room case)
	 if( isFilterClear(iSGoal, 2*R, std::min(maxRange - R, distToGoal - R), false, fullLP, scanResolution) )
	 {
	    TRACE("clear path to goal" );

	    iSTheta = iSGoal;
	 }
	 else
	 {
	    // Nowhere to go ... stay here and spin until a valley comes up
	    // write commands to robot
	    TRACE("spinning in place" );

	    iSTheta = 0;
	 }
      }
      else 
      {
	 // Determine scenario robot is in

	 int iRisingDisc = pBestValley->m_pRisingDisc->m_iSector;
	 int iValleyDir = pBestValley->m_iRisingToOther;

	 // Valley width is smaller of sector distance to next gap or sector distance to
	 // first point in valley that is closer to robot than rising gap
	 int iValleyWidth = pBestValley->getValleyWidth(fullLP);

	 TRACE("Best valley: " << iRisingDisc << " to " << pBestValley->m_pOtherDisc->m_iSector << ", dir " << iValleyDir );

	 // Find iSsrd, safe direction where robot will head towards rising disc but on the safe side of the corner that created it
	 double cornerDist = pBestValley->m_pRisingDisc->m_dist;
	 TRACE("Adjusted width of valley is " << iValleyWidth << ", with corner dist " << cornerDist );

	 int iSAngle;
	 if( cornerDist < (safetyDistMax + R) )
	 {
	    iSAngle = iNumSectors/4;
	 }
	 else
	 {
	    iSAngle = (int)round(asin( limit((safetyDistMax + R)/cornerDist, -1.0, 1.0) )/scanResolution);
	 }

	 // Issue occurs when goal is behind robot, close obstacle at edge of field of view
	 // can cause iSsrd to point 90 degrees away from right/left edge of visibility
	 // Quick fix: limit iSAngle to less than 1/2 of laser fov
	 // TODO: this is kind of a hack
	 iSAngle = std::min(iSAngle, numScanRays/3);

	 // iSSrd, safe rising discontinuity
	 int iSsrd = getIndex(iRisingDisc + iSAngle*iValleyDir, iNumSectors);

	 // iSMid, middle of valley
	 int iSMid = getIndex( iRisingDisc + iValleyDir*((iValleyWidth/2) - 1) , iNumSectors );

	 int iSt = -1;

	 // Goal position behavior
	 if( iSt < 0 && fabs(getSectorsBetween(iSGoal,iNumSectors/2,iNumSectors)) < std::min(iNumSectors/4, numScanRays/2) )
	 {
	    // Goal is in front of robot
	    if( isFilterClear(iSGoal, 2*R, std::min(maxRange - R, distToGoal - R), false, fullLP, scanResolution) )
	    {
	       TRACE("Clear path to goal" );
	       iSt = iSGoal;
	    }
	 }

	 if( iSt < 0 && fabs(getSectorsBetween(iRisingDisc,iSMid,iNumSectors)) < fabs(getSectorsBetween(iRisingDisc,iSsrd,iNumSectors)) )
	 {
	    iSt = iSMid;
	 }

	 if( iSt < 0 ) 
	 {
	    iSt = iSsrd;
	 }

	 assert( iSt >= 0 && iSt < iNumSectors );

	 TRACE("Best valley has rising disc. at " << iRisingDisc << " with iSSrd " << iSsrd << ", iSMid " << iSMid << ", iSt " << iSt );

	 double fracExp = 1.0;
	 double Sao = 0;

	 double modS;
	 double modAreaSum = 0.0;
	 int iDeltaS;
	 int iSaoS;

	 for (int i = 0; i < iNumSectors; i++)
	 {
	    modS = pow(limit((safetyDist + R - fullLP[i])/safetyDist,0.0,1.0),fracExp);
	    iSaoS = getIndex( i + iNumSectors/2, iNumSectors );
	    iDeltaS = getSectorsBetween( iSt, iSaoS, iNumSectors );

	    modAreaSum += modS*modS;

	    if( false ) 
	    {
	       // Weighted by "perimeter" method for all-in-one nav
	       Sao += modS*modS*iDeltaS;
	    }
	    else
	    {
	       // Weighted by "area" method for all-in-one nav
	       Sao += modS*modS*modS*iDeltaS;
	    }
	 }

	 if( modAreaSum > 0 )
	 {
	    Sao /= modAreaSum;
	 }
	 else
	 {
	    Sao = 0;
	 }

	 TRACE("Sao " << (int)Sao << ", mod area sum " << modAreaSum );

	 thetaAvoid = scanResolution*(Sao);
	 thetaDes = scanResolution*(iSt - iNumSectors/2.0);

	 iSTheta = (int)round(iSt + Sao);

	 //iSTheta = getIndex( iSTheta, iNumSectors );
	 // Don't let obstacle avoidance change turn direction, we can turn in place
	 iSTheta = std::max(0,iSTheta);
	 iSTheta = std::min(iNumSectors-1,iSTheta);

      }

      theta = scanResolution*(iSTheta - iNumSectors/2.0);
      fullTheta = theta;
      theta = limit( theta, -M_PI/2.0, M_PI/2.0 );

      newTurnRate = maxTurnRate*(2.0*theta/M_PI);

      theta = limit( theta, -M_PI/4.0, M_PI/4.0 );

      newSpeed = maxSpeed;

      if (path.shouldSlowDown())
	 newSpeed *= limit(2*distToGoal,0.0,1.0);

      newSpeed *= limit((minObsDist-R)/safetyDistMax,0.0,1.0);
      newSpeed *= limit((M_PI/6.0 - fabs(theta))/(M_PI/6.0),0.0,1.0);

      TRACE("Theta: " << theta << " (" << iSTheta << "),  Vel:  " << newSpeed << ",  Turn: " << newTurnRate);

      // write commands to robot
      /*			
				pp.SetSpeed(newSpeed, newTurnRate);
				*/
	 setSpeed( newSpeed, newTurnRate );
	 loopCount++;

	 gettimeofday(&endTimeval, NULL);
	 diffTime = timeval_subtract( &endTimeval, &loopTimeval );
	 totalTime = timeval_subtract( &endTimeval, &startTimeval );			
	 TRACE("SND: Execution time: "<< diffTime);

	 //if( gDebug > 0 ) printf("Data: %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f",pp.GetXPos(),pp.GetYPos(),pp.GetYaw(),newSpeed,newTurnRate,fullTheta,thetaDes,thetaAvoid,minObsDist,safetyDist,totalTime);


	 //usleep( 10000 );

	 //}		
   }
   catch (...)
   {
      ERROR("SND: Error!" );
      return;		
   }

   return;
}

