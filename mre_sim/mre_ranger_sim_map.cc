/*
 * File name: mre_ranger_sim_map.cc
 * Date:      2012/02/10 08:55
 * Author:    Jan Faigl
 */

#include <boost/foreach.hpp>

#include <logging.h>
#include <imr_assert.h>

#include "mre_sdl_gui.h"

#include "mre_ranger_sim_map.h"

#define foreach BOOST_FOREACH

using imr::logger;

using namespace mre;

/// - static method ------------------------------------------------------------
imr::CConfig& CRangerSimMap::getConfig(imr::CConfig& config) {
   config.add<double>("ranger-sim-map-real-width", "Real width dimension of the map", -1.0);
   config.add<double>("ranger-sim-map-real-heigh", "Real heigh dimension of the map", -1.0);
 //  config.add<double>("ranger-sim-map-cell-size", "Real heigh dimension of the map", -1.0);
   return config;
}

/// - constructor --------------------------------------------------------------
CRangerSimMap::CRangerSimMap(imr::CConfig& cfg, const std::string& filename, double cell_size) : 
   cfg(cfg),
   MAP_W(cfg.get<double>("ranger-sim-map-real-width")),
   MAP_H(cfg.get<double>("ranger-sim-map-real-heigh")),
   CELL_SIZE(cell_size),
 //  CELL_SIZE(cfg.get<double>("ranger-sim-map-cell-size")),
   W(round(MAP_W / CELL_SIZE)),
   H(round(MAP_H / CELL_SIZE)),
   MIN_X(-MAP_W / 2), MAX_X(MAP_W / 2),
   MIN_Y(-MAP_H / 2), MAX_Y(MAP_H / 2),
   KX(W / MAP_W), KY(H / MAP_H),
   grid(H, W)
{
   ASSERT_ARGUMENT(MAP_W > 0 and MAP_H > 0, "CRangerSimMap - Map dimensions must be given");
   DEBUG("CRangerSimMap - grid size " << W << "x" << H);
   SDL_Surface* s = mre::gui::CSDL::load_img(filename, true);
   DEBUG("CRangerSimMap - loaded map resolution " << s->w << "x" << s->h);
   SDL_Surface* s2 = mre::gui::CSDL::resize(s, 1.0*W / s->w, 1.0*H / s->h);
   ASSERT_ARGUMENT(s2, "CRangerSimMap - cannot resize map");
   DEBUG("CRangerSimMap - resized map resolution " << s2->w << "x" << s2->h);
   ASSERT_ARGUMENT(W == s2->w and H == s2->h, "Dimension of the resized map does not match with the required dimensions");

   unsigned char* pixels = (unsigned char*)s2->pixels;
   for(int i = 0; i < grid.SIZE; ++i) {
      grid(i) = *pixels != 0 ? FREESPACE : OBSTACLE;
      pixels += s2->format->BytesPerPixel;
   }
   SDL_FreeSurface(s);
   SDL_FreeSurface(s2);
}

/// - destructor ---------------------------------------------------------------
CRangerSimMap::~CRangerSimMap() {
}

/// - public method -----------------------------------------------------------
bool CRangerSimMap::isCollisionFree(const GridCoordsVector& pts, GridCoords& collisionPoint) const {
   bool ret = true;
   collisionPoint = pts.back();
   foreach(const GridCoords& pt, pts) {
      if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
         if (
               grid(pt.y, pt.x) == OBSTACLE 
               or (pt.x > 1 and grid(pt.y, pt.x - 1) == OBSTACLE) or (pt.x < (W-1) and grid(pt.y, pt.x + 1) == OBSTACLE) 
               or (pt.y > 1 and grid(pt.y - 1, pt.x) == OBSTACLE) or (pt.y < (H-1) and grid(pt.y+1, pt.x) == OBSTACLE) 
            )
         {
            ret = false;
            collisionPoint = pt;
            break;
         }
      } else {
         break; //ray goes out of the map
      }
   }
   return ret;
}

/* end of mre_ranger_sim_map.cc */
