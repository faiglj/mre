/*
 * File name: robot_player.cc
 * Date:      2009/06/27 12:33
 * Author:    Jan Faigl
 * Author:    Miroslav Kulich
 */

#include <fstream>
#include <boost/program_options.hpp>
#include <boost/foreach.hpp>

#include <logging.h>
#include <boost_args_config.h>
#include <stringconversions.h>

#include "mre_types.h"
#include "path_follower.h"
#include "mre_grid_utils.h"

#include "robot_player.h"

#define foreach BOOST_FOREACH

using namespace mre;

using imr::logger;

/// - static -------------------------------------------------------------------
imr::CConfig& CRobotPlayer::getConfig(imr::CConfig& config) {
   config.add<std::string>("host", "player server hostname", "localhost");
   config.add<int>("port", "player server port", 6665);
   config.add<int>("laser-index", "laser index", 0);
   config.add<int>("position2d-index", "position 2d index", 0);
   config.add<int>("global-position2d-index", "global position 2d index", 2);
   config.add<double>("robot-disk-radius", "Robot disk radius", 0.15);
   config.add<int>("snd-index", "snd index", 1);
   /*
   config.add<double>("x", "initial x position of the robot", 0.0);
   config.add<double>("y", "initial y position of the robot", 0.0);
   config.add<bool>("coords", "enable/disable settings of initial coords of the robot", false);
   config.add<double>("yaw", "initial yaw of the robot ", 0.0);
   */
   config.add<double>("forward-velocity-gain", "forward velocity gain", 0.09);
   config.add<double>("angular-velocity-gain", "angular velocity gain", 0.8);

   /*
   CLaserData::getConfig(config);
   CMapGrid::getConfig(config);
   CGuiUpdate::getConfig(config);
   CExploration::getConfig(config);
   */
   CPathFollower::getConfig(config);
   return config;
}

/// - constructor --------------------------------------------------------------
CRobotPlayer::CRobotPlayer(const std::string& cfgFilename, int label, CMap& map, EventQueue& queue, boost::barrier* waitForDataBarrier) : ThreadBase(),
   map(map), robot(label),
   client(0), quit(false), laser(0), position2d(0), globalPosition2d(0), queue(queue), pathFollower(0), waitForDataBarrier(waitForDataBarrier), travelledDistance(0), executionSteps(0)
{
   try {
      namespace po = boost::program_options;
      po::options_description options("Robot options");
      boost_args_add_options(getConfig(cfg), "", options);
      po::variables_map vm;
      std::ifstream ifs(cfgFilename.c_str());
      store(parse_config_file(ifs, options), vm);
      po::notify(vm);
      ifs.close();

   } catch (std::exception& e) {
      throw imr::argument_error(e.what());
   }
   std::stringstream ss;
   cfg.print(ss);
   DEBUG("CRobotPlayer -- file '" << cfgFilename << "' Config:\n" << ss.str());
  pathFollower = new CPathFollower(cfg);

   { //create robot disk representation
      const int n = 24;
      const double radius = cfg.get<double>("robot-disk-radius");
      IntVector vx;
      IntVector vy;
      double angle = -M_PI;
      for(int i = 0; i < n; ++i) {
         vx.push_back(map.real2gridX(radius * cos(angle)));
         vy.push_back(map.real2gridY(radius * sin(angle)));
         angle += 2*M_PI / n;
      }
      fillPolygon(vx, vy, map.W, robot.shape);
   }
connected = false;


 //     for(int
 //           robotShape;
 //           }

/* TODO for real robots
// check laser
for (int i = 0; i < 20; i++) { // laser init retry
client->Read();
if (laser->GetCount() > 0) {
assert_argument(laser->GetCount() == laserData.getNbr(), "Unexpected number of laser beams");
break;
}
}

// set robot to center of the map
double x = gridMap.getRealSize().width() / 2.;
double y = gridMap.getRealSize().height() / 2.;
double yaw = 0.0;
if (cfg.get<bool>("coords")) {
x = cfg.get<double>("x");
y = cfg.get<double>("y");
yaw = cfg.get<double>("yaw");
}
DEBUG("SetOdometry: " << x << " " << y << " " << yaw);
for (int i = 0; i < 10; i++) {
position2d->SetOdometry(x, y, yaw);
client->Read(); //to be sure set odometry is propageted to the robot
}
*/
}

/// - destructor ---------------------------------------------------------------
CRobotPlayer::~CRobotPlayer() {
}

/// - public method ------------------------------------------------------------
void CRobotPlayer::stop(void) {
   {
      ScopedLock lk(mtx);
      DEBUG("robot stop");
      quit = true;
   }
   join();
}


/// - public method -----------------------------------------------------------
void CRobotPlayer::setShape(double r) {
  robot.shape.clear();
  const int neigh = (ceil(r / map.CELL_SIZE)); //2
  const int neigh2 = neigh*neigh;
  for (int i = -neigh; i < neigh + 1; i++) { //enlarge grid
      for (int j = -neigh; j < neigh + 1; j++) {
        if (i*i + j*j <= neigh2) {
          const int x = robot.gridPose.x + j;
          const int y = robot.gridPose.y + i;
          if (x >= 0 and x < map.W and y >= 0 and y < map.H) {
            robot.shape.push_back(x + y * map.W);
          }
        }
      }
  } //end neigh
  ASSERT_ARGUMENT(!robot.shape.empty(), "Robot shape is empty");
}

/// - public method -----------------------------------------------------------
double CRobotPlayer::getGoalRemainingDistance(void) const {
   double ret = 0.0;
   if (pathFollower) {
      ret = pathFollower->currentRemainingDistance(robot.pose);
   }
   return ret;
}

/// - public method -----------------------------------------------------------
bool CRobotPlayer::waitForConnection(void) {
   bool ret = false;
   ScopedLock lk(mtx);
   if (!connected) {
      cond.wait(lk);
   }
   ret = connected;
   return ret;
}


/// - protected method ---------------------------------------------------------
void CRobotPlayer::threadBody(void) {
   std::string label = "r" + imr::string_format<int>(robot.LABEL);
   EventQueue::Message msg;
   try {
      {
         ScopedLock lk(mtx);
         connected = false;
         connect();
         DEBUG("CRobotPlayer::threadBody");
         cond.notify_all();
      }
      const double forwardVelGain = cfg.get<double>("forward-velocity-gain");
      const double angularVelGain = cfg.get<double>("angular-velocity-gain");
      bool q = false;
      bool motorEnabled = false;
      bool checkForDone = true; //TODO report if all goals has been traversed
      bool shouldWait = true;
      SPosition prevPose;
      GridCoords prevGridPose;

      position2d->SetMotorEnable(true);

      //      SPosition poseOdo;
      bool valid_pose = false;
      while (!q) {
         client->Read();
         while(client->Peek(0)) { client->Read(); }
         {
            ScopedLock lk(mtx);
            if (globalPosition2d->IsFresh() && globalPosition2d->IsValid()) { // process position 2d first
               robot.pose.x = globalPosition2d->GetXPos();
               robot.pose.y = globalPosition2d->GetYPos();
               robot.pose.yaw = globalPosition2d->GetYaw();
               robot.pose.time = globalPosition2d->GetDataTime();
               robot.gridPose.x = map.real2gridX(robot.pose.x);
               robot.gridPose.y = map.real2gridY(robot.pose.y);
               robot.gridPose.yaw = robot.pose.yaw;
               position2d->NotFresh();
               if (!valid_pose) {
                  //                 DEBUG("robot " << label << " global pose [x,y,yaw]: " << pose.x << " " << pose.y << " " << pose.yaw);
                  //                 DEBUG("robot " << label << " grid pose [x,y]: " << gridPose.x << " " << gridPose.y );
               }  else {
                  if (robot.gridPose != prevGridPose) {
                     ScopedLock plk(poseMtx);
                     //                   DEBUG("POSE " << gridPose.x << "  " << gridPose.y << " : " << map.getIndex(gridPose.y,gridPose.x));
                     travelledDistance += sqrt(robot.pose.squared_distance(prevPose));
                     //travelledPath.push_back(map.getIndex(gridPose.y,gridPose.x));
                     travelledPath.push_back(robot.gridPose);
                  }
               }
               prevPose.x = robot.pose.x;
               prevPose.y = robot.pose.y;
               prevPose.yaw = robot.pose.yaw;
               prevGridPose.x = robot.gridPose.x;
               prevGridPose.y = robot.gridPose.y;
               prevGridPose.yaw = robot.gridPose.yaw;
               valid_pose = true;
            }

            if (laser->IsFresh() && laser->IsValid()) { // process position 2d first
               //               sim->GetPose2d(label, pose.x, pose.y, pose.yaw); //TODO it tooks about 20 ms
               if (valid_pose) {
                  //                DEBUG("robot " << label << " pose [x,y,yaw]: " << pose.x << " " << pose.y << " " << pose.yaw);
                  laserData.robotPose = robot.pose;
                  if (laserData.size != laser->GetCount()) { laserData.resize(laser->GetCount()); }
                  //               DEBUG("GetCount: " << laser->GetCount());
                  for (int i = 0; i < laserData.size; ++i) {
                     //samples[i] = laser.GetRange(i) * normMul;
                     laserData.samples[i] = laser->GetRange(i);
                  }
                  laserData.time = laser->GetDataTime();
                  laserData.minAngle = laser->GetMinAngle();
                  laserData.maxAngle = laser->GetMaxAngle();
                  executionSteps++;
                  map.lock();
                  map.fillScan(laserData);
                  map.unlock();
                  //                DEBUG("               UPDATE");
                  if (shouldWait) {
                     DEBUG("robot " << label << " waiting for pose.");
                     waitForDataBarrier->wait();
                     shouldWait = false;
                  }
               } else {
                  DEBUG("Pose is not fresh.");
               }
               laser->NotFresh();
            }
            q = quit;
         }

         if (!q) { // following goals
            SPosition next;
            double goalDistance;
            if (pathFollower->next(robot.pose, next, goalDistance)) {
               if (!motorEnabled) {
                  position2d->SetMotorEnable(true);
                  motorEnabled = true;
               }
               DEBUG("robot " << label << ": Next goal is " << next.x << "," << next.y << " distance to goal: " << goalDistance);
               double dx = (next.x - robot.pose.x)*cos(robot.pose.yaw) + (next.y - robot.pose.y)*sin(robot.pose.yaw);
               double dy = -(next.x - robot.pose.x)*sin(robot.pose.yaw) + (next.y - robot.pose.y)*cos(robot.pose.yaw);
               //               DEBUG("dx: " << dx << " dy: " << dy);
               //               DEBUG("fv: " << 0.3*dx << " av: " << 0.8*dy);
               //               DEBUG("speed: " << forwardVelGain * dx << " ," << angularVelGain*dy);
               snd->GoTo(next.x, next.y,0);//pose.yaw);
               //               position2d->SetSpeed(forwardVelGain * dx, angularVelGain* dy);
               //               position2d->SetSpeed(1, 0);
            } else {					
               if (checkForDone) {
                  position2d->SetMotorEnable(false);
                  motorEnabled = false;
                  INFO("Path has been traversed");
                  //TODO report path has been finished
                  checkForDone = false;
                  msg.cmd = Event::ROBOT_REACH_GOAL;
                  msg.value = robot.LABEL;
                  ///               queue.send(msg);
               }
            }
         }
      } //end main loop
   } catch (PlayerCc::PlayerError& e) {
      //TODO add report to quiere
      ERROR("Player error: " << e.GetErrorStr() << " function: " << e.GetErrorFun());
   } catch (imr::exception& e) {
      ERROR("Imr error: " << e.what());
   }
   //TODO report finished
   DEBUG("robot " << label << " finished.");
}

/// - private method -----------------------------------------------------------
void CRobotPlayer::connect(void) {
   ASSERT_ARGUMENT(!client, "CRobotPlayer already connected");
   client = new PlayerCc::PlayerClient(cfg.get<std::string>("host"), cfg.get<int>("port"));
   laser = new PlayerCc::LaserProxy(client, cfg.get<int>("laser-index"));
   position2d = new PlayerCc::Position2dProxy(client, cfg.get<int>("position2d-index"));
   globalPosition2d = new PlayerCc::Position2dProxy(client, cfg.get<int>("global-position2d-index"));
   snd = new PlayerCc::Position2dProxy(client, cfg.get<int>("snd-index"));

   ASSERT_ARGUMENT(laser, "Can not create laser proxy");
   ASSERT_ARGUMENT(position2d, "Can not create position2d proxy");
   ASSERT_ARGUMENT(snd, "Can not create snd proxy");

   //retrive laser info
   laser->RequestConfigure();
   DEBUG("Laser - "
         << " min Angle:" << RAD2DEG(laser->GetConfMinAngle()) << " deg"
         << " max Angle:" << RAD2DEG(laser->GetConfMaxAngle()) << " deg"
         << " range resolution: " << laser->GetRangeRes() << " mm"
         << " scan resolution: " << RAD2DEG(laser->GetScanRes()) << " deg"
         << " no. of points in scan: " << laser->GetCount()
        );
   laserData.resAngle = laser->GetScanRes();
   laser->RequestGeom();
   player_pose3d_t pos = laser->GetPose();
   DEBUG("Laser - position: (" << pos.px << ", " << pos.py << ", " << pos.pz << ")");
   laserData.laserPose.x = pos.px;
   laserData.laserPose.y = pos.py;
   DEBUG("CRobotPlayer::connect - done");
}

/// - private method -----------------------------------------------------------
void CRobotPlayer::disconnect(void) {
   if (client) {
      delete client; client = 0;
      if (laser) { delete laser; laser = 0; }
      if (position2d) { delete position2d; position2d = 0; }
   }
}

/// - public method -----------------------------------------------------------
void CRobotPlayer::addTravelledPath(GridCoordsVector& path){
   ScopedLock lk(poseMtx);
   foreach(const GridCoords& pose, travelledPath) {
      path.push_back(pose);
   }
   travelledPath.clear();
}

/// - public method -----------------------------------------------------------
void CRobotPlayer::savePoses() {
   poseSaved.x = robot.pose.x; 
   poseSaved.y = robot.pose.y; 
   poseSaved.yaw = robot.pose.yaw; 

   gridPoseSaved.x = robot.gridPose.x; 
   gridPoseSaved.y = robot.gridPose.y; 
   gridPoseSaved.yaw = robot.gridPose.yaw;
}

/// - public method -----------------------------------------------------------
SLaserConfig CRobotPlayer::getLaserConfig() {
   SLaserConfig result;
   result.count = laser->GetCount();
   result.maxRange = laser->GetMaxRange();
   result.resolution = laser->GetScanRes();
   result.minAngle = laser->GetConfMinAngle();
   result.maxAngle = laser->GetConfMaxAngle();
   return result;
}

/* end of robot_player.cc */
