/*
 * File name: mre_sdl_gui.cc
 * Date:      2012/02/07 20:17
 * Author:    Jan Faigl
 */

#include <cmath>

extern "C" {
#include <SDL.h>
#include <SDL2_gfxPrimitives.h>
}

#include <logging.h>

#include "mre_sdl_gui.h"

using namespace mre;
using namespace mre::gui;

using imr::logger;

extern bool get_color(const char * strname, unsigned char* r, unsigned char* g, unsigned char* b);
imr::ByteMatrix& mre::gui::loadGridImage(const std::string& filename, unsigned char FREESPACE, unsigned char OBSTACLE, imr::ByteMatrix& map) {
   SDL_Surface* s = mre::gui::CSDL::load_img(filename, true);
   DEBUG("loadGridImage map resolution " << s->w << "x" << s->h);
   SDL_Surface* s2 = mre::gui::CSDL::resize(s, 1.0*map.NCOLS / s->w, 1.0*map.NROWS / s->h);
   unsigned char* pixels = (unsigned char*)s2->pixels;
   for(int i = 0; i < map.SIZE; ++i) {
      map(i) = *pixels != 0 ? FREESPACE : OBSTACLE;
      pixels += s2->format->BytesPerPixel;
   }
   SDL_FreeSurface(s);
   SDL_FreeSurface(s2);
   return map;
}


/// - static method ------------------------------------------------------------
SRGB CGui::getColor(const std::string& str) {
   SRGB c;
   ASSERT_ARGUMENT(get_color(str.c_str(), &c.r, &c.g, &c.b), "Color has not been found");
   return c;
}

/// - constructor --------------------------------------------------------------
CGui::CGui(CSDL& sdl) : sdl(sdl) {
   colors.push_back(getColor("red"));
   colors.push_back(getColor("blue"));
   colors.push_back(getColor("orange"));
   colors.push_back(getColor("green"));
   colors.push_back(getColor("seagreen"));
   colors.push_back(getColor("yellow"));
   colors.push_back(getColor("moccasin"));
   colors.push_back(getColor("cyan"));
   colors.push_back(getColor("RoyalBlue1"));
   colors.push_back(getColor("SpringGreen2"));
   colors.push_back(getColor("wheat1"));
   colors.push_back(getColor("sienna2"));
   colors.push_back(getColor("magenta"));
   colors.push_back(getColor("gold"));
   ci = 0;
}

/// - destructor ---------------------------------------------------------------
CGui::~CGui() {
}

/// - public method ------------------------------------------------------------
void CGui::drawRobot(const SRobot& robot, bool orientation) {
   const int rx = 4;
   const int ry = 4;

   filledEllipseRGBA(sdl.getScreenRenderer(), robot.gridPose.x, robot.gridPose.y,
         rx, ry,
         0, 240, 0, 150 //RGBA
         );
   if (orientation) {
      const int x = robot.gridPose.x +  round(6.0 * cos(robot.pose.yaw));
      const int y = robot.gridPose.y - round(6.0 * sin(robot.pose.yaw));
      lineRGBA(sdl.getScreenRenderer(),
	    robot.gridPose.x, robot.gridPose.y,
	    x, y,
	    255, 0, 0, 255);
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawRobot(const GridCoords& gridPose, const SPosition& pose, const bool orientation) {
   const int rx = 4;
   const int ry = 4;

   filledEllipseRGBA(sdl.getScreenRenderer(), gridPose.x, gridPose.y,
         rx, ry,
         0, 240, 0, 150 //RGBA
         );
   if (orientation) {
      const int x = gridPose.x +  round(6.0 * cos(pose.yaw));
      const int y = gridPose.y - round(6.0 * sin(pose.yaw));
      lineRGBA(sdl.getScreenRenderer(),
	    gridPose.x, gridPose.y,
	    x, y,
	    255, 0, 0, 255);
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawFrontier(int r, int c) {
   const int rx = 2;
   const int ry = 2;

   filledEllipseRGBA(sdl.getScreenRenderer(), c, r,
         rx, ry,
         240, 240, 0, 25 //RGBA
         );
}

/// - public method ------------------------------------------------------------
void CGui::drawFrontierRed(int r, int c) {
   const int rx = 2;
   const int ry = 2;

   filledEllipseRGBA(sdl.getScreenRenderer(), c, r,
         rx, ry,
         240, 0, 0, 25 //RGBA
         );
}

/// - public method ------------------------------------------------------------
void CGui::drawPoint(int r, int c, int cr, int cb, int cg) {
   const int rx = 1;
   const int ry = 1;

   filledEllipseRGBA(sdl.getScreenRenderer(), c, r,
         rx, ry,
         cr, cb, cg, 255
//         0, 50, 250, 255 //RGBA
         );
}

/// - public method ------------------------------------------------------------
void CGui::drawGoal(int r, int c, const SRGB& color, int alpha) {
   const int rx = 2;
   const int ry = 2;

   filledEllipseRGBA(sdl.getScreenRenderer(), c, r,
         rx, ry,
         color.r, color.g, color.b, alpha //RGBA
         );
}

/// - public method ------------------------------------------------------------
void CGui::drawDisk(const GridCoords& pt, int r, const SRGB& color, unsigned char alpha) {
   if (r > 1) {
      filledEllipseRGBA(sdl.getScreenRenderer(), pt.x, pt.y,
	    r, r,
	    color.r, color.g, color.b, alpha //RGBA
	    );
   } else {
      SDL_Surface* dst = sdl.getScreenSurface();
      SDL_Renderer* rnd = sdl.getScreenRenderer();
      SDL_SetRenderDrawColor(rnd, color.r, color.g, color.b, alpha);
      SDL_RenderDrawPoint(rnd, pt.x, pt.y);
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawDiskForce(const GridCoords& pt, int r, const SRGB& color, unsigned char alpha) {
   filledEllipseRGBA(sdl.getScreenRenderer(), pt.x, pt.y,
	 r, r,
	 color.r, color.g, color.b, alpha //RGBA
	 );
}


/// - public method ------------------------------------------------------------
void CGui::drawDisk(const GridCoordsVector& pts, int r, const SRGB& color, unsigned char alpha) {
   for(int i = 0; i < pts.size(); ++i) {
      drawDisk(pts[i], r, color, alpha);
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawGrid(const ByteMatrix& grid, const unsigned char FREESPACE_CELL, const unsigned char UNKNOWN_CELL) {
   SDL_Surface* dst = sdl.getScreenSurface();
   SDL_Renderer* rnd = sdl.getScreenRenderer();
   ASSERT_ARGUMENT(grid.NROWS == dst->h and grid.NCOLS == dst->w, "CGui::drawGrid -- Grid does not match to the image size");
   if (rnd and dst) {
      SRGB c;
      for(int i = 0; i < grid.SIZE; ++i) {
	 if (grid[i] == FREESPACE_CELL) {
	    c = SRGB(255, 255, 255);
	 } else if (grid[i] == UNKNOWN_CELL) {
	    c = SRGB(0x80, 0x80, 0x80);
	 } else {
	    c = SRGB(0, 0, 0);
	 }
	 SDL_SetRenderDrawColor(rnd, c.r, c.g, c.b, 255);
	 SDL_RenderDrawPoint(rnd, i%dst->w, i/dst->w);
      }
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawGridLight(const ByteMatrix& grid) {
   SDL_Surface* dst = sdl.getScreenSurface();
   SDL_Renderer* rnd = sdl.getScreenRenderer();
   ASSERT_ARGUMENT(grid.NROWS == dst->h and grid.NCOLS == dst->w, "Grid does not match with image");
   if (rnd and dst) {
      SRGB c;
      for(int i = 0; i < grid.SIZE; ++i) {
	 if (grid[i] == 0) {
	    c = SRGB(255, 255, 255);
	 } else if (grid[i] == 3) { //UNKNOWN_CELL
	    c = SRGB(0x80, 0x80, 0x80);
	 } else {
	    c = SRGB(0, 0, 0);
	 }
	 SDL_SetRenderDrawColor(rnd, c.r, c.g, c.b, 255);
	 SDL_RenderDrawPoint(rnd, i%dst->w, i/dst->w);
      }
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawGridLight(const FloatMatrix& grid) {
   SDL_Renderer* rnd = sdl.getScreenRenderer();
   SDL_Surface* dst = sdl.getScreenSurface();
   if (rnd and dst) {
      SRGB c;
      for(int i = 0; i < grid.SIZE; ++i) {
	 if (grid[i] == 0) {
	    c = SRGB(255, 255, 255);
	 } else if (grid[i] > 0.5) {
	    c = SRGB(0, 0, 0);
	 } else {
	    c = SRGB(0x80, 0x80, 0x80);
	 }
	 SDL_SetRenderDrawColor(rnd, c.r, c.g, c.b, 255);
	 SDL_RenderDrawPoint(rnd, i%dst->w, i/dst->w);
      }
   }
}

/// - public method ------------------------------------------------------------
void CGui::draw(const GridCoordsVector& pts, const SRGB& color) {
   SDL_Renderer* rnd = sdl.getScreenRenderer();
   if (rnd) {
      SDL_SetRenderDrawColor(rnd, color.r, color.g, color.b, 255);
      for(int i = 0; i < pts.size(); ++i) {
	 SDL_RenderDrawPoint(rnd, pts[i].x, pts[i].y);
      }
   }
}

/// - public method ------------------------------------------------------------
void CGui::drawPath(const GridCoordsVector& pts, const SRGB &color) {
   for(int i = 1; i < pts.size(); ++i) {
      lineRGBA(sdl.getScreenRenderer(),
            pts[i-1].x, pts[i-1].y,
            pts[i].x, pts[i].y,
            color.r, color.g, color.b, 255);
      /*
         thickLineRGBA(sdl.getScreenSurface(),
         pts[i-1].x, pts[i-1].y,
         pts[i].x, pts[i].y,
         4, color.r, color.g, color.b, 255);
         */
   }
}

/// - public method ------------------------------------------------------------
void CGui::savePNG(const std::string& name, int quality) {
   sdl.save_img_png(quality, name);
}

/// - public method ------------------------------------------------------------
void CGui::saveJPEG(const std::string& name, int quality) {
   sdl.save_img_jpeg(sdl.getScreenSurface(), quality, name);
}

/// - private method -----------------------------------------------------------


/* end of mre_sdl_gui.cc */
