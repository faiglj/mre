/*
 * File name: mre_events.h
 * Date:      2012/02/28 15:11
 * Author:    Jan Faigl
 */

#ifndef __MRE_EVENTS_H__
#define __MRE_EVENTS_H__

#include "eventqueue.h"

namespace mre {

   struct SEvent {
      enum TCmd { ROBOT_REACH_GOAL = 0 };
      TCmd cmd;
      int value;
   };
   typedef imr::concurrent::CEventQueue<SEvent> EventQueue;

   typedef SEvent Event;
} //end namespace mre

#endif

/* end of mre_events.h */
