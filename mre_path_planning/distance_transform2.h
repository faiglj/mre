/*
 * File name: distance_transform2.h
 * Date:      2011/05/21 17:03
 * Author:    Jan Faigl
 */

#ifndef __DISTANCE_TRANSFORM2_H__
#define __DISTANCE_TRANSFORM2_H__

#include <limits>
#include <vector>

#include "mre_types.h"

namespace mre {

   class CDistanceTransform2 {
      public:
         const unsigned char FREESPACE;
         const unsigned char OBSTACLE;

         CDistanceTransform2(const ByteMatrix& map, const unsigned char FREESPACE = 255, const unsigned char OBSTACLE = 0);
         ~CDistanceTransform2();

         const ByteMatrix& getGrid(void) const { return map; }

         GridCoordsVector& findPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path); 
         GridCoordsVector& findPath(const GridCoordsVector& starts, const GridCoords& goal, GridCoordsVector& path); 

         void setGoal(const GridCoords& goalI);
         GridCoordsVector& getPath(const GridCoords& start, GridCoordsVector& path) const;
         inline float getDistance(const GridCoords& start) const {
            return (grid(start.y, start.x) >= grid.SIZE) ?  - std::numeric_limits<float>::max() : grid(start.y, start.x);
         }
         inline float getDistance(const int idx) const {
            return (grid(idx) >= grid.SIZE) ?  - std::numeric_limits<float>::max() : grid(idx);
         }

         int getDistanceManhattan(const GridCoords& start) const;

         bool isReachable(const GridCoords& start) const;

         int getMaxValue(void) const { return 2 * grid.SIZE; }

         const FloatMatrix& getDistanceGrid(void) const { return grid; }

      private:
         FloatMatrix& compute(FloatMatrix& grid) const;

      private:
         const ByteMatrix& map; //source map
         GridCoords goal;
         FloatMatrix grid;

      public:
	 float getSizeOfMB(void) {
	    return (sizeof(map) + sizeof(float)*grid.SIZE) / (1024.0 * 1024.0);
	 }

   };

} //end namespace mre

#endif

/* end of distance_transform2.h */
