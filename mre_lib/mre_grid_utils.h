/*
 * File name: mre_grid_utils.h
 * Date:      2012/02/10 09:20
 * Author:    Jan Faigl
 */

#ifndef __MRE_GRID_UTILS_H__
#define __MRE_GRID_UTILS_H__

#include "mre_types.h"

namespace mre {

   IntVector& fillPolygon(const IntVector& vx, const IntVector& vy, const int W, IntVector& area);

   // By default the pt2 point is not added into line
   GridCoordsVector& bresenham(const GridCoords& pt1, const GridCoords& pt2, GridCoordsVector& line, bool clearLine = true, bool addLastPoint = false);
   GridCoordsVector& circle(const GridCoords& center, int rad, GridCoordsVector& circle);

   bool isObstacleFree(const GridCoordsVector& pts, const ByteMatrix& map, const unsigned char FREESPACE = 255);
   GridCoordsVector& simplify(const GridCoordsVector& pts, const ByteMatrix& map, GridCoordsVector& ptsSimpl);
   GridCoordsVector& simplify2(const GridCoordsVector& pts, const ByteMatrix& map, GridCoordsVector& ptsSimpl, int maxFails = -1, const unsigned char FREESPACE = 255);

   /// ----------------------------------------------------------------------------
   /// @brief fillPath - fill pts points using bresenham
   /// ----------------------------------------------------------------------------
   GridCoordsVector& fillPath(const GridCoordsVector& pts, GridCoordsVector& path);

   int getPathLengthManhattan(const GridCoordsVector& path);
   double getPathLength(const GridCoordsVector& path);

   bool isCollisionFree(const GridCoordsVector& pts, const ByteMatrix& grid, GridCoords& collisionPoint, GridCoordsVector& freePoints, unsigned char FREESPACE = 255); 

} //end namespace mre

#endif

/* end of mre_grid_utils.h */
