#!/bin/sh

lst="evg-thin libhungarian imr-h.lite imr-h.gui imr-h.algorithm"
for i in $lst
do
   if [ -f $i/clean.sh ]
   then 
      cd $i; ./clean.sh; cd ..
   fi
done


LIBRARY_PREFIX=../lib
INCLUDE_PREFIX=../include

rm -rf $LIBRARY_PREFIX $INCLUDE_PREFIX
