/*
 * File name: tsp_linkern.h
 * Date:      2015/07/25 12:40
 * Author:    Jan Faigl
 */

#ifndef __TSP_LINKERN_H__
#define __TSP_LINKERN_H__

extern "C" {
#include <linkern.h>
}

#include "mre_types.h"

namespace mre {

   class CTSPLinkern {

      public:
	 CTSPLinkern();
	 ~CTSPLinkern();

	 IntVector& solve(const int N, const DoubleVectorVector& costs, IntVector& sequence);
	 void setProblem(const IntVector& cities, const IntVectorVector& costs);
	 void releaseProblem();
	 void solve(const IntVector& cities, int* sequence);

	 //IntVector& solve(const int N, const IntVectorVector& costs, IntVector& sequence);

      private:
	 void fillWeights(const int N, const DoubleVectorVector& costs);
	 double solve(IntVector idx, int* outcycle, int iter);
	 int evalSmallRing(int* chromo);
	 int getSmallRingLength(IntVector &chromo);
	 void createChromoRing(IntVector &chromo, int ch[10]);
	 int getLen(int* chromo);
	 void swap(int a, int b);
	 void getNext();

      private:
	 CCdatagroup dat;
	 IntVectorVector weights;

	 int *incycle;
	 int tempcount, *templist;
	 CCrandstate rstate;
	 int smPerm[10];
	 int smNumber;
   };

} // end mre

#endif

/* end of tsp_linkern.h */
