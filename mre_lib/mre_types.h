/*
 * File name: mre_types.h
 * Date:      2012/02/01 16:43
 * Author:    Jan Faigl
 */

#ifndef __MRE_TYPES_H__
#define __MRE_TYPES_H__

#include <vector>
#include <cmath>
#include <cstdlib>

#include <matrix_utils.h>

namespace mre {

#define RAD2DEG(x) (((x) * 180.0) / M_PI)
#define DEG2RAD(x) ((x) * (M_PI / 180.0))

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SPosition {
      double x;
      double y;
      double yaw;
      double time;

      SPosition() {}
      SPosition(double x, double y) : x(x), y(y) {}
      SPosition(double x, double y, double yaw) : x(x), y(y), yaw(yaw) {}

      double squared_distance(const SPosition& p) const {
         double dx = x - p.x;
         double dy = y - p.y;
         return dx*dx + dy*dy;
      }

      double azimuth(const SPosition& pos) const {
         return atan2(pos.y - y, pos.x - x);
      }

      SPosition& operator=(const SPosition& pos) {
         if (this != &pos) {
            x = pos.x;
            y = pos.y;
            yaw = pos.yaw;
            time = pos.time;
         }
         return *this;
      }
   };

   typedef std::vector<SPosition> PositionVector;

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SGridCoords {
      int x;
      int y;
      double yaw;

      SGridCoords() {}
      SGridCoords(int x, int y) : x(x), y(y) {}

      SGridCoords& operator=(const SGridCoords& pos) {
         if (this != &pos) {
            x = pos.x;
            y = pos.y;
         }
         return *this;
      }

      double squared_distance(const SGridCoords& p) const {
         double dx = 1.0 * x - p.x;
         double dy = 1.0 * y - p.y;
         return 1.0 * (dx*dx + dy*dy);
      }

      int manhattan_distance(const SGridCoords& p) const {
         return abs(x - p.x) + abs(y - p.y);
      }

      bool operator==(const SGridCoords& pt) const {
         return x == pt.x and y == pt.y;
      }

      bool operator!=(const SGridCoords& pt) const {
         return x != pt.x or y != pt.y;
      } 
      double azimuth(const SGridCoords& pt) const {
	 return atan2(pt.y - y, pt.x - x);
      }
   };

   typedef SGridCoords GridCoords;
   typedef std::vector<SGridCoords> GridCoordsVector;
   typedef std::vector<GridCoordsVector> GridCoordsVectorVector;
   typedef std::vector<GridCoordsVectorVector> GridCoordsVectorVectorVector;


   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SFrontier {
      SGridCoords coords;
      double cost;
      double utility;
      int object;
      SFrontier() : coords(-1, -1), cost(1) {}
      SFrontier(int x, int y, int object, double cost = -1, double utility = -1) : coords(x, y), object(object), cost(cost), utility(utility) {}

      SFrontier& operator=(const SFrontier& f) {
         if (this != &f) {
            coords = f.coords;
            cost = f.cost;
            utility = f.utility;
            object = f.object;
         }
         return *this;
      }
   };
   typedef std::vector<SFrontier> FrontierVector;
   typedef std::vector<FrontierVector> FrontierVectorVector;

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SPathNode {
      SGridCoords coords;
      SPosition pose;

      SPathNode& operator=(const SPathNode& node) {
         if (this != &node) {
            coords = node.coords;
            pose = node.pose;
         }
         return *this;
      }
   };
   typedef std::vector<SPathNode> PathNodeVector;
   typedef std::vector<PathNodeVector> PathNodeVectorVector;

   typedef std::vector<int> IntVector;
   typedef std::vector<IntVector> IntVectorVector;
   typedef std::vector<float> FloatVector;
   typedef std::vector<FloatVector> FloatVectorVector;
   typedef std::vector<double> DoubleVector;
   typedef std::vector<DoubleVector> DoubleVectorVector;
   typedef std::vector<bool> BoolVector;
   typedef std::vector<char> CharVector;


   //map matrices to the mre namespace
   typedef imr::ByteMatrix ByteMatrix;
   typedef imr::FloatMatrix FloatMatrix;
   typedef imr::DoubleMatrix DoubleMatrix;
   typedef imr::IntMatrix IntMatrix;

} //end namespace mre

#endif

/* end of mre_types.h */
