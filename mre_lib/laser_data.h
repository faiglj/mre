/*
 * File name: laser_data.h
 * Date:      2015/08/15 18:38
 * Author:    Jan Faigl
 */

#ifndef __LASER_DATA_H__
#define __LASER_DATA_H__

#include "mre_types.h"

namespace mre {

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SLaserData {
      SPosition robotPose; //robot position
      SPosition laserPose; //laser position relative to robot
      double time;
      double minAngle;
      double maxAngle;
      double resAngle; //angle resolution
      double* samples;
      int size;

      SLaserData(int n = 0) : samples(0), size(n) {
	 if (size > 0) { samples = new double[size]; }
      }
      ~SLaserData() {
	 if (samples) { delete[] samples; }
      }

      void resize(int s) {
	 if (samples and size != s) { delete[] samples; samples = 0; }
	 size = s;
	 if (size > 0 and !samples) {
	    samples = new double[size];
	 }
      }
   };

} //end namespace mre

#endif

/* end of laser_data.h */
