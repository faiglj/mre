/*
 * File name: mre_strategy_mtsp_kmeans.cc
 * Date:      2012/02/21 14:14
 * Author:    Jan Faigl
 */

#include <algorithm>
#include <limits>
#include <fstream>

#include <sys/param.h>

#include <boost/foreach.hpp>

#include <logging.h>
#include <colors.h>
#include <imr_file_utils.h>

#include "distance_transform.h"
#include "mre_grid_utils.h"
#include "minmax_pairing.h"
#include "permutation.h"
#include "tsp_linkern.h"

#include "mre_strategy_mtsp_kmeans.h"

#define foreach BOOST_FOREACH

using imr::logger;

using namespace mre;

extern const FloatMatrix* globalGrid;
extern IntVectorVector* globalTravelledPaths;
extern FrontierVector* globalFrontiers;

struct SPair {
   int robotIDX;
   int clusterIDX;
   int goalIDX;
   double cost;
   SPair(int r, int c, double cost) : robotIDX(r), clusterIDX(c), goalIDX(-1), cost(cost) {}
};

template <class T> 
class Compare {
   public:
      bool operator()(const T& a, const T& b) const {
	 return a.cost < b.cost;
      }
      bool operator()(const T* a, const T* b) const {
	 return a->cost < b->cost;
      }
};

/// ----------------------------------------------------------------------------
static double getPathLength(const GridCoordsVector& path) {
   double ret = 0;
   for(int i = 1; i < path.size(); ++i) {
      ret += sqrt(path[i-1].squared_distance(path[i]));
   }
   return ret;
}

/// - static method ------------------------------------------------------------
imr::CConfig& CMTSPKMeans::getConfig(imr::CConfig& config) {
   config.add<int>("mtsp-kmeans-max-iter", "Maximal number of iteration in k-means clustering", 5);
   config.add<bool>("mtsp-kmeans-eval-all-clusters-cost", "Enable/disable evaluation of robot-cluster cost using LK", false);
   config.add<bool>("mtsp-kmeans-permutation-minmax", "Enable/disable minmax in permutation pairing, if false minsum is used", true); 
   config.add<bool>("gui-mtsp-kmeans-save-clusters", "Enable/disable saving clustes", false);
   return config;
}

/// - constructor --------------------------------------------------------------
CMTSPKMeans::CMTSPKMeans(imr::CConfig& cfg, CPathPlanning& paths, bool returnDepot, bool forceAssignGoals) : 
   cfg(cfg), gui(0), paths(paths), 
   navGrid(new ByteMatrix(paths.getGrid().NROWS, paths.getGrid().NCOLS)), 
   distances(0),
   MAX_ITER(cfg.get<int>("mtsp-kmeans-max-iter")),
   EVAL_ALL_CLUSTERS_COST(cfg.get<bool>("mtsp-kmeans-eval-all-clusters-cost")),
   MINMAX_PAIRING(cfg.get<bool>("mtsp-kmeans-permutation-minmax")),
   RETURN_DEPOT(returnDepot),
   FORCE_ASSIGN_SAME_GOALS(forceAssignGoals)
{
}

/// - destructor ---------------------------------------------------------------
CMTSPKMeans::~CMTSPKMeans() {
   release_vertices();
   if (distances) { delete distances; }
   delete navGrid;
}

/// - public method ------------------------------------------------------------
IntVector& CMTSPKMeans::assign(const DoubleMatrix& costMatrix, const GridCoordsVector& goals, const RobotPtrVector& robots, IntVector& goalsIDX) {
//   imr::CPerfTimer t_mtsp_all("Timer - CMTSPKMeans -- complete assign");
   const int M = robots.size();
   const int N = goals.size();
   ASSERT_ARGUMENT(goalsIDX.size() == M, "Goals must be set");

   //imr::CPerfTimer t("Timer - CMTSPKMeans compute distance matrix");
   //imr::CPerfTimer t_alloc("Timer - CMTSPKMeans allocate matrix");
   for(int i = 0; i < M; ++i) { goalsIDX[i] = -1; }
   //assign cluster to the robots
   alloc_vertices(goals, robots);

   const int L = vertices.size();
   if (distances and distances->NROWS < L) { 
      delete distances;
	 distances = 0;
   } 
   if (not distances) {
      distances = new DoubleMatrix(L, L);
   }
   DoubleMatrix& d = *distances;
   GridCoordsVector allGoals;
   // t_alloc.stop();
   for(int i = 0; i < L; ++i) {
      allGoals.push_back(vertices[i]->coords);
      for(int j = i; j < L; ++j) {
	 if (i == j) {
	    d(i, j) = 0.0;
	 } else {
	    GridCoordsVector pts;
	    GridCoordsVector ptsSimple;
	    vertices[i]->dt->getPath(vertices[j]->coords, pts);
	    const double d1 = getPathLength(simplify2(pts, *navGrid, ptsSimple, 10));
	    vertices[j]->dt->getPath(vertices[i]->coords, pts);
	    const double d2 = getPathLength(simplify2(pts, *navGrid, ptsSimple, 10));
	    d(i, j) = d(j, i) = (d1 < d2 ? d1 : d2);
	 }
      }
   }
   // t.stop();
   DEBUG("distances computed");

   GridCoordsVector means;
   GridCoordsVector clusterRobotPoses;
   IntVectorVector cluster;

 //  imr::CPerfTimer t_kmeans("Timer - CMTSPKMeans kmeans");
   { //use regular kmeans
      for(int i = 0; i < M; ++i) { means.push_back(robots[i]->gridPose); }
      kmeans(goals, means, cluster);
      for(int i = 0; i < M; ++i) { clusterRobotPoses.push_back(robots[i]->gridPose); }
   }
   for(int r = 0; r < M; ++r) { DEBUG("Cluster " << r << " goals: " << cluster[r].size()); }

   IntVectorVector routes(robots.size(), IntVector());
 //imr::CPerfTimer t_tsp("Timer - CMTSPKMeans solveLK");
   if (!EVAL_ALL_CLUSTERS_COST ) {
      for(int i = 0; i < M; ++i) {
	 double length;
	 goalsIDX[i] = solveLK(i, robots, goals, cluster[i], routes[i], length);
      }
   } else {
      //consider costs of all clusters
      DEBUG("Start evaluate all clusters");
      DoubleMatrix costs(M, M);
      IntMatrix firstGoals(M, M);
      for(int i = 0; i < M; ++i) {
	 for(int j = 0; j < M; ++j) {
	    IntVector route;
	    double length;
	    firstGoals(i, j) = solveLK(i, robots, goals, cluster[j], route, length);
	    DEBUG("solve robot: " << i << " cluster: " << j << " size: " << cluster[j].size() << " cost: " << length);
	    costs(i, j) = length;
	 }
      }
      CMinMaxPairing pairing(costs, MINMAX_PAIRING);
      //     CPermutation pairing(costs);
      DEBUG("pairing - done");
      const IntVector& assignment = pairing.getAssignment();
      DEBUG("assignment: " << assignment.size());
      ASSERT_ARGUMENT(assignment.size() == M, "Unexpected assignment");
      bool changed = false;
      std::stringstream ss;
      for(int i = 0; i < M; ++i) {
	 goalsIDX[i] = firstGoals(i, assignment[i]);
	 if (assignment[i] != i)  { changed = true; }
	 ss << assignment[i] << " ";
      }
      DEBUG("End evaluate all clusters assignment: " << ss.str());
   }
 //  t_tsp.stop();
 //  CPerfTimer t_fix_assignment("Timer - CMTSPKMeans fixAssignment");
   //if (FORCE_ASSIGN_SAME_GOALS) {
   fixAssignment(routes, goalsIDX);
   // }
  // t_fix_assignment.stop();
   release_vertices();
   if (distances) { delete distances; distances = 0; }
   DEBUG("CMTSPKMeans");
   return goalsIDX;
}

/// - private method -----------------------------------------------------------
void CMTSPKMeans::alloc_vertices(const GridCoordsVector& goals, const RobotPtrVector& robots) {
   release_vertices();
   ByteMatrix& navGrid = *(this->navGrid);
   { //update navigation Grid
      const ByteMatrix& grid = paths.getGrid();
      for(int i = 0; i < grid.SIZE; ++i) {
	 if (grid[i] == CDistanceTransform::OBSTACLE) {
	    navGrid(i) = grid[i];
	 } else { //all other is freespace
	    navGrid(i) = CDistanceTransform::FREESPACE;
	 }
      } //end update navigation grid ( consider robots as freespace )
   }

   for(int g = 0; g < goals.size(); ++g) {
      vertices.push_back(new SVertex(goals[g], g));
      vertices.back()->dt = new CDistanceTransform(navGrid);
      vertices.back()->dt->setGoal(goals[g]);
   }
   for(int r = 0; r < robots.size(); ++r) {
      vertices.push_back(new SVertex(robots[r]->gridPose, goals.size() + r));
      vertices.back()->dt = new CDistanceTransform(navGrid);
      vertices.back()->dt->setGoal(robots[r]->gridPose);
   }
}

/// - private method -----------------------------------------------------------
void CMTSPKMeans::release_vertices(void) {
   foreach(SVertex* vt, vertices) { delete vt; } 
   vertices.clear();
}

/// - private method -----------------------------------------------------------
IntVectorVector& CMTSPKMeans::kmeans(const GridCoordsVector& goals, GridCoordsVector& means, IntVectorVector& cluster) {
   const int N = goals.size();
   const int M = means.size();

   int step = 0;
   bool changed = true;
   IntVector group(N, -1); //
   while (changed) {
      DoubleVector count(M, 0.0);
      step++;
      changed = false;
      int k = 0;
      GridCoordsVector centers(M, GridCoords(0, 0));
      foreach(const GridCoords& g, goals) {
	 int min = std::numeric_limits<int>::max();
	 int idx = -1;
	 for(int m = 0; m < means.size(); ++m) {
	    int xx = g.x - means[m].x;
	    int yy = g.y - means[m].y;
	    const int dd = xx*xx + yy*yy;
	    if ( dd < min ) { min = dd; idx = m; }
	 }
	 ASSERT_ARGUMENT(idx != -1, "best mean not found");
	 centers[idx].y += g.y;
	 centers[idx].x += g.x;
	 count[idx] += 1.0;
	 if (group[k] != idx) {
	    group[k] = idx;
	    changed = true;
	 }
	 k++;
      }
      for(int j = 0; j < centers.size(); ++j) {
	 if (count[j] > 0) {
	    means[j].x = (int)round(1.0 * centers[j].x / count[j]);
	    means[j].y = (int)round(1.0 * centers[j].y / count[j]);
	 }
      }
      if (step == MAX_ITER) { changed = false; } //force quit
   } //end loop while changed
   cluster.clear();
   cluster.resize(M, IntVector());
   for(int r = 0; r < M; r++) {
      for(int g = 0; g < goals.size(); ++g) {
	 if (group[g] == r) {
	    cluster[r].push_back(g);
	 }
      }
   }
   return cluster;
}

/// - private method -----------------------------------------------------------
int CMTSPKMeans::solveLK(const int robotIDX, const RobotPtrVector& robots, const GridCoordsVector& goals, const IntVector& robotGoals, IntVector& newRoute, double& length) {
   const int N = goals.size();
   const int M = robots.size();
   const GridCoords& robotPose = robots[robotIDX]->gridPose;
   const DoubleMatrix& d = *distances;
   newRoute.clear();
   int robotGoal = -1;
   /* TODO PATHS
   //DEBUG("solveLK robotGoals: " << robotGoals.size());
   if (robotGoals.size() > 1) {
      GridCoordsVector routeGoals;
      IntVector idx;
      routeGoals.push_back(robotPose);
      idx.push_back(0);  //depot (robotPose) has label 0
      for(int i = 0; i < robotGoals.size(); i++) { 
	 idx.push_back(i+1); 
	 //        DEBUG("goals: " << goals.size() << " robotGoals[i]:" << robotGoals[i]);
	 routeGoals.push_back(goals[robotGoals[i]]);
      }
      const int last = idx.size();
      idx.push_back(last); //virtual node
      if (RETURN_DEPOT) {
      } else { //use the robot position
	 routeGoals.push_back(robotPose);  //virtual is identical to the depot
      }

      CTSPLinkern lksolver;
      DoubleVectorVector costs;
      IntVectorVector costsInt;

      costs.resize(idx.size() , DoubleVector(idx.size() ));
      const int N1 = routeGoals.size() - 1;
      double maxValue = std::numeric_limits<double>::min();
      for(int r = 0; r < N1; ++r) {
	 const int i1 = r == 0 ? N + robotIDX : robotGoals[r-1];
	 for(int c = r; c < N1; ++c) {
	    if (c == r) { 
	       costs[r][c] = costs[c][r] = 0.0; 
	    } else {
	       const int i2 = c == 0 ? N + robotIDX : robotGoals[c-1];
	       costs[r][c] = costs[c][r] = d(i1, i2);
	       if (costs[r][c] > maxValue) { maxValue = costs[r][c]; }
	    }
	 }
	 // delete dt;
      } 

      IntVector distancesToDepot;
      if (RETURN_DEPOT) {
	 paths.getDTPathLength(routeGoals, robots[robotIDX]->depot, distancesToDepot);
	 for(int i = 0; i < distancesToDepot.size(); ++i) {
	    if (distancesToDepot[i] > maxValue) { maxValue = distancesToDepot[i]; }
	 }
      }

      const int16_t MAX_WEIGHT = (1 << 15) -10;
      ASSERT_ARGUMENT(maxValue < MAX_WEIGHT, "costs matrix contains values larger than MAX_WEIGHT");
      // DEBUG("maxValue:" << maxValue << " MAX_WEIGHT: " << MAX_WEIGHT);
      const double factor = MAX_WEIGHT / maxValue;
      // DEBUG("factor: " << factor);
      costsInt.resize(idx.size(), IntVector(idx.size()));
      for(int r = 0; r < N1; ++r) {
	 for(int c = r; c < N1; ++c) {
	    if (c == r) { 
	       costsInt[r][c] = costsInt[c][r] = 0; 
	    } else {
	       costsInt[r][c] = costsInt[c][r] = (int)round(costs[r][c] * factor); //concorde LK use integers
	       //        DEBUG("costsInt[" << r << "][" << c << "] = " << costsInt[r][c]);
	    }
	 }
      }
      costsInt[0][last] = costsInt[last][0] = 1;
      costsInt[last][last] = 0;
      //const int max = (int)round(navGrid->NCOLS * navGrid->NROWS * factor); 
      const int max =  MAX_WEIGHT + 10;
      if (RETURN_DEPOT) {
	 for(int i = 1; i < last; i++) { 
	    costsInt[i][last] = distancesToDepot[i];
	    costsInt[last][i] = max; 
	 }
      } else {
	 for(int i = 1; i < last; i++) { costsInt[i][last] = costsInt[last][i] = max; }
      }
      int out[idx.size() + 1]; //+1 for size of the out that is stored in out[0]
      int outTemp[idx.size() + 1]; //+1 for size of the out that is stored in out[0]
      outTemp[0] = idx.size();
      int bestLength = std::numeric_limits<int>::max();
      lksolver.setProblem(idx, costsInt);
      for (int j = 0; j < 1; j++) {

	 lksolver.solve(idx, &outTemp[0]);

	 double l = costsInt[outTemp[outTemp[0]]][outTemp[1]];
	 for(int i = 2; i <= outTemp[0]; ++i) {
	    const int cost = costsInt[outTemp[i-1]][outTemp[i]];
	    if (cost < max) { l += cost; }
	 }
	 if (bestLength > l) {
	    bestLength = l;
	    for(int i = 0; i <= outTemp[0]; ++i) { out[i] = outTemp[i]; }
	 }
	 std::stringstream ss;
	 ss << "found solution[" << j << "]: ";
	 for(int i = 1; i <= outTemp[0]; ++i) {
	    ss << outTemp[i] << " ";
	 }
	 DEBUG("Call LKsolver - done solution: " << ss.str() << " length: " << bestLength);
      } //end tsp iterations
      lksolver.releaseProblem();
      length = bestLength;
      int depotIDX = -1;
      for(int i = 1; i <= out[0]; i++) {
	 if (out[i] == 0) { depotIDX = i; break; }
      }
      ASSERT_ARGUMENT(depotIDX == 1, "depot must be first");
      int n1 = out[depotIDX+1] == last ? depotIDX + 2 : depotIDX + 1;
      int n2 = out[out[0]] == last ? out[0]-1 : out[0];
      bool reverse = costs[out[depotIDX]][out[n1]] >  costs[out[depotIDX]][out[n2]];
      int s = depotIDX;
      for(int i = 0; i < out[0]; i++) {
	 if (out[s] != last and s != depotIDX) {
	    const int gI = out[s] - 1; //-1 because the first is depot is depot
	    ASSERT_ARGUMENT(gI >= 0 and gI < robotGoals.size(), "gI out of range");
	    newRoute.push_back(robotGoals[out[s]-1]);
	 }
	 s = (s + 1) % (idx.size() + 1);
	 if (s == 0) { s = 1; }
      }
      if (reverse) {
	 std::reverse(newRoute.begin(), newRoute.end());
      }
      if (0 and gui) {
	 gui->drawDisk(robotPose, 5, gui->getColor("yellow"));
	 GridCoords prev = robotPose;
	 for(int i = 0; i < newRoute.size(); ++i) {
	    const GridCoords& cur = goals[newRoute[i]];
	    DEBUG("Route[i]: " << i << " " << newRoute[i]);
	    CDistanceTransform dt(*navGrid);
	    dt.setGoal(cur);
	    GridCoordsVector pts;
	    GridCoordsVector ptsSimple;
	    dt.getPath(prev, pts);
	    simplify2(pts, *navGrid, ptsSimple, 10); //10 force forward testing if obstacle has been detected
	    gui->drawDisk(cur, 2, gui->getColor("red"));
	    gui->drawPath(ptsSimple, gui->getColor("green"));
	    prev = cur;
	    gui->refresh()->refresh();
	    getchar();
	 }

	 gui->refresh()->refresh();
	 DEBUG("Found route");
	 getchar();
      }
      ASSERT_ARGUMENT(!newRoute.empty(), "newRoute is empty");
      robotGoal = newRoute.front();
   } else if (!robotGoals.empty())  {
      const DoubleMatrix& d = *distances;
      robotGoal = robotGoals.front();
      length = (int)round(100 * d(robotIDX + N, robotGoals.front()));
   } else {
      length = -1;
   }
   */
   return robotGoal;
}

/// - private method -----------------------------------------------------------
void CMTSPKMeans::fixAssignment(const IntVectorVector& routes, IntVector& goalsIDX) {
   const int N = goalsIDX.size();
   IntVector robotsWithoutGoals;
   for(int i = 0; i < N; ++i) {
      if (goalsIDX[i] == -1) {
	 robotsWithoutGoals.push_back(i);
      }
   }
   DEBUG("fixAssignment robotsWithoutGoals: " << robotsWithoutGoals.size());
   if (!robotsWithoutGoals.empty()) {
      //determine the longest 
      std::vector<SPair*> goalsCosts;
      for(int r = 0; r < N; ++r) {
	 DEBUG("fixAssignment routes[" << r << "] size: " << routes[r].size());
	 if (routes[r].size() > 1) {
	    double prevDistance = 0.0;
	    const IntVector& route = routes[r];
	    for(int i = 1; i < route.size(); ++i) {
	       GridCoordsVector pts;
	       GridCoordsVector ptsSimple;
	       vertices[route[i-1]]->dt->getPath(vertices[route[i]]->coords, pts);
	       simplify2(pts, *navGrid, ptsSimple, 10);
	       prevDistance += getPathLength(ptsSimple);
	       DEBUG("fixAssignment goalsCosts.push_back: " << i << " route size: " << route.size() << " " << route[i] << " " << prevDistance);
	       goalsCosts.push_back(new SPair(route[i], route[i], prevDistance));
	    }
	 }
      }
      DEBUG("goalsCosts: " << goalsCosts.size());
      if (!goalsCosts.empty()) {
	 std::sort(goalsCosts.begin(), goalsCosts.end(), Compare<SPair>());
	 std::reverse(goalsCosts.begin(), goalsCosts.end());
	 int idx = 0;
	 foreach(int i, robotsWithoutGoals) {
	    goalsIDX[i] = goalsCosts[idx]->robotIDX;
	    DEBUG("FIXING goal for robot: " << i << " goal: " << goalsCosts[idx]->robotIDX);
	    idx = (idx + 1) % goalsCosts.size();
	 }
      }
      for(int i = 0; i < goalsCosts.size(); ++i) { delete goalsCosts[i]; }
   }
}

/// - private method -----------------------------------------------------------
void CMTSPKMeans::drawAssignment(const GridCoordsVector& goals, const RobotPtrVector& robots, const IntVectorVector& cluster, const IntMatrix& firstGoals, const IntVector& assignment) {
   if (gui) {
      const int M = robots.size();
      const int N = goals.size();
      gui->drawGrid255(*globalGrid);
      DEBUG("drawAssignment - drawGrid255");
      {
	 gui->resetColor();
	 gui->drawGrid255(*globalGrid);
	 for(int i = 0; i < globalTravelledPaths->size(); ++i) {
	    gui->draw((*globalTravelledPaths)[i], gui->nextColor());
	 }
	 foreach(const SFrontier& f, *globalFrontiers) {
	    //           gui->drawFrontier(f.coords.y, f.coords.x);
	 }
      }
      imr::gui::CColors colors;
      for(int r = 0; r < M; r++) {
	 const SRGB& color = gui->getColor(colors.next());
	 for(int j = 0; j < cluster[r].size(); ++j) {
	    gui->drawDisk(goals[cluster[r][j]], 4, color);
	 }
	 gui->drawRobot(*robots[r]);
	 DEBUG("drawAssignment - drawRobot: " << r);
      }
      for(int r = 0; r < M; r++) {
	 GridCoordsVector pts;
	 GridCoordsVector ptsSimple1;
	 GridCoordsVector ptsSimple2;
	 SVertex* robot = vertices[N+r];
	 const int goalIDX = assignment[r] >= 0 ? firstGoals(r, assignment[r]) : -1;
	 if (goalIDX >= 0) {
	    SVertex* goal = vertices[goalIDX];
	    robot->dt->getPath(goal->coords, pts);
	    const double d1 = getPathLength(simplify2(pts, *navGrid, ptsSimple1, 10));
	    goal->dt->getPath(robot->coords, pts);
	    const double d2 = getPathLength(simplify2(pts, *navGrid, ptsSimple2, 10));
	    GridCoordsVector path;
	    if (d1 < d2) {
	       fillPath(ptsSimple1, path);
	    } else {
	       fillPath(ptsSimple2, path);
	    }
	    gui->drawPath(path, SRGB(0, 255, 255));
	 }
      }
      gui->refresh()->refresh();
   }
}

/* end of mre_strategy_mtsp_kmeans.cc */
