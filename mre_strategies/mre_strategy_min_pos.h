/*
 * File name: mre_strategy_min_pos.h
 * Date:      2014/08/20 14:38
 * Author:    Jan Faigl
 */

#ifndef __MRE_STRATEGY_MIN_POS_H__
#define __MRE_STRATEGY_MIN_POS_H__

#include <vector>

#include <imr_config.h>

#include "mre_types.h"
#include "path_planning.h"

namespace mre {

   class CMinPos {
      imr::CConfig& cfg;
      CPathPlanning& paths;
      ByteMatrix* navGrid;

      public:

      CMinPos(imr::CConfig& cfg, CPathPlanning& paths);
      ~CMinPos();

      IntVector& assign(const GridCoordsVector& goals, const RobotPtrVector& robots, IntVector& goalsIDX);
   };

} //end namespace mre

#endif

/* end of mre_strategy_min_pos.h */
