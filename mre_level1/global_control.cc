/*
 * File name: global_control.cc
 * Date:      2012/02/08 07:03
 * Author:    Jan Faigl
 */

#include <boost/foreach.hpp>

#include <logging.h>
#include <stringconversions.h>
#include <imr_assert.h>

#include "global_control.h"

using namespace mre;

#define foreach BOOST_FOREACH

using imr::logger;

/// - constructor --------------------------------------------------------------
SPlayerClient::SPlayerClient(const std::string& host, int port, int laser_idx, int id) {
   DEBUG("Robot (PlayerClient) [" << id << " ] connect to '" << host << "' using port " << port << " a request laser_idx: " << laser_idx);
   client = new PlayerCc::PlayerClient(host, port);
   client->SetDataMode(PLAYER_DATAMODE_PULL);
   client->SetReplaceRule(true);
   laser = new PlayerCc::LaserProxy(client, laser_idx);
   ASSERT_ARGUMENT(laser, "Can not create laser proxy");

   //retrive laser info
   laser->RequestConfigure();
   DEBUG("Laser - " 
         << " min Angle:" << RAD2DEG(laser->GetConfMinAngle()) << " deg"
         << " max Angle:" << RAD2DEG(laser->GetConfMaxAngle()) << " deg"
         << " range resolution: " << laser->GetRangeRes() << " mm"
         << " scan resolution: " << RAD2DEG(laser->GetScanRes()) << " deg"
         << " no. of points in scan: " << laser->GetCount() 
        );
   laserData.resAngle = laser->GetScanRes();
   laser->RequestGeom();
   player_pose3d_t pos = laser->GetPose();
 //  DEBUG("Laser - position: (" << pos.px << ", " << pos.py << ", " << pos.pz << ")");
   laserData.laserPose.x = pos.px;
   laserData.laserPose.y = pos.py;

   this->id = id;
   labelStr = "r" + imr::string_format<int>(id);
   label = const_cast<char*>(labelStr.c_str());
   rangerSim = 0;
}
/// - constructor --------------------------------------------------------------
SPlayerClient::SPlayerClient(const CRangerSim* s, int id) : client(0), laser(0), rangerSim(s) {
   DEBUG("Robot (PlayerClient) [" << id << " ] is using rangerSim");
   this->id = id;
   labelStr = "r" + imr::string_format<int>(id);
   label = const_cast<char*>(labelStr.c_str());
}

/// - destructor ---------------------------------------------------------------
SPlayerClient::~SPlayerClient() {
   if (laser) { delete laser; laser = 0; }
   if (client) { delete client; client = 0; }
}

/// - public method ------------------------------------------------------------
void SPlayerClient::read(void) {
   if (client and laser) {
      while(!laser->IsFresh() and !laser->IsValid()) {
         client->Read();
      }
      if (laserData.size != laser->GetCount()) { laserData.resize(laser->GetCount()); }
      DEBUG("GetCount: " << laser->GetCount());
      for (int i = 0; i < laserData.size; ++i) {
         laserData.samples[i] = laser->GetRange(i); //samples[i] = laser.GetRange(i) * normMul;
      }
      laserData.time = laser->GetDataTime();
      laserData.minAngle = laser->GetMinAngle();
      laserData.maxAngle = laser->GetMaxAngle();
   } else if (rangerSim) {
      rangerSim->getScan(laserData.robotPose, laserData);
   }
}

/// - static -------------------------------------------------------------------
imr::CConfig& CGlobalControl::getConfig(imr::CConfig& config) {
   config.add<std::string>("robots", "a list of robots connections in a format host:port:laser_idx;host:port:laser_idx;", "localhost:6665:0");
   config.add<double>("global-robot-disk-radius", "Robot disk radius", 0.1);
   return config;
}

/// - constructor --------------------------------------------------------------
CGlobalControl::CGlobalControl(imr::CConfig& cfg) :
   cfg(cfg), sim(0), 
   ROBOT_DISK_RADIUS(cfg.get<double>("global-robot-disk-radius"))
{
}

/// - destructor ---------------------------------------------------------------
CGlobalControl::~CGlobalControl() {
   if (sim) { delete sim; }
   foreach(SPlayerClient* clt, clients) {
      delete clt;
   }
   clients.clear();
}

/// - public method ------------------------------------------------------------
void CGlobalControl::connect(void) {
   typedef std::vector<std::string> StringVector;
   StringVector clts;
   imr::parse_list(cfg.get<std::string>("robots"), ";", clts);
   foreach(const std::string& clt, clts) {
      StringVector parms;
      imr::parse_list(clt, ":", parms);
      int port;
      int laser_idx;
      ASSERT_ARGUMENT(parms.size() >= 3, "At least 3 parms per robot must be given");
      ASSERT_ARGUMENT(imr::string_cast(parms[1], port), "Cannot parse port");
      ASSERT_ARGUMENT(imr::string_cast(parms[2], laser_idx), "Cannot parse laser_idx");
      DEBUG("Connection: " << clt);
      if (rangerSim) {
         clients.push_back(new SPlayerClient(rangerSim, clients.size()));
      } else {
         clients.push_back(new SPlayerClient(parms[0], port, laser_idx, clients.size()));
      }
   }
   if (!rangerSim and !clients.empty()) {
      sim = new PlayerCc::SimulationProxy(clients.front()->client, 0);
   }

   //TODO consider initialization of the robot positions
   if (rangerSim) {
      if (clients.size() > 0) {
         clients[0]->laserData.robotPose = SPosition(-3.98, -4.93, DEG2RAD(123.9));
      }
      if (clients.size() > 1) {
         clients[1]->laserData.robotPose = SPosition( 8.36, -7.08, DEG2RAD(-124.38));
      }
      if (clients.size() > 2) {
         clients[2]->laserData.robotPose = SPosition( 6.36, -7.08, DEG2RAD(-124.38));
      }

   }
}

/// - public method ------------------------------------------------------------
void CGlobalControl::read(void) {
   if (rangerSim) {
      foreach(SPlayerClient* client, clients) {
         client->read();
      }
   } else {
      foreach(SPlayerClient* client, clients) {
         client->laser->NotFresh(); //invalidate laser data
         SPosition& pose = client->laserData.robotPose;
         sim->GetPose2d(client->label, pose.x, pose.y, pose.yaw); //it can took about 20 ms 
         DEBUG("Client: " << client->labelStr << " pose: " << pose.x << "," << pose.y << " yaw: " << pose.yaw);
         client->read();
      }
   }
}

/// - public method ------------------------------------------------------------
void CGlobalControl::setRobotPose(int idx, const SPosition& pose) {
   if (idx >= 0 and idx < clients.size()) {
      if (sim) {
         sim->SetPose2d(clients[idx]->label, pose.x, pose.y, pose.yaw);
      } else if (rangerSim) {
         clients[idx]->laserData.robotPose = pose;
      }
   }
}

/// - private method -----------------------------------------------------------

/* end of global_control.cc */
