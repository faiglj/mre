/*
 * File name: mre_sdl_gui.h
 * Date:      2012/02/07 20:11
 * Author:    Jan Faigl
 */

#ifndef __MRE_SDL_GUI_H__
#define __MRE_SDL_GUI_H__

#include "mre_types.h"
#include "mre_robot.h"

#include "sdl_gui.h"

namespace mre { namespace gui {

   imr::ByteMatrix& loadGridImage(const std::string& filename, unsigned char FREESPACE, unsigned char OBSTACLE, imr::ByteMatrix& map);

   class CGui {
      CSDL& sdl;
      public:
      typedef std::vector<SRGB> RGBVector;
      private:
      RGBVector colors;
      int ci;
      public:

      static SRGB getColor(const std::string& str);

      CGui(CSDL& sdl);
      ~CGui();

      void resetColor() { ci = 0; }
      const SRGB& curColor(void) const { return colors[ci]; }
      const SRGB& nextColor(void) {
         const SRGB& r = colors[ci];
         ci = (ci+1) % colors.size();
         return r;
      }

      void drawRobot(const SRobot& robot, const bool orientation = true);
      void drawRobot(const GridCoords& gridPose, const SPosition& pose, const bool orientation = true);
      void drawFrontier(int r, int c);
      void drawFrontierRed(int r, int c);
      void drawPoint(int r, int c, int cr, int cb, int cg);
      void drawGoal(int r, int c, const SRGB& color, int alpha);
      void drawGoal(int r, int c) { drawGoal(r, c, SRGB(255, 0, 0), 255); }
      void drawDisk(const GridCoords& pt, int r, const SRGB& color, unsigned char alpha = 255);
      void drawDiskForce(const GridCoords& pt, int r, const SRGB& color, unsigned char alpha = 255);
      void drawDisk(const GridCoordsVector& pts, int r, const SRGB& color, unsigned char alpha = 255);
      void draw(const GridCoordsVector& pts, const SRGB& color); 
      void drawPath(const GridCoordsVector& pts, const SRGB &color);

      CGui* refresh(void) { sdl.refresh(); return this; }
      void draw(const IntVector& pixels, const SRGB& color) { sdl.draw(pixels, color, sdl.getScreenSurface()); }
      void drawGrid255(const FloatMatrix& grid) { sdl.drawGrid255(grid, sdl.getScreenSurface()); }
      void drawGrid(const ByteMatrix& grid) { sdl.drawGrid(grid, sdl.getScreenSurface()); }
      void drawGrid(const ByteMatrix& grid, unsigned char FREESPACE_CELL, unsigned char UNKNOWN_CELL); 

      void drawGridLight(const ByteMatrix& grid);
      void drawGridLight(const FloatMatrix& grid);
     
      void draw(const GridCoords& pt, const SRGB& color) { sdl.draw(pt.x, pt.y, color, sdl.getScreenSurface()); }
      void draw(const int idx, const SRGB& color) { sdl.draw(idx, color, sdl.getScreenSurface()); }

      void savePNG(const std::string& name, int quality);
      void saveJPEG(const std::string& name, int quality);
   };

} }  //end namespace mre::gui

#endif

/* end of mre_sdl_gui.h */
