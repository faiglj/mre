/*
 * File name: laser_config.h
 * Date:      2015/08/28 15:51
 * Author:    Jan Faigl
 */

#ifndef __LASER_CONFIG_H__
#define __LASER_CONFIG_H__

namespace mre {
/// ----------------------------------------------------------------------------
      /// @brief 
      /// ----------------------------------------------------------------------------
      struct SLaserConfig {
	 int count;
	 double maxRange;
	 double resolution;
	 double minAngle;
	 double maxAngle;     
      };

} //end namespace mre

#endif

/* end of laser_config.h */
