/*
 * File name: goals.cc
 * Date:      2012/08/06 13:41
 * Author:    Jan Faigl
 */

#include <algorithm>
#include <limits>
#include <set>

#include <boost/foreach.hpp>

#include <fov/fov.h>

#include <logging.h>
#include <random.h>
#include <colors.h>
#include <perf_timer.h>

#include "mre_grid_utils.h"
#include "distance_transform.h"

#include "theta_star_heap.h"

#include "goals.h"

#define foreach BOOST_FOREACH

using imr::logger;

using namespace mre;

class CCoordsLess {
   public:
      CCoordsLess(const GridCoords& ct) : ct(ct) {}
      bool operator()(const GridCoords& a, const GridCoords& b) const {
         return ct.squared_distance(a) < ct.squared_distance(b);
      }
      const GridCoords& ct;
};

static IntVector scanArea2;
static GridCoords scanAreaCenter;

static IntVectorVector beams;
static IntVector scanAreaNew;
static bool scanAreaDone = false;

/// ----------------------------------------------------------------------------
static void createPermutation(int number, IntVector& permutation) {
   permutation.clear(); 
   for (int i = 0; i < number; i++) { 
      permutation.push_back(i); 
   }
}

/// ----------------------------------------------------------------------------
static void permute(IntVector& permutation) {
   int k, tmp;
   imr::CRandom::randomize();
   for (int i = permutation.size(); i > 0; --i) {
      k = imr::CRandom::random()%i;
      tmp = permutation[i-1];
      permutation[i-1] = permutation[k];
      permutation[k] = tmp;
   }
}

/// ----------------------------------------------------------------------------
static IntVectorVector& kmeans(const GridCoordsVector& goals, GridCoordsVector& means, IntVectorVector& cluster, int MAX_ITER = 5) {
   const int N = goals.size();
   const int M = means.size();

   int step = 0;
   bool changed = true;
   IntVector group(N, -1); //
   while (changed) {
      DoubleVector count(M, 0.0);
      step++;
      changed = false;
      int k = 0;
      GridCoordsVector centers(M, GridCoords(0, 0));
      foreach(const GridCoords& g, goals) {
         int min = std::numeric_limits<int>::max();
         int idx = -1;
         for(int m = 0; m < means.size(); ++m) {
            int xx = g.x - means[m].x;
            int yy = g.y - means[m].y;
            const int dd = xx*xx + yy*yy;
            if ( dd < min ) { min = dd; idx = m; }
         }
         ASSERT_ARGUMENT(idx != -1, "best mean not found");
         centers[idx].y += g.y;
         centers[idx].x += g.x;
         count[idx] += 1.0;
         if (group[k] != idx) {
            group[k] = idx;
            changed = true;
         }
         k++;
      }
      for(int j = 0; j < centers.size(); ++j) {
         if (count[j] > 0) {
            means[j].x = (int)round(1.0 * centers[j].x / count[j]);
            means[j].y = (int)round(1.0 * centers[j].y / count[j]);
         }
      }
      if (step == MAX_ITER) { changed = false; } //force quit
   } //end loop while changed
   cluster.clear();
   cluster.resize(M, IntVector());
   for(int r = 0; r < M; r++) {
      for(int g = 0; g < goals.size(); ++g) {
         if (group[g] == r) {
            cluster[r].push_back(g);
         }
      }
   }
   return cluster;
}
/// ----------------------------------------------------------------------------
const char MAP_OCUPPIED_CELL = 0;
const char MAP_FREE_CELL = 1;
const char MAP_SEEN_CELL = 2;

static int extNROWS;
static int extNCOLS;

static void apply(void *map, int x, int y, int dx, int dy, void* src) {
   int& v = ((IntMatrix*)map)->at(x, y); 
   if (v != MAP_OCUPPIED_CELL) {
      v++;
   }
}

static bool opaque(void *map, int x, int y) {
   return (x < extNROWS and y < extNCOLS and ((IntMatrix*)map)->at(x, y) != MAP_OCUPPIED_CELL) ? 0 : 1;
}

static IntVector extTr;
static IntVector extUsed;
static IntVector extCovered;
static int extCoveredGoals;

void applyCov(void *map, int x, int y, int dx, int dy, void* src) {
   const int& v = ((IntMatrix*)map)->at(x, y); 
   if (v != MAP_OCUPPIED_CELL) {
      const int i = x * extNCOLS + y;
      const int idx = extTr[i];
      if (idx != -1 and extCovered[idx] == false) { //it is a frontier cell and it is not covered yet
	 extCoveredGoals++;
      }
   }
}

void applyUse(void *map, int x, int y, int dx, int dy, void* src) {
   const int& v = ((IntMatrix*)map)->at(x, y); 
   if (v != MAP_OCUPPIED_CELL) {
      const int idx = extTr[x * extNCOLS + y];
      if (idx != -1 and extCovered[idx] == false) { //it is a frontier cell and it is not covered yet
	 extCovered[idx] = true;
      }
   }
}


struct SCoverage {
   int idx;
   int covered;
   SCoverage(int idx, int covered) : idx(idx), covered(covered) {}

   SCoverage& operator=(const SCoverage& a) {
      if (this != &a) {
	 idx = a.idx;
	 covered = a.covered;
      }
      return *this;
   }
};

class CCoverageLess {
   public:
      bool operator()(const SCoverage& a, const SCoverage& b) const {
	 return a.covered < b.covered;
      }
};

class CCoverageMore {
   public:
      bool operator()(const SCoverage& a, const SCoverage& b) const {
         return a.covered > b.covered;
      }
};

typedef std::vector<SCoverage> CoverageVector;

/// ----------------------------------------------------------------------------
/// - class CGoals -------------------------------------------------------------
/// ----------------------------------------------------------------------------

/// - static method ------------------------------------------------------------
ByteMatrix& CGoals::getPlanningGrid(const CMap& map, ByteMatrix& grid) {
   const ByteMatrix& navGrid = map.getNavGrid();
   ASSERT_ARGUMENT(grid.SIZE == navGrid.SIZE, "Grids must have some size");
   for(int i = 0; i < navGrid.SIZE; ++i) {
      grid(i) = (navGrid(i) != CMap::FREESPACE_CELL ? CDistanceTransform::OBSTACLE : CDistanceTransform::FREESPACE);
   }
   return grid;
}

/// - static method ------------------------------------------------------------
GridCoordsVectorVector& CGoals::findPath(const ByteMatrix& grid, const GridCoords& start, const GridCoordsVector& goals, GridCoordsVectorVector& paths) {
   if (!goals.empty()) {
      paths.resize(goals.size(), GridCoordsVector());
      CDistanceTransform* dt = new CDistanceTransform(grid);
      dt->setGoal(start);
      for(int i = 0; i < goals.size(); ++i) {
         GridCoordsVector pathDT;
         //  DEBUG("path from : " << start.x << " " << start.y << " -> " << goals[i].x << " " << goals[i].y);
         dt->getPath(goals[i], pathDT);
         std::reverse(pathDT.begin(), pathDT.end());
         simplify2(pathDT, grid, paths[i], 10);
      }
      delete dt;
   } else {
      paths.clear();
   }
   return paths;
}

/// - static method ------------------------------------------------------------
GridCoordsVector& CGoals::findPath(const ByteMatrix& grid, const GridCoords& start, const GridCoords& goal, GridCoordsVector& path) {
   path.clear();
   CDistanceTransform* dt = new CDistanceTransform(grid);
   dt->setGoal(start);
   GridCoordsVector pathDT;
   dt->getPath(goal, pathDT);
   std::reverse(pathDT.begin(), pathDT.end());
   simplify2(pathDT, grid, path, 10);
   delete dt;
   return path;
}

/// - constructor --------------------------------------------------------------
CGoals::CGoals(const CMap& map, const GridCoordsVectorVector& goalSetsI, const RobotPtrVector& robots, double visibilityRangeRatio, bool computeCoverage, gui::CGui* gui) : 
   gui(gui), map(map), grid(map.H, map.W), 
   RANGE_RATIO(visibilityRangeRatio),
   LASER_RANGE(visibilityRangeRatio * map.getMaxLaserRange()),
   NUM_ROBOTS(robots.size())
{
   imr::CPerfTimer t("CGoals::CGoals in");
   const ByteMatrix& navGrid = map.getNavGrid();
   for(int i = 0; i < navGrid.SIZE; ++i) {
      grid(i) = (navGrid(i) != CMap::FREESPACE_CELL ? CDistanceTransform::OBSTACLE : CDistanceTransform::FREESPACE);
   }
   foreach(const GridCoordsVector& goals, goalSetsI) {
      foreach(const GridCoords& pt, goals) {
         ASSERT_ARGUMENT(grid(pt.y, pt.x) == CDistanceTransform::FREESPACE, "Goal must be in freespace");
      }
   }
   DEBUG("No. of goal sets: " << goalSetsI.size());

   std::vector<CDistanceTransform*> dts; 
   const bool CHECK_REACHABILITY = false;
   if (CHECK_REACHABILITY) {
      for(int r = 0; r < robots.size(); ++r) {
         const SRobot& robot = *(robots[r]);
         dts.push_back(new CDistanceTransform(grid));
         dts.back()->setGoal(robot.gridPose);
      }
   }
   foreach(const GridCoordsVector& pts, goalSetsI) {
      GridCoords ct(0,0);
      int offset = goals.size();
      GoalPtrVector gt;
      foreach(const GridCoords& pt, pts) {
         bool reachable = true;
         if (CHECK_REACHABILITY) {
            foreach(CDistanceTransform* dt, dts) {
               GridCoordsVector path;
               dt->getPath(pt, path);
               if (path.empty()) {
                  reachable = false;
                  break;
               }
            }
         }
         if (reachable) {
            goals.push_back(new SGoal(pt, goals.size(), allGoals.size(), grid.index(pt.y, pt.x), goalSets.size()));
            allGoals.push_back(goals.back());
            gt.push_back(goals.back());
            ct.x += pt.x; ct.y += pt.y;
         }
      }
      if (gt.size() > 0) {
         ct.x = int(round(ct.x * 1.0 / gt.size()));
         ct.y = int(round(ct.y * 1.0 / gt.size()));
         {
            GridCoordsVector pts;
            bresenham(ct, gt[gt.size() / 2]->coords, pts, false, true);
            ct = pts[pts.size() / 2];
         }
         SGoal* representative = 0;
         bool done = false;
         if (grid(ct.y, ct.x) == CDistanceTransform::FREESPACE) {
            //check if the new representative is reachable
            bool reachable = true;
            if (CHECK_REACHABILITY) {
               foreach(CDistanceTransform* dt, dts) {
                  GridCoordsVector path;
                  dt->getPath(ct, path);
                  if (path.empty()) {
                     reachable = false;
                     break;
                  }
               }
            }
            if (reachable) {
               representative = new SGoal(ct, representatives.size(), allGoals.size(), grid.index(ct.y, ct.x));
               representatives.push_back(representative);
               allGoals.push_back(representatives.back());
               done = true;
            }
         }
         if (!done) { // use mid point of the goal set
            // DEBUG("goalSet[" << goalSets.size() << "] uses midpoint");
            representative = gt[gt.size() / 2];
            ct = representative->coords;
         }
         goalSets.push_back(new SGoalSet(representative, goalSets.size()));
         for(int i = offset; i < goals.size(); ++i) {
            goalSets.back()->goals.push_back(goals[i]);
         }
      }
   }
   foreach(CDistanceTransform* dt, dts) { delete dt; } //cleanup dt
   DEBUG("goal sets: " << goalSets.size());
   DEBUG("goals: " << goals.size());
   DEBUG("grid size: " << grid.NCOLS << "x" << grid.NROWS);
   if (computeCoverage) {
     computeCoveringCells();
   }
}

/// - destructor ---------------------------------------------------------------
CGoals::~CGoals() {
   foreach(SGoal* g, allGoals) { delete g; }
   foreach(SGoalSet* goalSet, goalSets) { delete goalSet; }
   foreach(SCell* cell, cells) { if (cell) delete cell; }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsRepresentatives(GridCoordsVector& newGoals) {
   newGoals.clear();
   foreach(const SGoalSet* goalSet, goalSets) {
      newGoals.push_back(goalSet->representative->coords);
   }
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsGoalSetCoverage(GridCoordsVector& newGoals) {
   const int W = map.W;
   newGoals.clear();
   foreach(SGoal* goal, goals) {  goal->covered = false; }
   foreach(const SGoalSet* goalSet, goalSets) {
      IntVector coverage(grid.SIZE, 0);
      int best = -1;
      int max = 0;
      foreach(const SGoal* goal, goalSet->goals) {
         foreach(int i, goal->coveringCells) {
            coverage[i]++;
            if (coverage[i] > max) {
               best = i; max = coverage[i];
            }
         }
      }
      if (best != -1) {
         newGoals.push_back(GridCoords(best%W, best/W));
      }
   }
   DEBUG("getGoalsGoalSetCoverage newGoals: " << newGoals.size());
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverage(GridCoordsVector& newGoals, IntVector& goalsCoverage) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverage in");
   const int W = map.W;
   newGoals.clear(); 
   goalsCoverage.clear();
   foreach(SGoal* goal, goals) {  goal->covered = false; }
   bool hasUncovered = true;
   BoolVector used(grid.SIZE, false);
   int nbrCovered = 0;

   IntVector activeCells;
   foreach(const SCell* cell, cells) {
      if (cell) { activeCells.push_back(cell->index); }
   }

   while(hasUncovered) {
      const SCell* best = 0;
      int max = 0;
      IntVector coverage(grid.SIZE, 0);
      for(int i = 0; i < activeCells.size(); ++i) {
         const SCell* cell = cells[activeCells[i]];
         if (cell and !used[cell->index]) {
            int c = 0;
            foreach(const SGoal* goal, cell->coveredGoals) { 
               if (!goal->covered) { 
                  c++; 
               } 
            }
            if (c > max) { max = c; best = cell; }
         }
      }
      DEBUG("next best, coverage: " << max);
      if (best) {
         newGoals.push_back(GridCoords(best->index%W, best->index/W));
         goalsCoverage.push_back(max);
         used[best->index] = true;
         foreach(SGoal* goal, best->coveredGoals) { 
            if (!goal->covered) { nbrCovered++; }
            goal->covered = true; 
         }
      }
      DEBUG("Covered " << nbrCovered << "/" << goals.size());
      hasUncovered = false;
      foreach(const SGoal* goal, goals) { 
         if (!goal->covered) {
            hasUncovered = true;
            break;
         }
      }
   } //end while hasUncovered
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverage2(GridCoordsVector& newGoals) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverage2 - done in ");
   if (scanAreaDone == false) { //compute scan end points
      scanAreaDone = true;
      {
	 const int radius = map.real2gridX(LASER_RANGE) - map.real2gridX(0.0);
	 const GridCoords ct(map.W / 2, map.H / 2); 
	 GridCoordsVector circle;
	 mre::circle(ct, radius, circle);
	 scanAreaCenter.x = 0; scanAreaCenter.y = 0;
	 foreach(const GridCoords& pt, circle) { 
	    scanAreaCenter.x += pt.x;
	    scanAreaCenter.y += pt.y;
	 }
	 scanAreaCenter.x /= circle.size();
	 scanAreaCenter.y /= circle.size();


	 beams.clear();

	 int nbpt = 0;
	 foreach(const GridCoords& pt, circle) {
	    GridCoordsVector beam;
	    bresenham(pt, ct, beam, true, false); //do not add the center
	    beams.push_back(IntVector());
	    foreach(const GridCoords& bt, beam) {
	       const int idx = map.getIndex(bt);
	       beams.back().push_back(idx);
	       nbpt++;
	    }
	    std::reverse(beams.back().begin(), beams.back().end());
	 }



	 if (0 and gui) {
	    foreach(const GridCoords pt, circle) {
	       gui->drawDisk(pt, 1, SRGB(255, 0, 0));
	    }
	    imr::gui::CColors colors;
	    foreach(const IntVector& beam, beams) {
	       SRGB color = gui->getColor(colors.next());
	       foreach(const int idx, beam) {
		  const int x = idx % map.W - scanAreaCenter.x + ct.x;
		  const int y = idx / map.W - scanAreaCenter.y + ct.y;
		  gui->drawDisk(GridCoords(x, y), 1, color);
	       gui->refresh();
	       }
	       gui->refresh();
	    }

	    DEBUG("draw circle points:" << circle.size() << " nbpt: " << nbpt); 
	    gui->refresh(); getchar();
	 }
      }




      // DEBUG("scanArea2: " << scanArea2.size() << " set: " << set.size());  getchar();
   } //end compute scan end points
   /// ----------------------------------------------------------------------------
   /// --- end compute beams and scan area
   /// ----------------------------------------------------------------------------
   

   // An approach based on a matrix for storing information about cells
   
   const int W = map.W;
   const int H = map.H;
   bool coveredGoals[grid.SIZE]; 
   bool used[grid.SIZE];
   bool used2[grid.SIZE];
   bool frontiers[grid.SIZE];
   // typedef std::set<int> IntSet; //TODO consider saving covering cells after first run
   int numCoveredGoals = 0;
   int coverage[grid.SIZE]; 
   int goalsIDX[goals.size()]; 
   for(int i = 0; i < grid.SIZE; ++i) { 
      coveredGoals[i] = false; 
      used2[i] = false; 
      frontiers[i] = false;
   }
   for(int i = 0; i < goals.size(); ++i) { 
      goalsIDX[i] = map.getIndex(goals[i]->coords); 
      frontiers[goalsIDX[i]] = true;
   }

   // --- Initial coverage from all the goals
   imr::CPerfTimer tinit("Initial covering cells");
   foreach(SGoal* goal, goals) {
      const GridCoords& gt = goal->coords;
      const int goalIDX = map.getIndex(goal->coords);
      goal->coveringCells.clear();
      for(int i = 0; i < grid.SIZE; ++i) { used[i] = false; }
      foreach(const IntVector& beam, beams) {
	 foreach(const int idx, beam) {
	    const int x = idx % W - scanAreaCenter.x + gt.x;
	    const int y = idx / W - scanAreaCenter.y + gt.y;
	    const int scanIDX = y * map.W  + x;
	    if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == CDistanceTransform::FREESPACE) {
	       if (used[scanIDX] == false) { //avoid duplicities due to "redundat" beams
		  coverage[scanIDX]++; //increase coverage count
		  used[scanIDX] = true;
		  goal->coveringCells.push_back(scanIDX);
	       }
	    } else { //it is obstacles of out of map skip this beam
	       break;
	    }
	 }
      } //end beam
   } //end all beams
 //  tinit.stop(); getchar();
   imr::CPerfTimer tconst("Construction of matrix");
   int numCoveringCells = 0;
   for(int i = 0; i < grid.SIZE; ++i) { 
      if (coverage[i] > 0) { numCoveringCells++; }
   }
   int trCC[grid.SIZE];
   int coveringCellIndexes[numCoveringCells];
   int c = 0;
   for(int i = 0; i < grid.SIZE; ++i) { 
      if (coverage[i] > 0) {
	 trCC[i] = c;
	 coveringCellIndexes[c] = i;
	 c++;
      } else {
	 trCC[i] = -1;
      }
   }

   ByteMatrix cov(goals.size(), numCoveringCells);
   DEBUG("Fill matrix: ");
   for(int i = 0; i < cov.SIZE; ++i) { cov(i) = 0; }
   int trGoals[grid.SIZE]; for(int i = 0; i < grid.SIZE; ++i) { trGoals[i] = -1; }
   int goalsIndexes[goals.size()];
   for(int g = 0; g < goals.size(); ++g) {
      const SGoal* goal = goals[g];
      const int goalIDX = map.getIndex(goal->coords);
      goalsIndexes[g] = goalIDX;
      trGoals[goalIDX] = g;
      foreach(int i, goal->coveringCells) {
	 cov(g, trCC[i]) = 1;
      }
   }
	
   tconst.stop();
   DEBUG("Num. covering cells: " << numCoveringCells << " goals: " << goals.size());
   DEBUG("Matrix size: " << cov.SIZE);
   //getchar();

   while(numCoveredGoals < goals.size()) {
      for(int i = 0; i < grid.SIZE; ++i) { coverage[i] = 0; } //clear coverage
      for(int g = 0; g < goals.size(); ++g) {
	 const int goalIDX = goalsIndexes[g];
	 if (coveredGoals[goalIDX] == false) {
	    for(int i = 0; i < numCoveringCells; ++i) {
	       if (cov(g, i)) {
		  coverage[coveringCellIndexes[i]]++;
	       }
	    }
	 }
      }
      int best = -1; int max = -1;
      for(int i = 0; i < grid.SIZE; ++i) {
	 if (coverage[i] > 0 and coverage[i] >= max and used2[i] == false) { max = coverage[i]; best = i; }
      }
      if (best != -1) {
	 DEBUG("best: " << best << " max coverage: " << max);
	 used2[best] = true;
	 newGoals.push_back(GridCoords(best % W, best / W));
	 const int bi = trCC[best];
	 int cfg = 0;
	 for(int g = 0; g < goals.size(); ++g) {
	    if (cov(g, bi)) { //fill the covered goals
	       coveredGoals[goalsIndexes[g]] = true;
	       cfg++;
	    }
	 }
	 DEBUG("Covered goals: " << cfg << " newGoals: " << newGoals.size());
	 //getchar();
      } else {
	 DEBUG("Best covering cell not found numCoveredGoals: " << numCoveredGoals << " goals: " << goals.size());
	 break;
      }
      numCoveredGoals = 0;
      for(int g = 0; g < goals.size(); ++g) {
	 if (coveredGoals[goalsIndexes[g]]) { numCoveredGoals++; }
      }
   } //end while
  //  */
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverageCumulativeCoverage(GridCoordsVector& newGoals) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverageCumulativeCoverage- done in ");
   if (scanAreaDone == false) { //compute scan end points
      imr::CPerfTimer t("CGoals::getGoalsCompleteCoverageCumulativeCoverage - determin scan area");
      GridCoordsVector endPts;
      const int N = 2 * 361;
      const double da = 2 * M_PI / N; //ASSUME 360 degree laser scanner and 2*361 measurements
      SPosition ct(map.grid2realX(map.W / 2), map.grid2realY(map.H / 2));
      double a = 0;
      for(int i = 0; i < N; ++i) {
	 SGridCoords endPt(GridCoords(
		  map.real2gridX(ct.x + LASER_RANGE * cos(a)),
		  map.real2gridY(ct.y + LASER_RANGE * sin(a))
		  ));
	 if (endPts.empty()) {
	    endPts.push_back(endPt);
	 } else {
	    if (endPts.back().manhattan_distance(endPt) > 0) {
	       endPts.push_back(endPt);
	    }
	 }
	 a += da;
      }
      IntVector vx; 
      IntVector vy;
      IntVector scanArea;
      foreach(const GridCoords& pt, endPts) { vx.push_back(pt.x); vy.push_back(pt.y); }
      fillPolygon(vx, vy, map.W, scanArea);

      GridCoordsVector pts;
      scanAreaCenter.x = 0; scanAreaCenter.y = 0;
      foreach(int i, scanArea) { 
	 //	 const GridCoords pt(i%map.W, i/map.H);
	 const GridCoords pt(i%map.W, i/map.W);
	 pts.push_back(pt); 
	 scanAreaCenter.x += pt.x;
	 scanAreaCenter.y += pt.y;
      }
      scanAreaCenter.x /= scanArea.size();
      scanAreaCenter.y /= scanArea.size();

      std::sort(pts.begin(), pts.end(), CCoordsLess(scanAreaCenter));
      std::reverse(pts.begin(), pts.end());
      int i = 0;
      std::set<int> set;
      foreach(const GridCoords& pt, pts) {
	 // DEBUG("scanArea2[" << i << "]: " << scanAreaCenter.squared_distance(pt));
	 i++;
	 const int idx = map.getIndex(pt);
	 if (set.find(idx) == set.end()) {
	    scanArea2.push_back(idx);
	    set.insert(scanArea2.back());
	 }
      }
      scanAreaDone = true;


      if (0 and gui) {
	 //debug draw scan area
	 gui->drawGrid255(map.getGrid());
	 GridCoords gt(map.W / 2, map.H / 2);
	 foreach(const int idx, scanArea2) {
	    const int x = idx%map.W - scanAreaCenter.x + gt.x;
	    const int y = idx/map.W - scanAreaCenter.y + gt.y;
	    gui->drawDisk(GridCoords(x, y), 1, SRGB(210, 210, 210));
	 }
	 gui->drawDisk(gt, 3, SRGB(0, 255, 0));
	 gui->drawDisk(gt, 1, SRGB(255, 255, 255));
	 DEBUG("scanArea2: " << scanArea2.size() << " set: " << set.size());
	 gui->refresh(); getchar();
      }

      set.clear();
      int nbpt = 0;
      foreach(const int idx2, scanArea2) {
	 if (set.find(idx2) == set.end()) {
	    GridCoordsVector beam;
	    const GridCoords endPt(idx2%map.W, idx2/map.W);
	    bresenham(endPt, GridCoords(map.W / 2, map.H / 2), beam, true, false); //do not add the center
	    beams.push_back(IntVector());
	    foreach(const GridCoords& pt, beam) {
	       const int idx = map.getIndex(pt);
	       if (set.find(idx) == set.end()) {
		  set.insert(idx);
	       }
	       scanAreaNew.push_back(idx);
	       beams.back().push_back(idx);
	       nbpt++;
	    }
	    std::reverse(beams.back().begin(), beams.back().end());
	 }
      }
      std::reverse(beams.begin(), beams.end()); //reverse to start with the longest beams
      if (0 and gui) {
	 //debug draw scan area
	 GridCoords gt(map.W / 2, map.H / 2);
	 foreach(const int idx, scanAreaNew) {
	    const int x = idx%map.W - scanAreaCenter.x + gt.x;
	    const int y = idx/map.W - scanAreaCenter.y + gt.y;
	    gui->drawDisk(GridCoords(x, y), 1, SRGB(110, 110, 110));
	 }
	 gui->drawDisk(gt, 3, SRGB(0, 255, 0));
	 gui->drawDisk(gt, 1, SRGB(255, 255, 255));
	 DEBUG("scanAreaNew: " << scanAreaNew.size()  << " set: " << set.size() << " beams: " << beams.size());
	 gui->refresh(); getchar();

	 int nb = 0;
	 std::reverse(beams.begin(), beams.end());
	 imr::gui::CColors colors;
	 foreach(const IntVector& beam, beams) {
	    nb += beam.size();
	    SRGB color = gui->getColor(colors.next());
	    foreach(const int idx, beam) {
	       const int x = idx%map.W - scanAreaCenter.x + gt.x;
	       const int y = idx/map.W - scanAreaCenter.y + gt.y;
	       gui->drawDisk(GridCoords(x, y), 1, color);
	       gui->refresh();
	       //gui->drawDisk(GridCoords(x, y), 1, SRGB(0, 110, 110));
	    }
	    gui->refresh();
	 }
	 DEBUG("draw beams total points: " << nb);
	 gui->refresh(); getchar();
      }
      // DEBUG("scanArea2: " << scanArea2.size() << " set: " << set.size());  getchar();
   } //end compute scan end points

   //  const int centerIDX = (map.H / 2) * map.W + map.W / 2;
   const char UNKNOWN = 0;
   const char COVERING = 1;
   const char NOT_COVERING = 2;
   const unsigned char FREESPACE = CDistanceTransform::FREESPACE;
   const int W = map.W;
   const int H = map.H;

   BoolVector covered(grid.NROWS * grid.NCOLS, false);
   CharVector visible(covered.size(), 0);

   int coverage[grid.SIZE];
   for(int i = 0; i < grid.SIZE; ++i) {
      coverage[i] = grid(i) == CDistanceTransform::FREESPACE ? MAP_FREE_CELL : MAP_OCUPPIED_CELL;
   }

   for(int g = 0; g < goals.size(); ++g) {
      SGoal* goal = goals[g];
      const GridCoords& gt = goal->coords;
      //ray cast beams
      std::set<int> set;
      goal->coveringCells.clear();
      foreach(const IntVector& beam, beams) {
	 foreach(const int idx, beam) {
	    const int x = idx % W - scanAreaCenter.x + gt.x;
	    const int y = idx / W - scanAreaCenter.y + gt.y;
	    const int scanIDX = y * map.W  + x;
	    if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == CDistanceTransform::FREESPACE) {
	       //       set.insert(scanIDX);
	       goal->coveringCells.push_back(scanIDX);
	       coverage[scanIDX]++; //increase count
	    } else {
	       //it is obstacles of out of map skip this beam
	       break;
	    }
	 } //end beam
      } //end all beams
      if (0 and gui) {
	 gui->drawGrid255(map.getGrid());
	 foreach(const int idx, goal->coveringCells) {
	    const GridCoords pt(idx % W, idx / W);
	    gui->drawDisk(pt, 1, SRGB(210, 210, 210));
	 }
	 gui->drawDisk(goal->coords, 3, SRGB(0, 255, 0));
	 gui->drawDisk(goal->coords, 1, SRGB(255, 255, 255));
	 DEBUG("goal[" << g << "] set: " << set.size() << " coveringCells: " << goal->coveringCells.size());
	 gui->refresh(); getchar();
      }
   } //end goals

   std::set<int> uniqGoals;
   foreach(const SGoal* goal, goals) {
      uniqGoals.insert(map.getIndex(goal->coords));
   }

   IntVector coveringCells;
   for(int i = 0; i < grid.SIZE; ++i) {
      if (coverage[i] > 1) { coveringCells.push_back(i); }
   }
   bool frontiers[grid.SIZE];
   bool used[grid.SIZE];
   for(int i = 0; i < grid.SIZE; ++i) { frontiers[i] = false; }
   foreach(const SGoal* goal, goals) { frontiers[map.getIndex(goal->coords)] = true; }
   std::set<int> coveredFrontiers;
   while(coveredFrontiers.size() < goals.size()) {
      int bestCovering = -1;
      int max = 0;
      for(int i = 0; i < coveringCells.size(); ++i) {
	 const int idx = coveringCells[i];
	 if (coverage[idx] > 1 and max <= coverage[idx]) {
	    max = coverage[idx];
	    bestCovering = idx;
	 }
      }
      if (bestCovering > -1) {
	 const GridCoords pt(bestCovering % W, bestCovering / W); //convert fov map coords -> gridcoords
	 newGoals.push_back(pt); 
	 coverage[bestCovering] = 1;
	 //1st get covered frontiers
	 IntVector curCoveredFrontiers;
	 IntVector pts;
	 for(int i = 0; i < grid.SIZE; ++i) { used[i] = false; }
	 foreach(const IntVector& beam, beams) {
	    foreach(const int idx, beam) {
	       const int x = idx % W - scanAreaCenter.x + pt.x;
	       const int y = idx / W - scanAreaCenter.y + pt.y;
	       const int scanIDX = y * map.W  + x;
	       if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == CDistanceTransform::FREESPACE) {
		  if (used[scanIDX] == false) {
		     used[scanIDX] = true;
		     if (frontiers[scanIDX]) {
			curCoveredFrontiers.push_back(scanIDX);
			coveredFrontiers.insert(scanIDX);
		     } else {
			pts.push_back(scanIDX);
		     }
		  }
	       } else {
		  //it is obstacles of out of map skip this beam
		  break;
	       }
	    } //end beam
	 } //end all beams
	 if (0 and gui) {
	    gui->drawGrid255(map.getGrid());
	    DEBUG("Best covering: " << bestCovering << " max: " << max << " curCoveredFrontiers: " << curCoveredFrontiers.size() << " coveredGoals: " << coveredFrontiers.size() << "/" << goals.size() << " uniqGoals: " << uniqGoals.size());
	    foreach(const int i, pts) {
	       gui->drawDisk(GridCoords(i % W, i / W), 1, SRGB(170, 170, 170));
	    }
	    foreach(const SGoal* goal, goals) {
	       gui->drawDisk(goal->coords, 1, SRGB(0, 0, 255));
	    }
	    foreach(const int i, coveredFrontiers) {
	       gui->drawDisk(GridCoords(i % W, i / W), 1, SRGB(255, 255, 0));
	    }
	    /*
	       foreach(const int i, curCoveredFrontiers) {
	       gui->drawDisk(GridCoords(i % W, i / W), 1, SRGB(0, 255, 0));
	       }*/
	    gui->drawDisk(pt, 3, SRGB(255, 0, 0));
	    gui->refresh(); getchar();
	 }
	 //2nd substract coverage from the covered frontiers
	 foreach(const int fIDX, curCoveredFrontiers) {
	    const GridCoords pt(fIDX % W, fIDX / W);
	    foreach(const IntVector& beam, beams) {
	       foreach(const int idx, beam) {
		  const int x = idx % W - scanAreaCenter.x + pt.x;
		  const int y = idx / W - scanAreaCenter.y + pt.y;
		  const int scanIDX = y * map.W  + x;
		  if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == CDistanceTransform::FREESPACE) {
		     if (coverage[scanIDX] > 1) {
			coverage[scanIDX]--;
		     }
		  } else {
		     //it is obstacles of out of map skip this beam
		     break;
		  }
	       } //end beam
	    } //end all beams
	 } //end all coveredFrontiers
      } else {
	 DEBUG("Best covering cell not found coveredFrontiers: " << coveredFrontiers.size() << " goals: " << goals.size());
	 break;
      }
   } //end coveredFrontiers
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverageFOV(GridCoordsVector& newGoals) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverageFOV in");
   imr::CPerfTimer tA("CGoals::getGoalsCompleteCoverageFOV covering area done in");
   newGoals.clear(); 
   if (!goals.empty()) {
      foreach(SGoal* goal, goals) {  goal->covered = false; }

      fov_settings_type fov_settings;
      IntMatrix m(grid.NCOLS, grid.NROWS); //must be transpose due to libfov
      extNROWS = m.NROWS;
      extNCOLS = m.NCOLS;
      // 1st - determine covering cells
      for(int r = 0; r < grid.NROWS; ++r) {
	 for(int c = 0; c < grid.NCOLS; ++c) { //transpose
	    m(c, r) = grid(r, c) == CDistanceTransform::FREESPACE ? MAP_FREE_CELL : MAP_OCUPPIED_CELL;
	 }
      }
      fov_settings_init(&fov_settings);
      fov_settings_set_opacity_test_function(&fov_settings, opaque);
      fov_settings_set_apply_lighting_function(&fov_settings, apply);
      fov_settings_set_shape(&fov_settings, FOV_SHAPE_CIRCLE_PRECALCULATE); //FOV_SHAPE_CIRCLE
      const int radius = map.real2gridX(LASER_RANGE) - map.real2gridX(0.0);
      foreach(const SGoal* g, goals) {
	 fov_circle(&fov_settings, &m, NULL, g->coords.x, g->coords.y, radius);
      }
      tA.stop();
      // 2nd - retrive covering cells and its coverage
      CoverageVector coverage;
      for(int i = 0; i < m.SIZE; ++i) {
	 if (m(i) > 1) {
	    coverage.push_back(SCoverage(i, m(i)));
	 }
      }
      std::sort(coverage.begin(), coverage.end(), CCoverageMore());

      // 3rd - get goals determining coverage of the frontiers
      bool hasUncovered = true;
      extTr.clear(); extTr.resize(m.SIZE, -1);
      extUsed.clear(); extUsed.resize(coverage.size(), false);
      extCovered.clear(); extCovered.resize(goals.size(), false);
      DEBUG("extTr.size: " << extTr.size());
      for(int i = 0; i < goals.size(); ++i) {
	 const int idx = goals[i]->coords.x * extNCOLS + goals[i]->coords.y;
	 //    DEBUG("goal[" << i << "] idx: " << idx << " pt in m: " << goals[i]->coords.y << ", " << goals[i]->coords.x);
	 extTr[idx] = i;
      }
      while(hasUncovered) {
	 int best = -1; int max = 0;
	 fov_settings_set_apply_lighting_function(&fov_settings, applyCov);
	 for(int i = 0; i < coverage.size(); ++i) {
	    if (!extUsed[i]) {
	       const SCoverage& cov = coverage[i];
	       const GridCoords pt(cov.idx / m.NCOLS, cov.idx % m.NCOLS); //convert fov map coords -> gridcoords
	       extCoveredGoals = 0;
	       fov_circle(&fov_settings, &m, NULL, pt.x, pt.y, radius);
	       //	    DEBUG("i: " << i << " extCoveredGoals: " << extCoveredGoals);
	       if (extCoveredGoals > max) {
		  best = i;
		  max = extCoveredGoals;
	       }
	    }
	 } //end all coverage
	 //     DEBUG("best: " << best << " coverage: " << max);
	 //use best as the new goal and recomputed uncovered goals (frontier cells).
	 ASSERT_ARGUMENT(best != -1, "best candidate has not been found");
	 const SCoverage& cov = coverage[best];
	 extUsed[best] = true;
	 const GridCoords pt(cov.idx / m.NCOLS, cov.idx % m.NCOLS); //convert fov map coords -> gridcoords
	 /*
	    if (gui) {
	    imr::gui::CColors colors;
	    gui->drawGrid255(map.getGrid());
	    gui->drawDisk(pt, 3, SRGB(0, 255, 255));
	    gui->refresh(); getchar();
	    }*/

	 newGoals.push_back(pt); 
	 fov_settings_set_apply_lighting_function(&fov_settings, applyUse);
	 fov_circle(&fov_settings, &m, NULL, pt.x, pt.y, radius);
	 hasUncovered = false;
	 for(int i = 0; i < extCovered.size(); ++i) {
	    if (extCovered[i] == false) {
	       hasUncovered = true;
	       break;
	    }
	 }
      } //end hasUncovered loop

      fov_settings_free(&fov_settings); 
   }
   return newGoals;
}

typedef std::set<int> IntSet;
typedef std::vector<IntSet> IntSetVector;

struct SCoveringSets {
   IntVector idx2CoveringCells;
   IntVector idx2Goals;
   IntSetVector goalsCoveringCells;
   IntSetVector coveringCellsGoals;
   int curGoal;
};


static SCoveringSets coveringSets;

void apply2(void *map, int x, int y, int dx, int dy, void* src) {
   int& v = ((IntMatrix*)map)->at(x, y); 
   if (v != MAP_OCUPPIED_CELL) {
      const int idx = x * extNCOLS + y;
      const int ci = coveringSets.idx2CoveringCells[idx];
      if (ci != -1) {
	 coveringSets.coveringCellsGoals[ci].insert(coveringSets.curGoal);
	 coveringSets.goalsCoveringCells[coveringSets.curGoal].insert(ci);
      }
   }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverageFOV2(GridCoordsVector& newGoals) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverageFOV in");
   imr::CPerfTimer tA("CGoals::getGoalsCompleteCoverageFOV covering area done in");
   newGoals.clear(); 
   if (!goals.empty()) {
      foreach(SGoal* goal, goals) {  goal->covered = false; }

      fov_settings_type fov_settings;
      IntMatrix m(grid.NCOLS, grid.NROWS); //must be transpose due to libfov
      extNROWS = m.NROWS;
      extNCOLS = m.NCOLS;
      // 1st - determine covering cells
      for(int r = 0; r < grid.NROWS; ++r) {
	 for(int c = 0; c < grid.NCOLS; ++c) { //transpose
	    m(c, r) = grid(r, c) == CDistanceTransform::FREESPACE ? MAP_FREE_CELL : MAP_OCUPPIED_CELL;
	 }
      }
      fov_settings_init(&fov_settings);
      fov_settings_set_opacity_test_function(&fov_settings, opaque);
      fov_settings_set_apply_lighting_function(&fov_settings, apply);
      fov_settings_set_shape(&fov_settings, FOV_SHAPE_CIRCLE_PRECALCULATE); //FOV_SHAPE_CIRCLE
      const int radius = map.real2gridX(LASER_RANGE) - map.real2gridX(0.0);
      foreach(const SGoal* g, goals) {
	 fov_circle(&fov_settings, &m, NULL, g->coords.x, g->coords.y, radius);
      }
      int maxCoverage = 1;
      /*
	 for(int i = 0; i < m.SIZE; ++i) {
	 if (m(i) > maxCoverage) { maxCoverage = m(i); }
	 }
	 DEBUG("Max coverage: " << maxCoverage);
	 maxCoverage = (maxCoverage - 1) * 3 / 4; 
	 DEBUG("Thus, consider only covering cells above " << maxCoverage);
	 */

      coveringSets.idx2CoveringCells.clear();
      coveringSets.idx2Goals.clear();
      coveringSets.goalsCoveringCells.clear();
      coveringSets.coveringCellsGoals.clear();
      //2nd - determine number of covering cells
      coveringSets.idx2Goals.resize(m.SIZE);
      coveringSets.idx2CoveringCells.resize(m.SIZE);
      int numCoveringCells = 0;
      IntVector coveringCells;
      for(int i = 0; i < m.SIZE; ++i) {
	 // coveringSets.idx2CoveringCells[i] = m(i) > 1 ? numCoveringCells++ : -1;
	 if (m(i) > maxCoverage) {
	    coveringSets.idx2CoveringCells[i] = numCoveringCells++;
	    coveringCells.push_back(i);
	 } else {
	    coveringSets.idx2CoveringCells[i] = -1;
	 }
      }
      DEBUG("numCoveringCells: " << numCoveringCells);
      coveringSets.coveringCellsGoals.resize(numCoveringCells);
      coveringSets.goalsCoveringCells.resize(goals.size());
      fov_settings_set_apply_lighting_function(&fov_settings, apply2);
      for(int g = 0; g < goals.size(); ++g) {
	 const GridCoords& pt = goals[g]->coords;
	 const int idx = pt.x * m.NCOLS + pt.y;
	 coveringSets.curGoal = g;
	 coveringSets.idx2Goals[idx] = g;
	 fov_circle(&fov_settings, &m, NULL, pt.x, pt.y, radius);
      }
      tA.stop();

      CHeap<int> heap(numCoveringCells);
      for(int i = 0; i < coveringSets.idx2CoveringCells.size(); ++i) {
	 const int idx = coveringSets.idx2CoveringCells[i];
	 if (idx != -1) {
	    heap.add(idx, -coveringSets.coveringCellsGoals[idx].size());
	 }
      }

      //3rd - get goals determining coverage of the frontiers
      bool hasUncovered = true;
      BoolVector used(numCoveringCells, false);
      IntSet coveredFrontiers;
      while(hasUncovered) {
	 int idx = heap.getFirst();
	 if (idx != -1) {
	    //    DEBUG("Idx of covering cell from the head: " << idx << " goals: " << coveringSets.coveringCellsGoals[idx].size());
	    const GridCoords pt(coveringCells[idx] / m.NCOLS, coveringCells[idx] % m.NCOLS); //convert fov map coords -> gridcoords
	    newGoals.push_back(pt); 
	    //remove covered frontiers from the covered set
	    foreach(int i, coveringSets.coveringCellsGoals[idx]) {
	       coveredFrontiers.insert(i);
	    } 
	    used[idx] = true;
	    for(int c = 0; c < coveringCells.size(); ++c) {
	       if (used[c] == false) {
		  foreach(int i, coveredFrontiers) { //remove covered from the covering cells
		     coveringSets.coveringCellsGoals[c].erase(i); 
		  }
		  heap.update(c, -coveringSets.coveringCellsGoals[c].size());
	       }
	    }
	 }
	 //	 DEBUG("coveredFrontiers: " << coveredFrontiers.size() << " / " << goals.size() << " newGoals: " << newGoals.size());
	 hasUncovered = coveredFrontiers.size() < goals.size();
      }
      fov_settings_free(&fov_settings); 
   }
   return newGoals;
}
static BoolVector extGoalMarks;
static IntVector extCoveredFrontiers;

void applyGetFrontiers(void *map, int x, int y, int dx, int dy, void* src) {
   int& v = ((IntMatrix*)map)->at(x, y); 
   if (v != MAP_OCUPPIED_CELL) {
      const int i = x * extNCOLS + y;
      if (extGoalMarks[i]) {
	 extCoveredFrontiers.push_back(i);
      }
   }
}

void applyDec(void *map, int x, int y, int dx, int dy, void* src) {
   int& v = ((IntMatrix*)map)->at(x, y); 
   if (v != MAP_OCUPPIED_CELL and v > 1) {
      v--;
   }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverageFOV3(GridCoordsVector& newGoals) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverageFOV3 in");
   newGoals.clear(); 
   if (!goals.empty()) {
      foreach(SGoal* goal, goals) {  goal->covered = false; }

      fov_settings_type fov_settings;
      IntMatrix m(grid.NCOLS, grid.NROWS); //must be transpose due to libfov
      extNROWS = m.NROWS;
      extNCOLS = m.NCOLS;
      // 1st - determine covering cells
      for(int r = 0; r < grid.NROWS; ++r) {
	 for(int c = 0; c < grid.NCOLS; ++c) { //transpose
	    m(c, r) = grid(r, c) == CDistanceTransform::FREESPACE ? MAP_FREE_CELL : MAP_OCUPPIED_CELL;
	 }
      }
      fov_settings_init(&fov_settings);
      fov_settings_set_opacity_test_function(&fov_settings, opaque);
      fov_settings_set_apply_lighting_function(&fov_settings, apply);
      fov_settings_set_shape(&fov_settings, FOV_SHAPE_CIRCLE_PRECALCULATE); //FOV_SHAPE_CIRCLE
      const int radius = map.real2gridX(LASER_RANGE) - map.real2gridX(0.0);
      extGoalMarks.clear();
      extGoalMarks.resize(m.SIZE, false);
      foreach(const SGoal* g, goals) {
	 fov_circle(&fov_settings, &m, NULL, g->coords.x, g->coords.y, radius);
	 extGoalMarks[g->coords.x * extNCOLS + g->coords.y] = true;
      }
      int maxCoverage = 1;


      int coveringCells[m.SIZE]; //indexs of the covering cells;
      int ncc = 0;

      // 2nd - count covering cells
      for(int i = 0; i < m.SIZE; ++i) {
	 if (m(i) > 1) { coveringCells[ncc++] = i; }
      }
      CHeap<int> heap(m.SIZE);
      IntSet coveredFrontiers;

      // 3rd - extract best covering cells for not covered frontier cells
      while(coveredFrontiers.size() < goals.size()) {
	 heap.clear();
	 for(int i = 0; i < ncc; ++i) {
	    const int idx = coveringCells[i];
	    if (m(idx) > 1) {
	       heap.add(idx, -m(idx));
	    }
	 } //end getting the covering cell with the largest coverage
	 int bestCovering = heap.getFirst(); //this is currently the best covering cell
	 if (bestCovering > -1) {
	    const GridCoords pt(bestCovering / m.NCOLS, bestCovering % m.NCOLS); //convert fov map coords -> gridcoords
	    newGoals.push_back(pt); 
	    m(bestCovering) = 1;
	    //get the covered frontiers
	    extCoveredFrontiers.clear();
	    fov_settings_set_apply_lighting_function(&fov_settings, applyGetFrontiers);
	    fov_circle(&fov_settings, &m, NULL, pt.x, pt.y, radius);
	    //remove the coverage from the frontiers
	    fov_settings_set_apply_lighting_function(&fov_settings, applyDec);
	    foreach(int i, extCoveredFrontiers) {
	       if (coveredFrontiers.find(i) == coveredFrontiers.end()) {
		  coveredFrontiers.insert(i);
		  fov_circle(&fov_settings, &m, NULL, i / m.NCOLS, i % m.NCOLS, radius);
	       }
	    }
	 } else {
	    DEBUG("Best covering cell not found coveredFrontiers: " << coveredFrontiers.size() << " goals: " << goals.size());
	    break;
	 }
      } //end while loop
      DEBUG("getGoalsCompleteCoverageFOV3: " << newGoals.size());
      fov_settings_free(&fov_settings); 
   }
   return newGoals;
}


/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getCoveringCells(GridCoordsVector& newGoals, IntVector& goalsCoverage) {
   const int W = map.W;
   newGoals.clear(); 
   goalsCoverage.clear();
   for(int i = 0; i < cells.size(); ++i) {
      if (cells[i]) {
	 newGoals.push_back(GridCoords(cells[i]->index%W, cells[i]->index/W));
	 goalsCoverage.push_back(cells[i]->coveredGoals.size());
      }
   }
   DEBUG("getCoveringCells cells: " << newGoals.size());
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getCoveringCellsRandom(double samplesSigma, GridCoordsVector& newGoals, IntVector& goalsCoverage) {
   const int W = map.W;
   const int N = map.W * map.H;
   newGoals.clear(); 
   goalsCoverage.clear();

   foreach(const SGoalSet* goalSet, goalSets) {
      IntVector covering(N, 0);
      foreach(const SGoal* goal, goalSet->goals) {
	 foreach(int i, goal->coveringCells) { covering[i]++; }
      }
      IntVector coveringCandidates;
      for(int i = 0; i < N; ++i) {
	 if (covering[i] > 0) { coveringCandidates.push_back(i); }
      }
      IntVector perm;
      createPermutation(coveringCandidates.size(), perm);
      permute(perm);
      //      const int n = goalSet->goals.size() * samples < coveringCandidates.size() ? goalSet->goals.size() * samples : coveringCandidates.size();
      int n = round(goalSet->goals.size() * samplesSigma);
      if (n == 0) { n = 1; }
      // < coveringCandidates.size() ? goalSet->goals.size() * samples : coveringCandidates.size();
      DEBUG("samplesSigma: " << samplesSigma << " goals set goals: " << goalSet->goals.size() << " n: " << n);
      for(int i = 0; i < n; ++i) {
	 // DEBUG("coveringCandidates: " << coveringCandidates.size());
	 const int idx = coveringCandidates[perm[i]];
	 const GridCoords pt(idx%W, idx/W);
	 IntVector coveredCells;
	 ASSERT_ARGUMENT(cells[idx] and cells[idx]->index == idx, "Wrong candidate");
	 { //determine candidate coverage
	    BoolVector covered(grid.NROWS * grid.NCOLS, false);
	    CharVector visible(covered.size(), 0);
	    const int W = map.W;
	    const int H = map.H;
	    const char COVERING = 1;
	    const char NOT_COVERING = 2;
	    const char UNKNOWN = 0;
	    const ByteMatrix& navGrid = map.getNavGrid();
	    IntVector goalArea;
	    foreach(int idx, scanArea2) {
	       const int x = idx%W - scanAreaCenter.x + pt.x;
	       const int y = idx/W - scanAreaCenter.y + pt.y;
	       const int scanIDX = y * map.W  + x;
	       if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H) {
		  goalArea.push_back(scanIDX);
	       }
	    }
	    // end preparing goal area - cells candidates from which goal can be covered

	    foreach(int i, goalArea) {
	       if (visible[i] == UNKNOWN) { //check if goal cell is visible from the i-th cell
		  GridCoordsVector beam;
		  const GridCoords endPt(i%W, i/W);
		  bresenham(pt, endPt, beam);
		  bool obstacle = false;
		  for(int j = 0; j < beam.size(); ++j) {
		     const GridCoords& pt = beam[j];
		     const unsigned char cell = navGrid(pt.y, pt.x);
		     if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
			if (cell == CMap::UNKNOWN_CELL and !obstacle) {
			   visible[beam[j].y * W + beam[j].x] = COVERING; 
			} else if (cell == CMap::FREESPACE_CELL) {
			   visible[beam[j].y * W + beam[j].x] = NOT_COVERING; 
			} else {
			   obstacle = true;
			   visible[beam[j].y * W + beam[j].x] = NOT_COVERING; 
			}
		     }
		  }
		  if (visible[i] == COVERING) { coveredCells.push_back(i); }
	       } else if (visible[i] == COVERING) {
		  coveredCells.push_back(i);
	       }
	    } //end processing all cells from the goalArea
	    // for(int i = 0; i < visible.size(); ++i) { visible[i] = UNKNOWN; } //reset visible 
	 } //end determine candidate coverage
	 newGoals.push_back(GridCoords(idx%W, idx/W));
	 goalsCoverage.push_back(coveredCells.size());
      }
   }
   //  DEBUG("newGoals: " << newGoals.size());
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getAlternateGoals(const GridCoords& robotPose, const GridCoordsVector& goals, GridCoordsVector& alternateGoals) const {
   CDistanceTransform* dt = new CDistanceTransform(grid);
   dt->setGoal(robotPose);
   alternateGoals.clear();
   foreach(const GridCoords& goal, goals) {
      GridCoordsVector pathDT;
      GridCoordsVector pathDTSimpl;
      GridCoordsVector pathDTSimplFull;
      dt->getPath(goal, pathDT);
      simplify2(pathDT, grid, pathDTSimpl, 10);
      if (pathDTSimpl.size() > 1) {
	 const GridCoords& ptStart = pathDTSimpl[0];
	 const GridCoords& ptEnd = pathDTSimpl[1];
	 GridCoordsVector path;
	 bresenham(ptStart, ptEnd, path, true, true);
	 int i = 0;
	 SPosition p1;
	 SPosition p2;
	 while(i < path.size() and sqrt(map.grid2real(path[i], p1).squared_distance(map.grid2real(ptEnd, p2))) > LASER_RANGE) {
	    i++;
	 }
	 alternateGoals.push_back(i < path.size() ? path[i] : ptEnd);
      } else {
	 alternateGoals.push_back(goal);
      }
      //     DEBUG("alternate goal for : " << robotPose.x << " " << robotPose.y << " -> " << alternateGoals.back().x << " " << alternateGoals.back().y << " prev goal " << goal.x << " " << goal.y);
   }
   delete dt;
   return alternateGoals;
}

/// - public method ------------------------------------------------------------
ByteMatrix& CGoals::getUnknownSpacePartition(const RobotPtrVector& robots, ByteMatrix& partition) {
   ASSERT_ARGUMENT(!robots.empty(), "At least one robot is need to determine the reachable freespace");
   const int H = grid.NROWS;
   const int W = grid.NCOLS;
   for(int i = 0; i < partition.SIZE; ++i) { partition[i] = -1; } // clear partition  
   const GridCoords& pt = robots.front()->gridPose;

   const ByteMatrix& navGrid = map.getNavGrid();
   ByteMatrix ugrid(grid.NROWS, grid.NCOLS);
   IntVector u;
   for(int i = 0; i < grid.SIZE; ++i) {
      if (navGrid(i) == CMap::UNKNOWN_CELL or navGrid(i) == CMap::FREESPACE_CELL) {
	 ugrid(i) = CDistanceTransform::FREESPACE;
      } else {
	 ugrid(i) = CDistanceTransform::OBSTACLE;
      }
   }
   CDistanceTransform* dt = new CDistanceTransform(ugrid);
   dt->setGoal(pt);
   const int MAX = dt->getMaxValue();
   const DoubleMatrix& dGrid = dt->getDistanceGrid();
   GridCoordsVector unknownCells;
   for(int i = 0; i < grid.SIZE; ++i) {
      if (navGrid(i) == CMap::UNKNOWN_CELL and dGrid(i) < MAX) {
	 unknownCells.push_back(GridCoords(i%W, i/W));
	 u.push_back(i);
      }
   }

   //determine cluster using K-means
   GridCoordsVector means(robots.size(), GridCoords(W/2, H/2));
   IntVectorVector clusters;
   kmeans(unknownCells, means, clusters, 10);

   if (gui) {
      gui->drawGrid255(map.getGrid());
      DEBUG("getUnknownSpacePartition");
      gui->drawDisk(pt, 3, SRGB(0, 0, 255));
      imr::gui::CColors colors;
      foreach(const IntVector& cluster, clusters) {
	 SRGB color = gui->getColor(colors.next());
	 foreach(int i, cluster) {
	    gui->draw(unknownCells[i], color);
	 }
      }
      foreach(const GridCoords& ct, means) {
	 gui->drawDisk(ct, 5, SRGB(255, 250, 250));
      }
      gui->refresh(); getchar();
      //	gui->draw(u, SRGB(0, 255, 0));
      //      gui->refresh(); getchar();
   }
   delete dt;
   return partition;
}

/// - private method -----------------------------------------------------------
void CGoals::computeCoveringCells(void) {
   imr::CPerfTimer t("CGoals::computeCoveringCells in ");
   DEBUG("CGoals -- computeCoveringCells - start");
   //IntVector scanArea;
   //IntVector scanArea2;
   if (scanAreaDone == false) { //compute scan end points
      imr::CPerfTimer t("CGoals::computeCoveringCells - determin scan area");
      GridCoordsVector endPts;
      const int N = 2 * 361;
      const double da = 2 * M_PI / N; //ASSUME 360 degree laser scanner and 2*361 measurements
      SPosition ct(map.grid2realX(map.W / 2), map.grid2realY(map.H / 2));
      double a = 0;
      for(int i = 0; i < N; ++i) {
	 SGridCoords endPt(GridCoords(
		  map.real2gridX(ct.x + LASER_RANGE * cos(a)),
		  map.real2gridY(ct.y + LASER_RANGE * sin(a))
		  ));
	 if (endPts.empty()) {
	    endPts.push_back(endPt);
	 } else {
	    if (endPts.back().manhattan_distance(endPt) > 0) {
	       endPts.push_back(endPt);
	    }
	 }
	 a += da;
      }
      IntVector vx; 
      IntVector vy;
      IntVector scanArea;
      foreach(const GridCoords& pt, endPts) { vx.push_back(pt.x); vy.push_back(pt.y); }
      fillPolygon(vx, vy, map.W, scanArea);

      GridCoordsVector pts;
      scanAreaCenter.x = 0; scanAreaCenter.y = 0;
      foreach(int i, scanArea) { 
	 // const GridCoords pt(i%map.W, i/map.H); bug
	 const GridCoords pt(i%map.W, i/map.W);
	 pts.push_back(pt); 
	 scanAreaCenter.x += pt.x;
	 scanAreaCenter.y += pt.y;
      }
      scanAreaCenter.x /= scanArea.size();
      scanAreaCenter.y /= scanArea.size();

      std::sort(pts.begin(), pts.end(), CCoordsLess(scanAreaCenter));
      std::reverse(pts.begin(), pts.end());
      foreach(const GridCoords& pt, pts) {
	 scanArea2.push_back(map.getIndex(pt));
      }
      scanAreaDone = true;
   } //end compute scan end points

   //  const int centerIDX = (map.H / 2) * map.W + map.W / 2;
   const char UNKNOWN = 0;
   const char COVERING = 1;
   const char NOT_COVERING = 2;
   const unsigned char FREESPACE = CDistanceTransform::FREESPACE;
   const int W = map.W;
   const int H = map.H;

   BoolVector covered(grid.NROWS * grid.NCOLS, false);
   CharVector visible(covered.size(), 0);

   for(int g = 0; g < goals.size(); ++g) {
      SGoal* goal = goals[g];
      const GridCoords& gt = goal->coords;
      IntVector goalArea;
      foreach(int idx, scanArea2) {
	 //const int x = idx%W - W / 2 + gt.x;
	 //const int y = idx/W - H / 2 + gt.y;
	 const int x = idx%W - scanAreaCenter.x + gt.x;
	 const int y = idx/W - scanAreaCenter.y + gt.y;
	 const int scanIDX = y * map.W  + x;
	 if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == CDistanceTransform::FREESPACE) {
	    goalArea.push_back(scanIDX);
	 }
      }
      // end preparing goal area - cells candidates from which goal can be covered
      goal->coveringCells.clear();
      foreach(int i, goalArea) {
	 if (visible[i] == UNKNOWN) { //check if goal cell is visible from the i-th cell
	    GridCoordsVector beam;
	    const GridCoords endPt(i%W, i/W);
	    bresenham(endPt, gt, beam);
	    bool collisionFree = true;
	    int collisionPtIdx = beam.size();
	    for(int j = 0; j < beam.size(); ++j) {
	       const GridCoords& pt = beam[j];
	       if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
		  if (grid(pt.y, pt.x) != FREESPACE) {
		     collisionFree = false;
		     collisionPtIdx = j;
		     break;
		  } else {
		  }
	       } else {
		  break; //ray goes out of the map
	       }
	    } //end processing beam
	    if (collisionFree) {  //mark all the beam as covering
	       visible[i] = COVERING;
	       goal->coveringCells.push_back(i);
	       foreach(const GridCoords& pt, beam) { visible[pt.y * W + pt.x] = COVERING; }
	    } else { //mark from 0 to collisionPtIdx as NOT_COVERING
	       for(int j = 0; j <= collisionPtIdx; ++j) { visible[beam[j].y * W + beam[j].x] = NOT_COVERING; }
	    }
	 } else if (visible[i] == COVERING) {
	    goal->coveringCells.push_back(i);
	 }
      } //end processing all cells from the goalArea
      for(int i = 0; i < visible.size(); ++i) { visible[i] = UNKNOWN; } //reset visible 
      //     DEBUG("Process goal: " << g << "/" << goals.size() << " coveringCells: " << goal->coveringCells.size());
   } //end all goals


   int nbrCells = 0;
   int c = 0;
   cells.clear();
   cells.resize(grid.SIZE, 0);
   IntVector activeCells;
   foreach(SGoal* goal, goals) {
      foreach(int i, goal->coveringCells) {
	 if (!cells[i]) {
	    cells[i] = new SCell(i);
	    activeCells.push_back(i);
	    nbrCells++;
	 }
	 cells[i]->coveredGoals.push_back(goal);
      }
      c += goal->coveringCells.size();
   }
   DEBUG("Total coveringCells: " << c << " no. of cells: " << nbrCells);

   t.stop();
   if (0) {
      imr::CPerfTimer tm("LibFOV Coverage");
      fov_settings_type fov_settings;
      IntMatrix m(grid.NROWS, grid.NCOLS);
      for(int i = 0; i < m.SIZE; ++i) { 
	 m(i) = grid(i) == CDistanceTransform::FREESPACE ? MAP_FREE_CELL : MAP_OCUPPIED_CELL;
      }
      fov_settings_init(&fov_settings);
      fov_settings_set_opacity_test_function(&fov_settings, opaque);
      fov_settings_set_apply_lighting_function(&fov_settings, apply);
      //const int radius = map.real2gridX(LASER_RANGE);
      //DEBUG("LASER_RANGE: " << LASER_RANGE << " real2gridX: " << (map.real2gridX(LASER_RANGE) - map.real2gridX(0.0)));
      const int radius = map.real2gridX(LASER_RANGE) - map.real2gridX(0.0);
      //const int radius = 10;
      fov_settings_set_shape(&fov_settings, FOV_SHAPE_CIRCLE_PRECALCULATE); //FOV_SHAPE_CIRCLE


      for(int g = 0; g < goals.size(); ++g) {
	 const GridCoords& gt = goals[g]->coords;
	 fov_circle(&fov_settings, &m, NULL, gt.x, gt.y, radius);  
	 goals[g]->coveringCells.clear();
      }

      //freespace == 1, goals > 1
      for(int i = 0; i < m.SIZE; ++i) {
	 if (m(i) > 1) {
	    const int v = m(i) - 2;
	    for(int g = 0; g < goals.size(); ++g) {
	       SGoal* goal = goals[g];
	       if (g <= v) { goal->coveringCells.push_back(i); }
	    }
	 }
      }

      tm.stop();
      fov_settings_free(&fov_settings); 
   }
   // exit(0);

}

/* end of goals.cc */
