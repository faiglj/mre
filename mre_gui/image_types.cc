/*
 * File name: image_types.cc
 * Date:      2010/06/19 07:34
 * Author:    Jan Faigl
 */

#include <fstream>
#include <cmath>

#include <logging.h>

#include "image_types.h"

#define MAX(x,y) ( (x) > (y) ? (x) : (y))
#define MIN(x,y) ( (x) < (y) ? (x) : (y))

using imr::logger;

namespace mre {

   /// ----------------------------------------------------------------------------
   /// struct SRGBCube
   SRGBCube::SRGBCube(uint8_t v) : dim(v), k(256/dim), nbr(0) {
      values.resize(dim * dim * dim, false);
   }

   /// - public method ------------------------------------------------------------
   void SRGBCube::add(const SHSV& hsv, int dh, int ds, int dv) {
      uint8_t sf = hsv.s - ds >= 0 ? hsv.s - ds : 0;
      uint8_t st = hsv.s + ds <= 255 ? hsv.s + ds : 255;
      uint8_t vf = hsv.v - dv >= 0 ? hsv.v - dv : 0;
      uint8_t vt = hsv.v + dv <= 255 ? hsv.v + dv : 255;
      SHSV v;
      SRGB rgb;
      for (int hi = hsv.h - dh; hi < hsv.h + dh; hi++) {
	 v.h = hi < 0 ? hi + 360 : hi;
	 for (v.s = sf; v.s < st; v.s++) {
	    for (v.v = vf; v.v < vt; v.v++) {
	       hsv2rgb(v, rgb);
	       const int idx = (rgb.r/k) * (dim*dim) + (rgb.g / k) * dim + rgb.b / k;

	       
	       if (!values[idx]) {
		  DEBUG("SRGBCube::add: " << int(rgb.r) << " " << int(rgb.g) << " " << int(rgb.b));
		  DEBUG("SRGBCube::add /k: " << int(rgb.r / k) << " " << int(rgb.g / k) << " " << int(rgb.b / k));
		  DEBUG("SRGBCube::add /k)*k: " << int((rgb.r / k)*k) << " " << int((rgb.g / k)*k) << " " << int((rgb.b / k)*k));
		  values[idx] = true;
		  nbr++;


		  const uint8_t r = uint8_t(rgb.r*1.0 / k);
		  const uint8_t g = uint8_t(rgb.g*1.0 / k);
		  const uint8_t b = uint8_t(rgb.b*1.0 / k);
		  DEBUG("SRGBCube::add (float) " << int(r*k) << " " << int(g*k) << " " << int(b*k));
	       }
	    }
	 }
      }
   }

   /// - public method ------------------------------------------------------------
   void SRGBCube::add(const SRGB& rgb) {
      const int idx = (rgb.r/k) * (dim*dim) + (rgb.g / k) * dim + rgb.b / k;
      if (!values[idx]) {
	 values[idx] = true;
	 nbr++;
      }
   }

   /// - public method ------------------------------------------------------------
   void SRGBCube::clear(void) {
      for (int i = 0; i < values.size(); i++) {
	 values[i] = false;
      }
      nbr = 0;
   }

   /// - functions ----------------------------------------------------------------
   SHSV& rgb2hsv(const SRGB& rgb, SHSV& hsv) {
      float h,s;

      hsv.clear();

      float min = MIN(rgb.r, MIN(rgb.g, rgb.b));
      float max = MAX(rgb.r, MAX(rgb.g, rgb.b));
      float v = max;				// v
      float delta = max - min;

      if (max != 0) {
	 s = MIN(delta * 255 / max, (float)255.);		// s

	 if ( rgb.r == max ) {
	    h = ( rgb.g - rgb.b ) / delta;		// between yellow & magenta
	 } else if( rgb.g == max ) {
	    h = 2 + ( rgb.b - rgb.r ) / delta;	// between cyan & yellow
	 } else {
	    h = 4 + ( rgb.r - rgb.g ) / delta;	// between magenta & cyan
	 }
	 h = h * 60;
	 if (h < 0) { 
	    h += 360;
	 }
      } else {
	 // r = g = b = 0		// s = 0, v is undefined
	 s = 0;
	 h = -1;
      }
      hsv.s = (unsigned char) s;
      hsv.v = (unsigned char) v;
      hsv.h = (unsigned int) h;
      return hsv;
   }

   /// ----------------------------------------------------------------------------
   SRGB& hsv2rgb(const SHSV& hsv, SRGB& rgb) {
      if( hsv.s == 0 ) {
	 rgb = SRGB(hsv.v, hsv.v, hsv.v);
      } else {
	 float h = hsv.h / 60.;
	 int i = floor(h);
	 float f = h - i;
	 uint8_t p = hsv.v * ( 1 - hsv.s / 255.);
	 uint8_t q = hsv.v * ( 1 - hsv.s / 255. * f );
	 uint8_t t = hsv.v * ( 1 - hsv.s / 255. * ( 1 - f ) );
	 switch(i) {
	    case 0: 
	       rgb = SRGB(hsv.v, t, p);
	       break;
	    case 1:
	       rgb = SRGB(q, hsv.v, p);
	       break;
	    case 2:
	       rgb = SRGB(p, hsv.v, t);
	       break;
	    case 3:
	       rgb = SRGB(p, q, hsv.v);
	       break;
	    case 4:
	       rgb = SRGB(t, p, hsv.v);
	       break;
	    default: 
	       rgb = SRGB(hsv.v, p, q);
	       break;
	 }
      }
      return rgb;
   }

   /// ----------------------------------------------------------------------------
   uint8_t rgb2gray(const SRGB& rgb) {
      return 0.2989 * rgb.r + 0.5870 * rgb.g + 0.1140 * rgb.b;
   }

   /// - public function ----------------------------------------------------------
   SRGBCube& loadCube(const std::string& filename, SRGBCube& rgbCube) {
      if (!filename.empty()) {
	 std::ifstream in(filename.c_str());
	 int i;
	 while (in >> i) {
	    if (i >= 0 and i < (rgbCube.dim*rgbCube.dim*rgbCube.dim) * !rgbCube.values[i]) {
	       rgbCube.values[i] = true;
	       rgbCube.nbr++;
	    }
	 }
	 DEBUG("Load rgb cube from file '" << filename << "' nbr values: " <<rgbCube.nbr);
      }
      return rgbCube;
   }

   /// - public function ----------------------------------------------------------
   void saveCube(const SRGBCube& rgbCube, const std::string& filename) {
      if (!filename.empty()) { 
	 DEBUG("Save rgb cube to file '" << filename << "'");
	 std::ofstream out(filename.c_str());
	 const int n =  rgbCube.dim * rgbCube.dim * rgbCube.dim;
	 for(int i = 0; i < n; i++) {
	    if (rgbCube.values[i]) { out << i << std::endl; }
	 }
	 out.close();
      }
   }


} // end namespace imr

/* end of image_types.cc */
