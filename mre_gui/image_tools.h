/*
 * File name: image_tools.h
 * Date:      2012/01/01 13:23
 * Author:    Jan Faigl
 */

#ifndef __IMAGE_TOOLS_H__
#define __IMAGE_TOOLS_H__

#include <cmath>

#include "image_types.h"

namespace mre {

   inline unsigned char rgb_to_gray(unsigned char r, unsigned char g, unsigned char b) {
      return (unsigned char)round(0.2989*r + 0.5870*g + 0.1140*b);
   }

   void yuv422_to_rgb888_float(unsigned char* src, const int w, const int h, unsigned char *dst);

   /// ----------------------------------------------------------------------------
   /// @brief yuv422_to_rgb888 optimized version of the yuv422_to_rgb888_old
   /// ----------------------------------------------------------------------------
   void yuv422_to_rgb888_shift(unsigned char* src, const int w, const int h, unsigned char *dst);

   void yuv422_to_rgb888_table_init(void);
   void yuv422_to_rgb888_table_free(void);
   void yuv422_to_rgb888_table(unsigned char* src, const int w, const int h, unsigned char *dst);
   void yuv422_to_gray_table(unsigned char* src, const int w, const int h, unsigned char *dst);

   void jpeg_compress(const unsigned char* src, int src_len, int w, int h, unsigned char** dst, unsigned long& dst_len, int quality, int components = 3);
   bool save_img_jpeg(const SImage* img, int quality, const std::string& filename);
   bool save_img_raw(const SImage* img, const std::string& filename);
   bool is_raw_img(const std::string& filename);
   SImage* load_img_raw(const std::string& filename);

   void draw(const IntVector& pixels, const SRGB& color, SImage* img);

} //end namespace mre

#endif

/* end of image_tools.h */
