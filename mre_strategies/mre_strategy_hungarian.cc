/*
 * File name: mre_strategy_hungarian.cc
 * Date:      2012/02/13 12:06
 * Author:    Jan Faigl
 */

#include <limits>
#include <cstdlib>

extern "C" {
#include <hungarian.h>
}

#include "mre_strategy_hungarian.h"

using namespace mre;

/// - constructor --------------------------------------------------------------
CHungarian::CHungarian() {
}


/// - destructor ---------------------------------------------------------------
CHungarian::~CHungarian() {
}

/// - public method ------------------------------------------------------------
IntVector& CHungarian::assign(const DoubleMatrix& costMatrix, IntVector& goalsIDX) {
   ASSERT_ARGUMENT(goalsIDX.size() == costMatrix.NROWS, "No. of goals does not match with no. of agents");
   const int nRobots = costMatrix.NROWS;
   const int nGoals = costMatrix.NCOLS;
   int **mm = (int**)malloc(nRobots*sizeof(int*));
   for(int i = 0; i < nRobots; ++i) {
      mm[i] = (int*)calloc(nGoals, sizeof(int));
      for(int j = 0; j < nGoals; ++j) {
	 mm[i][j] = round(costMatrix(i, j) * 100);
	 if (mm[i][j] < 0) { mm[i][j] = std::numeric_limits<int>::max(); }
      }
   }
   hungarian_problem_t problem;
   int matrix_size = hungarian_init(&problem, mm, nRobots, nGoals, HUNGARIAN_MODE_MINIMIZE_COST) ;
   hungarian_solve(&problem);
   // hungarian_print_assignment(&problem);
   for(int i = 0; i < nRobots; ++i) { goalsIDX[i] = -1; }
   for(int i = 0; i < nRobots; ++i) {
      for(int j = 0; j < nGoals; ++j) {
	 if (problem.assignment[i][j] == 1) {
	    goalsIDX[i] = j; 
	    break;
	 }
      }
   }
   hungarian_free(&problem);
   for(int i = 0; i < nRobots; ++i) {
      free(mm[i]);
   }
   free(mm);
   return goalsIDX;
}

/// - private method -----------------------------------------------------------

/* end of mre_strategy_hungarian.cc */
