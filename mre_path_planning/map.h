/*
 * File name: map.h
 * Date:      2012/01/24 18:29
 * Author:    Jan Faigl
 */

#ifndef __MAP_H__
#define __MAP_H__

#include <imr_config.h>
#include <imr_assert.h>
#include <bbox.h>
#include <concurrent/thread.h>

#include "mre_types.h"
#include "laser_data.h"

#include "mre_sdl_gui.h"

namespace mre {

   class CMap {
      private:
	 imr::CConfig& cfg;
      public:
	 const double MAP_W;
	 const double MAP_H;
	 const double CELL_SIZE;
	 const int W;
	 const int H;
      private:
	 const double MIN_X;
	 const double MAX_X;
	 const double MIN_Y;
	 const double MAX_Y;
	 const double KX;
	 const double KY;
      public:
	 const double MAX_LASER_RANGE;
	 const double GOALS_REPRESENTATIVE_DISTANCE_FACTOR;
	 const float INIT_PROB;
	 const float OBSTACLE_PROB;
	 const float FREESPACE_PROB;
	 FloatMatrix grid; //TODO
      public:
	 enum { FREESPACE_CELL = 0, OCCUPIED_CELL = 1, OCCUPIED_GROWN_CELL = 2, UNKNOWN_CELL = 3 };
      private:
	 ByteMatrix navGrid;
	 PositionVector border;

	 gui::CGui* gui;

	 imr::concurrent::Mutex mtx;

      public:
	 static imr::CConfig& getConfig(imr::CConfig& config);
	 CMap(imr::CConfig& cfg);
	 ~CMap();

	 void setGui(gui::CGui* g) { gui = g; }
	 double getMaxLaserRange(void) const { return MAX_LASER_RANGE; }

	 void lock(void) { mtx.lock(); }
	 void unlock(void) { mtx.unlock(); }

	 inline SPosition& grid2real(int r, int c, SPosition& pos) const {
	    pos.x = c / KX + MIN_X;
	    pos.y = MIN_Y - (r - H) / KY;
	    return pos;
	 }

	 inline SPosition& grid2real(const GridCoords& pt, SPosition& pos) const {
	    pos.x = pt.x / KX + MIN_X;
	    pos.y = MIN_Y - (pt.y - H) / KY;
	    return pos;
	 }

	 inline void real2grid(const SPosition& pos, int& r, int& c) const {
	    c = KX * (pos.x - MIN_X);
	    r = H - KY * (pos.y - MIN_Y);
	 }

	 inline double grid2realX(int c) const { return c / KX + MIN_X; }
	 inline double grid2realY(int r) const { return MIN_Y - (r - H) / KY; }
	 inline int real2gridX(double x) const { return KX * (x - MIN_X); }
	 inline int real2gridY(double y) const { return H - KY * (y - MIN_Y); }

	 inline bool isInMap(int r, int c) const { return r >= 0 and r <= H and c >= 0 and c <= W; }
	 inline int getIndex(int r, int c) const { return c + r * W; }
	 inline int getIndex(const GridCoords& pt) const { return pt.x + pt.y * W; }

	 void getCellsValues(const IntVector& cells, FloatVector& values);
	 void setCellsValues(const IntVector& cells, const FloatVector& values);
	 void setCellsValues(const IntVector& cells, float v);
	 void setCellValue(int r, int c, float v) { grid(r, c) = v; }
	 float getCellValue(int i) { return grid(i); }
	 void updateFreespace(const IntVector& cells);
	 void updateFreespaceFilter(const IntVector& cells, float threshold);
	 void updateObstacles(const IntVector& cells);
	 void updateObstacles(const IntVector& cells, const FloatVector& obstSave);

	 void fillScan(SLaserData& scan);

	 const FloatMatrix& getGrid(void) const { return grid; }
	 const ByteMatrix& getNavGrid(void) const { return navGrid; }

	 void fillScan(const SLaserData& scan, IntVector& freespace, IntVector& obstacle);
	 //void updateNavGrid(const double ROBOT_RADIUS, const RobotPtrVector& robots);
	 void updateNavGrid(const int ROBOT_CELLS_RADIUS, const RobotPtrVector& robots);
	 void updateNavGrid(const double ROBOT_RADIUS, const RobotPtrVector& robots);
	 bool isFrontier(const GridCoords& pt) const;
	 int findFrontiers(const GridCoordsVector& robotPoses, FrontierVector& frontiers) const;

	 int getNoOfFreespaceCells(void) const;

	 /// ----------------------------------------------------------------------------
	 /// @brief findAllGoals
	 ///
	 /// @param frontiers
	 /// @param max
	 /// @param minNumOfFrontierCells - if the frontiers (object) is formed from a less number of cells it is discarded
	 /// @param result
	 ///
	 /// @return
	 /// ----------------------------------------------------------------------------
	 FrontierVector& findAllGoals(const FrontierVector &frontiers, int max, int minNumOfFrontierCells, FrontierVector& result) const;
	 FrontierVector& findAllGoalsARN(const FrontierVector &frontiers, int max, int minNumOfFrontierCells, int nRobots, FrontierVector& result) const;

	 bool isCollisionFree(const GridCoordsVector& pts, GridCoords& collisionPoint) const;
      private:
	 bool isRobotNearBy(const GridCoordsVector& robotPoses, int r, int c, double radius) const;
	 FrontierVector& findGoals(const FrontierVector &frontiers, int number, int minNumOfFrontierCells, FrontierVector& result) const;
	 FrontierVector& rfe(const FrontierVector& object, const int num_repre, FrontierVector& result) const;
	 SPosition& getBorderIntersection(const SPosition& p1, const SPosition& p2, SPosition& p) const;
   };

} //end namespace mre

#endif

/* end of map.h */
