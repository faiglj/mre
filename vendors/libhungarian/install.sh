#!/bin/sh

if [ `uname` = FreeBSD ]
then
   gmake -C src install
else
   make -C src install
fi
