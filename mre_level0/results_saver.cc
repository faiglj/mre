/*
 * File name: results_saver.cc
 * Date:      2012/02/18 10:50
 * Author:    Jan Faigl
 */

#include <string>
#include <sstream>
#include <limits>

#include <sys/utsname.h>

#include <imr_file_utils.h>
#include <text_result_log.h>
#include <logging.h>

#include "results_saver.h"

using imr::logger;

using namespace mre;
using namespace imr;

/// - static method ------------------------------------------------------------
imr::CConfig& SResults::getConfig(imr::CConfig& config) {
   config.add<std::string>("output", "Ouput directory", "./");
   config.add<std::string>("problem", "name used in result log as user identification of the problem being solved");
   config.add<std::string>("variant", "name used in result log as user identification of a problem variant being solved");
   config.add<bool>("save-pic", "Enable/disable saving images", false);
   config.add<bool>("save-state", "Enable/disable saving state", false);
   config.add<std::string>("pic-dir", "Directory where pictures are save (within output directory", "pic");
   config.add<std::string>("state-dir", "Directory where states are save (within output directory", "state");
   config.add<std::string>("pic-ext", "Determine format for saving bitmaps (allowed are png or jpeg)", "png");
   config.add<std::string>("results", "result log file, it will be placed in output directory", "results.log");
   config.add<std::string>("info", "information file, it will be placed in particular experiment directory", "info.txt");
   config.add<std::string>("settings", "store configurations in boost::program_options config file format ", "settings.txt");
   config.add<bool>("save-results", "disable/enable save results,configs and so on", true);
   config.add<bool>("verbose-result-log", "disable/enable printing results log into logger", false);
   config.add<std::string>("result-pic", "final pic", "map.png");
   config.add<int>("iter", "set particular interation, if >=0 a subdirectory is created for the output results", -1); 

   config.add<bool>("enable-coverage-progress", "Enable/disable saving coverage progress", 1);
   config.add<std::string>("coverage-progress", "Filename to store coverage along the path", "coverage.txt");
   config.add<bool>("enable-robot-trajectory", "Enable/disable saving robot trajectory", 1);
   config.add<std::string>("robot-trajectory", "Filename to store robot trajectory", "trajectory.txt");
   return config;
}

/// - constructor --------------------------------------------------------------
SResults::SResults(imr::CConfig& config) :
   cfg(config),
   SAVE_PIC(cfg.get<bool>("save-pic")),
   SAVE_STATE(cfg.get<bool>("save-state")),
   SAVE_RESULTS(cfg.get<bool>("save-results")),
   IMAGE_EXT(cfg.get<std::string>("pic-ext")),
   ITER(cfg.get<int>("iter")),
   COVERAGE_PROGRESS(cfg.get<bool>("enable-coverage-progress")),
   ROBOT_TRAJECTORY(cfg.get<bool>("enable-robot-trajectory"))
{
   ASSERT_ARGUMENT(IMAGE_EXT == "png" or IMAGE_EXT == "jpeg" or IMAGE_EXT == "jpg", "Only png, jpg, or jpeg are supported");
   output = cfg.get<std::string>("output");
   if (!output.empty() and output[output.size() -1] != '/') {
      output += "/";
   } 
   if (!output.empty() and !imr::checkPath(output)) {
      ASSERT_ARGUMENT(imr::createDirectory(output), "Cannot create output directory");
   }
   picDir = output + (ITER >= 0 ? string_format(ITER) + "/" : "") + cfg.get<std::string>("pic-dir");
   stateDir = output + (ITER >= 0 ? string_format(ITER) + "/" : "") + cfg.get<std::string>("state-dir");
   if (!picDir.empty() and picDir[picDir.size() -1] != '/') { picDir += "/"; }
   if (SAVE_PIC and !picDir.empty() and !imr::checkPath(picDir)) { ASSERT_ARGUMENT(imr::createDirectory(picDir), "Cannot create picDir directory"); }
   if (!stateDir.empty() and stateDir[stateDir.size() -1] != '/') { stateDir += "/"; }
   if (SAVE_STATE and !stateDir.empty() and !imr::checkPath(stateDir)) { ASSERT_ARGUMENT(imr::createDirectory(stateDir), "Cannot create stateDir directory"); }
   saveResults= config.get<bool>("save-results");
   verboseLog = config.get<bool>("verbose-result-log");
}

/// - destructor ---------------------------------------------------------------
SResults::~SResults() {
}

/// - public method ------------------------------------------------------------
std::string SResults::getRevision(void) {
   return "$Id: results_saver.cc 2539 2012-11-13 15:16:48Z honza $";
}

/// - public method ------------------------------------------------------------
std::string SResults::getFinalPicFilename(void) {
   std::string dir;
   return getOutputIterPath(cfg.get<std::string>("result-pic"), dir);
}

/// - public method ------------------------------------------------------------
void SResults::init(int numRobots, double laserRange) {
   stepTotal = 0;
   step = 0;
   expectedExplorationTimeSteps = 0.0;
   resultLog 
      << CResultLog::SetDefaultValue("ROBOTS", string_format(numRobots))
      << CResultLog::SetDefaultValue("LASER_RANGE", string_format(laserRange))
      ;
   coverage.clear();
}

/// - public method ------------------------------------------------------------
void SResults::fillResults(void) {
   double min = std::numeric_limits<double>::max();
   double max = std::numeric_limits<double>::min();
   std::stringstream ss;
   for(int i = 0; i < travelledDistances.size(); ++i) {
      const double d = travelledDistances[i];
      if (d < min) { min = d; }
      if (d > max) { max = d; }
      ss << d << (i < (travelledDistances.size() -1) ? "," : "");
   }
   resultLog << result::newrec
      << result::nextcol << result::nextcol << result::nextcol << result::nextcol
      << result::nextcol // GOALS
      << result::nextcol // DEPOT_RETURN
      << result::nextcol << result::nextcol << result::nextcol << result::nextcol
      << result::addval
      << ITER
      << tm.realTime() << tm.cpuTime()
      << stepTotal << step << maxGridDistance
      << max << min << ss.str()
      << expectedExplorationTimeSteps
      << result::endrec;
}

/// - public method ------------------------------------------------------------
void SResults::defineResultLog(const std::string& method, 
      int w_grid, int h_grid, double w_map, double h_map, double cell_size
      ) 
{
   static bool resultLogInitialized = false;
   if (!resultLogInitialized) {
      resultLog 
         << CResultLog::AddColumn("PROBLEM", cfg.get<std::string>("problem"))
         << CResultLog::AddColumn("VARIANT", cfg.get<std::string>("variant"))
         << CResultLog::AddColumn("METHOD", method)
         << CResultLog::AddColumn("GOALS", cfg.get<std::string>("goals"))
	 << CResultLog::AddColumn("DEPOT_RETURN", string_format(cfg.get<bool>("enable-return-depot")))
         << CResultLog::AddColumn("GRID_SIZE", string_format(w_grid) + "x" + string_format(h_grid))
         << CResultLog::AddColumn("MAP_SIZE", string_format(w_map) + "x" + string_format(h_map))
         << CResultLog::AddColumn("CELL_SIZE", string_format(cell_size))
         << CResultLog::AddColumn("LASER_RANGE", "x")
         << CResultLog::AddColumn("ROBOTS", "x")
         << result::newcol << "ITER"
         << result::newcol << "RTIME"
         << result::newcol << "CTIME"
         << result::newcol << "NUM_EXECUTION_STEPS"
         << result::newcol << "NUM_PLANNING_STEPS"
         << result::newcol << "MAX_GRID_DISTANCE"
         << result::newcol << "MAX_DISTANCE"
         << result::newcol << "MIN_DISTANCE"
         << result::newcol << "DISTANCES"
	 << result::newcol << "EET_STEPS"
         //<< CResultLog::AddColumn
         ;
      resultLogInitialized = true;
   }
}

/// - public method ------------------------------------------------------------
void SResults::save(void) {
   if (cfg.get<bool>("save-results")) {
      std::string str;
      appendToLog();
      saveInfo(getOutputIterPath(cfg.get<std::string>("info"), str));
      saveSettings(getOutputIterPath(cfg.get<std::string>("settings"), str));
      if (COVERAGE_PROGRESS) {
         saveCoverage(getOutputIterPath(cfg.get<std::string>("coverage-progress"), str));
      }
      if (ROBOT_TRAJECTORY) {
         saveTrajectory(getOutputIterPath(cfg.get<std::string>("robot-trajectory"), str));
      }
   }
}

/// - public method ------------------------------------------------------------
void SResults::appendToLog(void) { //append last record
   std::string dir;
   std::string log = getOutputPath(cfg.get<std::string>("results") , dir);
   assert_io(createDirectory(dir), "Can not create directory '" + dir + "'");
   std::fstream out(log.c_str(), std::ios::app | std::ios::out);
   CTextLog t(resultLog);
   t.printRecord(resultLog.last(), out);
   if (verboseLog) {
      std::stringstream str;
      t.printRecord(resultLog.last(), str, false);
      INFO("result log: " << str.str());
   }
   out.close();
   if (!resultLog) {
      WARN("Error in resultLog:" << resultLog.getError());
      resultLog.clear();
   }
}

/// - public method ------------------------------------------------------------
void SResults::saveInfo(const std::string& file) throw(imr::io_error) {
   std::ostream* out;
   std::ofstream* ofs = 0;
   std::stringstream* ss = 0;
   if (saveResults) {
      assert_io(createDirectory(getDirname(file)), "Can not create file in path'" + file + "'");
      ofs = new std::ofstream(file.c_str());
      out = ofs;
   } else {
      ss = new std::stringstream();
      out = ss;
   }
   *out << "# program information " << std::endl;
   *out << "VERSION: " << getVersion() << std::endl;
   *out << "REVISION: " << getRevision() << std::endl;
   struct utsname n;
   if (uname(&n) == 0) {
      *out << std::endl << "# host information " << std::endl;
      *out << "PLATFORM:  " << n.sysname << " " << n.release << std::endl;
      *out << "VERSION: " << n.version << " " <<std::endl;
      *out << "HOST: " << n.nodename << std::endl;
      *out << "MACHINE: " << n.machine << std::endl;
   }
   {
      char data[26];
      const time_t seconds = time (NULL);
      *out << std::endl << "# time information " << std::endl;
      *out << "DATE: " << ctime_r(&seconds, &data[0]) << std::endl;
   }

   *out << "# detail performance log" << std::endl;
   *out << "TIME_CPU: " << tm.cpuTime() << std::endl;
   *out << "TIME_USR: " << tm.userTime() << std::endl;
   *out << "TIME_SYS: " << tm.sysTime() << std::endl;
   *out << std::endl << "# log results" << std::endl;
   CResultLog::RecordIterator record = resultLog.last();
   if (record != resultLog.records_end()) {
      result::StringArray::iterator col = resultLog.column_names_begin();
      for(result::StringArray::iterator v = record->begin(); v != record->end(); col++, v++) {
         *out << *col << ":" << *v << std::endl;
      } //end for loop
   }
   if (ofs) {
      assert_io(ofs->good(), "Can not save info file '" + file + "'");
      ofs->close();
      delete ofs;
   } else {
      INFO(ss->str());
      delete ss;
   }
}

/// - public method ------------------------------------------------------------
void SResults::saveSettings(const std::string& file) throw(imr::io_error) {
   if (saveResults) {
      assert_io(createDirectory(getDirname(file)), "Can not create file in path'" + file + "'");
      std::ofstream out(file.c_str());
      cfg.print(out);
      if (out) {
	 assert_io(out.good(), "Can not save info file '" + file + "'");
      }
      out.close();
   }
}

/// - public method ------------------------------------------------------------
std::string SResults::getOutputPath(const std::string filename, std::string& dir) {
   if (!output.empty()) {
      dir = (output[output.size() - 1] == '/') ? output : output + "/";
      return dir + filename;
   } else {
      dir = "./";
      return filename;
   }
}

/// - public method ------------------------------------------------------------
std::string SResults::getOutputIterPath(const std::string filename, std::string& dir) {
   if (ITER < 0) {
      return getOutputPath(filename, dir);
   } else {
      std::string i = imr::string_format<int>(ITER);
      if (output.size() > 0) {
         dir = (output[output.size() - 1] == '/') ? output + i + "/": output + "/" + i + "/";
         return dir + filename;
      } else {
         dir = i + "/";
         return dir + filename;
      }
   }
}

/// - public method ------------------------------------------------------------
void SResults::saveCoverage(const std::string& filename) throw(imr::io_error) {
   assert_io(createDirectory(getDirname(filename)), "Can not create directory '" + getDirname(filename) + "'");
   std::fstream out(filename.c_str(), std::ios::out);
   out << "#step longest_traveled_path coverage_in_percentage_of_the_final" << std::endl;
   for(int i = 0; i < coverage.size(); ++i) {
      out << coverage[i].step << " " << coverage[i].travelled_distance << " " << coverage[i].coverage << std::endl;
   }
   out.close();
}

/// - public method ------------------------------------------------------------
void SResults::saveTrajectory(const std::string& filename) throw(imr::io_error) {
   assert_io(createDirectory(getDirname(filename)), "Can not create directory '" + getDirname(filename) + "'");
   std::fstream out(filename.c_str(), std::ios::out);
   for(int i = 0; i < trajectory.size(); ++i) {
      out << trajectory[i].time << " " << trajectory[i].x << " " << trajectory[i].y << std::endl;
   }
   out.close();
}


/// - private method -----------------------------------------------------------

/* end of results_saver.cc */
