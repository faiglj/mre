/*
 * File name: paths.cc
 * Date:      2013/04/02 07:41
 * Author:    Jan Faigl
 */

#include <limits>

#include <imr_assert.h>
#include <logging.h>
#include <perf_timer.h>

#include "mre_grid_utils.h"
#include "paths.h"

#include <boost/foreach.hpp>

using namespace mre;

#define foreach BOOST_FOREACH

using imr::logger;

/// - static method ------------------------------------------------------------
imr::CConfig& CPaths::getConfig(imr::CConfig& config) {
   config.add<bool>("paths-distance-use-dijkstra", "Enable/disable using dijkstra for distance queries, if not set, distance transform is used", false);
   config.add<bool>("paths-path-use-dijkstra", "Enable/disable using dijkstra for path queries, if not set, distance transform is used", false);
   config.add<bool>("paths-distance-caching", "Enable/disable distance caching", true);
   config.add<bool>("paths-path-caching", "Enable/disable paths  caching", true);
   return config;
}

/// - constructor --------------------------------------------------------------
CPaths::CPaths(imr::CConfig& cfg, const ByteMatrix& iGrid, const unsigned char FREESPACE) :
   ENABLE_DISTANCE_CACHING(cfg.get<bool>("paths-distance-caching")),
   ENABLE_PATHS_CACHING(cfg.get<bool>("paths-path-caching")),
   DISTANCE_USE_DIJKSTRA(cfg.get<bool>("paths-distance-use-dijkstra")),
   PATHS_USE_DIJKSTRA(cfg.get<bool>("paths-path-use-dijkstra")),
   FREESPACE(FREESPACE),
   occ(iGrid.NROWS, iGrid.NCOLS),
   SIZE(iGrid.SIZE)
{
   graphNodes = 0;
   distances = 0;
   dt = 0;
   bts = 0;
   tr.resize(SIZE, -1);
}

/// - destructor ---------------------------------------------------------------
CPaths::~CPaths() {
   release();
};

/// - public method ------------------------------------------------------------
float CPaths::getDistance(const GridCoords& pt1, const GridCoords& pt2) {
   const int gridIDXfrom = occ.index(pt1.y, pt1.x);
   const int gridIDXto = occ.index(pt2.y, pt2.x);
   ASSERT_ARGUMENT(
         gridIDXfrom >= 0 and gridIDXfrom < SIZE and
         gridIDXto >= 0 and gridIDXto < SIZE,
         "Distance can be determined only for freespace cells");
   float ret = std::numeric_limits<float>::max();
   const int from = tr[gridIDXfrom];
   const int to = tr[gridIDXto];

   if (gridIDXfrom == gridIDXto) {
      ret = 0.0;
   } else if (from != -1 and to != -1) {
      if (ENABLE_DISTANCE_CACHING and distances[from] and distances[from][to] >= 0.0) { //use cached value 
         ret = distances[from][to];
      } else {
         if (DISTANCE_USE_DIJKSTRA) { //use dijkstra 
            const int N = graphNodes;
            IntVector ptmp(N, -1);
            FloatVector dist(N, std::numeric_limits<float>::max());
            dijkstra.solve(from, to, dist, ptmp);
            ret = dist[to];
            if (ENABLE_DISTANCE_CACHING) {
               updateDistances(from, dist);
            }
         } else { //use DT
            dt->setGoal(pt2);
            ret = dt->getDistance(pt1);
            if (ENABLE_DISTANCE_CACHING) {
               updateDistance(from, to, ret);
            }
         }
      }
   }
   return ret;
}

/// - public method ------------------------------------------------------------
bool CPaths::getDistance(const GridCoords& pt1, const GridCoords& pt2, float &dist) {
   bool ret = false;
   const int gridIDXfrom = occ.index(pt1.y, pt1.x);
   const int gridIDXto = occ.index(pt2.y, pt2.x);

   if (gridIDXfrom >= 0 and gridIDXfrom < SIZE and gridIDXto >= 0 and gridIDXto < SIZE) {
      dist = std::numeric_limits<float>::max();
      const int from = tr[gridIDXfrom];
      const int to = tr[gridIDXto];

      if (gridIDXfrom == gridIDXto) {
         dist = 0.0;
         ret = true;
         DEBUG("CPaths::getDistance gridIDXfrom == gridIDXto");
      } else if (from != -1 and to != -1) {
         if (ENABLE_DISTANCE_CACHING and distances[from] and distances[from][to] >= 0.0) { //use cached value 
            DEBUG("CPaths::getDistance from cache: " << distances[from][to]);
            dist = distances[from][to];
            ret = (dist >= 0.0);
         } else {
            if (DISTANCE_USE_DIJKSTRA) { //use dijkstra 
               const int N = graphNodes;
               IntVector ptmp(N, -1);
               FloatVector dists(N, std::numeric_limits<float>::max());
               dijkstra.solve(from, to, dists, ptmp);
               dist = dists[to];
               if (ENABLE_DISTANCE_CACHING) {
                  updateDistances(from, dists);
               }
            } else { //use DT
               dt->setGoal(pt2);
               dist = dt->getDistance(pt1);
               if (ENABLE_DISTANCE_CACHING) {
                  updateDistance(from, to, dist);
               }

               /* This is definitely slower than the above
               dt->setGoal(pt1);
               dist = dt->getDistance(pt2);
               if (ENABLE_DISTANCE_CACHING) {
                  updateDistance(from, to, dist);
               } */

               /*
               dt->setGoal(pt2);
               dist = dt->getDistance(pt1);
               if (ENABLE_DISTANCE_CACHING) {
                  const int N = graphNodes;
                  FloatVector dists(N, std::numeric_limits<float>::max());
                  for(int k = 0; k < N; ++k) {
                     dists[k] = dt->getDistance(gridIndexes[k]);
                  }
                  updateDistances(to, dists);
               }*/

               /*
               dt->setGoal(pt1);
               dist = dt->getDistance(pt2);
               if (ENABLE_DISTANCE_CACHING) {
                  const int N = graphNodes;
                  FloatVector dists(N, std::numeric_limits<float>::max());
                  for(int k = 0; k < N; ++k) {
                     dists[k] = dt->getDistance(gridIndexes[k]);
                  }
                  updateDistances(from, dists);
               }
               */
 
 


               /*
               dt->setGoal(pt1);
               dist = dt->getDistance(pt2);
               if (ENABLE_DISTANCE_CACHING) {
                  const int N = graphNodes;
                  FloatVector dists(N, std::numeric_limits<float>::max());
                  for(int k = 0; k < N; ++k) {
                     dists[k] = dt->getDistance(gridIndexes[k]);
                  }
 //                 dists[to] = 0.0;
                  updateDistances(from, dists);
                  //updateDistance(from, to, dist);
               }*/
            }
         }
         if (dist < std::numeric_limits<float>::max() and dist >= 0.0) {
            ret = true;
         }
      }
   }
   return ret;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CPaths::getPath(const GridCoords& pt1, const GridCoords& pt2, GridCoordsVector& path) {
   const int gridIDXfrom = occ.index(pt1.y, pt1.x);
   const int gridIDXto = occ.index(pt2.y, pt2.x);
   path.clear();
   ASSERT_ARGUMENT(
         gridIDXfrom >= 0 and gridIDXfrom < SIZE and
         gridIDXto >= 0 and gridIDXto < SIZE,
         "Path can be determined only for freespace cells"
         );

   //1. determine path 
   GridCoordsVector pts;
   if (pt1 == pt2) {
      pts.push_back(pt2);
   } else {
      const int from = tr[gridIDXfrom];
      const int to = tr[gridIDXto];
      if (from != -1 and to != -1) {
         const int N = graphNodes;
         const int pathIDX = from * N + to;

         PathCache::iterator it = pathCache.find(SPath(from, to));
         if (ENABLE_PATHS_CACHING and it != pathCache.end()) {
            path = *(it->path);
            //           nbrHits++;
         } else {
            if (PATHS_USE_DIJKSTRA) { // Dijkstra
               // CPerfTimer t("CPaths::path Dijkstra");
               IntVector ptmp(N, -1);
               FloatVector dst(N, std::numeric_limits<float>::max());
               dijkstra.solve(from, to, dst, ptmp);
               if ((!distances[from] or distances[from][to] < 0) and ENABLE_DISTANCE_CACHING and DISTANCE_USE_DIJKSTRA) {
                  updateDistances(from, dst);
               }
               foreach(int idx, ptmp) {
                  const int gridIDX = gridIndexes[idx];
                  pts.push_back(GridCoords(occ.col(gridIDX), occ.row(gridIDX)));
               }
            } else { // Distance transform
               //CPerfTimer t("CPaths::path DT");
               dt->setGoal(pt2);
               dt->getPath(pt1, pts);
            }

            //2. simplify the path 
            if (pts.size() > 2) {
               const int maxFails = 10;
               GridCoordsVector ptsSimpl;
               ptsSimpl.clear();
               GridCoordsVector ln;
               ptsSimpl.push_back(pts.front());
               int last = 1;
               while(ptsSimpl.back() != pts.back()) {
                  const int MAX = maxFails == -1 ? pts.size() - last : maxFails;
                  int e = 0;
                  for(int i = (last + 1); i < pts.size(); ++i) {
                     if (isFree(bresenham(ptsSimpl.back(), pts[i], ln))) {
                        last = i;
                     } else {
                        e++;
                        if (e > MAX) { break; } //parameters
                     }
                  }
                  ptsSimpl.push_back(pts[last]);
               }
               // the path includes the start and goal points
               // 3. fill the simplified path
               // fillPath(ptsSimpl, path);
               path = ptsSimpl;
            } else {
               path = pts;
            }
            if (ENABLE_PATHS_CACHING) {
               GridCoordsVector* p = new GridCoordsVector();
               *p = path;
               pathCache.insert(SPath(from, to, p));
               //              nbrAllocations++;
            }
         }
      } //end from and to are valid
   }
   return path;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CPaths::getPath2WaySimpl(const GridCoords& pt1, const GridCoords& pt2, GridCoordsVector& path) {
   GridCoordsVector pts1;
   GridCoordsVector pts2;
   //     std::reverse(robot.pathSimple.begin(), robot.pathSimple.end());
   const float d1 = getPathLength(getPath(pt1, pt2, pts1));
   const float d2 = getPathLength(getPath(pt2, pt1, pts2));
   if (d1 < d2) {
      path = pts1;
   } else {
      std::reverse(pts2.begin(), pts2.end());
      path = pts2;
   }
   return path;
}

/// - public method ------------------------------------------------------------
void CPaths::updateMap(const ByteMatrix& iGrid) {
   release();
   ASSERT_ARGUMENT(SIZE == iGrid.SIZE and occ.NROWS == iGrid.NROWS and occ.NCOLS == iGrid.NCOLS, "Grids dimensions do not match");
   tr.clear();
   tr.resize(occ.SIZE, -1);
   gridIndexes.clear();

   graphNodes = 0;
   for(int i = 0; i < SIZE; ++i) {
      if (iGrid[i] == FREESPACE) {
         occ[i] = FREESPACE_CELL;
         tr[i] = graphNodes++;
         gridIndexes.push_back(i);
      } else {
         occ[i] = OCCUPIED_CELL;
      }
   } //end for 
   ASSERT_ARGUMENT(graphNodes, "At least one freespace cell must be in the map");
   distances = new float*[graphNodes]; //prepare distances
   for (int i = 0; i < graphNodes; i++) {
      distances[i] = 0;
   }
   if (DISTANCE_USE_DIJKSTRA or PATHS_USE_DIJKSTRA) {
      createDijkstra();
   }
   //dt = new CDistanceTransform(occ, FREESPACE_CELL, OCCUPIED_CELL);
   dt = new CDistanceTransform(occ);
}

/// - public method ------------------------------------------------------------
PathNodeVector& CPaths::getPathPlan(const CMap& map, const SPosition& pose, const GridCoordsVector& path, PathNodeVector& plan) {
   plan.clear();
   if (!path.empty()) {
      SPathNode node;
      GridCoordsVector line;
      SPosition cur = pose;
      SPosition next(map.grid2realX(path[0].x), map.grid2realY(path[0].y));
      if (fabs(cur.yaw - cur.azimuth(next)) > DEG2RAD(3)) { // allow 3 degrees tolerance
         node.pose = cur; node.pose.yaw = next.yaw;
         node.coords = GridCoords(map.real2gridX(node.pose.x), map.real2gridY(node.pose.y));
         plan.push_back(node);
      } //end turn at the spot
      node.pose = next; node.coords = path[0]; plan.push_back(node);
      for(int i = 1; i < path.size(); ++i) {
         cur = next;
         next.x = map.grid2realX(path[i].x);
         next.y = map.grid2realY(path[i].y);
         node.pose.yaw = cur.azimuth(next);
         bresenham(path[i-1], path[i], line);
         line.push_back(path[i]);
         for(int j = 0; j < line.size(); ++j) {
            const GridCoords& pt = line[j];
            node.coords = pt; node.pose.x = map.grid2realX(pt.x); node.pose.y = map.grid2realY(pt.y);
            //           ASSERT_ARGUMENT(map.
            plan.push_back(node);
         }
      }
   } //end path is not empty
   return plan;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CPaths::fillPath(const GridCoordsVector& pts, GridCoordsVector& path) {
   return ::fillPath(pts, path);
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CPaths::simplify(const GridCoordsVector& pts, GridCoordsVector& path) {
   if (pts.size() > 2) {
      const int maxFails = 10;
      path.clear();
      GridCoordsVector ln;
      path.push_back(pts.front());
      int last = 1;
      while(path.back() != pts.back()) {
         const int MAX = maxFails == -1 ? pts.size() - last : maxFails;
         int e = 0;
         for(int i = (last + 1); i < pts.size(); ++i) {
            if (isFree(bresenham(path.back(), pts[i], ln))) {
               last = i;
            } else {
               e++;
               if (e > MAX) { break; } //parameters
            }
         }
         path.push_back(pts[last]);
      }
   } else {
      path = pts;
   }
   return path;
}

/// - private method -----------------------------------------------------------
void CPaths::createDijkstra(void) {
#define SQRT2 1.414213562373095048
   const int dr[8] = { -1, 0,  0, 1,  1, -1, 1, -1};
   const int dc[8] = {  0, 1, -1, 0, -1, -1, 1,  1};
   const float cost[8] = { 1, 1,  1, 1, SQRT2, SQRT2, SQRT2, SQRT2 };
   int e = 0;
   dijkstra.clear();
   for(int i = 0; i < SIZE; ++i) {
      if (tr[i] != -1) { //freespace node
         for(int k = 0; k < 8; ++k) {
            const int r = occ.row(i) + dr[k]; //i is the grid index
            const int c = occ.col(i) + dc[k]; //i is the grid index
            const int idx = occ.index(r, c);
            if (r >= 0 and r < occ.NROWS and c >= 0 and c < occ.NCOLS and occ(r, c) == FREESPACE_CELL and tr[idx] != -1) {
               dijkstra.addEdge(tr[i], tr[idx], cost[k]); 
               e++;
            }
         } //end all neighboring nodes
      } //end all nodes
   }
   DEBUG("CPaths: Dijkstra graph completed no. of nodes: " << graphNodes << " no. of edges: " << e);
}

/// - private method -----------------------------------------------------------
void CPaths::release(void) {
   if (distances) {
      for(int i = 0; i < graphNodes; ++i) {
         if (distances[i]) { delete[] distances[i]; }
      }
      delete[] distances;
      distances = 0;
   }
   if (dt) {
      delete dt;
      dt = 0;
   }
   if (bts) {
      delete bts;
      bts = 0;
   }
   pathCache.clear();

}

/// - private method -----------------------------------------------------------
bool CPaths::isFree(const GridCoordsVector& pts) {
   bool ret = true;
   foreach(const GridCoords& pt, pts) {
      if (occ(pt.y, pt.x) != FREESPACE_CELL) {
         ret = false;
         break;
      }
   }
   return ret;
}

/* end of paths.cc */
