/*
 * File name: basic_theta_star.cc
 * Date:      2012/02/16 16:21
 * Author:    Jan Faigl
 */

#include <queue>
#include <limits>
#include <algorithm>

#include "mre_grid_utils.h"

#include "theta_star_heap.h"

#include "basic_theta_star.h"

using namespace mre;

/// ----------------------------------------------------------------------------
/// @brief 
/// ----------------------------------------------------------------------------
struct SNode {
   SNode* parent;
   GridCoords coords;
   double g;

   SNode() : parent(0), coords(-1, -1) {
      g = std::numeric_limits<double>::max();
   }
   SNode(const GridCoords& coords) : parent(0), coords(coords) {
      g = std::numeric_limits<double>::max();
   }

   SNode(int x, int y) : parent(0), coords(x, y) {
      g = std::numeric_limits<double>::max();
   }

   double squared_distance(const SNode* a) const {
      return coords.squared_distance(a->coords);
   }
};

static int px[8] = { -1, 0,  1,  1,  1,  0, -1, -1};
static int py[8] = {  1, 1,  1,  0, -1, -1, -1,  0};

/// - constructor --------------------------------------------------------------
CBasicThetaStar::CBasicThetaStar(const ByteMatrix& navGrid, const unsigned char FREESPACE) : navGrid(navGrid), FREESPACE(FREESPACE) {
}

/// - destructor ---------------------------------------------------------------
CBasicThetaStar::~CBasicThetaStar() {
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CBasicThetaStar::getPath(const GridCoords& startI, const GridCoords& goalI, GridCoordsVector& path) const {
   typedef std::vector<SNode*> NodePtrVector;
   path.clear();
   BoolVector closeLst(navGrid.SIZE, false);
   NodePtrVector nodes;
   CHeap<double> open(navGrid.SIZE);

   nodes.resize(navGrid.SIZE, 0);

   SNode* start = new SNode(startI);
   SNode* goal = new SNode(goalI);
   nodes[index(start->coords)] = start;
   nodes[index(goal->coords)]= goal;

   start->g = 0;
   start->parent = start;
   open.add(index(start->coords), start->g + sqrt(goal->coords.squared_distance(start->coords)));

   int sIDX = open.getFirst();
   int c = 0;
   while(sIDX != -1) {
      c++;
      SNode* s = nodes[sIDX];
      if (s->g < goal->g) {
         closeLst[sIDX] = true;
         open.getIDX(sIDX) = -1;
         for(int i = 0; i < 8; i++) {
            const int x = s->coords.x + px[i];
            const int y = s->coords.y + py[i];
            const int scIDX = y * navGrid.NCOLS + x;
            if (x >= 0 and y >= 0 and x < navGrid.NCOLS and y < navGrid.NROWS and !closeLst[scIDX] and navGrid(y, x) == FREESPACE) { 
               if (!nodes[scIDX]) { 
                  nodes[scIDX] = new SNode(x, y); 
                  nodes[scIDX]->parent = s; 
               }
               SNode* sc = nodes[scIDX];
               const double g_old = sc->g;
               SNode* newParent = 0;
               double newCost = 0.0;
               { //compute cost(s, s')
                  GridCoordsVector pts;
                  if (isObstacleFree(bresenham(s->parent->coords, sc->coords, pts), navGrid, FREESPACE)) 
                  { // sc is reachable from the s->parent
                     newCost = s->parent->g + sqrt(s->parent->squared_distance(sc));
                     newParent = s->parent;
                  } else { //parent is s
                     newCost = s->g + sqrt(s->squared_distance(sc));
                     newParent = s;
                  }
               }
               if (newParent and newCost < g_old) { //update cost
                  sc->g = newCost; sc->parent = newParent; //update cost and parent
                  if (open.getIDX(scIDX) != -1) { //update priority_queue
                     open.update(scIDX, sc->g + sqrt(sc->squared_distance(goal)));
                  } else { //insert into priority_queue
                     open.add(scIDX, sc->g + sqrt(sc->squared_distance(goal)));
                  }
               }
            }
         } //end all neigh8
      } else {
         break;
      }
      sIDX = open.getFirst();
   }
   if (goal->g < std::numeric_limits<double>::max()) {
      SNode* n = goal;
      while(n->parent != n) {
         path.push_back(n->coords);
         n = n->parent;
      }
      path.push_back(n->coords);
      std::reverse(path.begin(), path.end());
      //path has been found
   } else {
      //path has not been found
   }
   for(int i = 0; i < nodes.size(); ++i) {
      if (nodes[i]) { delete nodes[i]; }
   }
   return path;
}

/// - public method ------------------------------------------------------------
float CBasicThetaStar::getDistance(const GridCoords& start, const GridCoords& goal) const {
   GridCoordsVector pts;
   getPath(start, goal, pts);
   return getPathLength(pts);
}


/// - private method -----------------------------------------------------------
bool CBasicThetaStar::lineOfSight(const GridCoords& s, const GridCoords& sc) const {
   bool ret = true;
   int x0 = s.x;
   int y0 = s.y;
   int x1 = sc.x;
   int y1 = sc.y;

   int dy = y1 - y0;
   int dx = x1 - x0;
   int f = 0;
   int sy;
   int sx;
   if (dy < 0) { dy = -dy; sy = -1; } else { sy = 1; }
   if (dx < 0) { dx = -dx; sx = -1; } else { dx = 1; }
   if (dx >= dy) {
      while (x0 != x1) {
         f = f + dy;
         if (f >= dx) {
            if (navGrid(y0 + ((sy-1)/2), x0 + ((sx - 1)/2)) == 0) { //obstacle
               return false;
            }
            y0 = y0 + sy;
            f = f - dx;
         }
         if (f != 0 and navGrid(y0 + ((sy - 1) / 2), x0 + ((sx - 1)/2)) == 0) { //obstacle
            return false;
         }
         if (dy == 0 and navGrid(y0, x0 + ((sx - 1)/2)) == 0 and navGrid(y0 - 1, x0 + ((sx - 1) /2) ) ) { //obstacle
            return false;
         }
         x0 = x0 + sx;
      } //end while
   } else {
      while (y0 != y1) {
         f = f + dx;
         if (f >= dy) {
            if (navGrid(y0 + ((sy - 1)/2), x0 + (sx - 1)/2) == 0) {
               return false;
            }
            x0 = x0 + sx;
            f = f - dy;
         }
         if (f != 0 and navGrid(y0 + ((sy - 1) / 2), x0 + ((sx - 1) /2)) == 0) {
            return false;
         }
         if (dx == 0 and navGrid(y0 + ((sy - 1) / 2), x0) == 0 and navGrid(y0 + ((sy - 1)/2), x0 - 1) == 0) {
            return false;
         }
      } //end while
   }
   return ret;
}

/* end of basic_theta_star.cc */
