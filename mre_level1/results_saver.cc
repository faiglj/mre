/*
 * File name: results_saver.cc
 * Date:      2012/02/18 10:50
 * Author:    Jan Faigl
 */

#include <string>
#include <sstream>
#include <limits>

#include <sys/utsname.h>

#include <imr_file_utils.h>
#include <text_result_log.h>
#include <logging.h>

#include "results_saver.h"

using namespace mre;
using namespace imr;

/// - static method ------------------------------------------------------------
imr::CConfig& SResults::getConfig(imr::CConfig& config) {
   config.add<std::string>("output", "Ouput directory", "./");
   config.add<std::string>("problem", "name used in result log as user identification of the problem being solved");
   config.add<std::string>("variant", "name used in result log as user identification of a problem variant being solved");
   config.add<bool>("save-pic", "Enable/disable saving images");
   config.add<std::string>("pic-dir", "Directory where pictures are save (within output directory", "pic");
   config.add<std::string>("pic-ext", "Determine format for saving bitmaps (allowed are png or jpeg)", "png");
   config.add<std::string>("results", "result log file, it will be placed in output directory", "results.log");
   config.add<std::string>("info", "information file, it will be placed in particular experiment directory", "info.txt");
   config.add<std::string>("settings", "store configurations in boost::program_options config file format ", "settings.txt");
   config.add<bool>("save-results", "disable/enable save results,configs and so on", true);
   config.add<bool>("verbose-result-log", "disable/enable printing results log into logger", false);
   config.add<std::string>("result-pic", "final pic", "map.png");
   config.add<std::string>("iter", "set particular interation, if >=0 a subdirectory is created for the output results", "-1");
   return config;
}

/// - constructor --------------------------------------------------------------
SResults::SResults(imr::CConfig& config) : cfg(config),
   SAVE_PIC(cfg.get<bool>("save-pic")),
   SAVE_RESULTS(cfg.get<bool>("save-results")),
   IMAGE_EXT(cfg.get<std::string>("pic-ext")),
   ITER(cfg.get<std::string>("iter"))
{
   ASSERT_ARGUMENT(IMAGE_EXT == "png" or IMAGE_EXT == "jpeg" or IMAGE_EXT == "jpg", "Only png, jpg, or jpeg are supported");
   output = cfg.get<std::string>("output");
   if (!output.empty() and output[output.size() -1] != '/') {
      output += "/";
   }
   if (!output.empty() and !imr::checkPath(output)) {
      ASSERT_ARGUMENT(imr::createDirectory(output), "Cannot create output directory");
   }
//   picDir = output + (ITER >= 0 ? string_format(ITER) + "/" : "") + cfg.get<std::string>("pic-dir");
   picDir = output + (ITER.compare("-1") == 0 ? "" : ITER +"/") + cfg.get<std::string>("pic-dir");
   if (!picDir.empty() and picDir[picDir.size() -1] != '/') {
      picDir += "/";
   }
   if (!picDir.empty() and !imr::checkPath(picDir)) {
      ASSERT_ARGUMENT(imr::createDirectory(picDir), "Cannot create picDir directory");
   }
   saveResults= config.get<bool>("save-results");
   verboseLog = config.get<bool>("verbose-result-log");
}

/// - destructor ---------------------------------------------------------------
SResults::~SResults() {
}

/// - public method ------------------------------------------------------------
std::string SResults::getRevision(void) {
   return "$Id: results_saver.cc 984 2012-02-18 17:40:18Z honza $";
}

/// - public method ------------------------------------------------------------
std::string SResults::getFinalPicFilename(void) {
   std::string dir;
   return getOutputIterPath(cfg.get<std::string>("result-pic"), dir);
}

/// - public method ------------------------------------------------------------
void SResults::init(int numRobots, double laserRange) {
   stepTotal = 0;
   step = 0;
   expectedExplorationTimeSteps = 0.0;
   resultLog
      << imr::CResultLog::SetDefaultValue("ROBOTS", imr::string_format(numRobots))
      << imr::CResultLog::SetDefaultValue("LASER_RANGE", imr::string_format(laserRange))
      ;
}

/// - public method ------------------------------------------------------------
void SResults::fillResults(void) {
   double min = std::numeric_limits<double>::max();
   double max = std::numeric_limits<double>::min();
   std::stringstream ss;
   for(int i = 0; i < travelledDistances.size(); ++i) {
      const double d = travelledDistances[i];
      if (d < min) { min = d; }
      if (d > max) { max = d; }
      ss << d << (i < (travelledDistances.size() -1) ? "," : "");
   }
   resultLog << result::newrec
      << result::nextcol << result::nextcol << result::nextcol << result::nextcol //problem,variant,method,goals
      << result::nextcol << result::nextcol << result::nextcol << result::nextcol //cost,rot,gridsize,mapsize,
      << result::nextcol << result::nextcol << result::nextcol << result::addval //cellssize,laserRange,robots,
      << ITER
      << tm.realTime() << tm.cpuTime()
      << stepTotal << step
      << max << min << ss.str()
      << expectedExplorationTimeSteps
      << result::endrec;
}

/// - public method ------------------------------------------------------------
void SResults::defineResultLog(const std::string& method, const std::string& cost, double weight,
      int w_grid, int h_grid, double w_map, double h_map, double cell_size
      )
{
   static bool resultLogInitialized = false;
   if (!resultLogInitialized) {
      resultLog
         << imr::CResultLog::AddColumn("PROBLEM", cfg.get<std::string>("problem"))
         << imr::CResultLog::AddColumn("VARIANT", cfg.get<std::string>("variant"))
         << imr::CResultLog::AddColumn("METHOD", method)
         << imr::CResultLog::AddColumn("GOALS", cfg.get<std::string>("goals"))
         << imr::CResultLog::AddColumn("COST", cost)
         << imr::CResultLog::AddColumn("ROT_WEIGHT", imr::string_format(weight))
         << imr::CResultLog::AddColumn("GRID_SIZE", imr::string_format(w_grid) + "x" + imr::string_format(h_grid))
         << imr::CResultLog::AddColumn("MAP_SIZE", imr::string_format(w_map) + "x" + imr::string_format(h_map))
         << imr::CResultLog::AddColumn("CELL_SIZE", imr::string_format(cell_size))
         << imr::CResultLog::AddColumn("LASER_RANGE", "x")
         << imr::CResultLog::AddColumn("ROBOTS", "x")
         << result::newcol << "ITER"
         << result::newcol << "RTIME"
         << result::newcol << "CTIME"
         << result::newcol << "NUM_EXECUTION_STEPS"
         << result::newcol << "NUM_PLANNING_STEPS"
         << result::newcol << "MAX_DISTANCE"
         << result::newcol << "MIN_DISTANCE"
         << result::newcol << "DISTANCES"
         << result::newcol << "EET_STEPS"
         //<< CResultLog::AddColumn
         ;
      resultLogInitialized = true;
   }
}

/// - public method ------------------------------------------------------------
void SResults::save(void) {
   if (cfg.get<bool>("save-results")) {
      std::string str;
      appendToLog();
      saveInfo(getOutputIterPath(cfg.get<std::string>("info"), str));
      saveSettings(getOutputIterPath(cfg.get<std::string>("settings"), str));
   }
}

/// - public method ------------------------------------------------------------
void SResults::appendToLog(void) { //append last record
   std::string dir;
   std::string log = getOutputPath(cfg.get<std::string>("results") , dir);
   assert_io(createDirectory(dir), "Can not create directory '" + dir + "'");
   std::fstream out(log.c_str(), std::ios::app | std::ios::out);
   CTextLog t(resultLog);
   t.printRecord(resultLog.last(), out);
   if (verboseLog) {
      std::stringstream str;
      t.printRecord(resultLog.last(), str, false);
      INFO("result log: " << str.str());
   }
   out.close();
   if (!resultLog) {
      WARN("Error in resultLog:" << resultLog.getError());
      resultLog.clear();
   }
}

/// - public method ------------------------------------------------------------
void SResults::saveInfo(const std::string& file) throw(imr::io_error) {
   std::ostream* out;
   std::ofstream* ofs = 0;
   std::stringstream* ss = 0;
   if (saveResults) {
      assert_io(createDirectory(getDirname(file)), "Can not create file in path'" + file + "'");
      ofs = new std::ofstream(file.c_str());
      out = ofs;
   } else {
      ss = new std::stringstream();
      out = ss;
   }
   *out << "# program information " << std::endl;
   *out << "VERSION: " << getVersion() << std::endl;
   *out << "REVISION: " << getRevision() << std::endl;
   struct utsname n;
   if (uname(&n) == 0) {
      *out << std::endl << "# host information " << std::endl;
      *out << "PLATFORM:  " << n.sysname << " " << n.release << std::endl;
      *out << "VERSION: " << n.version << " " <<std::endl;
      *out << "HOST: " << n.nodename << std::endl;
      *out << "MACHINE: " << n.machine << std::endl;
   }
   {
      char data[26];
      const time_t seconds = time (NULL);
      *out << std::endl << "# time information " << std::endl;
      *out << "DATE: " << ctime_r(&seconds, &data[0]) << std::endl;
   }

   *out << "# detail performance log" << std::endl;
   *out << "TIME_CPU: " << tm.cpuTime() << std::endl;
   *out << "TIME_USR: " << tm.userTime() << std::endl;
   *out << "TIME_SYS: " << tm.sysTime() << std::endl;
   *out << std::endl << "# log results" << std::endl;
   CResultLog::RecordIterator record = resultLog.last();
   if (record != resultLog.records_end()) {
      result::StringArray::iterator col = resultLog.column_names_begin();
      for(result::StringArray::iterator v = record->begin(); v != record->end(); col++, v++) {
         *out << *col << ":" << *v << std::endl;
      } //end for loop
   }
   if (ofs) {
      assert_io(ofs->good(), "Can not save info file '" + file + "'");
      ofs->close();
      delete ofs;
   } else {
      INFO(ss->str());
      delete ss;
   }
}

/// - public method ------------------------------------------------------------
void SResults::saveSettings(const std::string& file) throw(imr::io_error) {
   if (saveResults) {
      assert_io(createDirectory(getDirname(file)), "Can not create file in path'" + file + "'");
      std::ofstream out(file.c_str());
      cfg.print(out);
      if (out) {
	 assert_io(out.good(), "Can not save info file '" + file + "'");
      }
      out.close();
   }
}

/// - private method -----------------------------------------------------------
std::string SResults::getOutputPath(const std::string filename, std::string& dir) {
   if (!output.empty()) {
      dir = (output[output.size() - 1] == '/') ? output : output + "/";
      return dir + filename;
   } else {
      dir = "./";
      return filename;
   }
}

/// - private method -----------------------------------------------------------
std::string SResults::getOutputIterPath(const std::string filename, std::string& dir) {
   if (ITER.compare("-1") == 0) {
      return getOutputPath(filename, dir);
   } else {
      std::string i = ITER;//imr::string_format<int>(ITER);
      if (output.size() > 0) {
         dir = (output[output.size() - 1] == '/') ? output + i + "/": output + "/" + i + "/";
         return dir + filename;
      } else {
         dir = i + "/";
         return dir + filename;
      }
   }
}

/* end of results_saver.cc */
