/*
 * File name: minmax_pairing.h
 * Date:      23.2.2012
 * Author:    Miroslav Kulich
 */


#ifndef __MRE_MINMAX_PAIRING_H__
#define __MRE_MINMAX_PAIRING_H__

#include "mre_types.h"

namespace mre {

   class CMinMaxPairing {
      const DoubleMatrix& costMatrix;
      const bool MINMAX;
      const int N;
      double best;
      int c;
      IntVector bestPermutation;
      public:
      CMinMaxPairing(const DoubleMatrix& costMatrix, bool minmax = true);
      ~CMinMaxPairing();

      const IntVector& getAssignment(void) const { return bestPermutation; }

      private:
      void minmax(const IntVector& v);
      void minsum(const IntVector& v);
      void permute(IntVector v, const int start);
   };

} // end namespace mre

#endif
