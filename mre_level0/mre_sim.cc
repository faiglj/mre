/*
 * File name: mre.cc
 * Date:      2012/01/23 21:51
 * Author:    Jan Faigl
 */

#include <cstdio>
#include <sstream>
#include <iomanip>
#include <limits>
#include <fstream>

#include <boost/foreach.hpp>

#include <logging.h>
#include <colors.h>
#include <timerN.h>
#include <perf_timer.h>
#include <imr_file_utils.h>
#include <stringconversions.h>

#include "mre_hdf_loader.h"

#include "mre_grid_utils.h"
#include "distance_transform.h"
#include "distance_transform_exact_euclidean.h"
#include "mre_strategy_greedy_alloc_rand.h"
#include "mre_strategy_pairs.h"
#include "mre_strategy_min_pos.h"
#include "mre_strategy_hungarian.h"
#include "mre_strategy_mtsp_kmeans.h"

#include "basic_theta_star.h"
#include "voronoi_planning.h"
#include "goals.h"

#include "colormap.h"

#include "mre_sim.h"

using imr::logger;

using namespace mre;

#define foreach BOOST_FOREACH

const FloatMatrix* globalGrid;
IntVectorVector* globalTravelledPaths;
FrontierVector* globalFrontiers;

/// - static method ------------------------------------------------------------
imr::CConfig& CMRESim::getConfig(imr::CConfig& config) {
   config.add<std::string>("robot-config-pattern", "Pattern for robot configuration files" , "etc/robot-%d.cfg");
   config.add<std::string>("mre-strategy", "Strategy greedyallocrand|pairs|hungarian|mtspkmeans|hungariandepot|mtspkmeansdepot|hungariandepot2|minpos", "greedyallocrand");
   config.add<int>("number-robots", "Number of robots", 1);
   config.add<std::string>("robots-positions", "A list of robots' positions in a format x:y:yaw;x:y:yaw (where x, y are in meters and yaw is in degrees");
   config.add<std::string>("gui", "Select gui none|sdl|sdl-nowin" , "sdl");
   config.add<int>("image-quality", "Save image quality png (-1 defult, 0 - off, 9 max), jpeg (0-100)", 9);
   config.add<int>("max-nodes-execution", "Maximal number of nodes that are executed before new replanning, -1 till goal is reached, -2 checking if goal is still frontier", 10);
   config.add<int>("minimal-number-of-frontier-cells", "A frontier (object) has to be formed from at least this number of cells, otherwise it is discarded", 4);
   config.add<int>("max-total-execution-steps", "Terminate exploration if execution steps reached the given number (no results are saved)", 5000);
   config.add<std::string>("hdf-state-file", "State of the exploration stored in the hdf file", "");
   config.add<bool>("load-hdf-state", "Enable/disable loading state from the hdf file (if given)", false); 
   config.add<bool>("enable-path-length-check-before-assign", "Enable check of the longest path before assignment of the goal to the robot without the goal", false);
   config.add<double>("path-length-check-offset", "Do not be so strict when checking the longest path (in meters)", 0.2);
   config.add<bool>("use-theta-star", "Enable/disable using Theta* (default method is DT)", false);
   config.add<bool>("omnidirectional", "Enable/disable omnidirectional sensor", false);
   config.add<std::string>("goals", "A way how goals are selected from the frontier cells [allfrontiers|kmeansicra11|representative|goalsetcoverage|completecoverage|coveringcells|coveringcellsamples|completecoveragefov|completecoverage2|rd|kmeansarn]", "kmeansicra11");
   config.add<double>("goals-range-ratio", "Ratio of the visibility range used for computing coverage", 0.9);
   config.add<bool>("path-planning-dt-reverse", "If enabled, paths for the robot are found using DT with the robot pose goal and reversed path, this speed up the computation", false);
   config.add<double>("nbv-lambda", "Lambda parameter in the next-best-view approach", 20);
   config.add<bool>("gui-draw-goals", "Enable/disable drawing goals", true);
   config.add<bool>("gui-draw-red-frontiers", "Enable/disable drawing red frontiers", false);
   config.add<int>("covering-cells-samples", "Number of samples per goal sets for selecting covering cells", 5);
   config.add<double>("covering-cells-samples-sigma", "Percentage of the frontier set cells used for sampling", 0.3);
   config.add<bool>("enable-assignment-timinigs", "Parameter alpha of the solanas assignment", false);
   config.add<bool>("gui-draw-all-goals", "Enable/disable drawing all goals", false);
   config.add<std::string>("gui-window-title", "Set the window title", "mre");
   config.add<bool>("enable-robot-omnidirectional-motion", "Enable omnidirectional motion which causes the robot plan consists only from cell movements without considering turning", false);
   config.add<bool>("enable-return-depot", "Enable return to the depot", false);
   config.add<bool>("enable-force-assign-same-goals", "Enable assignment goals already assigned goals to the robots without the goals", 0);
   config.add<int>("robot-disk-cells-radius", "Size of the robot in no. of grid cells, if > 0 obstacles are growed", -1);
   config.add<bool>("enable-draw-enlarged-obstacles", "If true, enlarge obstacles are drawn", true);
   config.add<bool>("continuous-sensing", "Enable/disable continous sensing, i.e., performing map update at each robot pixels", true);
   CMap::getConfig(config);
   CRangerSim::getConfig(config);
   CGreedyAllocRand::getConfig(config);
   CMTSPKMeans::getConfig(config);
   SResults::getConfig(config);
   return config;
}

/// - static method ------------------------------------------------------------
CMRESim::TStrategy CMRESim::getStrategy(const std::string& str) {
   if (str == "greedyallocrand") {
      return GREEDY_ALLOC_RAND;
   } else if (str == "pairs") {
      return PAIRS;
   } else if (str == "hungarian") {
      return HUNGARIAN;
   } else if (str == "mtspkmeans") {
      return MTSP_KMEANS;
   } else if (str == "hungariandepot") {
      return HUNGARIAN_DEPOT;
   } else if (str == "mtspkmeansdepot") {
      return MTSP_KMEANS_DEPOT;
   } else if (str == "hungariandepot2") {
      return HUNGARIAN_DEPOT2;
   } else if (str == "minpos") {
      return MIN_POS;
   }
   ASSERT_ARGUMENT(false, "Unknown strategy");
}

/// - static method ------------------------------------------------------------
std::string CMRESim::getStrategyStr(const TStrategy strategy) {
   switch(strategy) {
      case GREEDY_ALLOC_RAND:
	 return "greedyallocrand";
	 break;
      case PAIRS:
	 return "pairs";
	 break;
      case HUNGARIAN:
	 return "hungarian";
	 break;
      case MTSP_KMEANS:
	 return "mtspkmeans";
	 break;
      case HUNGARIAN_DEPOT:
	 return "hungariandepot";
	 break;
      case MTSP_KMEANS_DEPOT:
	 return "mtspkmeansdepot";
	 break;
      case HUNGARIAN_DEPOT2:
	 return "hungariandepot2";
	 break;
      case MIN_POS:
	 return "minpos";
	 break;
   } //end switch
}

/// - static method ------------------------------------------------------------
CMRESim::TGoals CMRESim::getGoals(const std::string& str) {
   if (str == "allfrontiers") {
      return ALL_FRONTIERS;
   } else if (str == "kmeansicra11") {
      return KMEANS_ICRA11;
   } else if (str == "representative") {
      return REPRESENTATIVE;
   } else if (str == "goalsetcoverage") {
      return GOAL_SET_COVERAGE;
   } else if (str == "completecoverage") {
      return COMPLETE_COVERAGE;
   } else if (str == "coveringcells") {
      return COVERING_CELLS;
   } else if (str == "coveringcellsamples") {
      return COVERING_CELLS_SAMPLES;
   } else if (str == "completecoveragefov") {
      return COMPLETE_COVERAGE_FOV;
   } else if (str == "completecoverage2") {
      return COMPLETE_COVERAGE_2;
   } else if (str == "kmeansarn") {
      return KMEANS_ARN;
   }
   ASSERT_ARGUMENT(false, "Unknown goals selection method");
}

/// - static method ------------------------------------------------------------
std::string CMRESim::getGoalsStr(const TGoals goals) {
   switch(goals) {
      case ALL_FRONTIERS:
	 return "allfrontiers";
	 break;
      case KMEANS_ICRA11:
	 return "kmeansicra11";
	 break;
      case REPRESENTATIVE:
	 return "representative";
      case GOAL_SET_COVERAGE:
	 return "goalsetcoverage";
	 break;
      case COMPLETE_COVERAGE:
	 return "completecoverage";
	 break;
      case COVERING_CELLS:
	 return "coveringcells";
	 break;
      case COVERING_CELLS_SAMPLES:
	 return "coveringcellsamples";
	 break;
      case COMPLETE_COVERAGE_FOV:
	 return "completecoveragefov";
	 break;
      case COMPLETE_COVERAGE_2:
	 return "completecoverage2";
	 break;
      case KMEANS_ARN:
	 return "kmeansarn";
	 break;
   } //end switch
}

/// - constructor --------------------------------------------------------------
CMRESim::CMRESim(imr::CConfig& cfg) :
   cfg(cfg),
   map(new CMap(cfg)), rangerSim(new CRangerSim(cfg, map->CELL_SIZE)), paths(new CPathPlanning(*map, cfg.get<bool>("path-planning-dt-reverse"), cfg.get<bool>("enable-robot-omnidirectional-motion"))), results(cfg),
   sdl(cfg.get<std::string>("gui") == "sdl"), gui(0),
   LASER_RANGE_MAX(map->MAX_LASER_RANGE),
   IMAGE_QUALITY(cfg.get<int>("image-quality")),
   STRATEGY(getStrategy(cfg.get<std::string>("mre-strategy"))),
   GOALS_SELECTION(getGoals(cfg.get<std::string>("goals"))),
   MAX_NODES_EXECUTION(cfg.get<int>("max-nodes-execution")),
   MIN_NUMBER_OF_FRONTIER_CELLS(cfg.get<int>("minimal-number-of-frontier-cells")),
   ENABLE_PATH_LENGTH_CHECK(cfg.get<bool>("enable-path-length-check-before-assign")),
   PATH_LENGTH_CHECK_OFFSET(cfg.get<double>("path-length-check-offset")),
   USE_THETA_STAR(cfg.get<bool>("use-theta-star")),
   OMNIDIRECTIONAL(cfg.get<bool>("omnidirectional")),
   NBV_LAMBDA(cfg.get<double>("nbv-lambda")),
   GUI_DRAW_GOALS(cfg.get<bool>("gui-draw-goals")),
   GUI_DRAW_ALL_GOALS(cfg.get<bool>("gui-draw-all-goals")),
   RETURN_DEPOT(cfg.get<bool>("enable-return-depot")),
   FORCE_ASSIGN_SAME_GOALS(cfg.get<bool>("enable-force-assign-same-goals")),
   CONTINUOUS_SENSING(cfg.get<bool>("continuous-sensing"))
{
   const std::string g = cfg.get<std::string>("gui");
   ASSERT_ARGUMENT(
	 (GOALS_SELECTION != COVERING_CELLS) 
	 or (GOALS_SELECTION == COVERING_CELLS)
	 or (GOALS_SELECTION != COVERING_CELLS_SAMPLES)
	 or (GOALS_SELECTION == COVERING_CELLS_SAMPLES),
	 "NBV Hungarian requires covering cells goal selection method");
   if (g != "none") {
      if (g == "sdl") {
	 sdl.init(map->W, map->H, cfg.get<std::string>("gui-window-title"), "mre");
      } else {
	 sdl.initOffScreen(map->W, map->H);
      }
      gui = new gui::CGui(sdl);
      DEBUG("Init gui");
      map->setGui(gui);
   }

   DEBUG("CMRESim::constructor init");
   robots.resize(cfg.get<int>("number-robots"));
   typedef std::vector<std::string> StringVector;
   StringVector pstrs;
   imr::parse_list(cfg.get<std::string>("robots-positions"), ";", pstrs);
   ASSERT_ARGUMENT(robots.size() <= pstrs.size(), "Initial position of all robots must be specified");
   for(int r = 0; r < robots.size(); ++r) {
      robots[r] = new SRobot(r);
      SRobot& robot = *(robots[r]);
      SPosition& pose = robot.pose;
      StringVector parms;
      imr::parse_list(pstrs[r], ":", parms);
      ASSERT_ARGUMENT(parms.size() >= 3, "At least x, y, and yaw must be specified");
      ASSERT_ARGUMENT(imr::string_cast(parms[0], pose.x), "Cannot parse x coord of the robot pose");
      ASSERT_ARGUMENT(imr::string_cast(parms[1], pose.y), "Cannot parse x coord of the robot pose");
      ASSERT_ARGUMENT(imr::string_cast(parms[2], pose.yaw), "Cannot parse yaw of the robot pose");
      robot.gridPose.x = map->real2gridX(pose.x);
      robot.gridPose.y = map->real2gridY(pose.y);
      robot.depot = robot.gridPose;
      INFO("Robot " << r << " Pose: " << pose.x << "," << pose.y << " yaw: " << pose.yaw << " GridCoords: " << robot.gridPose.x << " " << robot.gridPose.y);
      robot.travelledGridDistance = 0.0;
   }
}

/// - destructor ---------------------------------------------------------------
CMRESim::~CMRESim() {
   foreach(SRobot* r, robots) {
      delete r;
   }
   delete paths;
   delete rangerSim;
   delete map;
}

/// - public method ------------------------------------------------------------
void CMRESim::execute(void) {
   simulation_control();
}

/// - private method -----------------------------------------------------------
void CMRESim::simulation_control(void) {

   try {
      const int N = robots.size();
      const double ROBOT_RADIUS = 0;
      const int MAX_TOTAL_EXECUTION_STEPS = cfg.get<int>("max-total-execution-steps");
      const double SENSOR_RANGE_GRID = LASER_RANGE_MAX / map->CELL_SIZE;
      const bool LOAD_HDF_STATE =  cfg.get<bool>("load-hdf-state");
      const bool GUI_DRAW_RED_FRONTIERS = cfg.get<bool>("gui-draw-red-frontiers");
      const bool OMNIDIRECTIONAL_MOTION = cfg.get<bool>("enable-robot-omnidirectional-motion");
      const int ROBOT_DISC_CELLS_RADIUS = cfg.get<int>("robot-disk-cells-radius");
      const bool DRAW_ENLARGE_OBSTACLES = cfg.get<bool>("enable-draw-enlarged-obstacles");

      const bool STEP_TIMING = cfg.get<bool>("enable-assignment-timinigs");

      double time_assign_time = 0.0;
      int time_assign_count = 0;
      double max_assign_time = -std::numeric_limits<double>::max();
      double tstep1;
      double tstep2;

      rangerSim->enlargeMap(0); //
      if (OMNIDIRECTIONAL) {
	 rangerSim->initScanOmni(LASER_RANGE_MAX);
      }
      if (GOALS_SELECTION == GOAL_SET_COVERAGE or GOALS_SELECTION == COMPLETE_COVERAGE or GOALS_SELECTION == COVERING_CELLS or GOALS_SELECTION == COVERING_CELLS_SAMPLES or GOALS_SELECTION == COMPLETE_COVERAGE_FOV or GOALS_SELECTION == COMPLETE_COVERAGE_2) {
	 ASSERT_ARGUMENT(OMNIDIRECTIONAL, "Goals selection goalsetcoverage and completecoverage needs OMNIDIRECTIONAL sensor");
      }

      results.defineResultLog(getStrategyStr(STRATEGY), map->W, map->H, map->MAP_W, map->MAP_H, map->CELL_SIZE);
      results.init(N, LASER_RANGE_MAX);
      results.tm.restart();

      FrontierVector frontiers;
      GridCoordsVector goalGridPts;
      IntVector coveringCells;
      imr::CTimerN tm;

      bool forceTermination = false;

      if (0 and gui) { //test voronoi_planning 
	 const ByteMatrix& grid3 = rangerSim->getMap().getGrid();
	 ByteMatrix grid(grid3.NROWS, grid3.NCOLS);
	 for(int i = 0; i < grid.SIZE; i++) { grid(i) = (grid3(i) == 0 ? 255 : 0); }

	 GridCoords start(10, 10);
	 GridCoords goal(380, 440);
	 GridCoordsVector pathVor;
	 FloatVector distances;
	 GridCoordsVector obstacles;
	 CVoronoiPlanning vor(grid, CDistanceTransform::FREESPACE);
	 {
	    imr::CPerfTimer t("voronoi.compute");
	    vor.compute();
	 }
	 {
	    imr::CPerfTimer t("voronoi.getPath");
	    vor.getPath(start, goal, pathVor);
	 }
	 {
	    imr::CPerfTimer t("voronoi.getDistances");
	    vor.getDistances(pathVor, obstacles, distances);
	 }

	 gui->drawGrid(grid);
	 gui->drawDisk(start, 4, gui->getColor("green"));
	 gui->drawDisk(goal, 4, gui->getColor("deepskyblue"));

	 gui->draw(vor.getVornoiCells(), gui->getColor("SteelBlue4"));
	 foreach(const GridCoords& pt, pathVor) { gui->drawDisk(pt, 2, gui->getColor("orange"), 50); }
	 DEBUG("Path voronoi_planning: " << pathVor.size());
	 for(int i = 0; i < pathVor.size(); ++i) {
	    DEBUG("Distance i: " << i << " " << distances[i]);
	    GridCoordsVector pts;
	    pts.push_back(pathVor[i]);
	    pts.push_back(obstacles[i]);
	    GridCoordsVector ptsFill;
	    fillPath(pts, ptsFill);
	    gui->draw(ptsFill, gui->getColor("MistyRose"));
	 }
	 gui->refresh();
	 getchar();



      }

      if (0 and gui) { //end paths
	 const ByteMatrix& grid3 = rangerSim->getMap().getGrid();
	 ByteMatrix grid(grid3.NROWS, grid3.NCOLS);
	 for(int i = 0; i < grid.SIZE; i++) { grid(i) = (grid3(i) == 0 ? 255 : 0); }
	 CDistanceTransform dt(grid);
	 GridCoords start(10, 10);
	 //GridCoords start(100, 100);
	 // GridCoords goal(280, 440);
	 GridCoords goal(380, 440);
	 // dt.setGoal(start);
	 DEBUG("dt.setGoal");
	 dt.setGoal(goal);
	 GridCoordsVector path;

	 dt.findPath(start, goal, path);
	 DEBUG("dt.findPath path.size: " << path.size());

	 GridCoordsVector ptsSimple;
	 DEBUG("simplify start");
	 simplify(path, grid, ptsSimple);
	 DEBUG("simplify done");
	 simplify2(path, grid, ptsSimple, 10);
	 DEBUG("simplify2 done");

	 CBasicThetaStar bts(grid);
	 GridCoordsVector btsPath;
	 {
	    imr::CPerfTimer t("bts.getPath");
	    bts.getPath(start, goal, btsPath);
	 }
	 DEBUG("bts done");

	 GridCoordsVector ptsSimpleFill;
	 fillPath(ptsSimple, ptsSimpleFill);

	 int i = 0;
	 // foreach(const GridCoords& pt, path) { gui->drawDisk(pt, 2, gui->getColor("cyan")); }
	 INFO("Draw move:");
	 GridCoordsVector btsPathFill;
	 fillPath(btsPath, btsPathFill);
	 // foreach(const GridCoords& pt, btsPathFill) { gui->drawDisk(pt, 2, gui->getColor("orange")); }
	 INFO("Draw basic_theta_star: " << btsPath.size());

	 foreach(const GridCoords& pt, path) { gui->drawDisk(pt, 2, gui->getColor("cyan")); }
	 // sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, "dt-path.png");
	 // foreach(const GridCoords& pt, ptsSimpleFill) { gui->drawDisk(pt, 2, gui->getColor("chocolate")); }
	 // sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, "dt-path-simplified.png");

	 gui->drawGrid(grid);
	 foreach(const GridCoords& pt, ptsSimpleFill) { gui->drawDisk(pt, 2, gui->getColor("chocolate")); }
	 gui->drawDisk(start, 4, gui->getColor("green"));
	 gui->drawDisk(goal, 4, gui->getColor("deepskyblue"));
	 gui->refresh(); 

	 /*
	    DEBUG("Path DT simplify2");
	    getchar();

	    foreach(const GridCoords& pt, btsPathFill) { gui->drawDisk(pt, 2, gui->getColor("orange")); }
	    DEBUG("Path btsPathFill");
	    gui->refresh(); 
	    getchar();
	    */

	 {
	    GridCoordsVector pathdt;
	    GridCoordsVector pathdtee;
	    GridCoordsVector pathVor;
	    CDistanceTransformEE dtee(grid);
	    CDistanceTransform dt(grid);
	    {
	       imr::CPerfTimer t("dtEE.setGoal"); 
	       dtee.setGoal(goal);
	    }
	    {
	       imr::CPerfTimer t("dt.setGoal"); 
	       dt.setGoal(goal);
	    }
	    {
	       // CPerfTimer t("dt.findPath"); 
	       //  dt.findPath(start, goal, pathdt);
	       imr::CPerfTimer t("dt.getPath"); 
	       dt.getPath(start, pathdt);
	    }
	    {
	       //CPerfTimer t("dtee.findPath"); 
	       //dtee.findPath(start, goal, pathdtee);
	       imr::CPerfTimer t("dtee.getPath"); 
	       dtee.getPath(start, pathdtee);
	    }


	    {
	       imr::CPerfTimer t("voronoi.getPath");
	       CVoronoiPlanning vor(grid, CDistanceTransform::FREESPACE);
	       vor.compute();
	       DEBUG("Compute voronoi path");
	       vor.getPath(start, goal, pathVor);
	    }
	    foreach(const GridCoords& pt, pathdt) { gui->drawDisk(pt, 2, gui->getColor("green")); }
	    GridCoordsVector path1;
	    fillPath(pathdtee, path1);
	    foreach(const GridCoords& pt, path1) { gui->drawDisk(pt, 2, gui->getColor("indianred")); }
	    foreach(const GridCoords& pt, pathdtee) { gui->drawDisk(pt, 3, gui->getColor("red")); }
	    DEBUG("Path distance_transform_exact_euclidean");
	    foreach(const GridCoords& pt, pathVor) { gui->drawDisk(pt, 3, gui->getColor("orange")); }
	    DEBUG("Path voronoi_planning: " << pathVor.size());
	    gui->refresh(); gui->refresh();   
	    getchar();
	 }
      } //end testing paths

      if (0 and gui) { //benchmark DTEE
	 const ByteMatrix& grid3 = rangerSim->getMap().getGrid();
	 ByteMatrix grid(grid3.NROWS, grid3.NCOLS);
	 for(int i = 0; i < grid.SIZE; i++) { grid(i) = (grid3(i) == 0 ? 255 : 0); }
	 int c = 0;
	 CDistanceTransformEE dtee(grid);
	 imr::CPerfTimer t("CDistanceTransformEE: ");

	 GridCoordsVector pts;
	 GridCoords pt;
	 std::ifstream in("jh-random_points.txt");
	 while(in >> pt.x >> pt.y) { pts.push_back(pt); }
	 imr::CTimerN timer;
	 timer.start();
	 foreach(const GridCoords& pt, pts) {
	    imr::CPerfTimer t("DTEE setGoal -- ");
	    dtee.setGoal(pt);
	 }
	 timer.stop();
	 DEBUG("Total cpu: " << timer.cpuTime() << " ms real: " << timer.realTime() << " ms, queries: " << pts.size() << " per query: " << timer.cpuTime() / pts.size() << " ms");
	 /*
	    std::ofstream out("jh-random_points.txt");
	    while(c < 100) {
	    GridCoords pt(CRandom::next(grid.NCOLS), CRandom::next(grid.NROWS)); 
	    if (grid(pt.y, pt.x) == 255) {
	    CPerfTimer t("DTEE setGoal: ");
	    dtee.setGoal(pt);
	    c++;
	    out << pt.x << " " << pt.y << std::endl;
	    }
	    }
	    out.close();
	    */
	 t.stop();
	 exit(0);
      } //end benchmark

      if (0 and gui) {
	 const ByteMatrix& grid3 = rangerSim->getMap().getGrid();
	 ByteMatrix grid(grid3.NROWS, grid3.NCOLS);
	 for(int i = 0; i < grid.SIZE; i++) { grid(i) = (grid3(i) == 0 ? 255 : 0); }

	 gui->drawGrid(grid);
	 //GridCoords goal(380, 440);
	 GridCoords goal(280, 410);
	 imr::gui::CColorMap cm;
	 cm.load("etc/jet.txt");
	 // cm.load("etc/jet2.txt");
	 //cm.load("etc/cool.txt");
	 {
	    float max = -std::numeric_limits<float>::max(); 
	    float min = std::numeric_limits<float>::max(); 
	    CDistanceTransformEE dtee(grid);
	    {
	       imr::CPerfTimer t("DTEE - setgoal ");
	       dtee.setGoal(goal);
	    }
	    const FloatMatrix& d = dtee.getDistanceGrid();
	    for(int i = 0; i < grid.SIZE; ++i) {
	       if (grid(i) == 255) {
		  if (d(i) > max) { max = d(i); }
		  if (d(i) < min) { min = d(i); }
	       }
	    }
	    cm.setRange(min, max);
	    for(int i = 0; i < grid.SIZE; ++i) {
	       if (grid(i) == 255) {
		  const imr::gui::SColor& c = cm.getColor(d(i));
		  gui->draw(i, SRGB(255 * c.red, 255 * c.green, 255 * c.blue));
	       }
	    }
	    DEBUG("Paths DTEE");
	    gui->refresh();
	    sdl.save_img_png(IMAGE_QUALITY, "jh-goal-dtee.png");
	    getchar();


	    for(int c = cm.getColorIndex(cm.getMin()); c < cm.getColorIndex(cm.getMax()); ++c) {
	       gui->drawGrid(grid);
	       const imr::gui::SColor& col = cm.getColor(c);
	       SRGB rgb(255 * col.red, 255 * col.green, 255 * col.blue);
	       for(int i = 0; i < grid.SIZE; ++i) {
		  if (grid(i) == 255 and cm.getColorIndex(d(i)) < c) {
		     GridCoordsVector pts;
		     GridCoordsVector ptsFill;
		     const GridCoords s(i % grid.NCOLS, i / grid.NCOLS);
		     dtee.getPath(s, pts);
		     fillPath(pts, ptsFill);
		     gui->draw(ptsFill, rgb);
		  }
	       }
	       DEBUG("Paths at the level: " << c);
	       gui->refresh();
	       std::stringstream ss;
	       ss << "jh-paths-dtee-distance_level-" << c << ".png";
	       DEBUG("save file : " << ss.str());
	       //sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, ss.str());
	       getchar();
	    }
	 }

	 {
	    gui->drawGrid(grid);
	    double max = -std::numeric_limits<double>::max(); 
	    double min = std::numeric_limits<double>::max(); 
	    CDistanceTransform dt(grid);
	    dt.setGoal(goal);
	    const DoubleMatrix& d = dt.getDistanceGrid();
	    for(int i = 0; i < grid.SIZE; ++i) {
	       if (grid(i) == 255) {
		  if (d(i) > max) { max = d(i); }
		  if (d(i) < min) { min = d(i); }
	       }
	    }
	    cm.setRange(min, max);
	    for(int i = 0; i < grid.SIZE; ++i) {
	       if (grid(i) == 255) {
		  const imr::gui::SColor& c = cm.getColor(d(i));
		  gui->draw(i, SRGB(255 * c.red, 255 * c.green, 255 * c.blue));
	       }
	    }
	    DEBUG("Paths DT");
	    gui->refresh();
	    sdl.save_img_png(IMAGE_QUALITY, "jh-goal-dt.png");
	    getchar();
	 }
	 exit(0);
      } //end gui
      /*
	 if (0 and gui) {
      //visualization of the robot positions
      gui->drawGrid(rangerSim->getGridMap());
      foreach(SRobot& robot, robots) {
      gui->drawRobot(robot); 
      }

      gui->refresh(); gui->refresh();
      sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, "jh-init-robots10.png");
      DEBUG("Map visualization");
      getchar();
      }
      // */

      bool q = false;

      if (LOAD_HDF_STATE)  {
	 const std::string hdfFile = cfg.get<std::string>("hdf-state-file");
	 DEBUG("Load state from the " << hdfFile);
	 loadState(hdfFile);
      } else {
	 updateMap(robots, ROBOT_RADIUS, LASER_RANGE_MAX);
	 foreach(const SRobot* robot, robots) { // the robots are placed at the freespace
	    map->setCellsValues(robot->shape, map->FREESPACE_PROB);
	    results.travelledPaths.push_back(IntVector());
	    results.travelledPaths.back().push_back(robot->gridPose.x + robot->gridPose.y * map->W);
	    results.travelledDistances.push_back(0.0);
	 }
      }

      CGreedyAllocRand* greedyAllocRand = new CGreedyAllocRand(cfg);
      CPairs* pairs = new CPairs;
      CHungarian* hungarian = new CHungarian;
      CMTSPKMeans* mtspKmeans = STRATEGY == MTSP_KMEANS or STRATEGY == MTSP_KMEANS_DEPOT ? new CMTSPKMeans(cfg, *paths, STRATEGY == MTSP_KMEANS_DEPOT, FORCE_ASSIGN_SAME_GOALS) : 0;
      CMinPos* minPos = STRATEGY == MIN_POS ? new CMinPos(cfg, *paths) : 0;

      IntVector goalsIDX; //just a single goal for each robot 
      //     IntVector prevGoalsGridIDX;
      GridCoordsVector goalGridPtsPrev;
      GridCoordsVectorVector robotsGoalGridPts;
      GridCoordsVectorVector allGoals(N, GridCoordsVector());

      IntVector robotGoalsIDX(1);

      int prevNFC = 0;
      int planningStep = 0;

      for(int r = 0; r < robots.size(); ++r) {
	 robots[r]->approaching_depot = false;
	 robots[r]->docked = false;
      }

      results.tm.reset().start();


      while(!q) {
	 //read laser data and robot positions
	 tm.restart();

	 //TODO
	 //         PathNodeVectorVector prevPlan;
	 //        foreach(const SRobot& robot, robots) { prevPlan.push_back(robot.plan); } //save the plan 
	 tm.restart();
	 bool execute_plan = true;
	 int cur_node = 0;

	 for(int r = 0; r < robots.size(); ++r) {
	    DEBUG("Robot[" << r << "] no. of plan steps: " << robots[r]->plan.size());
	 }

	 // ---------------------------------------------------
	 // -- Plan execution loop
	 //CPerfTimer t_execute_plan("Time - Plan execution loop");
	 BoolVector skipRobot;
	 for(int r = 0; r < robots.size(); ++r) {
	    skipRobot.push_back(robots[r]->plan.empty());
	    DEBUG("robot[" << r << "] plan size: " << robots[r]->plan.size());
	 }
	 while (!q and execute_plan) {
	    bool change = false;
	    execute_plan = true;
	    for(int r = 0; r < robots.size(); ++r) {
	       SRobot& robot = *(robots[r]);
	       if (cur_node < robot.plan.size()) {
		  results.travelledDistances[r] += sqrt(robot.pose.squared_distance(robot.plan[cur_node].pose));
		  robot.travelledGridDistance += sqrt(robot.gridPose.squared_distance(robot.plan[cur_node].coords));
		  // const double d = robot.gridPose.manhattan_distance(robot.plan[cur_node].coords);
		  rangerSim->setPose(robot.plan[cur_node].pose, robot.plan[cur_node].coords, robot);
		  change |= true;
	       } else if (not skipRobot[r] and not robot.approaching_depot and not robot.docked) {
		  execute_plan = false;
	       }
	       results.travelledPaths[r].push_back(robot.gridPose.x + robot.gridPose.y * map->W);
	    } //end all robots
	    cur_node++;
	    results.stepTotal++;
	    execute_plan = execute_plan and change and (MAX_NODES_EXECUTION < 0 or cur_node < MAX_NODES_EXECUTION);
	    if (CONTINUOUS_SENSING or not execute_plan) {
	       updateMap(robots, ROBOT_RADIUS, LASER_RANGE_MAX);
	       if (execute_plan and MAX_NODES_EXECUTION == -2) { //check if the goal/frontier is still a frontier
		  bool covered = false;
		  const ByteMatrix& navGrid = map->getNavGrid();
		  for (int i = 0; i < goalsIDX.size(); ++i) {
		     if (goalsIDX[i] != -1) {
			if (not map->isFrontier(goalGridPts[goalsIDX[i]])) {
			   covered = true;
			   break;
			}
		     }
		  }
		  if (covered) {
		     //check if all robots are not approach to the depot
		     if (RETURN_DEPOT) {
			for(int r = 0; r < robots.size(); ++r) {
			   SRobot& robot = *(robots[r]);
			   if (not robot.approaching_depot and not robot.docked) {
			      //there is a robot which does not approach the depot or it is docked
			      execute_plan = false;
			      break;
			   }
			} //end all robots
		     } else { //force to replan
			execute_plan = false;
		     }
		  }
	       } //end if execute_plan
	    } //end if CONTINUOUS_SENSING

	    if (results.COVERAGE_PROGRESS) {
	       //determine coverage of the current map
	       double c = 0;
	       const int SIZE = map->W * map->H;
	       FloatMatrix& grid = map->grid;
	       for(int i = 0; i < SIZE; ++i) { if (grid[i] < 0.2) { c += 1.0; } }
	       double distance = -std::numeric_limits<double>::max();
	       for(int r = 0; r < robots.size(); ++r) {
		  if (distance < results.travelledDistances[r]) {
		     distance = results.travelledDistances[r];
		  }
	       }
	       results.coverage.push_back(SCoverageStep(results.stepTotal, distance, c));
	    } //end coverage progress
	    if (results.ROBOT_TRAJECTORY) {
	       if (results.trajectory.empty()) {
		  results.trajectory.push_back(robots[0]->pose); //just first robot is considered
		  results.trajectory.back().time = planningStep;
	       } else if (results.trajectory.back().squared_distance(robots[0]->pose) > 0.001) {
		  results.trajectory.push_back(robots[0]->pose); //just first robot is considered
		  results.trajectory.back().time = planningStep;
	       }
	    }
	 }
	 // t_execute_plan.stop();

	 // - end plan execution loop -------------------------

	 //  CPerfTimer t_gd_ta("Time - goal detection and task allocation");
	 planningStep++;
	 tm.stop();
	 INFO("Plan consisting of " << cur_node << " steps has been performed in time real: " << tm.realTime() << " ms cpu: " << tm.cpuTime() << " ms");
	 results.step++;

	 /// fixing case when a frontier is not covered due to limited precision of the simulation
	 /* {
	    FloatMatrix& grid = map->grid;
	    const double laserGrid = LASER_RANGE_MAX / map->CELL_SIZE;
	    foreach(SRobot& robot, robots) {
	    for (int i = 0; i < cur_node; ++i) {
	    const GridCoords pt = robot.plan[i].coords;
	    grid(pt.y, pt.x) = 1.0; //set the travelled path as freespace
	    }
	    for (int i = cur_node; i < robot.plan.size(); ++i) {
	    const GridCoords pt = robot.plan[i].coords;
	    if (sqrt(pt.squared_distance(robot.gridPose)) < laserGrid and  grid(pt.y, pt.x) <= 0.7 and grid(pt.y, pt.x) >= 0.2) {
	    DEBUG("goal can be eventually covered");
	    GridCoordsVector beam;
	    bresenham(robot.gridPose, pt, beam);
	    bool cover = true;
	    foreach(const GridCoords& p, beam) {
	    if (grid(p.y, p.x) > 0.7) { cover = false; break; }
	    }
	    if (cover) {
	    if (rangerSim->getMap().grid(pt.y, pt.x) == CRangerSimMap::FREESPACE) {
	    grid(pt.y, pt.x) = 1.0;
	    } else {
	    grid(pt.y, pt.x) = 0.0;
	    }

	    }
	    }
	    }
	    }
	    }
	    */
	 bool hasGoals = false;

	 for(int r = 0; r < N; ++r) {
	    SRobot* robot = robots[r];
	    robot->pathSimple.clear();
	 }

	 if (!q) {
	    tm.restart();

	    for(int i = 0; i < N; ++i) { //Prepare for update 
	       setRobotShapeGrid(*robots[i], ROBOT_DISC_CELLS_RADIUS); 
	    }
	    //map->updateNavGrid(map->CELL_SIZE*3, robots); //update navigational grid
	    map->updateNavGrid(ROBOT_DISC_CELLS_RADIUS, robots); //update navigational grid
	    const int nfc = map->getNoOfFreespaceCells();
	    results.expectedExplorationTimeSteps += results.stepTotal * 1.0 * (nfc - prevNFC);
	    prevNFC = nfc;
	    //    results.expectedExplorationTimeSteps += results.stepTotal * 1.0 * map->getNoOfFreespaceCells();
	    //    DEBUG("EET add: stepTotal: " << results.stepTotal << " no of freespace cells: " << map->getNoOfFreespaceCells() << " eet: " << results.expectedExplorationTimeSteps);
	    //   CTimerN tm1;
	    //  tm1.start();
	    // CPerfTimer t_map_update("Timer - map update");
	    paths->updateMap(); //prepare planning grid
	    paths->computeRobotDT(robots); //need for determinig if a frontier is reachable
	    getGoals(robots, frontiers, goalGridPts, robotsGoalGridPts, coveringCells);
	    // tm1.stop();
	    //    INFO("Goals (" << goalGridPts.size() << ") determined in time real: " << tm1.realTime() << " ms cpu: " << tm1.cpuTime() << " ms");

	    //  t_map_update.stop();
	    hasGoals = !goalGridPts.empty();
	    /*
	       bool sameGoals = !goalGridPtsPrev.empty() and goalGridPtsPrev.size() == goalGridPts.size();
	       bool hasPreviousPlan = false;
	       if (sameGoals) { //check if new goals are found
	       goalGridPtsPrev = goalGridPts;
	       BoolVector same(goalGridPts.size(), false);
	       for(int i = 0; i < goalGridPtsPrev.size(); ++i) {
	       for(int j = 0; j < goalGridPts.size(); ++j) {
	       if (goalGridPts[i].x == goalGridPtsPrev[j].x and goalGridPts[i].y == goalGridPtsPrev[j].y) {
	       same[i] = true;
	       }
	       }
	       }
	       for(int i = 0; i < same.size(); i++) {
	       if (!same[i]) { 
	       sameGoals = false;
	       break;
	       }
	       }
	       if (sameGoals) {  //try to use the previous plan
	       for(int r = 0; r < robots.size(); ++r) {
	       SRobot& robot = robots[r];
	       robot.plan.clear();
	    //                    for(int i = cur_node; i < prevPlan[r].size(); ++i) { robot.plan.push_back(prevPlan[r][i]); }
	    hasPreviousPlan |= !robot.plan.empty();
	    }
	    }
	    }
	    */

	    ByteMatrix navGrid(map->getNavGrid().NROWS, map->getNavGrid().NCOLS);
	    GridCoordsVectorVectorVector individualPaths;

	    //if (!hasPreviousPlan and hasGoals) 
	    if (hasGoals) {
	       // CPerfTimer t_paths_update("Timer - paths update");
	       // tm1.restart();
	       DEBUG("Determine paths to goals robots:" << robots.size() << " goalGridPts: " << goalGridPts.size());
	       if (STEP_TIMING) {
		  tstep1 = imr::CTimerN::getUserTime();
	       }
	       paths->set(robots, goalGridPts);
	       if (USE_THETA_STAR) {
		  DEBUG("Use theta star");
		  ASSERT_ARGUMENT(false, "theta star disabled");
		  paths->computeThetaStar(robots);
	       } else {
		  //                    paths->compute(robots);
	       }
	       //   tm1.stop();
	       //  INFO("Paths computed in time real: " << tm1.realTime() << " ms cpu: " << tm1.cpuTime() << " ms");
	       DEBUG("Number of goals " << goalGridPts.size());
	       // t_paths_update.stop();

	       DoubleMatrix costMatrix(N, goalGridPts.size());

	       {
		  for(int r = 0; r < N; ++r) {
		     for(int g = 0; g < goalGridPts.size(); ++g) {
			//                       costMatrix(r, g) = paths->getPathLength(r, g);
			costMatrix(r, g) = paths->getRobotDTPathLength(r, goalGridPts[g]);
			if (STRATEGY == HUNGARIAN_DEPOT) {
			   //add cost from the goal to the depot
			   costMatrix(r, g) += paths->getDTPathLength(goalGridPts[g], robots[r]->depot);
			}
			if (STRATEGY == HUNGARIAN_DEPOT2) {
			   //add robot travelled distance
			   costMatrix(r, g) += robots[r]->travelledGridDistance;
			}
		     }
		  }
	       } 

	       goalsIDX.resize(N, -1);
	       //       tm1.restart();

	       // CPerfTimer t_ta("Timer - task allocation");
	       switch(STRATEGY) {
		  case GREEDY_ALLOC_RAND:
		     greedyAllocRand->assign(costMatrix, goalsIDX);
		     break;
		  case PAIRS:
		     pairs->assign(costMatrix, goalsIDX);
		     DEBUG("PAIRS done");
		     for(int r = 0; r < N; ++r) {
			DEBUG("Robot: " << r << " goal: " << goalsIDX[r]);
		     }
		     break;
		  case HUNGARIAN:
		  case HUNGARIAN_DEPOT:
		  case HUNGARIAN_DEPOT2:
		     if (costMatrix.NCOLS < costMatrix.NROWS) { //less number of goals than robots:
			pairs->assign(costMatrix, goalsIDX); //use pairs
		     } else {
			hungarian->assign(costMatrix, goalsIDX);
		     }
		     break;
		  case MTSP_KMEANS:
		  case MTSP_KMEANS_DEPOT:
		     DEBUG("NROWS: " << costMatrix.NROWS << " NCOLS: " << costMatrix.NCOLS);
		     if (costMatrix.NCOLS < costMatrix.NROWS) { //less number of goals than robots:
			DEBUG("MTSP_KMEANS - call pairs");
			pairs->assign(costMatrix, goalsIDX); //use pairs
		     } else if (costMatrix.NROWS == costMatrix.NCOLS) { //same number of robots as goals
			DEBUG("MTSP_KMEANS* - call hungarian");
			hungarian->assign(costMatrix, goalsIDX); //consider hungarian
		     } else {
			if (STRATEGY == MTSP_KMEANS or STRATEGY == MTSP_KMEANS_DEPOT) {
			   mtspKmeans->assign(costMatrix, goalGridPts, robots, goalsIDX);
			} else {
			   ASSERT_ARGUMENT(false, "MTSP_KMEANS or MTSP_KMEANS_DEPOT or MTSP_KMEANS_SGA strategy is expected");
			}
		     }
		     DEBUG("MTSP* - assign done");
		     break;
		  case MIN_POS:
		     minPos->assign(goalGridPts, robots, goalsIDX);
		     break;
		  default:
		     ASSERT_ARGUMENT(false, "Unsupported strategy");
		     break;
	       } //end switch

	       //       t_ta.stop();
	       ASSERT_ARGUMENT(goalsIDX.size() == N, "All goals must be filled");
	       if (ENABLE_PATH_LENGTH_CHECK) {
		  //consider greedy only if the the robot traveled distance is shorter than the current longest path 
		  double max_length = 0.0;
		  for (int r = 0; r < N; ++r) {
		     if (max_length < results.travelledDistances[r]) { max_length = results.travelledDistances[r]; } 
		  }
		  max_length -= PATH_LENGTH_CHECK_OFFSET;
		  for (int r = 0; r < N; ++r) {
		     if (goalsIDX[r] == -1 and results.travelledDistances[r] < max_length) {
			DEBUG("Robot " << r << " does not have a valid goal");
			double min = std::numeric_limits<double>::max();
			for(int g = 0; g < costMatrix.NCOLS; ++g) {
			   if (min > costMatrix(r, g)) {
			      min = costMatrix(r, g);
			      goalsIDX[r] = g;
			   }
			}
		     }
		  }
	       } else { //consider greedy
		  for(int r = 0; r < N; ++r) {
		     if (goalsIDX[r] == -1) { 
			DEBUG("Robot " << r << " does not have a valid goal");
			if (FORCE_ASSIGN_SAME_GOALS) {
			   double min = std::numeric_limits<double>::max();
			   for(int g = 0; g < costMatrix.NCOLS; ++g) {
			      if (min > costMatrix(r, g)) {
				 min = costMatrix(r, g);
				 goalsIDX[r] = g;
			      }
			   }
			} else if (RETURN_DEPOT) { 
			   SRobot* robot = robots[r];
			   if (not robot->docked and robot->gridPose != robot->depot) {
			      paths->getRobotDTPath(r, robot->depot, robot->pathSimple);
			      paths->getPathPlan(robot->pose, robot->pathSimple, robot->plan);
			   }
			} //end return robot to the depot
			//it may happend that the environment is not connected; thus, a robot can be isolated, and therefore, it can already explored the whole component 
		     }
		  }
	       }
	       for(int r = 0; r < N; ++r) {
		  //if (goalsIDX[r] != -1 and goalsIDX[r] >= 0 and goalsIDX[r] < goalGridPts.size()) 
		  if (goalsIDX[r] != -1 and goalsIDX[r] >= 0) {
		     SRobot& robot = *(robots[r]);
		     paths->getRobotDTPath(r, goalGridPts[goalsIDX[r]], robot.pathSimple);
		     //filter robot plans
		     if (1 and !robot.pathSimple.empty()) {
			GridCoordsVector& ps = robot.pathSimple;
			GridCoordsVector psf;
			psf.push_back(ps.front());
			for(int i = 1; i < ps.size(); ++i) {
			   if (psf.back().manhattan_distance(ps[i]) > 0) {
			      psf.push_back(ps[i]);
			   }
			}
			robot.pathSimple = psf;
		     }
		     paths->getPathPlan(robot.pose, robot.pathSimple, robot.plan);
		     if (not robot.pathSimple.empty()) {
			robot.docked = false;
		     }
		  }
	       }
	       tm.stop();
	       INFO("Determine new plans time real: " << tm.realTime() << " ms cpu: " << tm.cpuTime() << " ms");

	       if (MAX_NODES_EXECUTION < 0) {
		  for(int r = 0; r < N; ++r) {
		     SRobot* robot = robots[r];
		     if (robot->plan.size() > 3) {
			robot->plan.pop_back();
			robot->plan.pop_back();
		     }
		  }
	       }
	       if (STEP_TIMING) {
		  tstep2 = imr::CTimerN::getUserTime();
		  time_assign_count++;
		  const double dt = tstep2 - tstep1;
		  time_assign_time += dt;
		  if (dt > max_assign_time) {
		     max_assign_time = dt;
		  }
	       }
	    } else {
	       INFO("Use the previous plan");
	    }

	    /*
	       prevGoalsGridIDX.clear();
	       for(int r = 0; r < N; ++r) {
	       int idx = -1;
	       if (!robots[r].pathSimple.empty()) {
	       const GridCoords& pt = robots[r].pathSimple.back();
	       idx = pt.y * navGrid.NCOLS + pt.x;
	       }
	       prevGoalsGridIDX.push_back(idx);
	       } */
	 } //end if(!q)
	 // t_gd_ta.stop();

	 if (sdl() and gui) {
	    tm.restart();
	    //sdl.drawGrid255(map->getGrid(), sdl.getScreenSurface());
	    if (DRAW_ENLARGE_OBSTACLES) {
	       FloatMatrix drawGrid(map->getGrid());
	       const ByteMatrix& ng = map->getNavGrid();
	       for(int i = 0; i < ng.SIZE; ++i) { if (ng(i) == map->OCCUPIED_GROWN_CELL) { drawGrid(i) = 1.0; } }
	       gui->drawGridLight(drawGrid); 
	    } else {
	       gui->drawGridLight(map->getGrid());
	    }
	    foreach(const SFrontier& f, frontiers) {
	       if (GUI_DRAW_RED_FRONTIERS) {
		  gui->drawFrontierRed(f.coords.y, f.coords.x);
	       } else {
		  gui->drawFrontier(f.coords.y, f.coords.x);
	       }
	    }

	    if (GUI_DRAW_GOALS) {
	       if (GUI_DRAW_ALL_GOALS) {
		  DEBUG("allGoals: " << allGoals.size());
		  for(int i = 0; i < goalsIDX.size(); ++i) {
		     if (goalsIDX[i] != -1) {
			allGoals[i].push_back(goalGridPts[goalsIDX[i]]);
		     }
		  }
		  gui->resetColor();
		  gui->nextColor();
		  foreach(const GridCoordsVector& goals, allGoals) {
		     const SRGB& color = gui->nextColor();
		     foreach(const GridCoords& pt, goals) {
			gui->drawGoal(pt.y, pt.x, color, 250);
		     }
		  }
	       } else {
		  foreach(const GridCoords& pt, goalGridPts) {
		     gui->drawGoal(pt.y, pt.x);
		  }
	       }
	    }

	    if (0) 
	    {
	       for(int r = 0; r < robots.size(); ++r) {
		  for(int g = 0; g < goalGridPts.size(); ++g) {
		     IntVector path;
		     gui->draw(paths->getPath(r, g, path), SRGB(0, 144, 255));
		  }
	       }
	    }

	    gui->resetColor();
	    gui->nextColor();
	    for(int i = 0; i < results.travelledPaths.size(); ++i) {
	       gui->draw(results.travelledPaths[i], gui->nextColor());
	    }
	    foreach(SRobot* robot, robots) {
	       gui->drawRobot(*robot, not OMNIDIRECTIONAL_MOTION); 
	       gui->drawPath(robot->pathSimple, SRGB(0, 255, 255));
	    }
	    sdl.refresh();
	    tm.stop();
	    INFO("Gui time real: " << tm.realTime() << " ms cpu: " << tm.cpuTime() << " ms");
	    if (results.SAVE_PIC) {
	       savePic(results.step);
	    }
	 }
	 if (results.SAVE_STATE) {
	    std::stringstream ss2;
	    ss2 << results.stateDir << std::setw(6) << std::setfill('0') << results.step << ".h5";
	    saveState(ss2.str());
	 }
	 if (!hasGoals) {
	    bool allindepots = true;
	    if (RETURN_DEPOT) {
	       for(int r = 0; r < robots.size(); ++r) {
		  SRobot* robot = robots[r];
		  if (not robot->docked) {
		     if (robot->gridPose == robot->depot) {
			robot->docked = true;
			robot->approaching_depot = false;
		     } else { //plan path to the depot
			allindepots = false;
			robot->approaching_depot = true;
			paths->getRobotDTPath(r, robot->depot, robot->pathSimple);
			paths->getPathPlan(robot->pose, robot->pathSimple, robot->plan);
		     }
		  }
	       } // end all robots
	    } //end check all robots in the depot 
	    if (allindepots) {
	       INFO("All goals (frontiers) has been visited - terminated exporation");
	       INFO("Total number of steps: " << results.stepTotal);
	       for(int r = 0; r < robots.size(); ++r) {
		  INFO("Travelled distance robot[" << r << "]: " << results.travelledDistances[r]);
	       }
	       q = true;
	       if (STEP_TIMING) {
		  INFO("No. of steps: " << time_assign_count << " avg time: " << (time_assign_time*1000 / time_assign_count) << " ms --- max time: " << max_assign_time * 1000 << " ms");
	       }
	    }
	 }
	 if (results.stepTotal > MAX_TOTAL_EXECUTION_STEPS) {
	    q = true;
	    forceTermination = true;
	    WARN("Force terminated stepTotal reached max total execution steps " << results.stepTotal << "/" << MAX_TOTAL_EXECUTION_STEPS);
	 }
      } //end while

      if (results.COVERAGE_PROGRESS and !results.coverage.empty()) {  //recompute coverage
	 double max = results.coverage.back().coverage;
	 for(int i = 0; i < results.coverage.size(); ++i) {
	    results.coverage[i].coverage = results.coverage[i].coverage * 100.0 / max;
	 }
      }
      DEBUG("delete basic strategies");


      results.tm.stop();
      if (!forceTermination) {
	 DEBUG("get expectedExplorationTimeSteps");
	 // DEBUG("EET fin before : eet: " << results.expectedExplorationTimeSteps << "  no. of freespace cells: " << map->getNoOfFreespaceCells());
	 results.expectedExplorationTimeSteps = results.expectedExplorationTimeSteps / (1.0 * map->getNoOfFreespaceCells());
	 // DEBUG("EET fin after: " << results.expectedExplorationTimeSteps);

	 results.maxGridDistance = 0.0;
	 for(int r = 0; r < robots.size(); ++r) {
	    double d1 = robots[r]->travelledGridDistance;
	    const IntVector& idxs = results.travelledPaths[r];
	    GridCoordsVector path;
	    foreach(int i, idxs) { path.push_back(GridCoords(i%map->W, i/map->W)); }
	    double d2 = getPathLength(path);
	    DEBUG("Robot [" << r << "] d1: " << d1 << " d2: " << d2);
	    if (d1 > results.maxGridDistance) {
	       results.maxGridDistance = d1;
	    }
	 }


	 DEBUG("get fill results");
	 results.fillResults();
	 DEBUG("get save results");
	 results.save();
	 DEBUG("save final");
	 if (results.SAVE_RESULTS and gui) {
	    sdl.save_img_png(IMAGE_QUALITY, results.getFinalPicFilename());
	 }
	 DEBUG("done");
      }
      delete greedyAllocRand;
      delete pairs;
      delete hungarian;
      DEBUG("delete sophisticated strategies");
      if (mtspKmeans) { delete mtspKmeans; mtspKmeans = 0; } 
      if (minPos) { delete minPos; minPos = 0; }
      results.tm.stop();
      for(int r = 0; r < N; ++r) {
	 const IntVector& idxs = results.travelledPaths[r];
	 GridCoordsVector path;
	 foreach(int i, idxs) { path.push_back(GridCoords(i%map->W, i/map->W)); }
	 INFO("Path Length in pixels robot[" << r << "]: " << getPathLength(path));
      }
   } catch (imr::exception& e) {
      ERROR("CMRESim: " << e.what());
   }
}

/// - private method -----------------------------------------------------------
void CMRESim::updateMap(RobotPtrVector& robots, double robotRadius, double laserRange) {
   // CTimerN tm;
   // tm.start();
   const int N = robots.size();
   IntVectorVector freespaceCells(N);
   IntVectorVector obstaclesCells(N);
   FloatVectorVector obstaclesCellsValues(N);

   //  CPerfTimer t("Update map ");

   for(int i = 0; i < N; ++i) { //Prepare for update 
      IntVector vx, vy;
      SRobot& robot = *(robots[i]);
      robot.shape.clear();
      robot.shape.push_back(robot.gridPose.x + robot.gridPose.y * map->W);
      // setRobotShape(robot, robotRadius);  map->getCellsValues(robot.shape, robotsCellsValues[i]); //TODO consider robot shape
      if (OMNIDIRECTIONAL) {
	 rangerSim->getScanOmni(robot, laserRange, vx, vy, obstaclesCells[i]);
      } else {
	 rangerSim->getScan(robot, laserRange, vx, vy, obstaclesCells[i]);
      }
      //     rangerSim->getScanReduced(robot, laserRange, vx, vy, obstaclesCells[i]);
      vx.insert(vx.begin(), robot.gridPose.x); vy.insert(vy.begin(), robot.gridPose.y);
      fillPolygon(vx, vy, map->W, freespaceCells[i]);
      map->getCellsValues(obstaclesCells[i], obstaclesCellsValues[i]);
   }

   FloatMatrix& grid = map->grid;
   for(int i = 0; i < N; ++i) { // update freespace
      foreach(int idx, freespaceCells[i]) { if (grid(idx) < 0.6) { grid(idx) = 0.0; } }
   }
   for(int i = 0; i < N; ++i) { // restore obstacles
      map->setCellsValues(obstaclesCells[i], obstaclesCellsValues[i]);
   }
   for(int i = 0; i < N; ++i) { // update obstacles
      foreach(int idx, obstaclesCells[i]) { grid(idx) = 1.0; }
   }
   //  for(int i = 0; i < N; ++i) { //restore robot positions
   //     map->setCellsValues(robots[i].shape, robotsCellsValues[i]);
   //  }
   for(int i = 0; i < N; ++i) { //set robots as freespace
      grid(robots[i]->gridPose.y, robots[i]->gridPose.x) = 0.0;
   }
   // tm.stop();
   // INFO("Update map real: " << tm.realTime() << " ms cpu: " << tm.cpuTime() << " ms");
}

/// - private method -----------------------------------------------------------
GridCoordsVectorVector& CMRESim::getGoalsSets(const FrontierVector& frontiers, int nbrObjects, GridCoordsVectorVector& goalSets) {
   goalSets.clear();
   GridCoordsVectorVector goalSetsPrev;
   int nbrGoals = 0;
   IntVector sets(nbrObjects, -1);
   int c = 0;
   foreach(const SFrontier& f, frontiers) {
      if (sets[f.object] == -1) { sets[f.object] = c++; }
   }
   if (!frontiers.empty()) {
      goalSetsPrev.resize(c, GridCoordsVector());
      foreach(const SFrontier& f, frontiers) { 
	 goalSetsPrev[sets[f.object]].push_back(f.coords); 
	 nbrGoals++;
      }
   }
   if (MIN_NUMBER_OF_FRONTIER_CELLS > 0) {
      foreach(const GridCoordsVector& set, goalSetsPrev) {
	 if (set.size() >= MIN_NUMBER_OF_FRONTIER_CELLS) {
	    goalSets.push_back(set);
	 }
      }
   } else {
      goalSets = goalSetsPrev;
   }
   return goalSets;
}

/// - private method -----------------------------------------------------------
GridCoordsVector& CMRESim::getGoals(const RobotPtrVector& robots, FrontierVector& frontiers, GridCoordsVector& goalGridPts, GridCoordsVectorVector& robotsGoalGridPts, IntVector& coveringCosts) {
   FrontierVector allFrontiers;
   frontiers.clear();
   goalGridPts.clear();
   GridCoordsVector robotsPoses;
   foreach(const SRobot* robot, robots) { robotsPoses.push_back(robot->gridPose); }
   int nbrObjects = map->findFrontiers(robotsPoses, allFrontiers);
   paths->getReachableFrontiers(allFrontiers, frontiers);

   switch(GOALS_SELECTION) {
      case ALL_FRONTIERS:
	 DEBUG("ALL_FRONTIERS");
	 foreach(SFrontier& f, frontiers)  { goalGridPts.push_back(f.coords); }
	 break;
      case KMEANS_ICRA11:
	 {
	    DEBUG("KMEANS_ICRA11 frontiers.size: " << frontiers.size());
	    FrontierVector goals;
	    if ( !frontiers.empty() ) { //find frontiers representatives
	       FrontierVector repre;
	       DEBUG("frontiers: " << frontiers.size() << " mfc: " << MIN_NUMBER_OF_FRONTIER_CELLS);
	       map->findAllGoals(frontiers, nbrObjects, MIN_NUMBER_OF_FRONTIER_CELLS, repre);
	       foreach(const SFrontier& r, repre) { if ( r.coords.x != -1 ) { goals.push_back(r); } }
	    }
	    foreach(SFrontier& f, goals)  { goalGridPts.push_back(f.coords); }
	    /*
	       GridCoordsVectorVector goalSets;
	       getGoalsSets(frontiers, nbrObjects, goalSets);
	       gui->drawGrid(paths->getMap().getNavGrid());
	       foreach(const SFrontier& f, frontiers) {
	       gui->drawFrontier(f.coords.y, f.coords.x);
	       }
	       gui->refresh();
	       sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, "frontiers.png");
	       gui->resetColor();
	       gui->nextColor();
	       gui->nextColor();
	       foreach(GridCoordsVector& set, goalSets) {
	       const SRGB& color = gui->nextColor();
	       foreach(const GridCoords& pt, set) {
	       gui->drawDisk(pt, 2, color);
	       }
	       }
	       gui->refresh();
	       sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, "free_edges.png");
	       gui->drawDisk(goalGridPts, 4, SRGB(255, 0, 0));
	       gui->refresh();
	       sdl.save_img_png(sdl.getScreenSurface(), IMAGE_QUALITY, "goal_candidates.png");
	       getchar();
	       */
	 }
	 break;
      case KMEANS_ARN:
	 {
	    DEBUG("KMEANS_ARN frontiers.size: " << frontiers.size());
	    FrontierVector goals;
	    if ( !frontiers.empty() ) { //find frontiers representatives
	       FrontierVector repre;
	       DEBUG("frontiers: " << frontiers.size() << " mfc: " << MIN_NUMBER_OF_FRONTIER_CELLS);
	       map->findAllGoalsARN(frontiers, nbrObjects, MIN_NUMBER_OF_FRONTIER_CELLS, robots.size(), repre);
	       foreach(const SFrontier& r, repre) { if ( r.coords.x != -1 ) { goals.push_back(r); } }
	    }
	    foreach(SFrontier& f, goals)  { goalGridPts.push_back(f.coords); }
	 }
	 break;
      case REPRESENTATIVE:
      case GOAL_SET_COVERAGE:
      case COMPLETE_COVERAGE:
      case COVERING_CELLS:
      case COVERING_CELLS_SAMPLES:
      case COMPLETE_COVERAGE_FOV:
      case COMPLETE_COVERAGE_2:
	 {
	    GridCoordsVectorVector goalSets;
	    getGoalsSets(frontiers, nbrObjects, goalSets);
	    CGoals* g = new CGoals(paths->getMap(), goalSets, robots, 
		  cfg.get<double>("goals-range-ratio"),
		  GOALS_SELECTION == REPRESENTATIVE or GOALS_SELECTION == COMPLETE_COVERAGE_FOV or GOALS_SELECTION == COMPLETE_COVERAGE_2 ? false : true,
		  gui);
	    if (GOALS_SELECTION == GOAL_SET_COVERAGE) {
	       DEBUG("GOAL_SET_COVERAGE");
	       g->getGoalsGoalSetCoverage(goalGridPts);
	    } else if (GOALS_SELECTION == COMPLETE_COVERAGE) {
	       DEBUG("COMPLETE_COVERAGE");
	       IntVector coverage; // not currently used
	       g->getGoalsCompleteCoverage(goalGridPts, coverage);
	    }  else if (GOALS_SELECTION == COMPLETE_COVERAGE_2) {
	       g->getGoalsCompleteCoverage2(goalGridPts);
	       //g->getGoalsCompleteCoverageCumulativeCoverage(goalGridPts);
	    }  else if (GOALS_SELECTION == COMPLETE_COVERAGE_FOV) {
	       // g->getGoalsCompleteCoverageFOV(goalGridPts);
	       // g->getGoalsCompleteCoverageFOV2(goalGridPts);
	       g->getGoalsCompleteCoverageFOV3(goalGridPts);
	    } else if (GOALS_SELECTION == REPRESENTATIVE) {
	       DEBUG("REPRESENTATIVE");
	       g->getGoalsRepresentatives(goalGridPts);
	    } else if (GOALS_SELECTION == COVERING_CELLS) {
	       DEBUG("COVERING_CELLS");
	       g->getCoveringCells(goalGridPts, coveringCosts);
	    } else if (GOALS_SELECTION == COVERING_CELLS_SAMPLES) {
	       DEBUG("COVERING_CELLS_SAMPLES");
	       g->getCoveringCellsRandom(cfg.get<double>("covering-cells-samples-sigma"), goalGridPts, coveringCosts);
	    }
	    delete g; 
	 }
	 break;
   } //end switch
   DEBUG("All frontiers: " << allFrontiers.size() << " frontiers: " << frontiers.size() << " objects: " << nbrObjects << " goals: " << goalGridPts.size());
   return goalGridPts;
}

/// - private method -----------------------------------------------------------
void CMRESim::setRobotShape(SRobot& robot, double r) const {
   robot.shape.clear();
   const int neigh = (ceil(r / map->CELL_SIZE)); //2
   const int neigh2 = neigh*neigh;
   for (int i = -neigh; i < neigh + 1; i++) { //enlarge grid
      for (int j = -neigh; j < neigh + 1; j++) {
	 if (i*i + j*j <= neigh2) {
	    const int x = robot.gridPose.x + j;
	    const int y = robot.gridPose.y + i;
	    if (x >= 0 and x < map->W and y >= 0 and y < map->H) {
	       robot.shape.push_back(x + y * map->W);
	    }
	 }
      }
   } //end neigh
   if (robot.shape.empty()) {
      robot.shape.push_back(robot.gridPose.x + robot.gridPose.y * map->W);
   }
   ASSERT_ARGUMENT(!robot.shape.empty(), "Robot shape is empty");
}

/// - private method -----------------------------------------------------------
void CMRESim::setRobotShapeGrid(SRobot& robot, int c) const {
   robot.shape.clear();
   const int neigh = c;
   const int neigh2 = neigh*neigh;
   for (int i = -neigh; i < neigh + 1; i++) { //enlarge grid
      for (int j = -neigh; j < neigh + 1; j++) {
	 if (i*i + j*j <= neigh2) {
	    const int x = robot.gridPose.x + j;
	    const int y = robot.gridPose.y + i;
	    if (x >= 0 and x < map->W and y >= 0 and y < map->H) {
	       robot.shape.push_back(x + y * map->W);
	    }
	 }
      }
   } //end neigh
   if (robot.shape.empty()) {
      robot.shape.push_back(robot.gridPose.x + robot.gridPose.y * map->W);
   }
   ASSERT_ARGUMENT(!robot.shape.empty(), "Robot shape is empty");
}

/// - private method -----------------------------------------------------------
void CMRESim::getFrontiersContour(const RobotPtrVector& robots, const FrontierVector& frontiers, GridCoordsVector& goalGridPts) {
   GridCoordsVector reachableFrontiers;
   foreach(const SFrontier& fr, frontiers) { reachableFrontiers.push_back(fr.coords); }

   const imr::ByteMatrix& navGrid = map->getNavGrid();
   ByteMatrix grid(navGrid.NROWS, navGrid.NCOLS);
   ASSERT_ARGUMENT(grid.SIZE == navGrid.SIZE, "Grids must have some size");
   for(int i = 0; i < navGrid.SIZE; ++i) {
      grid(i) = (navGrid(i) == map->FREESPACE_CELL ? CDistanceTransform::FREESPACE : CDistanceTransform::OBSTACLE);
   }
   const int D = 12; //
   GridCoordsVector disc;
   for (int i = -D; i < D + 1; i++) { //enlarge grid
      for (int j = -D; j < D + 1; j++) {
	 if (i*i + j*j <= D*D) {
	    disc.push_back(GridCoords(i, j));
	 }
      }
   } //end disc
   //Enlarge frontiers
   ByteMatrix fr(navGrid.NROWS, navGrid.NCOLS);
   for(int i = 0; i < fr.SIZE; ++i) { fr[i] = 0; }

   for(int i = 0; i < reachableFrontiers.size(); ++i) {
      fr(reachableFrontiers[i].y, reachableFrontiers[i].x) = 1;
      for(int j = 0; j < disc.size(); ++j) {
	 const int x = reachableFrontiers[i].x + disc[j].x;
	 const int y = reachableFrontiers[i].y + disc[j].y;
	 if (x >= 0 and x < navGrid.NCOLS-1 and y >= 0 and y < navGrid.NROWS-1 and navGrid(y, x) == map->FREESPACE_CELL) {
	    fr(y, x) = 1;
	 }
      }
   }

   const int D2 = 12; //
   GridCoordsVector disc2;
   for (int i = -D2; i < D2 + 1; i++) { //enlarge grid
      for (int j = -D2; j < D2 + 1; j++) {
	 if (i*i + j*j <= D2*D2) {
	    disc2.push_back(GridCoords(i, j));
	 }
      }
   } //end disc

   //enlarge obstacles
   for(int r = 0; r < navGrid.NROWS; ++r) {
      for(int c = 0; c < navGrid.NCOLS; ++c) {
	 if (navGrid(r, c) != map->FREESPACE_CELL) {
	    for(int j = 0; j < disc2.size(); ++j) {
	       const int x = c + disc2[j].x;
	       const int y = r + disc2[j].y;
	       if (x >= 0 and x < navGrid.NCOLS and y >= 0 and y < navGrid.NROWS) {
		  fr(y, x) = 0;
	       }
	    }
	 }
      }
   } //end enlarge obstacles

   goalGridPts.clear();
   for(int r = 0; r < navGrid.NROWS; ++r) {
      for(int c = 0; c < navGrid.NCOLS; ++c) {
	 if (fr(r, c) == 1) { 
	    goalGridPts.push_back(GridCoords(c, r));
	    //     grid(r, c) = CDistanceTransform::FREESPACE;
	 }
      }
   }
   if (0 and gui) {
      DEBUG("getFrontiersContour");
      gui->drawGridLight(map->getGrid());

      gui->draw(goalGridPts, SRGB(197, 40, 40));

      foreach(const SFrontier& f, frontiers) {
	 gui->drawFrontier(f.coords.y, f.coords.x);
      }
      foreach(const SRobot* robot, robots) {
	 gui->drawRobot(*robot); 
	 gui->drawPath(robot->pathSimple, SRGB(0, 255, 255));
      }
      DEBUG("goalGridPts:" << goalGridPts.size()); gui->refresh();
      getchar();
   }
}

/// - private method -----------------------------------------------------------
void CMRESim::savePic(int iter) {
   if (gui) {
      std::stringstream ss;
      ss << results.picDir << std::setw(6)  << std::setfill('0') << iter << "." << results.IMAGE_EXT;
      std::string s = ss.str();
      DEBUG("Save visualization to file " << s);
      if (results.IMAGE_EXT == "png") {
	 sdl.save_img_png(IMAGE_QUALITY, s);
      } else {
	 sdl.save_img_jpeg(sdl.getScreenSurface(), IMAGE_QUALITY, s);
      }
   }
}

/// - private method -----------------------------------------------------------
void CMRESim::saveState(const std::string& filename) {
   IntMatrix posesI(robots.size(), 2);
   FloatMatrix posesF(robots.size(), 4);
   IntMatrix pathsLen(robots.size(), 1);
   FloatMatrix distances(robots.size(), 1);
   IntMatrix plansLen(robots.size(), 1);

   int maxPathLen = -1;
   int maxPlanLen = -1;

   for(int r = 0; r < robots.size(); ++r) {
      const SRobot& robot = *(robots[r]);
      posesI(r, 0) = robot.gridPose.x;
      posesI(r, 1) = robot.gridPose.y;
      posesF(r, 0) = robot.pose.x;
      posesF(r, 1) = robot.pose.y;
      posesF(r, 2) = robot.pose.yaw;
      posesF(r, 3) = robot.pose.time;
      pathsLen(r, 0) = results.travelledPaths[r].size();
      distances(r, 0) = results.travelledDistances[r];
      plansLen(r, 0) = robot.plan.size() < MAX_NODES_EXECUTION ? robot.plan.size() : MAX_NODES_EXECUTION;
      if (pathsLen(r, 0) > maxPathLen) { maxPathLen = pathsLen(r, 0); }
      if (plansLen(r, 0) > maxPlanLen) { maxPlanLen = plansLen(r, 0); }
   }
   IntMatrix paths(robots.size(), maxPathLen);
   IntMatrix planCoords(robots.size() * 2, maxPlanLen); //x, y
   FloatMatrix planPose(robots.size() * 3, maxPlanLen); //x, y, yaw
   for(int r = 0; r < robots.size(); ++r) {
      for(int i = 0; i < results.travelledPaths[r].size(); ++i) {
	 paths(r, i) = results.travelledPaths[r][i];
      }
      for(int i = 0; i < plansLen(r, 0); ++i) {
	 planCoords(2*r + 0, i) = robots[r]->plan[i].coords.x;
	 planCoords(2*r + 1, i) = robots[r]->plan[i].coords.y;
	 planPose(3*r + 0, i) = robots[r]->plan[i].pose.x;
	 planPose(3*r + 1, i) = robots[r]->plan[i].pose.y;
	 planPose(3*r + 2, i) = robots[r]->plan[i].pose.yaw;
      }
   }

   CHDFLoader hdf;
   hdf 
      << loader::FILENAME << filename << loader::OPENWRITETRUNC
      << loader::ATTRIBUTE << "name" << "mre_sim"
      << loader::ATTRIBUTE << "num_robots" << int(robots.size())
      << loader::ATTRIBUTE << "stepTotal" << results.stepTotal
      << loader::ATTRIBUTE << "step" << results.step
      << loader::DATASET << "gridPoses" << posesI
      << loader::DATASET << "poses" << posesF
      << loader::DATASET << "map" << map->grid
      << loader::DATASET << "travelledDistances" << distances
      << loader::DATASET << "travelledPathsLens" << pathsLen
      << loader::DATASET << "travelledPaths" << paths
      << loader::DATASET << "planLens" << plansLen
      << loader::DATASET << "planCoords" << planCoords
      << loader::DATASET << "planPose" << planPose
      << loader::CLOSE;
   DEBUG("saveState to : " << filename);
}

/// - private method -----------------------------------------------------------
void CMRESim::loadState(const std::string& filename) {
   loader::IMatrixDataset posesI;
   loader::FMatrixDataset posesF;
   loader::FMatrixDataset mapData(&map->grid);
   loader::FMatrixDataset distances;
   loader::IMatrixDataset paths;
   loader::FMatrixDataset pathsLen;
   loader::IMatrixDataset plansLen;
   loader::IMatrixDataset planCoords;
   loader::FMatrixDataset planPose;
   int num_robots = -1;

   CHDFLoader hdf;
   hdf 
      << loader::FILENAME << filename << loader::OPENREADONLY
      << loader::ATTRIBUTE << "num_robots" >> num_robots
      << loader::ATTRIBUTE << "stepTotal" >> results.stepTotal
      << loader::ATTRIBUTE << "step" >> results.step
      << loader::DATASET << "gridPoses" >> posesI
      << loader::DATASET << "poses" >> posesF
      << loader::DATASET << "travelledDistances" >> distances
      << loader::DATASET << "travelledPathsLens" >> pathsLen
      << loader::DATASET << "travelledPaths" >> paths
      << loader::DATASET << "planLens" >> plansLen
      << loader::DATASET << "planCoords" >> planCoords
      << loader::DATASET << "planPose" >> planPose
      << loader::DATASET << "map" >> mapData
      << loader::CLOSE;

   ASSERT_ARGUMENT(num_robots == robots.size(), "Wrong number of the robots, loaded number is " + imr::string_format<int>(num_robots));
   ASSERT_ARGUMENT(posesI()->NROWS == robots.size() and posesI()->NCOLS == 2, "Wrong dimensions of the gridPoses");
   ASSERT_ARGUMENT(posesF()->NROWS == robots.size() and posesF()->NCOLS == 4, "Wrong dimensions of the poses");
   results.travelledDistances.resize(robots.size());
   results.travelledPaths.resize(robots.size(), IntVector());
   for(int r = 0; r < robots.size(); ++r) {
      SRobot& robot = *(robots[r]);
      robot.gridPose.x = posesI()->at(r, 0);
      robot.gridPose.y = posesI()->at(r, 1);
      robot.pose.x = posesF()->at(r, 0);
      robot.pose.y = posesF()->at(r, 1);
      robot.pose.yaw = posesF()->at(r, 2);
      robot.pose.time = posesF()->at(r, 3);
      results.travelledDistances[r] = distances()->at(r, 0);
      const int pathLen = pathsLen()->at(r, 0);
      results.travelledPaths[r].resize(pathLen, 0);
      for(int i = 0; i < pathLen; ++i) {
	 results.travelledPaths[r][i] = paths()->at(r, i);
      }
      robot.plan.resize(plansLen()->at(r, 0), SPathNode());
      for(int i = 0; i < robot.plan.size(); ++i) {
	 robot.plan[i].coords.x = planCoords()->at(r * 2 + 0, i);
	 robot.plan[i].coords.y = planCoords()->at(r * 2 + 1, i);
	 robot.plan[i].pose.x = planPose()->at(r * 3 + 0, i);
	 robot.plan[i].pose.y = planPose()->at(r * 3 + 1, i);
	 robot.plan[i].pose.yaw = planPose()->at(r * 3 + 2, i);
      }
   }
   delete posesI();
   delete posesF();
   delete distances();
   delete pathsLen();
   delete paths();
   delete plansLen();
   delete planCoords();
   delete planPose();
}

/// - private method -----------------------------------------------------------

/* end of mre.cc */
