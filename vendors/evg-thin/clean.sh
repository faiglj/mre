#!/bin/sh

if [ `uname` = FreeBSD ]
then
   mcmd=gmake
else
   mcmd=make
fi

$mcmd -C src clean

LIBRARY_PREFIX=lib
INCLUDE_PREFIX=include

rm -rf  libevg-thin.a $INCLUDE_PREFIX
