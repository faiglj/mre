/*
 * File name: results_saver.h
 * Date:      2012/02/18 10:49
 * Author:    Jan Faigl
 */

#ifndef __RESULTS_SAVER_H__
#define __RESULTS_SAVER_H__

#include <string>
#include <vector>

#include <imr_exceptions.h>
#include <imr_config.h>
#include <result_log.h>
#include <timerN.h>

#include "mre_types.h"

namespace mre {

   struct SCoverageStep {
      int step;
      double travelled_distance;
      double coverage;
      SCoverageStep(int s, double t, double c) : step(s), travelled_distance(t), coverage(c) {
      }

      SCoverageStep& operator=(const SCoverageStep& a) {
	 if (this != &a) {
	    step = a.step;
	    travelled_distance = a.travelled_distance;
	    coverage = a.coverage;
	 }
	 return *this;
      }
   };

   typedef std::vector<SCoverageStep> CoverageVector;

   struct SResults {
      imr::CConfig& cfg;

      const bool SAVE_PIC;
      const bool SAVE_RESULTS;
      const bool SAVE_STATE;
      const std::string IMAGE_EXT;
      const int ITER;
      const bool COVERAGE_PROGRESS;
      const bool ROBOT_TRAJECTORY;

      imr::CResultLog resultLog;
      std::string output;
      std::string picDir;
      std::string stateDir;

      bool saveResults;
      bool verboseLog;

      imr::CTimerN tm;

      int stepTotal;
      int step;
      IntVectorVector travelledPaths;
      DoubleVector travelledDistances;
      double expectedExplorationTimeSteps;
      double maxGridDistance;
      CoverageVector coverage;
      PositionVector trajectory;

      static imr::CConfig& getConfig(imr::CConfig& config);

      SResults(imr::CConfig& config);
      ~SResults();

      std::string getVersion(void) { return "IMR MRE - Multi-Robot Exploration - 0.7"; }
      std::string getRevision(void);
      std::string getFinalPicFilename(void);

      void init(int numRobots, double laserRange);
      void fillResults(void);
      void defineResultLog(const std::string& method, int w_grid, int h_grid, double w_map, double h_map, double cell_size);
      void save(void);
      void appendToLog(void);
      void saveInfo(const std::string& file) throw(imr::io_error);
      void saveSettings(const std::string& file) throw(imr::io_error);

      std::string getOutputPath(const std::string filename, std::string& dir);
      std::string getOutputIterPath(const std::string filename, std::string& dir);

      void saveCoverage(const std::string& filename) throw(imr::io_error);
      void saveTrajectory(const std::string& filename) throw(imr::io_error);

      private:

   };

} //end namespace mre

#endif

/* end of results_saver.h */
