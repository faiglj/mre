-------------------------------------------------------------------------------
Multi-Robot Exploration Framework
-------------------------------------------------------------------------------

This is a set of implementations for evaluating multi-robot exploration 
strategies. It consists of several algorithms proposed in [1,2,3] and 
consolidated in a methodology for benchmarking exploration strategies in [4].

There are basically two types of dependencies common libraries and specific
libraries developed for exploration. The common library are mainly Boost, Cairo,
SDL2, and log4cxx. In addition, libraries that can also be found in the sw
managers are concorde, libfov, hdf5 and jpeg.

The specific libraries are imr-h.lite, which is a set of helpers, and libhungarian [5]
and evg-thin [6]. These libraries are provided in the 'vendors' directory for an 
convenience. However, the last two libraries are independent software packages,
and therefore, they can be cited appropriately.

In addition to the aforementioned dependencies, Player/Stage robotic simulator is need
for the benchmarking framework for the Level-1, see [5] for further details.

The provided sources can be built using the provided simple Makefiles. All the paths
to the dependency libraries can be adjusted in MK/libs.mk. A complete list of
dependencies is as follows:

* log4cxx  - (ver. 0.10.0)
* boost    - (ver. 1.55.0) - other version may work as well

* cairo    - (ver. 1.14.2) - other version may work as well 
             Needed for imr-h.gui
* sdl2     - (ver. 2.0.3 accompanied by sdl2_gfx (ver. 1.0.1) and sdl2_image (2.0.0) 
             Needed for a visualization of the grid based map.

* hdf5     - (ver. 1.8.15) 
             Need for mre_level0 for saving the current state of exploration
* libfov   - (ver. 1.0.4) - http://code.google.com/p/libfov/
             C library for calculating fields of view on low resolution rasters
	     Needed for 'mre_level0' for an efficient computation of the coverage
* Concorde - ver. 20031219 - http://www.tsp.gatech.edu/concorde.html
             Combinatorial Optimization package
	     Needed for 'mre_strategy_mtsp' to solve TSP instances
* Player   - (ver. 3.0.2) other versions may also work. 
             http://playerstage.sourceforge.net/
	     Needed  for 'mre_level1' framework
* Stage    - (ver. 3.2.2) other versions may also work, but this one is highly
             recommended http://playerstage.sourceforge.net/
	     Needed  for 'mre_level1' framework


The framework provides two levels of realism (Level-0 and Level-1) that can be
found under directories (modules) 'mre_level0' and 'mre_level1'. These modules
are based on particular algorithms that are implemented in other modules and can
be used independently. A basic structure of the modules is following:
* mre_lib           - Fundamental structures and types for the exploration strategies
* mre_gui           - Support for drawing, saving, and also loading bitmaps images
                      based on SDL2.
* mre_path_planning - A set of functions for a grid based path planning.
* mre_sim           - Discrete event-based simulator for the Level-0 of the realism.
* mre_strategies    - A set of exploration strategies (assignment algorithms)
                      Yamauchi's greedy approach, Hungarian algorithm-based,
		      Iterative assignment and MinPos.
* mre_strategy_mtsp - MTSP based algorithm for task-allocation problem considered
                      as a problem formulation in the multi-robot exploration
* snd_simulator     - Simulator to provide expected travel cost with SND reactive
                      motion controller from the Player 
* mre_level0        - Benchmarking framework for the Level-0 of the realism based
                      on the discrete simulator ('mre_sim')
* mre_level1        - Benchmarking framework for the Level-1 of the realism based 
                      on Player/Stage simulator

Compilation
-----------

First, install the required dependencies, best using system software package manager.
Then, go to the 'vendors' directory and install the specific libraries, e.g., by calling './install.sh'
The specific libraries are installed locally into 'include' and 'lib' directories in the
root of the sources. 

To compile Level-0 go to 'mre_level0' and compiled dependency modules by 'make depend'
or 'gmake depend' on FreeBSD. Then, the executable for benchmarking exploration 
strategies under Level-0 can be build by 'make'. Do the same for 'mre_level1'.

Usage
-----
The evaluation of exploration strategies is executable by the 'tmre' binary which is
built for both levels of realism. In the both cases, the programs have several parameters
that can be provided from the command line option or using a configuration file. See './tmre -h'
(or './tmre --help') and example of the attached 'tmre.cfg' for a list of options.

[1] Miroslav Kulich, Jan Faigl, Libor Preucil: On distance utility in the exploration task. ICRA 2011: 4455-4460

[2] Jan Faigl, Miroslav Kulich: On determination of goal candidates in frontier-based multi-robot exploration. ECMR 2013: 210-215

[3] Jan Faigl, Miroslav Kulich, Libor Preucil: Goal assignment using distance cost in multi-robot exploration. IROS 2012: 3741-3746

[4] Jan Faigl, Miroslav Kulich: On Benchmarking of Frontier-Based Multi-Robot Exploration Strategies. ECMR 2015: 210-215

[5] Cyrill Stachniss: C Implementation of the Hungarian Method, 2004, http://www2.informatik.uni-freiburg.de/~stachnis/misc.html

[6] Patrick Beeson: EVG-Thin: A Thinning Approximation to the Extended Voronoi Graph, 2006, http://www.cs.utexas.edu/users/qr/software/evg-thin.html
