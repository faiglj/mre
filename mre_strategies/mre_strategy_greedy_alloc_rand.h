/*
 * File name: mre_strategy_greedy_alloc_rand.h
 * Date:      2012/02/08 15:44
 * Author:    Jan Faigl
 */

#ifndef __MRE_STRATEGY_GREEDY_ALLOC_RAND_H__
#define __MRE_STRATEGY_GREEDY_ALLOC_RAND_H__

#include <imr_config.h>

#include "mre_types.h"

namespace mre {
   class CGreedyAllocRand {
      const bool RANDOM_ROBOT_ORDER;

      public:
      static imr::CConfig& getConfig(imr::CConfig& config);

      CGreedyAllocRand(imr::CConfig& cfg);
      ~CGreedyAllocRand();

      IntVector& assign(const DoubleMatrix& costMatrix, IntVector& goalsIDX);

   };
} //end namespace mre

#endif

/* end of mre_strategy_greedy_alloc_rand.h */
