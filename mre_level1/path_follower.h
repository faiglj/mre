/*
 * File name: path_follower.h
 * Date:      2009/08/06 14:27
 * Author:    Jan Faigl
 */

#ifndef __PATH_FOLLOWER_H__
#define __PATH_FOLLOWER_H__

#include <queue>

#include <imr_config.h>

#include "mre_types.h"

namespace mre {

   class CPathFollower {

      public:
         static imr::CConfig& getConfig(imr::CConfig& config);

         CPathFollower(imr::CConfig& cfg);
         ~CPathFollower();

         void reset(void);
         void add(const SPosition& pos);
         void add(const PositionVector& newPlan);

         int size(void) const { return path.size(); }

         /// ----------------------------------------------------------------------------
         /// @brief next
         /// 
         /// @param cur 
         /// @param next 
         /// 
         /// @return true if next position is return
         /// ----------------------------------------------------------------------------
         bool next(const SPosition& cur, SPosition& next, double& goalDistance);
         
         double currentRemainingDistance(const SPosition& cur) const;

      protected:
         typedef std::queue<SPosition> Path;
         double reachGoalDistance;
         Path path;
   };

} // end namespace mre


#endif

/* end of path_follower.h */
