#
# Date:      2013/06/10 15:21
# Author:    Jan Faigl
#

uniq = $(if $1,$(firstword $1) $(call uniq,$(filter-out $(firstword $1),$1)))

add_lib=$(value $(1))
add_module_lib=$(if $(wildcard $(root_dir)/$(1)/lib$(1).a), -l$(1))
add_module_dir=$(if $(wildcard $(root_dir)/$(1)/lib$(1).a), -L$(root_dir)/$(1))

CPPFLAGS+=$(foreach i,$(addsuffix _CFLAGS,$(LIBS)),$(call add_lib,$i))
CPPFLAGS+=$(addprefix -I$(root_dir)/,$(MODULES))

LDFLAGS+=$(foreach i,$(MODULES),$(call add_module_lib,$i))
LDFLAGS+=$(foreach i,$(MODULES),$(call add_module_dir,$i))
LDFLAGS+=$(foreach i,$(addsuffix _LDFLAGS,$(LIBS)),$(call add_lib,$i))

CXXFLAGS:=$(call uniq,$(CXXFLAGS))
CPPFLAGS:=$(call uniq,$(CPPFLAGS))
LDFLAGS:=$(call uniq,$(LDFLAGS))
clean_files=

bin: $(TARGETS)

ifneq ($(LIBRARY_OBJS),)
   lib=lib$(LIBRARY).a
   clean_files+=$(LIBRARY_OBJS)
   clean_files+=$(lib)
   $(LIBRARY)_OBJS=$(LIBRARY_OBJS)
   $(LIBRARY)_LDFLAGS=-l$(LIBRARY) -L.

$(LIBRARY_OBJS): %.o: %.cc
	$(CXX) -c $< $(CXXFLAGS) $(CPPFLAGS) -o $@

$(LIBRARY)_MODULE: lib

lib$(LIBRARY).a : $(LIBRARY_OBJS)
	$(AR) rcs $@ $(LIBRARY_OBJS)
endif

lib: $(lib)

add_module=$(1)+=$($(2)_LDFLAGS)

define BIN_tmpl
clean_files+=$$($(1)_OBJS)
$(1)_LIBS_DEPS=
$(foreach module,$($(1)_MODULES),$(call add_module,$(1)_MODULE_DEPS,$(module)))

$(1)_CPPFLAGS+=$(foreach i,$(addsuffix _CFLAGS,$($(1)_LIBS)),$(call add_lib,$i))
$(1)_LDFLAGS+=$(foreach i,$(addsuffix _LDFLAGS,$($(1)_LIBS)),$(call add_lib,$i))

$(1)_MODULES:=$(addsuffix _MODULE,$($(1)_MODULES))

$$($(1)_OBJS): %.o: %.cc
	$(CXX) -c $$< $(CXXFLAGS) $(CPPFLAGS) $$($(1)_CPPFLAGS) -o $$@
$(1) : $$($(1)_OBJS) $$($(1)_MODULES)
	@echo "$($(1)_OBJS_DEPS)"
	$(CXX) $$($(1)_OBJS) $$($(1)_OBJS_DEPS) $$($(1)_MODULE_DEPS) $$($(1)_LDFLAGS) $(LDFLAGS) -o $$@
endef

$(foreach bin,$(TARGETS),$(eval $(call BIN_tmpl,$(bin))))

clean:
	$(RM) $(OBJS) $(TARGETS) $(clean_files)

define DEPEND_tmpl
$(1) :
	$(MAKE) -C $(root_dir)/$(1) depend
	$(MAKE) -C $(root_dir)/$(1) lib
endef

$(foreach lib,$(MODULES),$(eval $(call DEPEND_tmpl,$(lib))))

depend: $(MODULES)
