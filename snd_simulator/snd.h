/*
 *      gap_nd_nav.cpp
 *
 *      Copyright 2009 Luca Invernizzi <invernizzi.l@gmail.com>
 *      Player dependences removed by
 * Petr Martinec <petr.martinec@gmail.com>
 * Miroslav Kulich <kulich@labe,felk.cvut.cz>
 * 
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef GAP_ND_NAV_H_
#define GAP_ND_NAV_H_

#include <imr_config.h>

#include "mre_types.h"
#include "laser_config.h"

#include "gap_and_valley.h"
#include "path.h"
namespace mre {

   /// Normalize angle to domain -pi, pi
   inline double normalize(double z)
   {
      return atan2(sin(z), cos(z));
   }

   class CSnd {
      public:
	 typedef  std::vector<double> SScan;
	 static imr::CConfig& getConfig(imr::CConfig& config);
	 CSnd(SLaserConfig lconf, imr::CConfig& config);
	 void oneStep(const SPosition &pos, CPath &path, const SScan &scan);
	 double getTurnSpeed(void); 
	 double getDriveSpeed(void);

      private:
	 imr::CConfig& cfg;
	 double R;
	 double minGapWidth;
	 double safetyDistMax ;
	 double maxSpeed;
	 double maxTurnRate;
	 double goalPositionTol;
	 double goalAngleTol;
	 double maxRange;
	 double scanResolution;
	 int numScanRays;
	 double turnSpeed;
	 double driveSpeed;

	 double timeval_subtract( timeval *end, timeval *start );
	 bool isFilterClear( int iCenterSector, double width, double forwardLength, bool bDoRearCheck, std::vector<double> fullLP, double angRes);
	 bool isRisingGapSafe( Gap* pRisingGap, int iValleyDir, std::vector<double> fullLP, double fScanRes, double fMaxRange, double R );
	 void setSpeed(double driveSpeed, double turnSpeed);
   };

} //namespace mre

#endif //GAP_ND_NAV_H_
