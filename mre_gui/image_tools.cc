/*
 * File name: image_tools.cc
 * Date:      2012/01/01 13:23
 * Author:    Jan Faigl
 */

#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <cstring>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <jpeglib.h>

#include <imr_assert.h>

#include "image_tools.h"

#define CLIP(x) ( (x)>=0xFF ? 0xFF : ( (x) <= 0x00 ? 0x00 : (x) ) )

namespace mre {

   /// - public function ----------------------------------------------------------
   void yuv422_to_rgb888_float(unsigned char* src, const int w, const int h, unsigned char *dst) {
      int line, column;
      unsigned char *py, *pu, *pv;
      unsigned char *tmp = dst;

      // In this format each four bytes is two pixels. Each four bytes is two Y's, a Cb and a Cr. 
      //   Each Y goes to one of the pixels, and the Cb and Cr belong to both pixels.
      py = src;
      pu = src + 1;
      pv = src + 3;

      int c = 0;
      for (line = 0; line < h; ++line) {
	 for (column = 0; column < w; ++column) {
	    *tmp++ = CLIP((float)*py + 1.402*((float)*pv-128.0));
	    *tmp++ = CLIP((float)*py - 0.344*((float)*pu-128.0) - 0.714*((float)*pv-128.0));
	    *tmp++ = CLIP((float)*py + 1.772*((float)*pu-128.0));
	    c += 3;
	    py += 2; // increase py every time

	    if ((column & 1) == 1) { // increase pu,pv every second time
	       pu += 4;
	       pv += 4;
	    }
	 }
      }
   }

   /// - public function ----------------------------------------------------------
   void yuv422_to_rgb888_shift(unsigned char* src, const int w, const int h, unsigned char *dst) {
      int line, column;
      unsigned char *py, *pu, *pv;
      unsigned char *tmp = dst;

      // In this format each four bytes is two pixels. Each four bytes is two Y's, a Cb and a Cr. 
      //  Each Y goes to one of the pixels, and the Cb and Cr belong to both pixels.
      py = src;
      pu = src + 1;
      pv = src + 3;

      int c = 0;
      int j = 0;
      for (line = 0; line < h; ++line) {
	 for (column = 0; column < w; ++column) {
	    const int C = *py - 16;
	    const int D = *pu - 128;
	    const int E = *pv - 128;

	    *tmp++ = CLIP((298 * C + 409 * E+ 128) >> 8);
	    *tmp++ = CLIP((298 * C - 100 * D - 208* E + 128) >> 8);
	    *tmp++ = CLIP((298 * C + 516 * D + 128) >> 8);
	    c += 3;
	    py += 2; // increase py every time
	    if ((column & 1)==1) { // increase pu,pv every second time
	       pu += 4;
	       pv += 4;
	    }
	    j++;
	 }
      }
   }

   static int *LutRv = NULL;
   static int *LutGu = NULL;
   static int *LutGv = NULL;
   static int *LutBu = NULL;

   unsigned char
      R_FROMYV(unsigned char y, unsigned char v)
      {
	 return CLIP((y) + LutRv[(v)]);
      }
   unsigned char
      G_FROMYUV(unsigned char y, unsigned char u, unsigned char v)
      {
	 return CLIP((y) + LutGu[(u)] + LutGv[(v)]);
      }
   unsigned char
      B_FROMYU(unsigned char y, unsigned char u)
      {
	 return CLIP((y) + LutBu[(u)]);
      }

   /// - public function ----------------------------------------------------------
   void yuv422_to_rgb888_table_init(void) {
      int i;
#define Rcoef 299 
#define Gcoef 587 
#define Bcoef 114 
#define Vrcoef 711 
#define Ubcoef 560 

#define CoefRv 1402
#define CoefGu 714
#define CoefGv 344
#define CoefBu 1772

      LutRv = (int*)malloc(256*sizeof(int));
      LutGu = (int*)malloc(256*sizeof(int));
      LutGv = (int*)malloc(256*sizeof(int));
      LutBu = (int*)malloc(256*sizeof(int));
      for (i= 0;i < 256;i++){
	 LutRv[i] = (i-128)*CoefRv/1000;
	 LutBu[i] = (i-128)*CoefBu/1000;
	 LutGu[i] = (128-i)*CoefGu/1000;
	 LutGv[i] = (128-i)*CoefGv/1000;
      }	
   }

   /// - public function ----------------------------------------------------------
   void yuv422_to_rgb888_table_free(void) {
      free(LutRv);
      free(LutGu);
      free(LutGv);
      free(LutBu);
   }

   /// - public function ----------------------------------------------------------
   void yuv422_to_rgb888_table(unsigned char* src, const int w, const int h, unsigned char *dst) {
      unsigned char *buff = src;
      unsigned char *output_pt = dst;
      for (int i = w * h / 2; i > 0; --i) {
	 const unsigned char Y = buff[0];
	 const unsigned char U = buff[1];
	 const unsigned char Y1 = buff[2];
	 const unsigned char V = buff[3];
	 buff += 4;
	 *output_pt++ = R_FROMYV(Y,V);
	 *output_pt++ = G_FROMYUV(Y,U,V); //b
	 *output_pt++ = B_FROMYU(Y,U); //v

	 *output_pt++ = R_FROMYV(Y1,V);
	 *output_pt++ = G_FROMYUV(Y1,U,V); //b
	 *output_pt++ = B_FROMYU(Y1,U); //v
      }
   }

   /// - public function ----------------------------------------------------------
   /*
      void yuv422_to_gray_table(unsigned char* src, const int w, const int h, unsigned char *dst) {
      unsigned char *buff = src;
      unsigned char *output_pt = dst;
      for (int i = w * h / 2; i > 0; --i) {
      const char g = CLIP((buff[0]) + LutRv[(buff[3])]);
      buff += 4;
      for(int j = 0; j < 6; j++) { //3BPP GRAY
    *output_pt++ = g;
    }
    }
    }
    */
   void yuv422_to_gray_table(unsigned char* src, const int w, const int h, unsigned char *dst) {
      unsigned char *buff = src;
      unsigned char *output_pt = dst;
      for (int i = w * h / 2; i > 0; --i) {
	 const unsigned char RV = LutRv[buff[3]];
	 *output_pt++ = CLIP((buff[0]) + RV);
	 *output_pt++ = CLIP((buff[2]) + RV);
	 buff += 4;
      }
   }

   /// - public function ----------------------------------------------------------
   void jpeg_compress(const unsigned char* src, int src_len, int w, int h, unsigned char** dst, unsigned long& dst_len, int quality, int components) {
      struct jpeg_compress_struct cinfo;
      struct jpeg_error_mgr jerr;
      cinfo.err = jpeg_std_error( &jerr );
      jpeg_create_compress(&cinfo);
      cinfo.image_width = w;	
      cinfo.image_height = h;
      ASSERT_ARGUMENT(components == 3 or components == 1, "imr::jpeg_compress support only 1 or 3 components");
      if (components == 3) {
	 cinfo.input_components = components;
	 cinfo.in_color_space = JCS_RGB; 
      } else {
	 cinfo.input_components = components;
	 cinfo.in_color_space = JCS_GRAYSCALE;
      }
      jpeg_set_defaults(&cinfo);
      jpeg_set_quality(&cinfo, quality, TRUE);
      jpeg_mem_dest (&cinfo, dst, &dst_len); 
      JSAMPROW row_pointer[1];
      jpeg_start_compress(&cinfo, TRUE);
      while (cinfo.next_scanline < cinfo.image_height) {
	 row_pointer[0] = (unsigned char*)&src[cinfo.next_scanline * cinfo.image_width *  cinfo.input_components];
	 jpeg_write_scanlines(&cinfo, row_pointer, 1);
      }
      jpeg_finish_compress(&cinfo);
      jpeg_destroy_compress(&cinfo);
   }

   /// - public function ----------------------------------------------------------
   bool save_img_jpeg(const SImage* img, int quality, const std::string& filename) {
      bool ret = false;
      if (img) {
	 ASSERT_ARGUMENT(img->bpp == 1 or img->bpp == 3, "imr::save_img_jpeg support only 1 or 3 bytes per pixel");
	 unsigned long jsize = 0;
	 unsigned char* jbuf;
	 jpeg_compress(img->getImageBuffer(), img->size, img->w, img->h, &jbuf, jsize, quality, img->bpp);
	 int fd = open(filename.c_str(), O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
	 if (fd != -1 ) {
	    ret = (write(fd, &jbuf[0], jsize) == jsize);
	    ret = ret and (close(fd) == 0);
	 }
	 free(jbuf);
      }
      return ret;
   }

   /// - public function ----------------------------------------------------------
   bool save_img_raw(const SImage* img, const std::string& filename) {
      bool ret = false;
      if (img) {
	 int fd = open(filename.c_str(), O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
	 if (fd != -1 ) {
	    std::stringstream ss;
	    ss << "SImage " << img->w << " " << img->h << " " << img->bpp << std::endl; 
	    const char* str = ss.str().c_str();
	    const int slen = strlen(str);
	    ret = write(fd, str, slen) == slen;
	    ret = ret and (write(fd, img->getImageBuffer(), img->size) == img->size);
	    ret = ret and (close(fd) == 0);
	 }
      }
      return ret;
   }

   /// - public function ----------------------------------------------------------
   bool is_raw_img(const std::string& filename) {
      bool ret = false;
      const char* head = "SImage ";
      const int size = strlen(head);
      char buf[size + 1];

      int fd = open(filename.c_str(), O_RDONLY);
      if (fd != -1 and read(fd, &buf[0], size) == size) {
	 buf[size] = '\0';
	 ret = strncmp(head, &buf[0], size) == 0;
      }
      close(fd);
      return ret;
   }

   /// - public function ----------------------------------------------------------
   SImage* load_img_raw(const std::string& filename) {
      SImage* ret = 0;
      int fd = open(filename.c_str(), O_RDONLY);
      if (fd != -1) {
	 char buf[23];  //read head
	 int l = read(fd, &buf[0], 22);
	 int end = -1;
	 for(int i = 0; i < l; ++i) {
	    if (buf[i] == '\n') { end = i; break; }
	 }
	 ASSERT_IO(end != -1, "File '" + filename + "' does not contain valid SImage header!");
	 buf[end] = '\0'; //create a null terminated string
	 std::stringstream ss(buf);
	 std::string s;
	 int width, height, bpp;
	 ASSERT_IO(ss >> s >> width >> height >> bpp and s == "SImage" and lseek(fd, end + 1, SEEK_SET) != -1, "Cannot read SImage header");
	 ret = new SImage(width, height, bpp);
	 if (read(fd, ret->getImageBuffer(), ret->size) != ret->size) {
	    delete ret; ret = 0;
	    ASSERT_IO(false, "Cannot expected size of the SImage from the file '" + filename + "'");
	 }
	 close(fd);
      }
      return ret;
   }

   /// - public function ----------------------------------------------------------
   void draw(const IntVector& pixels, const SRGB& color, SImage* img) {
      if (img) {
	 unsigned char* buf = img->getImageBuffer();
	 if (img->bpp == 1) {
	    for(int i = 0; i < pixels.size(); ++i) {
	       *(buf + ( (pixels[i]/img->w) * img->w + pixels[i]%img->w)) = rgb_to_gray(color.r, color.g, color.b);
	    }
	 } else if (img->bpp == 3) {
	    for(int i = 0; i < pixels.size(); ++i) {
	       unsigned char* s = buf + ( (pixels[i]/img->w) * img->w + pixels[i]%img->w) * 3;
	       *s = color.r; 
	       *(s + 1) = color.g;
	       *(s + 2) = color.b;
	    }
	 } else {
	    ASSERT_ARGUMENT(false, "imr::draw support only surface with 1 or 3 bytes per pixel");
	 }
      }
   }

} //end namespace mre

/* end of image_tools.cc */
