/*
 * File name: mre_hdf_loader.cc
 * Date:      2011/08/11 12:17
 * Author:    Jan Faigl
 */

#include <hdf5.h>
#include <hdf5_hl.h>

#include <logging.h>

#include "mre_hdf_loader.h"

using imr::logger;

using namespace mre;

/// - constructor --------------------------------------------------------------
CHDFLoader::CHDFLoader() : file_id(-1), group_id(-1) {
   strType = loader::ATTRIBUTE;
}

/// - destructor ---------------------------------------------------------------
CHDFLoader::~CHDFLoader() {
   if (file_id >= 0) { H5Fclose(file_id); }
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator<<(const loader::TParam& p) {
   switch(p) {
      case loader::DATASET:
      case loader::FILENAME:
      case loader::ATTRIBUTE:
      case loader::ATTRIBUTE_VALUE:
      case loader::GROUP:
         strType = p; //switch string interpretation
         break;
      case loader::CLOSE_GROUP:
         if (group_id >= 0) {
            ASSERT_IO(H5Gclose(group_id) >= 0, "Close group faild");
            group_id = -1;
         }
         break;
      case loader::OPENREADONLY:
         open(filename, H5F_ACC_RDONLY);
         break;
      case loader::OPENWRITETRUNC:
         open(filename, H5F_ACC_TRUNC);
         break;
      case loader::CLOSE:
         if (file_id >= 0) { 
            H5Fclose(file_id); 
         }
         file_id = -1;
         break;
      default:
         //ignore 
         break;
   }
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator<<(const std::string& str) {
   switch(strType) {
      case loader::FILENAME:
         filename = str;
         break;
      case loader::ATTRIBUTE:
         attrName = str;
         strType = loader::ATTRIBUTE_VALUE; //the next string will be the attribute value 
         break;
      case loader::ATTRIBUTE_VALUE:
         setRootAttribute(attrName, str);  //TODO currently only the root attribute is supported
         break;
      case loader::GROUP:
         if (group_id >= 0) {
            ASSERT_IO(H5Gclose(group_id) >= 0, "Close group faild");
         }
//#if H5_VERS_MAJOR==1 && H5_VERS_MINOR==8 && H5_VERS_RELEASE>=9
	 group_id = H5Gcreate(file_id, str.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
//#else
//	 group_id = H5Gcreate(file_id, str.c_str(), H5P_DEFAULT);
//#endif
	 ASSERT_IO(group_id >= 0, "Group '" + str + "' creation faild");
	 break;
      case loader::DATASET:
	 datasetName = str;
	 break;
      default:
	 ASSERT_ARGUMENT(false, "Unsupported type of str object");
	 break;
   } //end switch
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator<<(const loader::OpenReadonly& o) {
   if (!o().empty()) { filename = o(); }
   open(filename, H5F_ACC_RDONLY);
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator<<(const FloatMatrix& matrix) {
   setFloatMatrixDataset(datasetName, matrix);
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator<<(const IntMatrix& matrix) {
   setIntMatrixDataset(datasetName, matrix);
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator<<(int v) {
   ASSERT_IO(strType == loader::ATTRIBUTE_VALUE, "Int value can be set only for the attribute");
   ASSERT_IO(!attrName.empty(), "Attribute name must be given");
   ASSERT_IO(H5LTset_attribute_int(file_id, "/", attrName.c_str(), &v, 1) >= 0, "Cannot set int attribute '" + attrName + "'");
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator>>(std::string& str) {
   switch(strType) {
      case loader::ATTRIBUTE_VALUE: 
         getRootAttribute(attrName, str); //TODO currently only the root attribute is supported
         break;
      case loader::DATASET:
         datasetName = str;
      default:
         ASSERT_ARGUMENT(false, "Unsupported type of str object");
         break;
   } //end switch
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator>>(loader::FMatrixDataset& c) {
   c.v = getFloatMatrixDataset(datasetName, c.v);
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator>>(loader::IMatrixDataset& c) {
   c.v = getIntMatrixDataset(datasetName, c.v);
   return *this;
}

/// - public method ------------------------------------------------------------
CHDFLoader& CHDFLoader::operator>>(int& v) {
   ASSERT_IO(strType == loader::ATTRIBUTE_VALUE, "Int value can be gonly for the attribute");
   ASSERT_IO(!attrName.empty(), "Attribute name must be given");
   ASSERT_IO(file_id >= 0, "File must be opened prior reading problem name");
   hid_t a_id = H5Aopen_name(file_id, attrName.c_str());
   H5A_info_t info;
   ASSERT_IO(a_id >= 0, "Cannot open root attribute '" + attrName + "'");
   ASSERT_IO(H5Aget_info(a_id, &info) >= 0, "Cannot get info about root attribute '" + attrName + "'");
   if (info.data_size > 0) {
      char d[info.data_size];
      ASSERT_IO(H5LTget_attribute_int(file_id, "/", attrName.c_str(),  &v) >= 0, "Error during reading attribute '" + attrName + "'");
   }
   H5Aclose(a_id);
   return *this;
}

/// - public method ------------------------------------------------------------
std::string& CHDFLoader::getRootAttribute(const std::string attrName, std::string& value) {
   ASSERT_IO(!attrName.empty(), "Attribute name cannot be empty");
   ASSERT_IO(file_id >= 0, "File must be opened prior reading problem name");
   hid_t a_id = H5Aopen_name(file_id, attrName.c_str());
   H5A_info_t info;
   ASSERT_IO(a_id >= 0, "Cannot open root attribute '" + attrName + "'");
   ASSERT_IO(H5Aget_info(a_id, &info) >= 0, "Cannot get info about root attribute '" + attrName + "'");
   if (info.data_size > 0) {
      char d[info.data_size];
      ASSERT_IO(H5LTget_attribute_string(file_id, "/", attrName.c_str(),  &d[0]) >= 0, "Error during reading attribute '" + attrName + "'");
      value = std::string(d);
   }
   H5Aclose(a_id);
   return value;
}

/// - public method ------------------------------------------------------------
FloatMatrix* CHDFLoader::getFloatMatrixDataset(std::string& datasetName, FloatMatrix* m) {
   FloatMatrix* ret = 0;
   ASSERT_IO(file_id >= 0, "File must be opened prior reading cities");
   int r;
   ASSERT_IO(H5LTget_dataset_ndims (file_id, datasetName.c_str(), &r) >= 0, "Cannot retrive dimensionality of the " + datasetName);
   ASSERT_IO(r == 2, "Required dataset '" + datasetName + "' must be matrix");
   hsize_t dims[r];
   H5T_class_t cls;
   size_t ts;
   ASSERT_IO(H5LTget_dataset_info(file_id, datasetName.c_str(), &dims[0], &cls, &ts) >= 0, "Cannot retrive info about " + datasetName);
   if (m) {
      ASSERT_IO(m->NROWS == dims[0] and m->NCOLS == dims[1], "getFloatMatrixDataset unexpected matrix dimensions");
      ret = m;
   } else {
      ret = new FloatMatrix(dims[0], dims[1]);
   }
   if (H5LTread_dataset_float(file_id, datasetName.c_str(), ret->values()) < 0) {
      if (!m) { delete ret; } //delete allocated matrix
      ret = 0;
      ASSERT_IO(false, "Error during loader float matrix '" + datasetName + "'");
   }
   return ret;
}

/// - public method ------------------------------------------------------------
IntMatrix* CHDFLoader::getIntMatrixDataset(std::string& datasetName, IntMatrix* m) {
   IntMatrix* ret = 0;
   ASSERT_IO(file_id >= 0, "File must be opened prior reading cities");
   int r;
   ASSERT_IO(H5LTget_dataset_ndims (file_id, datasetName.c_str(), &r) >= 0, "Cannot retrive dimensionality of the " + datasetName);
   ASSERT_IO(r == 2, "Required dataset '" + datasetName + "' must be matrix");
   hsize_t dims[r];
   H5T_class_t cls;
   size_t ts;
   ASSERT_IO(H5LTget_dataset_info(file_id, datasetName.c_str(), &dims[0], &cls, &ts) >= 0, "Cannot retrive info about " + datasetName);
   if (m) {
      ASSERT_IO(m->NROWS == dims[0] and m->NCOLS == dims[1], "getIntMatrixDataset unexpected matrix dimensions");
      ret = m;
   } else {
      ret = new IntMatrix(dims[0], dims[1]);
   }
   if (H5LTread_dataset_int(file_id, datasetName.c_str(), ret->values()) < 0) {
      if (!m) { delete ret; } //delete allocated matrix
      ret = 0;
      ASSERT_IO(false, "Error during loader float matrix '" + datasetName + "'");
   }
   return ret;
}

/// - public method ------------------------------------------------------------
void CHDFLoader::open(const std::string& filename, int flags) {
   if (file_id >= 0) { H5Fclose(file_id); }
   ASSERT_IO(!filename.empty(), "Filename must be set");
   if (flags == H5F_ACC_RDONLY) {
   file_id = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
   } else if (flags == H5F_ACC_TRUNC) {
      file_id = H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
   } else {
      ASSERT_IO(false, "Unsupported open flags");
   }
   ASSERT_IO(file_id >= 0, "Cannot open file");
}

/// - public method ------------------------------------------------------------
void CHDFLoader::setRootAttribute(const std::string attrName, const std::string& value) {
   ASSERT_IO(!attrName.empty(), "Attribute name must be given");
   ASSERT_IO(H5LTset_attribute_string(file_id, "/", attrName.c_str(), value.c_str()) >= 0, "Cannot set attribute '" + attrName + "'");
}

/// - public method ------------------------------------------------------------
void CHDFLoader::setFloatMatrixDataset(std::string& datasetName, const FloatMatrix& matrix) {
   ASSERT_IO(!datasetName.empty(), "Matrix datasetName must be given");
   hsize_t dims[2] = { (hsize_t)matrix.NROWS, (hsize_t)matrix.NCOLS};
   ASSERT_IO(H5LTmake_dataset(file_id, datasetName.c_str(), 2, dims, H5T_NATIVE_FLOAT, matrix.values()) >= 0, "Cannot create matrix for the dataset name '" + datasetName + "'");
}

/// - public method ------------------------------------------------------------
void CHDFLoader::setIntMatrixDataset(std::string& datasetName, const IntMatrix& matrix) {
   ASSERT_IO(!datasetName.empty(), "Matrix datasetName must be given");
   hsize_t dims[2] = { (hsize_t)matrix.NROWS, (hsize_t)matrix.NCOLS};
   ASSERT_IO(H5LTmake_dataset(file_id, datasetName.c_str(), 2, dims, H5T_NATIVE_INT, matrix.values()) >= 0, "Cannot create matrix for the dataset name '" + datasetName + "'");
}

/* end of mre_hdf_loader.cc */
