/*
 * File name: distance_transform_exact_euclidean.h
 * Date:      2013/09/30 17:55
 * Author:    Jan Faigl
 */

#ifndef __DISTANCE_TRANSFORM_EXACT_EUCLIDEAN_H__
#define __DISTANCE_TRANSFORM_EXACT_EUCLIDEAN_H__

#include <limits>
#include <vector>

#include "mre_types.h"

namespace mre {

   /// ----------------------------------------------------------------------------
   /// @brief Based on the work
   /// 
   /// International Journal of Advanced Robotic Systems", ISSN 1729-8806
   /// The Exact Euclidean Distance Transform: A New Algorithm for Universal Path Planning
   /// By Juan Carlos Elizondo-Leal, Ezra Federico Parra-Gonzalez and Jose Gabriel Ramirez-Torres
   /// DOI: 10.5772/56581, http://www.intechopen.com/journals/international_journal_of_advanced_robotic_systems/the-exact-euclidean-distance-transform-a-new-algorithm-for-universal-path-planning
   ///
   /// ----------------------------------------------------------------------------
   class CDistanceTransformEE {
      public:
	 const unsigned char FREESPACE;
	 const unsigned char OBSTACLE;
	 const int H;
	 const int W;
	 const float fw[4];
	 const int fi[4];
	 const float bw[4];
	 const int bi[4];

	 CDistanceTransformEE(const ByteMatrix& map, const unsigned char FREESPACE = 255, const unsigned char OBSTACLE = 0);
	 ~CDistanceTransformEE();

	 const ByteMatrix& getGrid(void) const { return map; }

	 GridCoordsVector& findPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path); 
	 GridCoordsVector& findPath(const GridCoordsVector& starts, const GridCoords& goal, GridCoordsVector& path); 

	 void setGoal(const GridCoords& goalI);
	 GridCoordsVector& getPath(const GridCoords& start, GridCoordsVector& path) const;
	 inline float getDistance(const GridCoords& start) const {
	    return (grid(start.y, start.x) >= grid.SIZE) ?  - std::numeric_limits<float>::max() : grid(start.y, start.x);
	 }
	 inline float getDistance(const int idx) const {
	    return (grid(idx) >= grid.SIZE) ?  - std::numeric_limits<float>::max() : grid(idx);
	 }

	// int getDistanceManhattan(const GridCoords& start) const;

	 bool isReachable(const GridCoords& start) const;

	 int getMaxValue(void) const { return 2 * grid.SIZE; }

	 const FloatMatrix& getDistanceGrid(void) const { return grid; }

      private:
	 FloatMatrix& compute(FloatMatrix& grid);
	 FloatMatrix& computeDT(FloatMatrix& grid);
	 FloatMatrix& computeDTOld(FloatMatrix& grid);
	 bool visible(int s, int g, int neigh);

      private:
	 const ByteMatrix& map; //source map
	 GridCoords goal;
	 FloatMatrix grid;
	 IntMatrix vis;

      public:
	 float getSizeOfMB(void) {
	    return (sizeof(map)+ sizeof(float)*grid.SIZE + sizeof(int)*vis.SIZE) / (1024.0 * 1024.0);
	 }
   };

} //end namespace mre

#endif

/* end of distance_transform_exact_euclidean.h */
