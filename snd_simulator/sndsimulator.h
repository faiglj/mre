/*
 * File name: sndsimulator.h
 * Date:      Mon Oct 15 14:50:45 +0200 2012
 * Author:    Miroslav Kulich
 */

#ifndef __SND_SIMULATOR_H__
#define __SND_SIMULATOR_H__

#include <fov/fov.h>

#include <imr_config.h>

#include "mre_sdl_gui.h"
#include "laser_config.h"

#include "map.h"
#include "snd.h"

namespace mre {

   class CSndSimulator {
      public:
	 CSndSimulator(int mapW, int mapH, SLaserConfig &lconfig, imr::CConfig& config, gui::CGui* g);
	 ~CSndSimulator();
	 static imr::CConfig& getConfig(imr::CConfig& config);
	 void setMap(CMap* map);
	 void setSeen(unsigned int x, unsigned int y);
	 bool onMap(unsigned int x, unsigned int y);
	 bool blockLOS(unsigned int x, unsigned int y);
	 CSnd::SScan getScan(const SPosition& pose,bool draw);
	 int getTrajectory(const SPosition& pose, CPath& path);

      private:
	 fov_settings_type fov_settings;  
	 ByteMatrix grid;
	 imr::CConfig& cfg;
	 CMap* map;
	 CSnd snd;
	 gui::CGui* gui;
	 int W;
	 int H;
	 double maxRange;
	 double cellSize;
	 double robot_x;
	 double robot_y;
	 std::vector<double> scan;
	 SLaserConfig laserConf;
	 int numSamples;
	 double getAngle(double x, double y);
	 int getScanIndex(double dir, bool down);
   };

}

#endif

/* end of sndsimulator.h */
