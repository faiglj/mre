/*
 * File name: mre_strategy_greedy_alloc_rand.cc
 * Date:      2012/02/08 15:48
 * Author:    Jan Faigl
 */

#include <limits>

#include <logging.h>
#include <random.h>

#include "mre_strategy_greedy_alloc_rand.h"

using namespace mre;

/// ----------------------------------------------------------------------------
static void createPermutation(int number, IntVector& permutation) {
   permutation.clear(); 
   for (int i = 0; i < number; i++) { 
      permutation.push_back(i); 
   }
}

/// ----------------------------------------------------------------------------
static void permute(IntVector& permutation) {
   int k, tmp;
   imr::CRandom::randomize();
   for (int i = permutation.size(); i > 0; --i) {
      k = imr::CRandom::random()%i;
      tmp = permutation[i-1];
      permutation[i-1] = permutation[k];
      permutation[k] = tmp;
   }
}

/// - static members -----------------------------------------------------------
imr::CConfig& CGreedyAllocRand::getConfig(imr::CConfig& config) {
   config.add<bool>("greedy-alloc-rand-use-random", "Enable/disable randomization in allocation of the goals", true);
   return config;
}


/// - constructor --------------------------------------------------------------
CGreedyAllocRand::CGreedyAllocRand(imr::CConfig& cfg) : 
   RANDOM_ROBOT_ORDER(cfg.get<bool>("greedy-alloc-rand-use-random"))
{
}

/// - destructor ---------------------------------------------------------------
CGreedyAllocRand::~CGreedyAllocRand() {
}

/// - public method ------------------------------------------------------------
IntVector& CGreedyAllocRand::assign(const DoubleMatrix& costMatrix, IntVector& goalsIDX) {
   IntVector perm;
   createPermutation(costMatrix.NROWS, perm);
   if (costMatrix.NCOLS >= costMatrix.NROWS and RANDOM_ROBOT_ORDER) {
      permute(perm);
   }
   BoolVector used(costMatrix.NCOLS, false);
   //costMatrix rows - robots, columns - goals
   for(int r = 0; r < costMatrix.NROWS; ++r) {
      goalsIDX[perm[r]] = -1; 
      double minCost = std::numeric_limits<double>::max();
      for(int g = 0; g < costMatrix.NCOLS; ++g) {
         if (!used[g] and costMatrix(perm[r], g) >= 0 and costMatrix(perm[r], g) < minCost) {
            goalsIDX[perm[r]] = g; minCost = costMatrix(perm[r], g);
         }
      }
      if (goalsIDX[perm[r]] != -1) {
         used[goalsIDX[perm[r]]] = true;
      }
   }
   return goalsIDX;
}

/// - private method -----------------------------------------------------------

/* end of mre_strategy_greedy_alloc_rand.cc */
