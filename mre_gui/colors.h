/*
 * File name: colors.h
 * Date:      2006/04/30 15:57
 * Author:    Jan Faigl
 */

#ifndef __COLORS_H__
#define __COLORS_H__

#include <vector>
#include <string>

namespace imr {
   namespace gui {
      typedef std::string Color;

      class CColors {
         typedef std::vector<std::string*> StringPtrVector;
         int i;
         StringPtrVector colors;
         public:
         CColors();
         ~CColors();
         void reset(void);

         /// 
         /// @brief cur
         /// 
         /// @return current color
         /// 
         const Color& cur(void);

         /// 
         /// @brief next 
         /// 
         /// @return 
         /// 
         const Color& next(void);
      };

      namespace Colors {
         extern CColors DEFAULT;
      }
   }
}

#endif

/* end of colors.h */
