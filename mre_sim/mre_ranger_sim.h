/*
 * File name: mre_ranger_sim.h
 * Date:      2012/02/10 08:40
 * Author:    Jan Faigl
 */

#ifndef __MRE_RANGER_SIM_H__
#define __MRE_RANGER_SIM_H__

#include <imr_config.h>

#include "mre_types.h"

#include "laser_data.h"
#include "mre_robot.h"
#include "mre_ranger_sim_map.h"

namespace mre {

   class CRangerSim {
      private:
	 imr::CConfig& cfg;
	 CRangerSimMap map;
	 ByteMatrix enlargedMap;

	 const double LASER_RANGE;
	 SLaserData laser;
      public:

	 static imr::CConfig& getConfig(imr::CConfig& config);
	 CRangerSim(imr::CConfig& cfg, double cell_size);
	 ~CRangerSim();

	 const SLaserData& getLaserParams(void) const { return laser; }
	 void enlargeMap(double r);

	 const CRangerSimMap& getMap(void) const { return map; }

	 void setPose(const SPosition& pose, const GridCoords& gridPose, SRobot& robot);
         SLaserData& getScan(const SPosition& pos, SLaserData& scan) const;
	 int getScan(const SRobot& robot, double range, IntVector& vx, IntVector& vy, IntVector& obstacles) const;

	 /// ----------------------------------------------------------------------------
	 /// @brief getScanReduced - similar to the getScan, but it avoids placing two 
	 /// consecutive endpoints with zero manhattan distance 
	 /// ----------------------------------------------------------------------------
	 int getScanReduced(const SRobot& robot, double range, IntVector& vx, IntVector& vy, IntVector& obstacles) const;

	 int getScanOmni(const SRobot& robot, double range, IntVector& vx, IntVector& vy, IntVector& obstacles) const;
	 void initScanOmni(const double LASER_RANGE);

	 const ByteMatrix& getGridMap(void) const { return map.getGrid(); }
      private:
	 GridCoordsVector scanEndPoints;
   };

} //end namespace mre

#endif

/* end of mre_ranger_sim.h */
