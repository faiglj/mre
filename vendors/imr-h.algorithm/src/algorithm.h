/*
 * File name: algorithm.h
 * Date:      2010/03/30 18:17
 * Author:    Jan Faigl
 */

#ifndef __ALGORITHM_H__
#define __ALGORITHM_H__

#include "imr_algorithm.h"

namespace imr { namespace alg {
   class CAlgorithm : public imr::CAlgorithm {
      public:

         static imr::CConfig& getConfig(CConfig& config);
         CAlgorithm(imr::CConfig& config);
         ~CAlgorithm();

         std::string getVersion(void);
         std::string getRevision(void);

      protected:
         void load(void);
         void initialize(void);
         void after_init(void) { }; //nothing to do
         void iterate(int iter);
         void save(void);
         void release(void);

         void defineResultLog(void);
         void fillResultRecord(int iter, double coverage, double distCoverage);
   };
} } //end namespace imr::alg


#endif

/* end of algorithm.h */
