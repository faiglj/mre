/*
 * File name: distance_transform2.cc
 * Date:      2011/05/21 17:03
 * Author:    Jan Faigl
 */

#include <cmath> //for sqrt
#include <limits>

#include <logging.h>
#include <imr_assert.h>

#include "distance_transform2.h"

using imr::logger;

using namespace mre;

static GridCoords& min8Point(const FloatMatrix& grid, GridCoords& p) {
   float min = std::numeric_limits<float>::max();
   const int H = grid.NROWS;
   const int W = grid.NCOLS;
   GridCoords t;

   for (int r = p.y - 1; r <= p.y + 1; r++) {
      if (r < 0 or r >= H) { continue; }
      for (int c = p.x - 1; c <= p.x + 1; c++) {
         if (c < 0 or c >= W) { continue; }
         if (min > grid(r, c)) {
            min = grid(r, c);
            t.y = r; t.x = c;
         }
      }
   }
   p = t;
   return p;
}

/// ----------------------------------------------------------------------------
/// Class CDistanceTransform2

/// - constructor --------------------------------------------------------------
CDistanceTransform2::CDistanceTransform2(const ByteMatrix& map, const unsigned char FREESPACE, const unsigned char OBSTACLE) : 
   FREESPACE(FREESPACE), OBSTACLE(OBSTACLE), 
   map(map), grid(map.NROWS, map.NCOLS) 
{
   goal = GridCoords(-1, -1);
}

/// - destructor ---------------------------------------------------------------
CDistanceTransform2::~CDistanceTransform2() {
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CDistanceTransform2::findPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path) {
   const int H = map.NROWS;
   const int W = map.NCOLS;
   FloatMatrix grid(H, W);
   for(int i = 0; i < grid.SIZE; ++i) { grid[i] = getMaxValue(); }
   grid(goal.y, goal.x) = 0;
   compute(grid);
   path.clear();

   DEBUG("H: " << H << " W: " << W << " H*W: " << H*W);
   DEBUG("start: " << grid(start.y, start.x));
   if (grid(start.y, start.x) >= H*W) {
      WARN("Path has not been found");
   } else {
      GridCoords pt = start;
      while (pt.y != goal.y or pt.x != goal.x) {
         path.push_back(pt);
         min8Point(grid, pt);
      }
      path.push_back(goal);
   }
   return path;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CDistanceTransform2::findPath(const GridCoordsVector& starts, const GridCoords& goal, GridCoordsVector& path) {
   const int H = map.NROWS;
   const int W = map.NCOLS;
   FloatMatrix grid(H, W);

   grid(goal.y, goal.x) = 0;
   compute(grid);
   path.clear();
   DEBUG("H: " << H << " W: " << W << " H*W: " << H*W);
   int iStart = 0;
   ASSERT_ARGUMENT(!starts.empty(), "Starts must be given");
   float min  = grid(starts[0].y, starts[0].x);
   for(int i = 1; i < starts.size(); i++) {
      const float d = grid(starts[i].y, starts[i].x);
      if (min > d) { min = d; iStart = i; }
   }
   GridCoords start = starts[iStart];
   DEBUG("start [" << iStart << "]: " << grid(start.y, start.x));
   if (grid(start.y, start.x) >= H*W) {
      WARN("Path has not been found");
   } else {
      GridCoords pt = start;
      while (pt.y != goal.y or pt.x != goal.x) {
         path.push_back(pt);
         min8Point(grid, pt);
      }
      path.push_back(goal);
   }
   return path;
}

/// - public method ------------------------------------------------------------
void CDistanceTransform2::setGoal(const GridCoords& goalI) {
   if (goal != goalI) {
      goal = goalI;
      for(int i = 0; i < grid.SIZE; ++i) { grid[i] = 2 * grid.SIZE; }
      grid(goal.y, goal.x) = 0;
      compute(grid);
   }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CDistanceTransform2::getPath(const GridCoords& start, GridCoordsVector& path) const {
   path.clear();
   if (grid(start.y, start.x) >= grid.SIZE) {
 //     WARN("Path has not been found");
   } else {
      GridCoords pt = start;
      while (pt.y != goal.y or pt.x != goal.x) {
         path.push_back(pt);
         min8Point(grid, pt);
      }
      path.push_back(goal);
   }
   return path;
}

/// - public method ------------------------------------------------------------
int CDistanceTransform2::getDistanceManhattan(const GridCoords& start) const {
   int l = 0.0;
   if (grid(start.y, start.x) >= grid.SIZE) {
      l = - std::numeric_limits<int>::max();
   } else {
      GridCoords pt = start;
      GridCoords prev = pt;
      while (pt.y != goal.y or pt.x != goal.x) {
         min8Point(grid, pt);
         l += prev.manhattan_distance(pt);
         prev = pt;
      }
      l += prev.manhattan_distance(goal);
   }
   return l;
}

/// - public method ------------------------------------------------------------
bool CDistanceTransform2::isReachable(const GridCoords& start) const {
   return grid(start.y, start.x) < grid.SIZE;
}

/// - private method -----------------------------------------------------------
FloatMatrix& CDistanceTransform2::compute(FloatMatrix& grid) const {
   static const float SQRT2 = sqrt(2);
   static const float DIAGONAL = SQRT2;
   static const float ORTOGONAL = 1;

   const int H = map.NROWS;
   const int W = map.NCOLS;
   ASSERT_ARGUMENT(grid.NROWS == H and grid.NCOLS == W, "Map grid does not match with the computational grid");
   bool anyChange = true;
   int counter = 0;

   while (anyChange) {
      anyChange = false;
      for (int r = 1; r < H - 1; r++) {
         for (int c = 1; c < W - 1; c++) {
               if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
               float t[4];
               t[3] = grid(r, c - 1) + ORTOGONAL;
               t[0] = grid(r - 1, c - 1) + DIAGONAL;
               t[1] = grid(r - 1, c) + ORTOGONAL;
               t[2] = grid(r - 1, c + 1) + DIAGONAL;
               float pom = grid(r, c);
               for (int i = 0; i < 4; i++) { 
                  if (pom > t[i]) { 
                     pom = t[i]; 
                     anyChange = true; 
                  } 
               }
               if (anyChange) { grid(r, c) = pom; }
         }
      }

      for (int r = H - 2; r >= 0; r--) {
         for (int c = W - 2; c > 0; c--) {
            if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
            float t[4];
            t[1] = grid(r + 1, c) + ORTOGONAL;
            t[0] = grid(r + 1, c + 1) + DIAGONAL;
            t[3] = grid(r, c + 1) + ORTOGONAL;
            t[2] = grid(r + 1, c - 1) + DIAGONAL;
            float pom = grid(r, c);
            bool s = false;
            for (int i = 0; i < 4; i++) { 
               if (pom > t[i]) { pom = t[i]; s = true; } 
            }
            if (s) { 
               anyChange = true;
               grid(r, c) = pom;
            }
         }
      }
      counter++;
   } //end while any change
 //  DEBUG("No. interations: " << counter);
   return grid;
}

/* end of distance_transform2.cc */
