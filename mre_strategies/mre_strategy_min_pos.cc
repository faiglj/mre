/*
 * File name: mre_strategy_min_pos.cc
 * Date:      2014/08/20 14:39
 * Author:    Jan Faigl
 */

#include <algorithm>

#include <logging.h>

#include "mre_strategy_min_pos.h"

using namespace mre;

/// ----------------------------------------------------------------------------
struct SPair {
   int goalIDX;
   int rank;
   double cost;
   SPair(int g, int r, double cost) : goalIDX(g), rank(r), cost(cost) {}
};

/// ----------------------------------------------------------------------------
template <class T> 
class Compare {
   public:
      bool operator()(const T& a, const T& b) const {
	 if (a.rank == b.rank) {
	    return a.cost < b.cost;
	 } else {
	    return a.rank < b.rank;
	 }
      }
      bool operator()(const T* a, const T* b) const {
	 if (a->rank == b->rank) {
	    return a->cost > b->cost;
	 } else {
	    return a->rank > b->rank;
	 }
      }
};

/// - constructor --------------------------------------------------------------
CMinPos::CMinPos(imr::CConfig& cfg, CPathPlanning& paths) : 
   cfg(cfg), paths(paths) {
   }

/// - destructor ---------------------------------------------------------------
CMinPos::~CMinPos() {
}

/// - public method ------------------------------------------------------------
IntVector& CMinPos::assign(const GridCoordsVector& goals, const RobotPtrVector& robots, IntVector& goalsIDX) {
   const int M = robots.size();
   const int N = goals.size();

   for(int r = 0; r < robots.size(); ++r) {
      int bestG = -1;
      int bestRank = -1;
      double bestCost = 0;
      for(int g = 0; g < goals.size(); ++g) {
	 const GridCoords& goal = goals[g];
	 int rank = 0;
	 double c = paths.getRobotDTPathLength(r, goal);
	 //compute rank
	 for (int rr = 0; rr < robots.size(); ++rr) {
	    if (rr != r) {
	       if (paths.getRobotDTPathLength(rr, goal) < c) {
		  rank++;
	       }
	    }
	 }
	 if (
	       (bestG == -1) 
	       or (rank < bestRank)
	       or (rank == bestRank and c < bestCost)
	    )
	 { 
	    bestG = g; bestRank = rank; bestCost = c;
	 }
      }
      goalsIDX[r] = bestG;
   }
   return goalsIDX;
}

/// - private method -----------------------------------------------------------

/* end of mre_strategy_min_pos.cc */
