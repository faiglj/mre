/*
 * File name: mre.cc
 * Date:      2012/01/23 21:51
 * Author:    Jan Faigl
 * Author:    Miroslav Kulich
 */

#include <cstdio>
#include <sstream>
#include <iomanip>

#include <boost/foreach.hpp>

#include <logging.h>
#include <timerN.h>
#include <perf_timer.h>
#include <imr_file_utils.h>

#include "mre_grid_utils.h"

#include "distance_transform.h"
#include "mre_strategy_greedy_alloc_rand.h"
#include "mre_strategy_pairs.h"
#include "mre_strategy_hungarian.h"
#include "mre_strategy_mtsp_kmeans.h"

#include "sndsimulator.h"

#include "goals.h"
#include "voronoi_planning.h"

#include "mre.h"

using namespace mre;

#define foreach BOOST_FOREACH

using imr::logger;

const FloatMatrix* globalGrid;
IntVectorVector* globalTravelledPaths;
FrontierVector* globalFrontiers;

/// - static method ------------------------------------------------------------
imr::CConfig& CMRE::getConfig(imr::CConfig& config) {
   config.add<std::string>("robot-config-pattern", "Pattern for robot configuration files" , "etc/robot-%d.cfg");
   config.add<std::string>("mre-strategy", "Strategy greedyallocrand|hungarian|pairs|mtspkmeans", "greedyallocrand");
   config.add<int>("number-robots", "Number of robots", 1);
   config.add<int>("max-robot-config-pattern-size", "Length of the pattern", 1024);
   config.add<std::string>("gui", "Select gui none|sdl|sdl-nowin" , "sdl");
   config.add<bool>("global", "Enable/disable global approach with determined time steps" , false);
   config.add<bool>("all-frontiers", "Enable/disable considering all frontiers as goals", false);
   config.add<int>("max-total-execution-steps", "Terminate exploration if execution steps reached the given number (no results are saved)", 15000);
   config.add<std::string>("goals", "A way how goals are selected from the frontier cells [allfrontiers|kmeansicra11|representative|goalsetcoverage|completecoverage|coveringcells|coveringcellsamples]", "kmeansicra11");
   config.add<double>("goals-range-ratio", "Ratio of the visibility range used for computing coverage", 0.9);
   config.add<int>("image-quality", "Save image quality png (-1 defult, 0 - off, 9 max), jpeg (0-100)", 9);
   config.add<int>("minimal-number-of-frontier-cells", "A frontier (object) has to be formed from at least this number of cells, otherwise it is discarded", 4);
   config.add<int>("max-events", "Maximal number of events in the event queue", 100);
   config.add<double>("rot-weight", "Weight of rotation", 0.3);
   config.add<std::string>("cost-function", "Cost utility function dist|snd|rot|turnpenalty" , "dist");

   config.add<int>("replanning-period", "Desired re-planning period (in ms), disabled if < 0", 1000);
   config.add<double>("replanning-path", "Re-plan if the size of the plan is bellow the value, disabled if < 0", 1);
   config.add<double>("goal-cluster-radius", "Radius of the goal cluster for inhibiting other goals from the selection", 1.0);
   
   CMap::getConfig(config);
   CGlobalControl::getConfig(config);
   SResults::getConfig(config);
   CGreedyAllocRand::getConfig(config);
   CMTSPKMeans::getConfig(config);
   CSndSimulator::getConfig(config);
   CPaths::getConfig(config);

   return config;
}

/// - static method ------------------------------------------------------------
CMRE::TGoals CMRE::getGoals(const std::string& str) {
   if (str == "allfrontiers") {
      return ALL_FRONTIERS;
   } else if (str == "kmeansicra11") {
      return KMEANS_ICRA11;
   } else if (str == "representative") {
      return REPRESENTATIVE;
   } else if (str == "goalsetcoverage") {
      return GOAL_SET_COVERAGE;
   } else if (str == "completecoverage") {
      return COMPLETE_COVERAGE;
   } else if (str == "coveringcells") {
      return COVERING_CELLS;
   } else if (str == "coveringcellsamples") {
      return COVERING_CELLS_SAMPLES;
   }
   ASSERT_ARGUMENT(false, "Unknown goals selection method");
}

/// - static method ------------------------------------------------------------
std::string CMRE::getGoalsStr(const TGoals goals) {
   switch(goals) {
      case ALL_FRONTIERS:
         return "allfrontiers";
         break;
      case KMEANS_ICRA11:
         return "kmeansicra11";
         break;
      case REPRESENTATIVE:
         return "representative";
      case GOAL_SET_COVERAGE:
         return "goalsetcoverage";
         break;
      case COMPLETE_COVERAGE:
         return "completecoverage";
         break;
      case COVERING_CELLS:
         return "coveringcells";
         break;
      case COVERING_CELLS_SAMPLES:
         return "coveringcellsamples";
         break;
   } //end switch
}

/// - constructor --------------------------------------------------------------
CMRE::CMRE(imr::CConfig& cfg) :
   cfg(cfg), map(new CMap(cfg)), 
   //paths(new CPathPlanning(*map, 255)),
   sdl(cfg.get<std::string>("gui") == "sdl"), gui(0), global(cfg),results(cfg),
   LASER_RANGE_MAX(map->MAX_LASER_RANGE), quit(false),
   GLOBAL_CONTROL(cfg.get<bool>("global")),
   IMAGE_QUALITY(cfg.get<int>("image-quality")),
   //   IMAGE_EXT(cfg.get<std::string>("pic-ext")),
   STRATEGY(getStrategy(cfg.get<std::string>("mre-strategy"))),
   GOALS_SELECTION(getGoals(cfg.get<std::string>("goals"))),
   COST_FUNCTION(getCost(cfg.get<std::string>("cost-function"))),
   MIN_NUMBER_OF_FRONTIER_CELLS(cfg.get<int>("minimal-number-of-frontier-cells")),
   GOAL_CLUSTER_RADIUS(cfg.get<double>("goal-cluster-radius")),
   queue(cfg.get<int>("max-events")),
   waitForDataBarrier(0)
{

   started = false;
//    if (cfg.get<bool>("ranger-sim")) {
//       rangerSim = new CRangerSim(cfg);
//       global.setRangerSim(rangerSim);
//    }

   if (!GLOBAL_CONTROL) {
      const int N = cfg.get<int>("number-robots");
      const int MAXNAME = cfg.get<int>("max-robot-config-pattern-size");
      const char* pattern = cfg.get<std::string>("robot-config-pattern").c_str();
      char h[MAXNAME];
      waitForDataBarrier = new boost::barrier(N+1);
      for(int i = 0; i < N; ++i) {
         snprintf(&h[0], MAXNAME-1, pattern, i);
         robots.push_back(new CRobotPlayer(std::string(&h[0]), i, *map, queue, waitForDataBarrier));
	 mreRobots.push_back(&(robots.back()->robot));
      }
      DEBUG("robots: " << robots.size());
   }

   const std::string g = cfg.get<std::string>("gui");
   if (g != "none") {
     if (g == "sdl") {
       sdl.init(map->W, map->H, "mre", "mre");
     } else {
       sdl.initOffScreen(map->W, map->H);
     }
     gui = new gui::CGui(sdl);
     
     DEBUG("Init gui");
     map->setGui(gui);
   }

/*
   ASSERT_ARGUMENT(IMAGE_EXT == "png" or IMAGE_EXT == "jpeg" or IMAGE_EXT == "jpg", "Only png, jpg, or jpeg are supported");
   output = cfg.get<std::string>("output");
   if (!output.empty() and output[output.size() -1] != '/') {
      output += "/";
   }
   if (!output.empty() and !imr::checkPath(output)) {
      ASSERT_ARGUMENT(imr::createDirectory(output), "Cannot create output directory");
   }
   picDir = output + cfg.get<std::string>("pic-dir");
    if (!picDir.empty() and picDir[picDir.size() -1] != '/') {
      picDir += "/";
   }
   if (!picDir.empty() and !imr::checkPath(picDir)) {
      ASSERT_ARGUMENT(imr::createDirectory(picDir), "Cannot create picDir directory");
   }
*/
}

/// - destructor ---------------------------------------------------------------
CMRE::~CMRE() {
   stop();
   foreach(CRobotPlayer* robot, robots) {
      delete robot;
   }
   robots.clear();
//   if (rangerSim) { delete rangerSim; }
   if (gui) { delete gui; }
   if (map)  { delete map; }
   if (waitForDataBarrier) { delete waitForDataBarrier; }
}

/// - public method ------------------------------------------------------------
void CMRE::stop() {
   {
      ScopedLock lk(mtx);
      DEBUG("MRE stop");
      quit = true;
   }
   join();
}

/// - public method ------------------------------------------------------------
bool CMRE::waitForExecution(void) {
   bool ret = false;
   ScopedLock lk(mtx);
   if (!started) {
      cond.wait(lk);
   }
   ret = started;
   return ret;

}
/// - protected method ---------------------------------------------------------
void CMRE::threadBody(void) {
   //    if (GLOBAL_CONTROL) {
   //       global_control();
   //    } else {
   individial_robots();
   //    }
}

/// - private method -----------------------------------------------------------
void CMRE::individial_robots(void) {
   try {
      DEBUG("individial_robots: " << robots.size());
      bool q = false;

      {
         ScopedLock lk(mtx);
         // initialize robots
         foreach(CRobotPlayer* robot, robots) {
            robot->execute();
            robot->waitForConnection();
            usleep(50*1000);
         }
         DEBUG("Waiting for positions.");
         waitForDataBarrier->wait();
         DEBUG("All robots have got the position.");
         started = true;
         cond.notify_all();
      }
      EventQueue::Message msg;


      const int N = robots.size();
      const double ROT_WEIGHT= cfg.get<double>("rot-weight");
      const double ROBOT_RADIUS = cfg.get<double>("global-robot-disk-radius");//0.15;
      const bool ALL_FRONTIERS = cfg.get<bool>("all-frontiers");
      const int MAX_TOTAL_EXECUTION_STEPS = cfg.get<int>("max-total-execution-steps");
      //    rangerSim->enlargeMap(0); //

      results.defineResultLog(getStrategyStr(STRATEGY), getCostStr(COST_FUNCTION),ROT_WEIGHT, map->W, map->H, map->MAP_W, map->MAP_H, map->CELL_SIZE);
      results.init(N, LASER_RANGE_MAX);

      FrontierVector frontiers;
      GridCoordsVector goalGridPts;
      IntVector coveringCells;
      imr::CTimerN tm;

      bool forceTermination = false;

      //CVoronoiPlanning -- demonstration
      if (0 and gui) {
         ByteMatrix grid3(map->getNavGrid().NROWS, map->getNavGrid().NCOLS);
         gui::loadGridImage("etc/scenarios/bitmaps/jh-640x719.png", 0, 1, grid3);
         ByteMatrix grid(grid3.NROWS, grid3.NCOLS);
         for(int i = 0; i < grid.SIZE; i++) { grid(i) = (grid3(i) == 0 ? 255 : 0); }

         GridCoords start(10, 10);
         GridCoords goal(380, 440);
         GridCoordsVector pathVor;
         FloatVector distances;
         GridCoordsVector obstacles;
         //CVoronoiPlanning vor(grid, CDistanceTransform::FREESPACE);
         CVoronoiPlanning vor(grid, 255);
         {
	    imr::CPerfTimer t("voronoi.compute");
            vor.compute();
         }
         {
	    imr::CPerfTimer t("voronoi.getPath");
            vor.getPath(start, goal, pathVor);
         }
         {
	    imr::CPerfTimer t("voronoi.getDistances");
            vor.getDistances(pathVor, obstacles, distances);
         }

         gui->drawGrid(grid);
         gui->drawDisk(start, 4, gui->getColor("green"));
         gui->drawDisk(goal, 4, gui->getColor("deepskyblue"));

         gui->draw(vor.getVornoiCells(), gui->getColor("SteelBlue4"));
         foreach(const GridCoords& pt, pathVor) { gui->drawDisk(pt, 2, gui->getColor("orange"), 50); }
         DEBUG("Path voronoi_planning: " << pathVor.size());
         for(int i = 0; i < pathVor.size(); ++i) {
            DEBUG("Distance i: " << i << " " << distances[i]);
            GridCoordsVector pts;
            pts.push_back(pathVor[i]);
            pts.push_back(obstacles[i]);
            GridCoordsVector ptsFill;
            ::fillPath(pts, ptsFill);
            gui->draw(ptsFill, gui->getColor("MistyRose"));
         }
         gui->refresh();
         getchar();
      } //end CVoronoiPlanning demonstration


      ///    updateMap(robots, ROBOT_RADIUS, LASER_RANGE_MAX);
      foreach(const CRobotPlayer* robot, robots) { // the robots are placed at the freespace
         //      map->setCellsValues(robot->getShape(), map->FREESPACE_PROB);
         results.travelledPaths.push_back(GridCoordsVector());
         results.travelledDistances.push_back(0.0);
      }

      ByteMatrix occ(map->getNavGrid().NROWS, map->getNavGrid().NCOLS);
      CPaths paths(cfg, occ, CPaths::FREESPACE_CELL);

      CGreedyAllocRand* greedyAllocRand = new CGreedyAllocRand(cfg);
      CPairs* pairs = new CPairs;
      CHungarian* hungarian = new CHungarian;
      //TODO MTSP CMTSPKMeans* mtspKmeans = new CMTSPKMeans(cfg, paths);

      IntVector goalsIDX; //just a single goal for each robot
      GridCoordsVector goalGridPtsPrev;
      GridCoordsVectorVector robotsGoalGridPts;
      SLaserConfig laserConf = robots[0]->getLaserConfig(); //TODO: supposing that robots are identical    
      CSndSimulator* sndSimul = COST_FUNCTION == SND_COST ? new CSndSimulator(map->W, map->H, laserConf, cfg,gui) : 0;

      const int planning_period = cfg.get<int>("replanning-period");
      const double planning_path = cfg.get<double>("replanning-path");

      int prevNFC = 0;
      long prevTime = imr::CTimerN::getTime();
      results.tm.restart();
      while(!q) {
         DEBUG("CMRE: main loop");
         if (planning_period > 0) {
            long current = imr::CTimerN::getTime();
            int dt = planning_period - (current - prevTime);
            if (dt > 0) {
               usleep(dt * 1000); // 10 ms period
            }
         } else if (planning_path >= 0) {
            bool replan = false;
            while (!q and !replan) {
               for(int i=0;i<robots.size();i++) { // the robots are placed at the freespace
                  const double d = robots[i]->getGoalRemainingDistance();
                  DEBUG("Robot[" << i << "] remaining distance is " << d);
                  if (d <= planning_path) {
                     replan = true;
                     break;
                  }
                  /*
                     if (robots[i]->getPathFollower()->size() < planning_path) {
                     replan = true;
                     DEBUG("A robot reaches its goal ");
                     break;
                     }*/

               }
               if (!replan) {
                  // DEBUG("Robots are navigated towards their goals");
                  usleep(1 * 1000); //wait for check
               }
               q |= quit;
            }
         }

         {
            ScopedLock lk(mtx);
            for(int i=0;i<robots.size();i++) { // the robots are placed at the freespace
               results.travelledDistances[i] = robots[i]->getTravelledDistance();
               robots[i]->addTravelledPath(results.travelledPaths[i]);
            }
            tm.restart();

            bool hasGoals = false;

            if (!q) {
               map->lock();
               tm.restart();
               foreach(CRobotPlayer* robot, robots) {
                  robot->setShape(ROBOT_RADIUS);
                  robot->savePoses();
               }
               /* update sndSimul after determining goals
                  if (sndSimul) {
                  sndSimul->setMap(map);
                  }*/

               //map->updateNavGrid(map->CELL_SIZE*0, robots); //update navigational grid
               map->updateNavGrid(ROBOT_RADIUS, mreRobots); //update navigational grid
	       imr::CTimerN tm1;
               tm1.start();

               {
                  const ByteMatrix& navGrid = map->getNavGrid();
                  for(int i = 0; i < occ.SIZE; ++i) {
                     if (navGrid[i] == CMap::FREESPACE_CELL ) {
                        occ[i] = CPaths::FREESPACE_CELL;
                     } else {
                        occ[i] = CPaths::OCCUPIED_CELL;
                     }
                  }
                  paths.updateMap(occ);
               }
               //TODO            paths->updateMap(); //prepare planning grid
               //TODO            paths->computeRobotDT(robots);

               const int nfc = map->getNoOfFreespaceCells();
               //results.expectedExplorationTimeSteps += results.stepTotal * 1.0 * (nfc - prevNFC);
               results.expectedExplorationTimeSteps += results.step * 1.0 * (nfc - prevNFC);

               DEBUG("nfc: " << nfc << " expectedExplorationTimeSteps: " << results.expectedExplorationTimeSteps);
               prevNFC = nfc;
               map->unlock();
               getGoals(robots, paths, *map, frontiers, goalGridPts, robotsGoalGridPts, coveringCells);

               map->lock();
               foreach(CRobotPlayer* robot, robots) {
                  robot->setShape(ROBOT_RADIUS);
                  robot->savePoses();
               }
               {
                  const ByteMatrix& navGrid = map->getNavGrid();
                  for(int i = 0; i < occ.SIZE; ++i) {
                     if (navGrid[i] == CMap::FREESPACE_CELL ) {
                        occ[i] = CPaths::FREESPACE_CELL;
                     } else {
                        occ[i] = CPaths::OCCUPIED_CELL;
                     }
                  }
                  paths.updateMap(occ);
               }


               if (sndSimul) {
                  sndSimul->setMap(map);
               }


               //TODO paths->updateMap(); //update map for planning as robots can move significantly
               hasGoals = !goalGridPts.empty();
               //TODO  paths->computeRobotDT(robots);

               if (hasGoals) {
                  tm1.restart();
                  DEBUG("Determine paths to goals robots:" << robots.size() << " goalGridPts: " << goalGridPts.size());
                  //             map->lock();
                  //TODO paths->set(robots, goalGridPts);
                  //TODO paths->compute(robots);
                  tm1.stop();
                  INFO("Paths computed in time real: " << tm1.realTime() << " ms cpu: " << tm1.cpuTime() << " ms");
               }
               map->unlock();

               DEBUG("Robot poses");
               for(int i = 0; i < robots.size(); ++i) {
                  DEBUG("Robot poses  robot: " << i << " d: " << sqrt(robots[i]->getPosition().squared_distance(robots[i]->getCurrentPosition()))); 
               }
               tm1.stop();
               INFO("Goals determined in time real: " << tm1.realTime() << " ms cpu: " << tm1.cpuTime() << " ms");

               if (hasGoals) {
                  DEBUG("Number of goals " << goalGridPts.size());
                  DoubleMatrix costMatrix(N, goalGridPts.size());
                  //
                  if (sdl() and gui) {
                     gui->drawGridLight(map->getNavGrid());
                  }

                  tm1.restart();
                  for(int r = 0; r < N; ++r) {
                     for(int g = 0; g < goalGridPts.size(); ++g) {
                        switch (COST_FUNCTION) {
                           case DISTANCE_COST:
                              costMatrix(r, g) = paths.getDistance(goalGridPts[g], robots[r]->getGridPose());
                              //TODO costMatrix(r, g) = paths->getPathLength(r, g);
                              break;

                           case TURN_PENALTY_COST:
                              {
                                 float d;
                                 if (paths.getDistance(goalGridPts[g], robots[r]->getGridPose(), d)) {
                                    costMatrix(r, g) = addTurnPenalty(paths, robots[r]->getGridPose(), robots[r]->getCurrentPosition(), goalGridPts[g], d);
                                    //costMatrix(r, g) = addTurnPenalty(paths, robots[r]->getGridPose(), robots[r]->getPosition(), goalGridPts[g], d);
                                 } else {
                                    costMatrix(r, g) = std::numeric_limits<double>::max();
                                 }
                              }
                              break;
                           case SND_COST:  
                              {
                                 GridCoordsVector pathSimple;
                                 paths.getPath2WaySimpl(robots[r]->getGridPose(), goalGridPts[g], pathSimple);
                                 //TODO  paths->getSimplifiedPath(r, g, pathSimple);
                                 CPath path;
                                 for(int i=1;i<pathSimple.size();i++) {
                                    SPosition pos(map->grid2realX(pathSimple[i].x), map->grid2realY(pathSimple[i].y));
                                    path.add(pos);
                                 }
                                 costMatrix(r, g) = sndSimul->getTrajectory(robots[r]->getPosition(),path);
                              }
                              break;
                           case ROT_PENALTY_COST:
                              GridCoordsVector pathSimple;
                              paths.getPath2WaySimpl(robots[r]->getGridPose(), goalGridPts[g], pathSimple);
                              costMatrix(r, g) = getPathLength(pathSimple) + ROT_WEIGHT*pathSimple.size();
                              //TODO paths->getSimplifiedPath(r, g, pathSimple);
                              //TODO costMatrix(r, g) = paths->getPathLength(r, g) + ROT_WEIGHT*pathSimple.size();
                              break;
                        }
                        //                 DEBUG("cost " << r << " " << g << " =  " <<   costMatrix(r, g));
                     }
                  }
                  tm1.stop();
                 INFO("Cost matrix determined in time real: " << tm1.realTime() << " ms cpu: " << tm1.cpuTime() << " ms");

                  goalsIDX.resize(N, -1);
                  tm1.restart();

                  switch(STRATEGY) {
                     case GREEDY_ALLOC_RAND:
                        greedyAllocRand->assign(costMatrix, goalsIDX);
                        break;
                     case PAIRS:
                        pairs->assign(costMatrix, goalsIDX);
                        DEBUG("PAIRS done");
                        for(int r = 0; r < N; ++r) {
                           DEBUG("Robot: " << r << " goal: " << goalsIDX[r]);
                        }
                        break;
                     case HUNGARIAN:
                        if (costMatrix.NCOLS < costMatrix.NROWS) { //less number of goals than robots:
                           pairs->assign(costMatrix, goalsIDX); //use pairs
                        } else {
                           hungarian->assign(costMatrix, goalsIDX);
                        }
                        break;
                     case MTSP_KMEANS:
                        DEBUG("NROWS: " << costMatrix.NROWS << " NCOLS: " << costMatrix.NCOLS);
                        if (costMatrix.NCOLS < costMatrix.NROWS) { //less number of goals than robots:
                           DEBUG("MTSP_KMEANS - call pairs");
                           pairs->assign(costMatrix, goalsIDX); //use pairs
                        } else if (costMatrix.NROWS == costMatrix.NCOLS) { //same number of robots as goals
                           DEBUG("MTSP_KMEANS - call hungarian");
                           hungarian->assign(costMatrix, goalsIDX); //consider hungarian
                        } else {
                           // TODO MTSP mtspKmeans->assign(goalGridPts, mreRobots, goalsIDX);
                        }
                        DEBUG("MTSP_KMEANS - assign done");

                        break;
                     default:
                        ASSERT_ARGUMENT(false, "Unsupported strategy");
                        break;
                  } //end switch
                  ASSERT_ARGUMENT(goalsIDX.size() == N, "All goals must be filled");
                  for(int r = 0; r < N; ++r) {
                     if (goalsIDX[r] == -1) { //consider greedy
                        DEBUG("Robot " << r << " does not have a valid goal");
                        double min = std::numeric_limits<double>::max();
                        for(int g = 0; g < costMatrix.NCOLS; ++g) {
                           if (min > costMatrix(r, g)) {
                              min = costMatrix(r, g);
                              goalsIDX[r] = g;
                           }
                        }
                        //it may happen that the environment is not connected; thus, a robot can be isolated, and therefore, it can already explored the whole component
                     }
                  }
                  tm1.stop();
                  INFO("Goals assignment using '" << getStrategyStr(STRATEGY) << "' strategy done in time real: " << tm1.realTime() << " ms cpu: " << tm1.cpuTime() << " ms");
                  // DEBUG("step[" << step << "] goalsIDX: " << goalsIDX.size());
                  {  //determine robot execution plan
                     for(int r = 0; r < goalsIDX.size(); ++r) {
                        if (goalsIDX[r] != -1) {
                           CRobotPlayer* robot = robots[r];
                           GridCoordsVector &pathSimple = robot->robot.pathSimple;
                           paths.getPath2WaySimpl(robots[r]->getGridPose(), goalGridPts[goalsIDX[r]], pathSimple);
                           //TODO  paths->getSimplifiedPath(r, goalsIDX[r], pathSimple);
                           INFO("Best cost for robot " << r << " is " << costMatrix(r, goalsIDX[r]));
                           CPathFollower *pf = robot->getPathFollower();
                           pf->reset();
                           int ii = 0;
                           foreach(SGridCoords pt,pathSimple) {
                              if (ii++==0) continue;
                              //                  SGridCoords &pt = pathSimple.back();
                              pf->add(SPosition(map->grid2realX(pt.x), map->grid2realY(pt.y)));
                           }
                        }
                     }
                  }
                  tm.stop();
                  INFO("Determine new plans time real: " << tm.realTime() << " ms cpu: " << tm.cpuTime() << " ms");
               } else {
                  INFO("Use the previous plan");
               }
            } //end if (!q)

            results.step++;
            if (sdl() and gui) {
               tm.restart();
               //  sdl.drawGrid255(map->getGrid(), sdl.getScreenSurface());
               //        gui->drawGrid(map->getNavGrid());

               //           gui->drawGridLight(map->getNavGrid());

               foreach(const SFrontier f, frontiers) {
                  gui->drawFrontier(f.coords.y, f.coords.x);
               }

               foreach(const GridCoords& pt, goalGridPts) {
                  gui->drawGoal(pt.y, pt.x);
               }

               gui->resetColor();
               gui->nextColor();
               for(int i = 0; i < results.travelledPaths.size(); ++i) {
                  GridCoordsVector pts;
                  gui->draw(::fillPath(results.travelledPaths[i], pts), gui->nextColor());
               }
               int rrr = 0; 
               foreach(CRobotPlayer* robot, robots) {
                  gui->drawRobot(robot->getGridPose(), robot->getPosition());
                  gui->drawPath(robot->robot.pathSimple, SRGB(0, 255, 255));
                  if (rrr==0) {
                     CPath path;
                     GridCoordsVector trav = robot->robot.pathSimple;
                     for(int i=1;i<trav.size();i++) {
                        SPosition pos(map->grid2realX(trav[i].x), map->grid2realY(trav[i].y));
                        path.add(pos);
                     }
                     //               sndSimul.getTrajectory(robots[0]->getPosition(),path);
                  }
                  rrr++;
               }

               sdl.refresh();
               sdl.refresh();
               tm.stop();
               INFO("Gui time real: " << tm.realTime() << " ms cpu: " << tm.cpuTime() << " ms");
               if (results.SAVE_PIC) {
                  savePic(results.step);
               }
            }

            if (!hasGoals) {
               results.stepTotal = 0;
               int tmp;
               for(int i=0;i<robots.size();i++) { // the robots are placed at the freespace
                  tmp = robots[i]->getExecutionSteps();
                  if (tmp > results.stepTotal) { results.stepTotal = tmp; }
               }
               INFO("All goals (frontiers) has been visited - terminated exporation");
               INFO("Total number of steps: " << results.stepTotal);
               for(int r = 0; r < robots.size(); ++r) {
                  INFO("Travelled distance robot[" << r << "]: " << results.travelledDistances[r]);
               }
               q = true;
            }

            if (results.stepTotal > MAX_TOTAL_EXECUTION_STEPS) {
               q = true;
               forceTermination = true;
               WARN("Force terminated stepTotal reached max total execution steps " << results.stepTotal << "/" << MAX_TOTAL_EXECUTION_STEPS);
            }
            q |= quit;
         }
      } //end while
      DEBUG("End main loop forceTermination: " << forceTermination);
      if (!forceTermination) {
         for(int i=0;i<robots.size();i++) { // the robots are placed at the freespace
            results.travelledDistances[i] = robots[i]->getTravelledDistance();
            robots[i]->addTravelledPath(results.travelledPaths[i]);
         }
         results.expectedExplorationTimeSteps = results.expectedExplorationTimeSteps / (1.0 * map->getNoOfFreespaceCells());

         results.tm.stop();
         results.fillResults();
         results.save();
         if (results.SAVE_PIC and gui) {
            sdl.save_img_png(IMAGE_QUALITY, results.getFinalPicFilename());
         }
      }
      if (sndSimul) { delete sndSimul; }


      //TODO MTSP if (mtspKmeans) { delete mtspKmeans; }
      if (pairs) { delete pairs; }
   } catch (imr::exception& e) {
      ERROR("CMRE: " << e.what());
   }

   DEBUG("Exploration loop finished.");
   foreach(CRobotPlayer* robot, robots) {
      robot->stop();
   }
}

/// - private method -----------------------------------------------------------
GridCoordsVector& CMRE::getGoals(const RobotPlayerPtrVector& robots, CPaths& paths, CMap& map, FrontierVector& frontiers, GridCoordsVector& goalGridPts, GridCoordsVectorVector& robotsGoalGridPts, IntVector& coveringCosts) {
   FrontierVector allFrontiers;
   frontiers.clear();
   goalGridPts.clear();
   GridCoordsVector robotsPoses;
   foreach(const CRobotPlayer* robot, robots) { robotsPoses.push_back(robot->getGridPose()); }
   int nbrObjects = map.findFrontiers(robotsPoses, allFrontiers);

   BoolVector unreachable(allFrontiers.size(), false);
   for(int r = 0; r < robotsPoses.size(); ++r) {
      float dist;
      const GridCoords& pt = robotsPoses[r];
      for(int i = 0; i < allFrontiers.size(); ++i) {
         if (!unreachable[i]) {
            if (paths.getDistance(allFrontiers[i].coords, pt, dist) and dist > 0.0) {
               //reachable
            } else {
               unreachable[i] = true;
            } 
         }
      }
   }
   for(int i = 0; i < unreachable.size(); ++i) {
      if (unreachable[i] == false) {
         frontiers.push_back(allFrontiers[i]);
      }
   }
   DEBUG("allFrontiers: " << allFrontiers.size() << " frontiers: " << frontiers.size());
   //TODO paths->getReachableFrontiers(allFrontiers, frontiers);

   switch(GOALS_SELECTION) {
      case ALL_FRONTIERS:
         DEBUG("ALL_FRONTIERS");
         foreach(SFrontier& f, frontiers)  { goalGridPts.push_back(f.coords); }
         break;
      case KMEANS_ICRA11:
         {
            DEBUG("KMEANS_ICRA11");
            FrontierVector goals;
            if ( !frontiers.empty() ) { //find frontiers representatives
               FrontierVector repre;
               map.findAllGoals(frontiers, nbrObjects, MIN_NUMBER_OF_FRONTIER_CELLS, repre);
               foreach(const SFrontier& r, repre) { if ( r.coords.x != -1 ) { goals.push_back(r); } }
            }
            foreach(SFrontier& f, goals)  { goalGridPts.push_back(f.coords); }
         }
         break;
      case REPRESENTATIVE:
      case GOAL_SET_COVERAGE:
      case COMPLETE_COVERAGE:
      case COVERING_CELLS:
      case COVERING_CELLS_SAMPLES:
         {
            //           DEBUG("GOAL_SET_COVERAGE or COMPLETE_COVERAGE");
            GridCoordsVectorVector goalSetsPrev;
            int nbrGoals = 0;
            IntVector sets(nbrObjects, -1);
            int c = 0;
            foreach(const SFrontier& f, frontiers) {
               if (sets[f.object] == -1) { sets[f.object] = c++; }
            }
            if (!frontiers.empty()) {
               goalSetsPrev.resize(c, GridCoordsVector());
               foreach(const SFrontier& f, frontiers) { 
                  goalSetsPrev[sets[f.object]].push_back(f.coords); 
                  nbrGoals++;
               }
            }
            GridCoordsVectorVector goalSets;
            if (MIN_NUMBER_OF_FRONTIER_CELLS > 0) {
               foreach(const GridCoordsVector& set, goalSetsPrev) {
                  if (set.size() >= MIN_NUMBER_OF_FRONTIER_CELLS) {
                     goalSets.push_back(set);
                  }
               }
            } else {
               goalSets = goalSetsPrev;
            }
            CGoals* g = new CGoals(map, goalSets, robots, 
                  cfg.get<double>("goals-range-ratio"),
                  GOALS_SELECTION == REPRESENTATIVE ? false : true,
                  gui);
            if (GOALS_SELECTION == GOAL_SET_COVERAGE) {
               DEBUG("GOAL_SET_COVERAGE");
               g->getGoalsGoalSetCoverage(goalGridPts);
            } else if (GOALS_SELECTION == COMPLETE_COVERAGE) {
               DEBUG("COMPLETE_COVERAGE");
               IntVector coverage; // not currently used
               g->getGoalsCompleteCoverage(goalGridPts, coverage);
            } else if (GOALS_SELECTION == REPRESENTATIVE) {
               DEBUG("REPRESENTATIVE");
               g->getGoalsRepresentatives(goalGridPts);
            } else if (GOALS_SELECTION == COVERING_CELLS) {
               DEBUG("COVERING_CELLS");
               g->getCoveringCells(goalGridPts, coveringCosts);
            } else if (GOALS_SELECTION == COVERING_CELLS_SAMPLES) {
               DEBUG("COVERING_CELLS_SAMPLES");
               g->getCoveringCellsRandom(cfg.get<double>("covering-cells-samples-sigma"), goalGridPts, coveringCosts);
            }
            delete g; 
         }
         break;
   } //end switch
   DEBUG("All frontiers: " << allFrontiers.size() << " frontiers: " << frontiers.size() << " objects: " << nbrObjects << " goals: " << goalGridPts.size());
   return goalGridPts;
}

/// - public method ------------------------------------------------------------
float CMRE::addTurnPenalty(CPaths& paths, const GridCoords& gridPose, const SPosition& pose, const GridCoords& pt, float dist) {
   SPosition pos;
   GridCoordsVector pts;
   paths.getPath2WaySimpl(gridPose, pt, pts);
   if (pts.size() > 1) {
      dist = getPathLength(pts);
      const GridCoords& nextPt = pts[1];
      const double a = pose.azimuth(map->grid2real(nextPt.y, nextPt.x, pos)); 
      const double alpha = a - pose.yaw;
      const double t = -1 / LASER_RANGE_MAX;
      const double angle_ratio = (-1.0 * (cos(alpha / 2) - 1)); // * (laserRange * map->CELL_SIZE) / dist;
      const double distance_ratio = exp(t * dist * map->CELL_SIZE);
   //   DEBUG("Dist: " << dist << " yaw: " << pose.yaw << " a: " << a << " alpha: " << alpha << "rad alpha: " << RAD2DEG(alpha) << " deg");
  //    DEBUG("Angle ratio: " << angle_ratio << " distance ratio: " << distance_ratio);
      dist = dist + angle_ratio * distance_ratio * dist;
//     DEBUG("Dist2: " << dist);
 //     getchar();
   }
   return dist;
}

/*
/// - private method -----------------------------------------------------------
GridCoordsVector& CMRE::getGoals(const RobotPlayerPtrVector robots, FrontierVector& frontiers, GridCoordsVector& goalGridPts) {
frontiers.clear();
goalGridPts.clear();
GridCoordsVector robotsPoses;
foreach(const CRobot* robot, robots) { robotsPoses.push_back(robot->getGridPose()); }
int nbrObjects = map->findFrontiers(robotsPoses, frontiers);
DEBUG("Frontiers: " << frontiers.size() << " no. of objects: " << nbrObjects);

FrontierVector goals;

if ( !frontiers.empty() ) { //find frontiers representatives
FrontierVector repre;
map->findAllGoals(frontiers, nbrObjects, MIN_NUMBER_OF_FRONTIER_CELLS, repre);
foreach(const SFrontier& r, repre) {
if ( r.coords.x != -1 ) {
goals.push_back(r);
}
}
}
DEBUG("Goals: " << goals.size());
foreach(SFrontier& f, goals) {
goalGridPts.push_back(f.coords);
}
return goalGridPts;
}
*/

/*
/// - private method -----------------------------------------------------------
void CMRE::setRobotShape(SRobot& robot, double r) const {
robot.robotShape.clear();
const int neigh = (ceil(r / map.CELL_SIZE)); //2
const int neigh2 = neigh*neigh;
for (int i = -neigh; i < neigh + 1; i++) { //enlarge grid
for (int j = -neigh; j < neigh + 1; j++) {
if (i*i + j*j <= neigh2) {
const int x = robot.getGridPose().x + j;
const int y = robot.getGridPose().y + i;
if (x >= 0 and x < map.W and y >= 0 and y < map.H) {
robot.robotShape.push_back(x + y * map.W);
}
}
}
} //end neigh
ASSERT_ARGUMENT(!robot.robotShape.empty(), "Robot shape is empty");
}
*/

/// - private method -----------------------------------------------------------
void CMRE::savePic(int iter) {
   if (gui) {
      std::stringstream ss;
      ss << results.picDir << std::setw(6)  << std::setfill('0') << iter << "." << results.IMAGE_EXT;
      std::string s = ss.str();
      DEBUG("Save visualization to file " << s);
      if (results.IMAGE_EXT == "png") {
         sdl.save_img_png(IMAGE_QUALITY, s);
      } else {
         sdl.save_img_jpeg(sdl.getScreenSurface(), IMAGE_QUALITY, s);
      }
   }
}

/// - static method ------------------------------------------------------------
CMRE::TStrategy CMRE::getStrategy(const std::string& str) {
   if (str == "greedyallocrand") {
      return GREEDY_ALLOC_RAND;
   } else if (str == "pairs") {
      return PAIRS;
   } else if (str == "hungarian") {
      return HUNGARIAN;
   } else if (str == "mtspkmeans") {
      return MTSP_KMEANS;
   }
   ASSERT_ARGUMENT(false, "Unknown strategy");
}

/// - static method ------------------------------------------------------------
std::string CMRE::getStrategyStr(const TStrategy strategy) {
   switch(strategy) {
      case GREEDY_ALLOC_RAND:
         return "greedyallocrand";
         break;
      case PAIRS:
         return "pairs";
         break;
      case HUNGARIAN:
         return "hungarian";
         break;
      case MTSP_KMEANS:
         return "mtspkmeans";
         break;
   } //end switch
}


/// - static method ------------------------------------------------------------
CMRE::TCost CMRE::getCost(const std::string& str) {
   if (str == "dist") {
      return DISTANCE_COST;
   } else if (str == "snd") {
      return SND_COST;
   } else if (str == "rot") {
      return ROT_PENALTY_COST;
   } else if (str == "turnpenalty") {
      return TURN_PENALTY_COST;
   }
   ASSERT_ARGUMENT(false, "Unknown cost");
}

/// - static method ------------------------------------------------------------
std::string CMRE::getCostStr(const TCost cost) {
   switch(cost) {
      case DISTANCE_COST:
         return "dist";
         break;
      case SND_COST:
         return "snd";
         break;
      case ROT_PENALTY_COST:
         return "rot";
         break;
      case TURN_PENALTY_COST:
         return "turnpenalty";
         break;
   } //end switch
}

/* end of mre.cc */
