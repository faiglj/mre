/*
 * File name: distance_transform_exact_euclidean.cc
 * Date:      2013/09/30 17:55
 * Author:    Jan Faigl
 */

#include <cmath> //for sqrt
#include <limits>

#include <logging.h>
#include <imr_assert.h>

#include "mre_grid_utils.h"
#include "distance_transform_exact_euclidean.h"

using imr::logger;

using namespace mre;

const float SQRT2 = sqrt(2);
const float DIAGONAL = SQRT2;
const float ORTOGONAL = 1;

static GridCoords& min8Point(const FloatMatrix& grid, GridCoords& p) {
   float min = std::numeric_limits<float>::max();
   const int H = grid.NROWS;
   const int W = grid.NCOLS;
   GridCoords t;

   for (int r = p.y - 1; r <= p.y + 1; r++) {
      if (r < 0 or r >= H) { continue; }
      for (int c = p.x - 1; c <= p.x + 1; c++) {
         if (c < 0 or c >= W) { continue; }
         if (min > grid(r, c)) {
            min = grid(r, c);
            t.y = r; t.x = c;
         }
      }
   }
   p = t;
   return p;
}

/// ----------------------------------------------------------------------------
/// Class CDistanceTransformEE

/// - constructor --------------------------------------------------------------
CDistanceTransformEE::CDistanceTransformEE(const ByteMatrix& map, const unsigned char FREESPACE, const unsigned char OBSTACLE) : 
   FREESPACE(FREESPACE), OBSTACLE(OBSTACLE), H(map.NROWS), W(map.NCOLS),
   fw{ SQRT2, 1, SQRT2, 1 },
   fi{ -W - 1, -W, -W + 1, -1},
   bw{ 1, SQRT2, 1, SQRT2 },
   bi{ 1, W - 1, W, W + 1},
   map(map), grid(map.NROWS, map.NCOLS), vis(map.NROWS, map.NCOLS) 
{
   goal = GridCoords(-1, -1);
}

/// - destructor ---------------------------------------------------------------
CDistanceTransformEE::~CDistanceTransformEE() {
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CDistanceTransformEE::findPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path) {
   const int H = map.NROWS;
   const int W = map.NCOLS;
   FloatMatrix grid(H, W);
   for(int i = 0; i < grid.SIZE; ++i) { grid[i] = getMaxValue(); }
   grid(goal.y, goal.x) = 0;
   compute(grid);
   path.clear();

   int t = grid.index(start.y, start.x);
   const int g = grid.index(goal.y, goal.x);
   while(t != -1 and t != g) { 
      path.push_back(GridCoords(t % grid.NCOLS, t / grid.NCOLS));
      t = vis[t];
   }
   if (t == g) {
      path.push_back(GridCoords(t % grid.NCOLS, t / grid.NCOLS));
   } else {
      path.clear();
   }
   return path;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CDistanceTransformEE::findPath(const GridCoordsVector& starts, const GridCoords& goal, GridCoordsVector& path) {
   const int H = map.NROWS;
   const int W = map.NCOLS;
   FloatMatrix grid(H, W);

   grid(goal.y, goal.x) = 0;
   compute(grid);
   path.clear();
   DEBUG("H: " << H << " W: " << W << " H*W: " << H*W);
   int iStart = 0;
   ASSERT_ARGUMENT(!starts.empty(), "Starts must be given");
   float min  = grid(starts[0].y, starts[0].x);
   for(int i = 1; i < starts.size(); i++) {
      const float d = grid(starts[i].y, starts[i].x);
      if (min > d) { min = d; iStart = i; }
   }
   GridCoords start = starts[iStart];
   int t = grid.index(start.y, start.x);
   const int g = grid.index(goal.y, goal.x);
   while(t != -1 and t != g) { 
      path.push_back(GridCoords(t % grid.NCOLS, t / grid.NCOLS));
      t = vis[t];
   }
   if (t == g) {
      path.push_back(GridCoords(t % grid.NCOLS, t / grid.NCOLS));
   } else {
      path.clear();
   }
   return path;
}

/// - public method ------------------------------------------------------------
void CDistanceTransformEE::setGoal(const GridCoords& goalI) {
   if (goal != goalI) {
      goal = goalI;
      for(int i = 0; i < grid.SIZE; ++i) { 
	 grid[i] = 2 * grid.SIZE; 
	 vis[i] = -1;
      }
      grid(goal.y, goal.x) = 0;
      vis(goal.y, goal.x) = vis.index(goal.y, goal.x);
      compute(grid);
   }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CDistanceTransformEE::getPath(const GridCoords& start, GridCoordsVector& path) const {
   path.clear();
   if (grid(start.y, start.x) >= grid.SIZE) {
 //     WARN("Path has not been found");
   } else {
      int t = grid.index(start.y, start.x);
      const int g = grid.index(goal.y, goal.x);
      while(t != -1 and t != g) { 
	 path.push_back(GridCoords(t % grid.NCOLS, t / grid.NCOLS));
	 t = vis[t];
      }
      if (t == g) {
	 path.push_back(GridCoords(t % grid.NCOLS, t / grid.NCOLS));
      } else {
	 path.clear();
      }
   }
   return path;
}

/// - public method ------------------------------------------------------------
bool CDistanceTransformEE::isReachable(const GridCoords& start) const {
   return grid(start.y, start.x) < grid.SIZE;
}

/// - private method -----------------------------------------------------------
FloatMatrix& CDistanceTransformEE::compute(FloatMatrix& grid) {
   ASSERT_ARGUMENT(grid.NROWS == H and grid.NCOLS == W, "Map grid does not match with the computational grid");
   bool anyChange = true;
   while (anyChange) {
      anyChange = false;
      for (int r = 1; r < H - 1; r++) { //forward pass
	 const int w = r * map.NCOLS;
	 for (int c = 1; c < W - 1; c++) {
	    if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
	    const int idx = w + c;
	    const float& pom = grid(idx);
	    const GridCoords cur(c, r); 
	    for(int i = 0; i < 4; ++i) {
	       const int ni = idx + fi[i];
	       float t = grid(ni) + fw[i];
	       if (vis(ni) != -1) {
		  if (vis(idx) != vis(ni) and visible(idx, vis(ni), ni) ) { //check if the subgoal of the neighbors is visible
		     const GridCoords subgoal(vis(ni) % grid.NCOLS, vis(ni) / grid.NCOLS);
		     t = sqrt(cur.squared_distance(subgoal)) + grid(vis(ni));
		     if (t < pom) { vis(idx) = vis(ni); } //go over the sub-goal of ni
		  } else if (t < pom) { vis(idx) = ni; } //not visible and shorter path ; so, go over ni
	       }
	       if (t < pom) { //neigh does not have a subgoal
		  grid(idx) = t;
		  anyChange = true;
	       }
	    } //all neighbors
	 }

	 for (int c = W - 2; c > 0; c--) {
	    if (map(r, c) != FREESPACE) { continue; }
	    const int idx = w + c;
	    const float& pom = grid(idx);
	    const GridCoords cur(c, r); 
	    const int ni = idx + 1;
	    float t = grid(ni) + 1;
	    if (vis(ni) != -1) {
	       if (vis(idx) != vis(ni) and visible(idx, vis(ni), ni) ) { //check if the subgoal of the neighbors is visible
		  const GridCoords subgoal(vis(ni) % grid.NCOLS, vis(ni) / grid.NCOLS);
		  t = sqrt(cur.squared_distance(subgoal)) + grid(vis(ni));
		  if (t < pom) { vis(idx) = vis(ni); } //go over the sub-goal of ni
	       } else if (t < pom) { vis(idx) = ni; } //not visible and shorter path ; so, go over ni
	    }
	    if (t < pom) { //neigh does not have a subgoal
	       grid(idx) = t;
	       anyChange = true;
	    }
	 }
      }
      for (int r = H - 2; r >= 0; r--) {  //backward pass
	 for (int c = W - 2; c > 0; c--) {
	    if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
	    const int idx = grid.index(r, c);
	    const float& pom = grid(idx);
	    const GridCoords cur(c, r); 
	    for(int i = 0; i < 4; ++i) {
	       const int ni = idx + bi[i];
	       float t = grid(ni) + bw[i];
	       if (vis(ni) != -1) {
		  if (vis(idx) != vis(ni) and visible(idx, vis(ni), ni) ) { //check if the subgoal of the neighbor is visible
		     const GridCoords subgoal(vis(ni) % grid.NCOLS, vis(ni) / grid.NCOLS);
		     t = sqrt(cur.squared_distance(subgoal)) + grid(vis(ni));
		     if (t < pom) { vis(idx) = vis(ni); } //go over the sub-goal of ni
		  } else if (t < pom) { vis(idx) = ni; } //not visible and shorter path ; so, go over ni
	       } 
	       if (t < pom) { //neigh does not have a subgoal
		  grid(idx) = t;
		  anyChange = true;
	       }
	    }
	 }
	 for (int c = 1; c < W - 1; c++) {
	    if (map(r, c) != FREESPACE) { continue; }
	    const int idx = grid.index(r, c);
	    const float& pom = grid(idx);
	    const GridCoords cur(c, r); 
	    const int ni = idx - 1;
	    float t = grid(ni) + 1;
	    if (vis(ni) != -1) { //check if the subgoal of the neighbor is visible
	       if (vis(idx) != vis(ni) and visible(idx, vis(ni), ni) ) {
		  const GridCoords subgoal(vis(ni) % grid.NCOLS, vis(ni) / grid.NCOLS);
		  t = sqrt(cur.squared_distance(subgoal)) + grid(vis(ni));
		  if (t < pom) { vis(idx) = vis(ni); } //go over the sub-goal of ni
	       } else if (t < pom) { vis(idx) = ni; } //not visible and shorter path ; so, go over ni
	    } 
	    if (t < pom) { //neigh does not have a subgoal
	       grid(idx) = t;
	       anyChange = true;
	    }
	 }
      } 
   } 
   return grid;
}


/// - private method -----------------------------------------------------------
FloatMatrix& CDistanceTransformEE::computeDT(FloatMatrix& grid) {
   ASSERT_ARGUMENT(grid.NROWS == H and grid.NCOLS == W, "Map grid does not match with the computational grid");
   bool anyChange = true;
   while(anyChange) { //DT rewritten
      anyChange = false;
      for (int r = 1; r < H - 1; r++) { //forward pass
	 const int w = r * map.NCOLS;
	 for (int c = 1; c < W - 1; c++) {
	    if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
	    const int idx = w + c;
	    float& pom = grid(idx);
	    for(int i = 0; i < 4; ++i) {
	       const int ni = idx + fi[i];
	       const float t = grid(ni) + fw[i];
	       if (t < pom) { //neigh does not have a subgoal
		  pom = t;
		  anyChange = true;
	       }
	    }
	 }

	 for (int c = W - 2; c > 0; c--) {
	    if (map(r, c) != FREESPACE) { continue; }
	    const int idx = w + c;
	    float& pom = grid(idx);
	    const int ni = idx + 1;
	    const float t = grid(ni) + 1;

	    if (t < pom) { //neigh does not have a subgoal
	       pom = t;
	       anyChange = true;
	    }
	 }
      }
      for (int r = H - 2; r >= 0; r--) {  //backward pass
	 for (int c = W - 2; c > 0; c--) {
	    if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
	    const int idx = grid.index(r, c);
	    float& pom = grid(idx);
	    for(int i = 0; i < 4; ++i) {
	       const int ni = idx + bi[i];
	       const float t = grid(ni) + bw[i];
	       if (t < pom) { //neigh does not have a subgoal
		  pom = t;
		  anyChange = true;
	       }
	    }
	 } 
	 for (int c = 1; c < W - 1; c++) {
	    if (map(r, c) != FREESPACE) { continue; }
	    const int idx = grid.index(r, c);
	    float& pom = grid(idx);
	    const int ni = idx - 1;
	    const float t = grid(ni) + 1;
	    if (t < pom) { //neigh does not have a subgoal
	       pom = t;
	       anyChange = true;
	    }
	 }
      } 
   }
   return grid;
}

/// - private method -----------------------------------------------------------
FloatMatrix& CDistanceTransformEE::computeDTOld(FloatMatrix& grid) {
   ASSERT_ARGUMENT(grid.NROWS == H and grid.NCOLS == W, "Map grid does not match with the computational grid");
   bool anyChange = true;
   while (anyChange) {
      anyChange = false;
      for (int r = 1; r < H - 1; r++) { //forward pass
	 for (int c = 1; c < W - 1; c++) {
	    if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
	    float t[4];
	    t[3] = grid(r, c - 1) + ORTOGONAL;
	    t[0] = grid(r - 1, c - 1) + DIAGONAL;
	    t[1] = grid(r - 1, c) + ORTOGONAL;
	    t[2] = grid(r - 1, c + 1) + DIAGONAL;
	    float pom = grid(r, c);
	    for (int i = 0; i < 4; i++) { 
	       if (pom > t[i]) { 
		  pom = t[i]; 
		  anyChange = true; 
	       } 
	    }
	    if (anyChange) { grid(r, c) = pom; }
	 }
      }

      for (int r = H - 2; r >= 0; r--) {  //backward pass
	 for (int c = W - 2; c > 0; c--) {
	    if (map(r, c) != FREESPACE) { continue; } //an obstacle detected
	    float t[4];
	    t[1] = grid(r + 1, c) + ORTOGONAL;
	    t[0] = grid(r + 1, c + 1) + DIAGONAL;
	    t[3] = grid(r, c + 1) + ORTOGONAL;
	    t[2] = grid(r + 1, c - 1) + DIAGONAL;
	    float pom = grid(r, c);
	    bool s = false;
	    for (int i = 0; i < 4; i++) { 
	       if (pom > t[i]) { pom = t[i]; s = true; } 
	    }
	    if (s) { 
	       anyChange = true;
	       grid(r, c) = pom;
	    }
	 }
      }
   } //end while any change
   return grid;
}

/// - private method -----------------------------------------------------------
bool CDistanceTransformEE::visible(int s, int g, int neigh) {
   bool ret = s == g;
   if (!ret) {
      GridCoords start(s % grid.NCOLS, s / grid.NCOLS); //r = i / NCOLS, c = i % NCOLS
      GridCoords goal(g % grid.NCOLS, g / grid.NCOLS); //r = i / NCOLS, c = i % NCOLS
      //select two closet neighbor to the segment (s, g)a
      static const int dx[] = { -1, 0, 1,  0, -1, 1,  1, -1 };
      static const int dy[] = {  0, 1, 0, -1,  1, 1, -1, -1 };
      int i1, i2;
      i1 = i2 = -1;
      float min1 = std::numeric_limits<float>::max();
      float min2 = std::numeric_limits<float>::max();
      for(int i = 0; i < 8; ++i) {
	 const GridCoords c(start.x + dx[i], start.y + dy[i]);
	 if (grid.index(c.y, c.x) != neigh) {
	    const float l2 = start.squared_distance(goal);
	    const float t = (float(c.x - start.x) * float(goal.x - start.x) + float(c.y - start.y) * float(goal.y - start.y)) / l2;
	    float d;
	    if (t < 0) {
	       d = c.squared_distance(start);
	    } else if (t > 1) {
	       d = c.squared_distance(goal);
	    } else {
	       d = c.squared_distance(GridCoords(start.x + t * (goal.x - start.x), start.y + t * (goal.y - start.y)));
	    }
	    if (i1 == -1) {
	       i1 = i; min1 = t;
	    } else if (i2 == -1) {
	       i2 = i; min2 = t;
	    } else if (t < min1) {
	       if (min1 < min2) { min2 = min1; i2 = i1; }
	       min1 = t; i1 = i;
	    } else if (t < min2) {
	       if (min2 < min1) { min1 = min2; i1 = i2; }
	       min2 = t; i2 = i;
	    }
	 }
      }
      const int idx1 = s + dx[i1] + grid.NCOLS * dy[i1];
      const int idx2 = s + dx[i2] + grid.NCOLS * dy[i2];
      if (g == vis[idx1] and g == vis[idx2]) 
      {
	 ret = true;
      } else { //check visibility using bresenham
	 GridCoordsVector pts;
	 bresenham(start, goal, pts, false, true);
	 ret = isObstacleFree(pts, map, FREESPACE); 
      }
   }
   return ret;
}

/* end of distance_transform_exact_euclidean.cc */
