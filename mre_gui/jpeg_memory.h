/*
 * File name: jpeg_memory.h
 * Date:      2011/10/13 10:14
 * Author:    Jan Faigl
 */

#ifndef __JPEG_MEMORY_H__
#define __JPEG_MEMORY_H__

#include <jpeglib.h>

namespace mre {
   void jpeg_mem_dest (j_compress_ptr cinfo,  unsigned char** outbuffer, unsigned long* outsize);
} //end namespace mre

#endif

/* end of jpeg_memory.h */
