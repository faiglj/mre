/*
 * File name: mre_strategy_hungarian.h
 * Date:      2012/02/13 12:04
 * Author:    Jan Faigl
 */

#ifndef __MRE_STRATEGY_HUNGARIAN_H__
#define __MRE_STRATEGY_HUNGARIAN_H__

#include "mre_types.h"

namespace mre {

   class CHungarian {
      public:
         CHungarian();
         ~CHungarian();

         IntVector& assign(const DoubleMatrix& costMatrix, IntVector& goalsIDX);
   };

} //end namespace mre

#endif

/* end of mre_strategy_hungarian.h */
