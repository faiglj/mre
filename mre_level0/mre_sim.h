/*
 * File name: mre.h
 * Date:      2012/01/23 21:50
 * Author:    Jan Faigl
 */

#ifndef __MRE_H__
#define __MRE_H__

#include <vector>

#include <imr_config.h>

#include "sdl_gui.h"
#include "mre_sdl_gui.h"
#include "map.h"

#include "mre_ranger_sim.h"
#include "path_planning.h"
#include "results_saver.h"

namespace mre {

   class CMRESim {
      typedef enum { GREEDY_ALLOC_RAND = 1, PAIRS = 2, HUNGARIAN = 3, MTSP_KMEANS = 5, HUNGARIAN_DEPOT = 15, MTSP_KMEANS_DEPOT = 16, HUNGARIAN_DEPOT2 = 17, MIN_POS = 18} TStrategy;
      typedef enum { ALL_FRONTIERS = 0, KMEANS_ICRA11 = 1, REPRESENTATIVE = 2, GOAL_SET_COVERAGE = 3, COMPLETE_COVERAGE = 4, COVERING_CELLS = 5, COVERING_CELLS_SAMPLES = 6, COMPLETE_COVERAGE_FOV = 7, COMPLETE_COVERAGE_2 = 8, KMEANS_ARN = 9 } TGoals;
      private:
      imr::CConfig& cfg;
      CMap* map;
      CRangerSim* rangerSim;
      CPathPlanning* paths;
      SResults results;
      gui::CSDL sdl;
      gui::CGui* gui;
      const double LASER_RANGE_MAX;
      const int IMAGE_QUALITY;
      const TStrategy STRATEGY;
      const TGoals GOALS_SELECTION;
      const int MAX_NODES_EXECUTION;
      const int MIN_NUMBER_OF_FRONTIER_CELLS;
      const bool ENABLE_PATH_LENGTH_CHECK;
      const double PATH_LENGTH_CHECK_OFFSET;
      const bool USE_THETA_STAR;
      const bool OMNIDIRECTIONAL;
      const double NBV_LAMBDA;
      const bool GUI_DRAW_GOALS;
      const bool GUI_DRAW_ALL_GOALS;
      const bool RETURN_DEPOT;
      const bool FORCE_ASSIGN_SAME_GOALS;
      const bool CONTINUOUS_SENSING;
      RobotPtrVector robots;

      public:
         static imr::CConfig& getConfig(imr::CConfig& config);
         CMRESim(imr::CConfig& cfg);
         ~CMRESim();

         void execute(void);

      private:
         static TStrategy getStrategy(const std::string& str);
         static std::string getStrategyStr(const TStrategy strategy);
         static TGoals getGoals(const std::string& str);
         static std::string getGoalsStr(const TGoals goals);
         void simulation_control(void);

         void updateMap(RobotPtrVector& robots, double robotRadius, double laserRange);
	 GridCoordsVectorVector& getGoalsSets(const FrontierVector& frontiers, int nbrObjects, GridCoordsVectorVector& goalSets);
         GridCoordsVector& getGoals(const RobotPtrVector& robots, FrontierVector& frontiers, GridCoordsVector& goalGridPts, GridCoordsVectorVector& robotsGoalGridPts, IntVector& coveringCosts);

         void setRobotShape(SRobot& robot, double r) const;
         void setRobotShapeGrid(SRobot& robot, int c) const;

	 void getFrontiersContour(const RobotPtrVector& robots, const FrontierVector& frontiers, GridCoordsVector& goalGridPts);

         void savePic(int iter);

         void setRobotPose();

         void saveState(const std::string& filename);
         void loadState(const std::string& filename);
   };

} //end namespace mre

#endif

/* end of mre_sim.h */
