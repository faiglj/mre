#!/bin/sh

if [ `uname` = FreeBSD ]
then
   mcmd=gmake
else
   mcmd=make
fi

$mcmd -C src clean
$mcmd -C src 
rm -rf src/test.o src/libevg-thin.a ; ar rcs src/libevg-thin.a src/*.o 

PLATFORM=`uname -p`
ARCH=.$PLATFORM

LIBRARY_PREFIX=../../lib
INCLUDE_PREFIX=../../include

mkdir -p $LIBRARY_PREFIX $INCLUDE_PREFIX
cp src/libevg-thin.a $LIBRARY_PREFIX/
cp src/*.hh $INCLUDE_PREFIX/

$mcmd -C src clean
