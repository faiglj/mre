/*
 * File name: robot_player.h
 * Date:      2009/06/27 12:31
 * Author:    Jan Faigl
 */

#ifndef __ROBOT_PLAYER_H__
#define __ROBOT_PLAYER_H__

#include <libplayerc++/playerc++.h>

#include <boost/thread/barrier.hpp>

#include <concurrent/thread.h>
#include <imr_config.h>

#include "mre_types.h"
#include "mre_robot.h"

#include "laser_data.h"
#include "laser_config.h"

#include "map.h"

#include "mre_robot.h"
#include "mre_events.h"
#include "path_follower.h"

/*
      PathNodeVector plan;
      GridCoords depot;
      bool approaching_depot;
      bool docked;
      double travelledGridDistance;
*/

namespace mre {

   class CRobotPlayer : public imr::concurrent::CThread {
      typedef imr::concurrent::CThread ThreadBase;
      CMap& map;

      public:
      SRobot robot;
      private:

      imr::CConfig cfg;
      PlayerCc::PlayerClient* client;
      bool quit;
      PlayerCc::LaserProxy* laser;
      PlayerCc::Position2dProxy* position2d;
      PlayerCc::Position2dProxy* globalPosition2d;
      PlayerCc::Position2dProxy* snd;
      EventQueue& queue;
      SPosition poseSaved;
      GridCoords gridPoseSaved;
      //SPosition pose; in robot
      //GridCoords gridPose in robot
      // GridCoordsVector pathSimple in robot
      //IntVector robotShape; in robot
      SLaserData laserData;
      Mutex mtx;
      Condition cond;
      Mutex poseMtx;
      boost::barrier* waitForDataBarrier;
      GridCoordsVector travelledPath;
      CPathFollower *pathFollower;
      double travelledDistance;
      int executionSteps;
      bool connected;


      public:
      static imr::CConfig& getConfig(imr::CConfig& config);

      CRobotPlayer(const std::string& cfgFilename, int label, CMap& map, EventQueue& queue, boost::barrier* waitForDataBarrier);
      ~CRobotPlayer();

      void stop(void);
      void savePoses();
      const SPosition getCurrentPosition(void) const { return robot.pose; }
      const SPosition& getPosition(void) const { return poseSaved; }
      const IntVector& getShape(void) const { return robot.shape; }
      const GridCoords& getGridPose(void) const { return gridPoseSaved; }
      CPathFollower* getPathFollower(void) { return pathFollower; }
      double getTravelledDistance(void) { return travelledDistance; }
      void addTravelledPath(GridCoordsVector& path);
      void setShape(double r);
      int getExecutionSteps(void) { return executionSteps;}
      SLaserConfig getLaserConfig();

      double getGoalRemainingDistance(void) const;

      bool waitForConnection(void);
      protected:

      void threadBody(void);

      private:
      void connect(void);
      void disconnect(void);
   };

   typedef std::vector<CRobotPlayer*> RobotPlayerPtrVector;
}

#endif

/* end of robot_player.h */
