/*
 * File name: goals.cc
 * Date:      2012/08/06 13:41
 * Author:    Jan Faigl
 */

#include <algorithm>
#include <limits>
#include <boost/foreach.hpp>

#include <logging.h>
#include <random.h>
#include <colors.h>
#include <perf_timer.h>

#include "mre_grid_utils.h"
#include "distance_transform.h"

#include "goals.h"

#define foreach BOOST_FOREACH

using imr::logger;
using namespace mre;

class CCoordsLess {
   public:
      CCoordsLess(const GridCoords& ct) : ct(ct) {}
      bool operator()(const GridCoords& a, const GridCoords& b) const {
         return ct.squared_distance(a) < ct.squared_distance(b);
      }
      const GridCoords& ct;
};

static IntVector scanArea2;
static GridCoords scanAreaCenter;
static bool scanAreaDone = false;

/// ----------------------------------------------------------------------------
static void createPermutation(int number, IntVector& permutation) {
   permutation.clear(); 
   for (int i = 0; i < number; i++) { 
      permutation.push_back(i); 
   }
}

/// ----------------------------------------------------------------------------
static void permute(IntVector& permutation) {
   int k, tmp;
   imr::CRandom::randomize();
   for (int i = permutation.size(); i > 0; --i) {
      k = imr::CRandom::random()%i;
      tmp = permutation[i-1];
      permutation[i-1] = permutation[k];
      permutation[k] = tmp;
   }
}

/// ----------------------------------------------------------------------------
static IntVectorVector& kmeans(const GridCoordsVector& goals, GridCoordsVector& means, IntVectorVector& cluster, int MAX_ITER = 5) {
   const int N = goals.size();
   const int M = means.size();

   int step = 0;
   bool changed = true;
   IntVector group(N, -1); //
   while (changed) {
      DoubleVector count(M, 0.0);
      step++;
      changed = false;
      int k = 0;
      GridCoordsVector centers(M, GridCoords(0, 0));
      foreach(const GridCoords& g, goals) {
         int min = std::numeric_limits<int>::max();
         int idx = -1;
         for(int m = 0; m < means.size(); ++m) {
            int xx = g.x - means[m].x;
            int yy = g.y - means[m].y;
            const int dd = xx*xx + yy*yy;
            if ( dd < min ) { min = dd; idx = m; }
         }
         ASSERT_ARGUMENT(idx != -1, "best mean not found");
         centers[idx].y += g.y;
         centers[idx].x += g.x;
         count[idx] += 1.0;
         if (group[k] != idx) {
            group[k] = idx;
            changed = true;
         }
         k++;
      }
      for(int j = 0; j < centers.size(); ++j) {
         if (count[j] > 0) {
            means[j].x = (int)round(1.0 * centers[j].x / count[j]);
            means[j].y = (int)round(1.0 * centers[j].y / count[j]);
         }
      }
      if (step == MAX_ITER) { changed = false; } //force quit
   } //end loop while changed
   cluster.clear();
   cluster.resize(M, IntVector());
   for(int r = 0; r < M; r++) {
      for(int g = 0; g < goals.size(); ++g) {
         if (group[g] == r) {
            cluster[r].push_back(g);
         }
      }
   }
   return cluster;
}

/// ----------------------------------------------------------------------------
/// - class CGoals -------------------------------------------------------------
/// ----------------------------------------------------------------------------

/// - static method ------------------------------------------------------------
ByteMatrix& CGoals::getPlanningGrid(const CMap& map, ByteMatrix& grid) {
   const ByteMatrix& navGrid = map.getNavGrid();
   ASSERT_ARGUMENT(grid.SIZE == navGrid.SIZE, "Grids must have some size");
   for(int i = 0; i < navGrid.SIZE; ++i) {
     // grid(i) = (navGrid(i) != CMap::FREESPACE_CELL ? CDistanceTransform::OBSTACLE : CDistanceTransform::FREESPACE);
      grid(i) = (navGrid(i) != CMap::FREESPACE_CELL ? 0 : 255);
   }
   return grid;
}

/// - static method ------------------------------------------------------------
GridCoordsVectorVector& CGoals::findPath(const ByteMatrix& grid, const GridCoords& start, const GridCoordsVector& goals, GridCoordsVectorVector& paths) {
   if (!goals.empty()) {
      paths.resize(goals.size(), GridCoordsVector());
      CDistanceTransform* dt = new CDistanceTransform(grid);
      dt->setGoal(start);
      for(int i = 0; i < goals.size(); ++i) {
         GridCoordsVector pathDT;
         //  DEBUG("path from : " << start.x << " " << start.y << " -> " << goals[i].x << " " << goals[i].y);
         dt->getPath(goals[i], pathDT);
         std::reverse(pathDT.begin(), pathDT.end());
         simplify2(pathDT, grid, paths[i], 10);
      }
      delete dt;
   } else {
      paths.clear();
   }
   return paths;
}

/// - static method ------------------------------------------------------------
GridCoordsVector& CGoals::findPath(const ByteMatrix& grid, const GridCoords& start, const GridCoords& goal, GridCoordsVector& path) {
   path.clear();
   CDistanceTransform* dt = new CDistanceTransform(grid);
   dt->setGoal(start);
   GridCoordsVector pathDT;
   dt->getPath(goal, pathDT);
   std::reverse(pathDT.begin(), pathDT.end());
   simplify2(pathDT, grid, path, 10);
   delete dt;
   return path;
}

/// - constructor --------------------------------------------------------------
CGoals::CGoals(const CMap& map, const GridCoordsVectorVector& goalSetsI, const RobotPlayerPtrVector& robots, double visibilityRangeRatio, bool computeCoverage, gui::CGui* gui) : 
   gui(gui), map(map), grid(map.H, map.W), 
   RANGE_RATIO(visibilityRangeRatio),
   LASER_RANGE(visibilityRangeRatio * map.getMaxLaserRange())
{
   imr::CPerfTimer t("CGoals::CGoals in");
   const ByteMatrix& navGrid = map.getNavGrid();
   for(int i = 0; i < navGrid.SIZE; ++i) {
      //grid(i) = (navGrid(i) != CMap::FREESPACE_CELL ? CDistanceTransform::OBSTACLE : CDistanceTransform::FREESPACE);
      grid(i) = (navGrid(i) != CMap::FREESPACE_CELL ? 0 : 255);
   }
   foreach(const GridCoordsVector& goals, goalSetsI) {
      foreach(const GridCoords& pt, goals) {
        // ASSERT_ARGUMENT(grid(pt.y, pt.x) == CDistanceTransform::FREESPACE, "Goal must be in freespace");
         ASSERT_ARGUMENT(grid(pt.y, pt.x) == 255, "Goal must be in freespace");
      }
   }
  DEBUG("No. of goal sets: " << goalSetsI.size());

   std::vector<CDistanceTransform*> dts; 
   const bool CHECK_REACHABILITY = false;
   if (CHECK_REACHABILITY) {
      for(int r = 0; r < robots.size(); ++r) {
         const CRobotPlayer* robot = robots[r];
         dts.push_back(new CDistanceTransform(grid));
         dts.back()->setGoal(robot->getGridPose());
      }
   }
   foreach(const GridCoordsVector& pts, goalSetsI) {
      GridCoords ct(0,0);
      int offset = goals.size();
      GoalPtrVector gt;
      foreach(const GridCoords& pt, pts) {
         bool reachable = true;
         if (CHECK_REACHABILITY) {
            foreach(CDistanceTransform* dt, dts) {
               GridCoordsVector path;
               dt->getPath(pt, path);
               if (path.empty()) {
                  reachable = false;
                  break;
               }
            }
         }
         if (reachable) {
            goals.push_back(new SGoal(pt, goals.size(), allGoals.size(), grid.index(pt.y, pt.x), goalSets.size()));
            allGoals.push_back(goals.back());
            gt.push_back(goals.back());
            ct.x += pt.x; ct.y += pt.y;
         }
      }
      if (gt.size() > 0) {
         ct.x = int(round(ct.x * 1.0 / gt.size()));
         ct.y = int(round(ct.y * 1.0 / gt.size()));
         {
            GridCoordsVector pts;
            bresenham(ct, gt[gt.size() / 2]->coords, pts, false, true);
            ct = pts[pts.size() / 2];
         }
         SGoal* representative = 0;
         bool done = false;
         //if (grid(ct.y, ct.x) == CDistanceTransform::FREESPACE) {
         if (grid(ct.y, ct.x) == 255) {
            //check if the new representative is reachable
            bool reachable = true;
            if (CHECK_REACHABILITY) {
               foreach(CDistanceTransform* dt, dts) {
                  GridCoordsVector path;
                  dt->getPath(ct, path);
                  if (path.empty()) {
                     reachable = false;
                     break;
                  }
               }
            }
            if (reachable) {
               representative = new SGoal(ct, representatives.size(), allGoals.size(), grid.index(ct.y, ct.x));
               representatives.push_back(representative);
               allGoals.push_back(representatives.back());
               done = true;
            }
         }
         if (!done) { // use mid point of the goal set
            // DEBUG("goalSet[" << goalSets.size() << "] uses midpoint");
            representative = gt[gt.size() / 2];
            ct = representative->coords;
         }
         goalSets.push_back(new SGoalSet(representative, goalSets.size()));
         for(int i = offset; i < goals.size(); ++i) {
            goalSets.back()->goals.push_back(goals[i]);
         }
      }
   }
   foreach(CDistanceTransform* dt, dts) { delete dt; } //cleanup dt
   DEBUG("goal sets: " << goalSets.size());
   DEBUG("goals: " << goals.size());
   DEBUG("grid size: " << grid.NCOLS << "x" << grid.NROWS);

   /*
   if (gui) {
      imr::gui::CColors colors;
      gui->drawGrid(grid);
      gui->drawGrid255(map.getGrid());
      foreach(const SGoalSet* goalSet, goalSets) {
         SRGB color = gui->getColor(colors.next());
         foreach(const SGoal* goal, goalSet->goals) {
            gui->draw(goal->coords, color);
         }
      }
         gui->savePNG("frontiers-freeedges.png", 5);
      DEBUG("goalSet");
      gui->refresh(); getchar();
   }
   */


   if (computeCoverage) {
     computeCoveringCells();
   }
}

/// - destructor ---------------------------------------------------------------
CGoals::~CGoals() {
   foreach(SGoal* g, allGoals) { delete g; }
   foreach(SGoalSet* goalSet, goalSets) { delete goalSet; }
   foreach(SCell* cell, cells) { if (cell) delete cell; }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsRepresentatives(GridCoordsVector& newGoals) {
   newGoals.clear();
   foreach(const SGoalSet* goalSet, goalSets) {
      newGoals.push_back(goalSet->representative->coords);
   }
   /*
      if (gui) {
      imr::gui::CColors colors;
      gui->drawGrid(grid);
      foreach(const SGoalSet* goalSet, goalSets) {
      SRGB color = gui->getColor(colors.next());
      foreach(const SGoal* goal, goalSet->goals) {
      gui->draw(goal->coords, color);
      }
      gui->drawDisk(goalSet->representative->coords, 4, color);
      }
      DEBUG("getGoalsRepresentatives newGoals: " << newGoals.size());
      gui->refresh(); getchar();
      }
      */
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsGoalSetCoverage(GridCoordsVector& newGoals) {
   const int W = map.W;
   newGoals.clear();
   foreach(SGoal* goal, goals) {  goal->covered = false; }
   foreach(const SGoalSet* goalSet, goalSets) {
      IntVector coverage(grid.SIZE, 0);
      int best = -1;
      int max = 0;
      foreach(const SGoal* goal, goalSet->goals) {
         foreach(int i, goal->coveringCells) {
            coverage[i]++;
            if (coverage[i] > max) {
               best = i; max = coverage[i];
            }
         }
      }
      if (best != -1) {
         newGoals.push_back(GridCoords(best%W, best/W));
         /*
            if (gui) {
            gui->drawGrid(grid);
            foreach(const SGoal* goal, goalSet->goals) { gui->draw(goal->coords, SRGB(255, 0, 0)); }
            DEBUG("goals in the set: " << goalSet->goals.size() << " coverage: " << max);
            gui->drawDisk(newGoals.back(), 4, SRGB(255, 215, 0)); //gold1
            gui->refresh(); getchar();
            }*/
      }
   }
   /*
   if (gui) {
      imr::gui::CColors colors;
      gui->drawGrid(grid);
      foreach(const SGoalSet* goalSet, goalSets) {
         SRGB color = gui->getColor(colors.next());
         foreach(const SGoal* goal, goalSet->goals) {
            gui->draw(goal->coords, color);
         }
         gui->drawDisk(goalSet->representative->coords, 4, color);
      }
      DEBUG("Goal cells");
      gui->refresh(); getchar();
      foreach(const GridCoords& pt, newGoals) {
         gui->drawDisk(newGoals, 4, SRGB(0, 255, 255));
      }
      gui->refresh(); getchar();
   }*/
   DEBUG("getGoalsGoalSetCoverage newGoals: " << newGoals.size());
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getGoalsCompleteCoverage(GridCoordsVector& newGoals, IntVector& goalsCoverage) {
   imr::CPerfTimer t("CGoals::getGoalsCompleteCoverage in");
   const int W = map.W;
   newGoals.clear(); 
   goalsCoverage.clear();
   foreach(SGoal* goal, goals) {  goal->covered = false; }
   bool hasUncovered = true;
   BoolVector used(grid.SIZE, false);
   int nbrCovered = 0;

   IntVector activeCells;
   foreach(const SCell* cell, cells) {
      if (cell) { activeCells.push_back(cell->index); }
   }

   while(hasUncovered) {
      const SCell* best = 0;
      int max = 0;
      IntVector coverage(grid.SIZE, 0);
      //   foreach(const SCell* cell, cells) 
      for(int i = 0; i < activeCells.size(); ++i) {
         const SCell* cell = cells[activeCells[i]];
         if (cell and !used[cell->index]) {
            //           DEBUG("cell: " << cell->coveredGoals.size() << "/" << activeCells.size());
            int c = 0;
            foreach(const SGoal* goal, cell->coveredGoals) { 
               if (!goal->covered) { 
                  c++; 
               } 
            }
            if (c > max) { max = c; best = cell; }
         }
      }
      //      DEBUG("next best, coverage: " << max);
      if (best) {
         newGoals.push_back(GridCoords(best->index%W, best->index/W));
         goalsCoverage.push_back(max);
         used[best->index] = true;
         foreach(SGoal* goal, best->coveredGoals) { 
            if (!goal->covered) { nbrCovered++; }
            goal->covered = true; 
         }
      }
      DEBUG("Covered " << nbrCovered << "/" << goals.size());
      hasUncovered = false;
      foreach(const SGoal* goal, goals) { 
         if (!goal->covered) {
            hasUncovered = true;
            break;
         }
      }
   } //end while hasUncovered

   /*
   if (gui) {
      foreach(const GridCoords& pt, newGoals) {
         gui->drawDisk(pt, 4, SRGB(255, 0, 255));
      }
      DEBUG("getGoalsCompleteCoverage newGoals: " << newGoals.size());
      gui->refresh(); getchar();
   }
   */

   /*
   if (gui) { // draw all covering cells
      imr::gui::CColors colors;
      gui->drawGrid255(map.getGrid());
      foreach(const SGoalSet* goalSet, goalSets) {
         SRGB color = gui->getColor(colors.next());
         foreach(const SGoal* goal, goalSet->goals) {
            gui->draw(goal->coords, color);
         }
      }
      foreach(const GridCoords& pt, newGoals) {
         gui->drawDisk(pt, 3, SRGB(0, 255, 255));
      }
      gui->savePNG("goal_candidates.png", 5);
      DEBUG("newGoals: " << newGoals.size());
      gui->refresh(); getchar();
   }
   */

   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getCoveringCells(GridCoordsVector& newGoals, IntVector& goalsCoverage) {
   const int W = map.W;
   newGoals.clear(); 
   goalsCoverage.clear();
   for(int i = 0; i < cells.size(); ++i) {
      if (cells[i]) {
	 newGoals.push_back(GridCoords(cells[i]->index%W, cells[i]->index/W));
	 goalsCoverage.push_back(cells[i]->coveredGoals.size());
      }
   }
   DEBUG("getCoveringCells cells: " << newGoals.size());
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getCoveringCellsRandom(double samplesSigma, GridCoordsVector& newGoals, IntVector& goalsCoverage) {
   const int W = map.W;
   const int N = map.W * map.H;
   newGoals.clear(); 
   goalsCoverage.clear();

   foreach(const SGoalSet* goalSet, goalSets) {
      IntVector covering(N, 0);
      foreach(const SGoal* goal, goalSet->goals) {
	 foreach(int i, goal->coveringCells) { covering[i]++; }
      }
      IntVector coveringCandidates;
      for(int i = 0; i < N; ++i) {
	 if (covering[i] > 0) { coveringCandidates.push_back(i); }
      }
      IntVector perm;
      createPermutation(coveringCandidates.size(), perm);
      permute(perm);
      //      const int n = goalSet->goals.size() * samples < coveringCandidates.size() ? goalSet->goals.size() * samples : coveringCandidates.size();
      int n = round(goalSet->goals.size() * samplesSigma);
      if (n == 0) { n = 1; }
      // < coveringCandidates.size() ? goalSet->goals.size() * samples : coveringCandidates.size();
      DEBUG("samplesSigma: " << samplesSigma << " goals set goals: " << goalSet->goals.size() << " n: " << n);
      for(int i = 0; i < n; ++i) {
	 // DEBUG("coveringCandidates: " << coveringCandidates.size());
	 const int idx = coveringCandidates[perm[i]];
	 const GridCoords pt(idx%W, idx/W);
	 IntVector coveredCells;
	 ASSERT_ARGUMENT(cells[idx] and cells[idx]->index == idx, "Wrong candidate");
	 { //determine candidate coverage
	    BoolVector covered(grid.NROWS * grid.NCOLS, false);
	    CharVector visible(covered.size(), 0);
	    const int W = map.W;
	    const int H = map.H;
	    const char COVERING = 1;
	    const char NOT_COVERING = 2;
	    const char UNKNOWN = 0;
	    const ByteMatrix& navGrid = map.getNavGrid();
	    IntVector goalArea;
	    foreach(int idx, scanArea2) {
	       const int x = idx%W - scanAreaCenter.x + pt.x;
	       const int y = idx/W - scanAreaCenter.y + pt.y;
	       const int scanIDX = y * map.W  + x;
	       if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H) {
		  goalArea.push_back(scanIDX);
	       }
	    }
	    // end preparing goal area - cells candidates from which goal can be covered

	    foreach(int i, goalArea) {
	       if (visible[i] == UNKNOWN) { //check if goal cell is visible from the i-th cell
		  GridCoordsVector beam;
		  const GridCoords endPt(i%W, i/W);
		  bresenham(pt, endPt, beam);
		  bool obstacle = false;
		  for(int j = 0; j < beam.size(); ++j) {
		     const GridCoords& pt = beam[j];
		     const unsigned char cell = navGrid(pt.y, pt.x);
		     if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
			if (cell == CMap::UNKNOWN_CELL and !obstacle) {
			   visible[beam[j].y * W + beam[j].x] = COVERING; 
			} else if (cell == CMap::FREESPACE_CELL) {
			   visible[beam[j].y * W + beam[j].x] = NOT_COVERING; 
			} else {
			   obstacle = true;
			   visible[beam[j].y * W + beam[j].x] = NOT_COVERING; 
			}
		     }
		  }
		  if (visible[i] == COVERING) { coveredCells.push_back(i); }
	       } else if (visible[i] == COVERING) {
		  coveredCells.push_back(i);
	       }
	    } //end processing all cells from the goalArea
	    // for(int i = 0; i < visible.size(); ++i) { visible[i] = UNKNOWN; } //reset visible 

	   /*
	   if (gui) { // draw all covering cells
	      imr::gui::CColors colors;
	      gui->drawGrid255(map.getGrid());
	      foreach(const SGoalSet* goalSet, goalSets) {
		 SRGB color = gui->getColor(colors.next());
		 foreach(const SGoal* goal, goalSet->goals) {
		    gui->draw(goal->coords, color);
		 }
	      }
	      gui->draw(coveredCells, SRGB(0, 150, 100));
	      gui->drawDisk(pt, 3, SRGB(255, 0, 0));
	      DEBUG("candidate");
	      gui->refresh(); getchar();
	   }
	   */
	} //end determine candidate coverage
	newGoals.push_back(GridCoords(idx%W, idx/W));
	goalsCoverage.push_back(coveredCells.size());
      }
   }
   //  DEBUG("newGoals: " << newGoals.size());


   /* simple sampling of all covering cells
      IntVector cellsIDX;
      for(int i = 0; i < cells.size(); ++i) {
      if (cells[i]) {
      cellsIDX.push_back(i);
      }
      }
      int n = samples * goals.size();
      if (cellsIDX.size() < n) {
      for (int i = 0; i < cellsIDX.size(); ++i) {
      const int idx = cellsIDX[i];
      newGoals.push_back(GridCoords(cells[idx]->index%W, cells[idx]->index/W));
      goalsCoverage.push_back(cells[idx]->coveredGoals.size());
      }
      } else {
      IntVector perm;
      createPermutation(n, perm);
      permute(perm);
      for(int i = 0; i < perm.size(); ++i) {
      const int idx = cellsIDX[perm[i]];
      newGoals.push_back(GridCoords(cells[idx]->index%W, cells[idx]->index/W));
      goalsCoverage.push_back(cells[idx]->coveredGoals.size());
      }
      }
      DEBUG("getCoveringCellsRandom cells: " << newGoals.size());
      */
   return newGoals;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CGoals::getAlternateGoals(const GridCoords& robotPose, const GridCoordsVector& goals, GridCoordsVector& alternateGoals) const {
   CDistanceTransform* dt = new CDistanceTransform(grid);
   dt->setGoal(robotPose);
   alternateGoals.clear();
   foreach(const GridCoords& goal, goals) {
      GridCoordsVector pathDT;
      GridCoordsVector pathDTSimpl;
      GridCoordsVector pathDTSimplFull;
      dt->getPath(goal, pathDT);
      simplify2(pathDT, grid, pathDTSimpl, 10);
      if (pathDTSimpl.size() > 1) {
	 const GridCoords& ptStart = pathDTSimpl[0];
	 const GridCoords& ptEnd = pathDTSimpl[1];
	 GridCoordsVector path;
	 bresenham(ptStart, ptEnd, path, true, true);
	 int i = 0;
	 SPosition p1;
	 SPosition p2;
	 while(i < path.size() and sqrt(map.grid2real(path[i], p1).squared_distance(map.grid2real(ptEnd, p2))) > LASER_RANGE) {
	    i++;
	 }
	 alternateGoals.push_back(i < path.size() ? path[i] : ptEnd);
      } else {
	 alternateGoals.push_back(goal);
      }
      //     DEBUG("alternate goal for : " << robotPose.x << " " << robotPose.y << " -> " << alternateGoals.back().x << " " << alternateGoals.back().y << " prev goal " << goal.x << " " << goal.y);
   }
   delete dt;
   return alternateGoals;
}

/// - public method ------------------------------------------------------------
ByteMatrix& CGoals::getUnknownSpacePartition(const RobotPlayerPtrVector& robots, ByteMatrix& partition) {
   ASSERT_ARGUMENT(!robots.empty(), "At least one robot is need to determine the reachable freespace");
   const int H = grid.NROWS;
   const int W = grid.NCOLS;
   for(int i = 0; i < partition.SIZE; ++i) { partition[i] = -1; } // clear partition  
   const GridCoords& pt = robots.front()->getGridPose();

   const ByteMatrix& navGrid = map.getNavGrid();
   ByteMatrix ugrid(grid.NROWS, grid.NCOLS);
   IntVector u;
   for(int i = 0; i < grid.SIZE; ++i) {
      if (navGrid(i) == CMap::UNKNOWN_CELL or navGrid(i) == CMap::FREESPACE_CELL) {
         // ugrid(i) = CDistanceTransform::FREESPACE;
         ugrid(i) = 255;
      } else {
         //ugrid(i) = CDistanceTransform::OBSTACLE;
         ugrid(i) = 0;
      }
   }
   CDistanceTransform* dt = new CDistanceTransform(ugrid);
   dt->setGoal(pt);
   const int MAX = dt->getMaxValue();
   const DoubleMatrix& dGrid = dt->getDistanceGrid();
   GridCoordsVector unknownCells;
   for(int i = 0; i < grid.SIZE; ++i) {
      if (navGrid(i) == CMap::UNKNOWN_CELL and dGrid(i) < MAX) {
	 unknownCells.push_back(GridCoords(i%W, i/W));
	 u.push_back(i);
      }
   }

   //determine cluster using K-means
   GridCoordsVector means(robots.size(), GridCoords(W/2, H/2));
   IntVectorVector clusters;
   kmeans(unknownCells, means, clusters, 10);

   if (gui) {
      gui->drawGrid255(map.getGrid());
      DEBUG("getUnknownSpacePartition");
      gui->drawDisk(pt, 3, SRGB(0, 0, 255));
      imr::gui::CColors colors;
      foreach(const IntVector& cluster, clusters) {
	 SRGB color = gui->getColor(colors.next());
	 foreach(int i, cluster) {
	    gui->draw(unknownCells[i], color);
	 }
      }
      foreach(const GridCoords& ct, means) {
	 gui->drawDisk(ct, 5, SRGB(255, 250, 250));
      }
      gui->refresh(); getchar();
      //	gui->draw(u, SRGB(0, 255, 0));
      //      gui->refresh(); getchar();
   }
   delete dt;
   return partition;
}

/// - private method -----------------------------------------------------------
void CGoals::computeCoveringCells(void) {
   imr::CPerfTimer t("CGoals::computeCoveringCells in ");
   DEBUG("CGoals -- computeCoveringCells - start");
   //IntVector scanArea;
   //IntVector scanArea2;
   if (scanAreaDone == false) { //compute scan end points
      imr::CPerfTimer t("CGoals::computeCoveringCells - determin scan area");
      GridCoordsVector endPts;
      const int N = 2 * 361;
      const double da = 2 * M_PI / N; //ASSUME 360 degree laser scanner and 2*361 measurements
      SPosition ct(map.grid2realX(map.W / 2), map.grid2realY(map.H / 2));
      double a = 0;
      for(int i = 0; i < N; ++i) {
	 SGridCoords endPt(GridCoords(
		  map.real2gridX(ct.x + LASER_RANGE * cos(a)),
		  map.real2gridY(ct.y + LASER_RANGE * sin(a))
		  ));
	 if (endPts.empty()) {
	    endPts.push_back(endPt);
	 } else {
	    if (endPts.back().manhattan_distance(endPt) > 0) {
	       endPts.push_back(endPt);
	    }
	 }
	 a += da;
      }
      IntVector vx; 
      IntVector vy;
      IntVector scanArea;
      foreach(const GridCoords& pt, endPts) { vx.push_back(pt.x); vy.push_back(pt.y); }
      fillPolygon(vx, vy, map.W, scanArea);

      GridCoordsVector pts;
      scanAreaCenter.x = 0; scanAreaCenter.y = 0;
      foreach(int i, scanArea) { 
	 const GridCoords pt(i%map.W, i/map.H);
	 pts.push_back(pt); 
	 scanAreaCenter.x += pt.x;
	 scanAreaCenter.y += pt.y;
      }
      scanAreaCenter.x /= scanArea.size();
      scanAreaCenter.y /= scanArea.size();

      std::sort(pts.begin(), pts.end(), CCoordsLess(scanAreaCenter));
      std::reverse(pts.begin(), pts.end());
      foreach(const GridCoords& pt, pts) {
	 scanArea2.push_back(map.getIndex(pt));
      }

      /*
         if (gui) {
         imr::gui::CColors colors;
         gui->drawGrid(grid);
         foreach(const GridCoords& pt, pts) {
         gui->draw(pt, SRGB(255, 0, 0));
         gui->refresh(); 
         }
         getchar();
         }
         DEBUG("scanArea - no. of end points: " << vx.size());
         DEBUG("scanArea - no. of cells: " << scanArea.size());

         int max_x = map.real2gridX(ct.x + LASER_RANGE) - map.W / 2;
         int max_y = map.real2gridY(ct.y + LASER_RANGE) - map.H / 2;
         DEBUG("max_x: " << max_x << " max_y: " << max_y);
         GridCoordsVector circle;
         const GridCoords center(map.W / 2, map.H / 2);
         IntVector area(grid.NROWS * grid.NCOLS, 0);
         for(int r = 2; r < max_x; ++r) {
         imr::circle(center, r, circle);
         foreach(const GridCoords& pt, circle) { area[map.getIndex(pt)]++; }
         }
         int c = 0;
         IntVector area2;
         for(int i = 0; i < area.size(); ++i) { 
         if (area[i] > 0) { c++; 
         area2.push_back(i); 
         }
         }
         DEBUG("scanarea2: " << c);

         if (gui) {
         imr::gui::CColors colors;
         gui->drawGrid(grid);
         gui->draw(scanArea, SRGB(255, 0, 0));
         gui->refresh(); getchar();
         gui->draw(area2, SRGB(0, 255, 0));
         gui->refresh(); getchar();
         }
         getchar();
         */
      scanAreaDone = true;
   } //end compute scan end points

   //  const int centerIDX = (map.H / 2) * map.W + map.W / 2;
   const char UNKNOWN = 0;
   const char COVERING = 1;
   const char NOT_COVERING = 2;
   //const unsigned char FREESPACE = CDistanceTransform::FREESPACE;
   const unsigned char FREESPACE = 255;
   const int W = map.W;
   const int H = map.H;

   BoolVector covered(grid.NROWS * grid.NCOLS, false);
   CharVector visible(covered.size(), 0);

   for(int g = 0; g < goals.size(); ++g) {
      SGoal* goal = goals[g];
      const GridCoords& gt = goal->coords;
      IntVector goalArea;
      foreach(int idx, scanArea2) {
	 //const int x = idx%W - W / 2 + gt.x;
	 //const int y = idx/W - H / 2 + gt.y;
	 const int x = idx%W - scanAreaCenter.x + gt.x;
	 const int y = idx/W - scanAreaCenter.y + gt.y;
	 const int scanIDX = y * map.W  + x;
	 // if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == CDistanceTransform::FREESPACE) 
	 if (x >= 0 and y >= 0 and scanIDX >= 0 and scanIDX < grid.SIZE and x < W and y < H and grid(y, x) == 255) {
	    goalArea.push_back(scanIDX);
	 }
      }
      // end preparing goal area - cells candidates from which goal can be covered
      goal->coveringCells.clear();
      foreach(int i, goalArea) {
	 if (visible[i] == UNKNOWN) { //check if goal cell is visible from the i-th cell
	    GridCoordsVector beam;
	    const GridCoords endPt(i%W, i/W);
	    bresenham(endPt, gt, beam);
	    bool collisionFree = true;
	    int collisionPtIdx = beam.size();
	    for(int j = 0; j < beam.size(); ++j) {
	       const GridCoords& pt = beam[j];
	       if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
		  if (grid(pt.y, pt.x) != FREESPACE) {
		     collisionFree = false;
		     collisionPtIdx = j;
		     break;
		  } else {
		  }
	       } else {
		  break; //ray goes out of the map
	       }
	    } //end processing beam
	    if (collisionFree) {  //mark all the beam as covering
	       visible[i] = COVERING;
	       goal->coveringCells.push_back(i);
	       foreach(const GridCoords& pt, beam) { visible[pt.y * W + pt.x] = COVERING; }
	    } else { //mark from 0 to collisionPtIdx as NOT_COVERING
	       for(int j = 0; j <= collisionPtIdx; ++j) { visible[beam[j].y * W + beam[j].x] = NOT_COVERING; }
	    }
	 } else if (visible[i] == COVERING) {
	    goal->coveringCells.push_back(i);
	 }
      } //end processing all cells from the goalArea
      for(int i = 0; i < visible.size(); ++i) { visible[i] = UNKNOWN; } //reset visible 
      //     DEBUG("Process goal: " << g << "/" << goals.size() << " coveringCells: " << goal->coveringCells.size());
   } //end all goals


   int nbrCells = 0;
   int c = 0;
   cells.clear();
   cells.resize(grid.SIZE, 0);
   IntVector activeCells;
   foreach(SGoal* goal, goals) {
      foreach(int i, goal->coveringCells) {
	 if (!cells[i]) {
	    cells[i] = new SCell(i);
	    activeCells.push_back(i);
	    nbrCells++;
	 }
	 cells[i]->coveredGoals.push_back(goal);
      }
      c += goal->coveringCells.size();
   }
   DEBUG("Total coveringCells: " << c << " no. of cells: " << nbrCells);

   /*
      if (gui) { // draw covering cells for a frontier
      imr::gui::CColors colors;
      gui->drawGrid255(map.getGrid());
      int idx = 10;
      SRGB color;
      for (int i = 0; i < 4; ++i) { color = gui->getColor(colors.next()); }
      const SGoal* goal = goals[idx];
      gui->draw(goal->coveringCells, color);
      gui->drawDisk(goal->coords, 3, SRGB(255, 0, 0));
      gui->savePNG("frontier-covering_edges.png", 5);
      gui->refresh(); getchar();
      }
      */

   /*
      if (gui) { // draw all covering cells
      imr::gui::CColors colors;
      gui->drawGrid255(map.getGrid());
      IntVector allCoveringCells;
      foreach(const SCell* cell, cells) { if (cell) { allCoveringCells.push_back(cell->index); } }
      gui->draw(allCoveringCells, SRGB(100, 50, 100));
      foreach(const SGoalSet* goalSet, goalSets) {
      SRGB color = gui->getColor(colors.next());
      foreach(const SGoal* goal, goalSet->goals) {
      gui->draw(goal->coords, color);
      }
      }
      DEBUG("goalSets: " << goalSets.size());
   //     gui->savePNG("all_covering_cells.png", 5);
   gui->refresh(); getchar();
   }
   */
}

/* end of goals.cc */
