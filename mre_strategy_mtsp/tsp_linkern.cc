/*
 * File name: tsp_linkern.cc
 * Date:      2015/07/25 12:41
 * Author:    Jan Faigl
 */

#include <cmath>
#include <boost/foreach.hpp>

#include <logging.h>

#include "tsp_linkern.h"

extern "C" {
#include <edgegen.h>
}

#define foreach BOOST_FOREACH

using namespace mre;

using imr::logger;

/// - constructor --------------------------------------------------------------
CTSPLinkern::CTSPLinkern() {
}

/// - destructor ---------------------------------------------------------------
CTSPLinkern::~CTSPLinkern() {
}

/// - public method ------------------------------------------------------------
IntVector& CTSPLinkern::solve(const int N, const DoubleVectorVector& costs, IntVector& sequence) {
   incycle = NULL;
   CCutil_init_datagroup (&dat);
   CCutil_dat_setnorm (&dat, CC_MATRIXNORM);

   CCutil_sprand ((int)CCutil_real_zeit(), &rstate); 

   dat.x = CC_SAFE_MALLOC (N, double);
   dat.y = CC_SAFE_MALLOC (N, double);
   incycle = CC_SAFE_MALLOC (N, int);

   fillWeights(N, costs);

   IntVector idx;
   for(int i = 0; i < N; ++i) {
      idx.push_back(i);
   }

   int outTemp[idx.size() + 1]; //+1 for size of the out that is stored in out[0]
   outTemp[0] = idx.size();

   solve(idx, &outTemp[0], 100 * N); //number iterations

   //retrive sequence
   for(int i = 1; i <= outTemp[0]; ++i) {
      sequence.push_back(outTemp[i]);
   }

   CC_IFFREE (incycle, int);
   CCutil_freedatagroup (&dat);		
   return sequence;
}

/// - public method ------------------------------------------------------------
void CTSPLinkern::setProblem(const IntVector& cities, const IntVectorVector& costs) {
   const int N = cities.size();
   incycle = NULL;
   CCutil_init_datagroup (&dat);
   CCutil_dat_setnorm (&dat, CC_MATRIXNORM);

   CCutil_sprand ((int)CCutil_real_zeit(), &rstate); 

   dat.x = CC_SAFE_MALLOC (N, double);
   dat.y = CC_SAFE_MALLOC (N, double);
   incycle = CC_SAFE_MALLOC (N, int);

   weights = costs;
}

/// - public method ------------------------------------------------------------
void CTSPLinkern::releaseProblem() {
   CC_IFFREE (incycle, int);
   CCutil_freedatagroup(&dat);
   incycle = NULL;
}

/// - public method ------------------------------------------------------------
void CTSPLinkern::solve(const IntVector& cities, int* sequence) {
   const int N = cities.size();
   IntVector idx;
   for(int i = 0; i < N; ++i) {
      idx.push_back(i);
   }
   solve(idx, &sequence[0], 100 * N); //number iterations
}

/// - private method -----------------------------------------------------------
void CTSPLinkern::fillWeights(const int N, const DoubleVectorVector& costs) {
   double max = 0;

   weights.clear();
   foreach(const DoubleVector& cost, costs) {
      foreach(double d, cost) {
	 if (max < d) {
	    max = d;
	 }
      }
   }
   const int16_t MAX_WEIGHT = (1 << 15) - 10;
   const double factor = MAX_WEIGHT / max;
   DEBUG("CTSPLinkern factor: " << factor);
   weights.resize(N, IntVector(N));
   for(int r = 0; r < N; ++r) {
      for(int c = 0; c < N; ++c) {
	 weights[r][c] = (int)round(costs[r][c] * factor);
      }
   }
}

/// - private method -----------------------------------------------------------
double CTSPLinkern::solve(IntVector idx, int* outcycle, int iter) {
   int* oc = new int [10000]; //TODO
   double val;
   uint num_nodes = idx.size();
   if (num_nodes <= 5) {
      outcycle[0] = num_nodes;
      for (int i=1;i<=num_nodes;i++) {
	 outcycle[i] = idx[i-1];
      }
      int tmp = evalSmallRing(outcycle);
      return tmp; 
   }

   dat.adj = CC_SAFE_MALLOC (num_nodes, int*);
   dat.adjspace = CC_SAFE_MALLOC ((num_nodes)*(num_nodes+1)/2,int);

   CCutil_sprand ((int)CCutil_real_zeit(), &rstate); 

   for (uint i=0; i < num_nodes; i++) {
      incycle[i] = i;	
   }
   for (uint i = 0, j = 0; i < num_nodes; i++)  {
      dat.adj[i] = dat.adjspace + j;
      j += (i+1);
   }  
   for (uint i = 0; i < num_nodes; i++) {
      for (uint j = 0; j <= i; j++) {
	 dat.adj[i][j] = weights[idx[i]][idx[j]];
      }
   }
   if (CCedgegen_junk_k_nearest (num_nodes, 4, &dat, // 8
	    (double *) NULL, 1, &tempcount, &templist, 1)) {
      DEBUG("CCedgegen_junk_k_nearest failed");
   }        
   CCutil_sprand ((int)CCutil_real_zeit(), &rstate); 
   if ( CClinkern_tour (num_nodes, &dat, tempcount, templist, 10000000,
	    (iter < 0 ? num_nodes : iter)/*iter*//*in_repeater*/, incycle, &oc[0], &val, 1/*run_silently*/,
	    10.0/*time_bound*/, -1.0/*length_bound*/, (char *) NULL, CC_LK_RANDOM_KICK/*kick_type*/,
	    &rstate) )
   {
      ERROR("CClinkern_tour failed");
   }
   outcycle[0] = num_nodes;
   for (int k = 1; k <= num_nodes; k++) {
      outcycle[k] = idx[oc[k-1]];
   }
   CC_IFFREE (templist, int);
   CC_IFFREE (dat.adjspace, int);
   CC_IFFREE (dat.adj, int*);
   delete[] oc;
   return val;		
}

/// - private method -----------------------------------------------------------
int CTSPLinkern::evalSmallRing(int* chromo) {
   int np[] = { 1, 1, 2, 6, 24, 120, 720 }; 
   IntVector ring;
   int min;
   int val;
   int best[10];

   smNumber = chromo[0]-1;
   for(int i = 1; i <= chromo[0]; i++) {
      if (chromo[i] != 0) { //depot
	 ring.push_back(chromo[i]);
      }
   }

   for(int i = 0; i < smNumber; i++) {
      smPerm[i] = i;
   }
   min = getSmallRingLength(ring);
   createChromoRing(ring, best);
   for(int i=1;i<np[smNumber];i++) {
      getNext();
      val = getSmallRingLength(ring);
      if (val < min) {
	 min = val;
	 createChromoRing(ring,best);
	 //               printChromo(best);
      }
   }

   for (int i = 0; i <= chromo[0]; i++) {
      chromo[i] = best[i];
   }
   return min;
}

/// - private method -----------------------------------------------------------
int CTSPLinkern::getSmallRingLength(IntVector& chromo) {
   int ch[10];
   createChromoRing(chromo, ch);
   return getLen(ch);
}

/// - private method -----------------------------------------------------------
void CTSPLinkern::createChromoRing(IntVector& chromo, int ch[10]) {
   ch[0] = smNumber + 1;
   ch[1] = 0; //depot;
   for(int i = 0; i < smNumber; i++) {
      ch[i+2] = chromo[smPerm[i]];
   }
}

/// - private method -----------------------------------------------------------
int CTSPLinkern::getLen(int* chromo)  {
   int len = 0;
   for(int j = 1; j < chromo[0]; j++) {
      len += weights[chromo[j]][chromo[j+1]];
   }
   len += weights[chromo[1]][chromo[chromo[0]]];
   return len;
}	

/// - private method -----------------------------------------------------------
void CTSPLinkern::swap(int a, int b) {
   int tmp = smPerm[a];
   smPerm[a] = smPerm[b];
   smPerm[b] = tmp;
}   

/// - private method -----------------------------------------------------------
void CTSPLinkern::getNext() {
   int i= smNumber - 1;
   int j = smNumber;

   while (smPerm[i-1] >= smPerm[i]) { i--; }
   while (smPerm[j-1] <= smPerm[i-1]) { j--; }
   swap(i-1,j-1);
   i++; 
   j=smNumber;
   while (i<j) {
      swap(i-1,j-1);
      i++;
      j--;
   }
}

/* end of tsp_linkern.cc */
