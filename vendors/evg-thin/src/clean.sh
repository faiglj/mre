#!/bin/sh

if [ `uname` = FreeBSD ]
then
   mcmd=gmake
else
   mcmd=make
fi

$mcmd clean
rm -rf libevg-thin.a
