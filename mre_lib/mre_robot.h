/*
 * File name: mre_robot.h
 * Date:      2015/08/15 18:36
 * Author:    Jan Faigl
 */

#ifndef __MRE_ROBOT_H__
#define __MRE_ROBOT_H__

#include "mre_types.h"

namespace mre {

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SRobot {
      const int LABEL;
      IntVector shape;
      GridCoords gridPose;
      SPosition pose;
      GridCoordsVector pathSimple;
      PathNodeVector plan;
      GridCoords depot;
      bool approaching_depot;
      bool docked;
      double travelledGridDistance;

      SRobot(int l) : LABEL(l) {}
      SRobot(const SRobot& r) : LABEL(r.LABEL) {
	 shape = r.shape;
	 gridPose = r.gridPose;
	 pose = r.pose;
	 pathSimple = r.pathSimple;
	 plan = r.plan;
	 depot = r.depot;
	 approaching_depot = r.approaching_depot;
	 docked = r.docked;
	 travelledGridDistance = r.travelledGridDistance;
      }
   };

   typedef std::vector<SRobot> RobotVector;
   typedef std::vector<SRobot*> RobotPtrVector;

} //end namespace mre

#endif

/* end of mre_robot.h */
