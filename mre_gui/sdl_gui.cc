/*
 * File name: sdl_gui.cc
 * Date:      2011/12/26 20:52
 * Author:    Jan Faigl
 */

#include <cmath>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

extern "C" {
#include <SDL_render.h>
#include <SDL_image.h>
#include <SDL2_rotozoom.h>
#include <jpeglib.h>

}
#include <logging.h>
#include <imr_assert.h>

#include "image_tools.h"

#include "sdl_gui.h"

using namespace mre::gui;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
static Uint32 rmask = 0xff000000;
static Uint32 gmask = 0x00ff0000;
static Uint32 bmask = 0x0000ff00;
static Uint32 amask = 0x000000ff;
#else
static Uint32 rmask = 0x000000ff;
static Uint32 gmask = 0x0000ff00;
static Uint32 bmask = 0x00ff0000;
static Uint32 amask = 0xff000000;
#endif

using imr::logger;

/// - static attributies -------------------------------------------------------
bool CSDL::initialized = false;
bool CSDL::videoInit = false;
int CSDL::sdl_init_counter = 0;

/// - static method ------------------------------------------------------------
SDL_Surface* CSDL::load_img(const std::string& filename, bool check) {
   SDL_Surface* ret = IMG_Load(filename.c_str());
   ASSERT_ARGUMENT(!check or ret, "Image '" + filename + "' fail!");
   return ret;
}

/// - static method ------------------------------------------------------------
SDL_Surface* CSDL::load_img(const std::string& filename, SDL_Surface** img) {
   if (*img) { SDL_FreeSurface(*img); }
   *img = load_img(filename, false); //disable check
   return *img;
}

/// - static method ------------------------------------------------------------
SDL_Surface* CSDL::load_img_raw(const std::string& filename, SDL_Surface** img, bool force3bpp) {
   if (*img) { SDL_FreeSurface(*img); *img = 0; }
   SImage* i = 0;
   if (is_raw_img(filename) and (i = mre::load_img_raw(filename))) {
      *img = SDL_CreateRGBSurface(0, i->w, i->h, force3bpp ? 24 : i->bpp*8, 0, 0, 0, 0);
      if (img) {
	 get_img_rgb(i->getImageBuffer(), i->size, *img);
      }
      delete i;
   }
   return *img;
}

/// - static method ------------------------------------------------------------
SDL_Surface* CSDL::get_img(const unsigned char* buf, const int size, SDL_Surface** img) {
   if (*img) { SDL_FreeSurface(*img); }
   *img = IMG_Load_RW(SDL_RWFromMem((void*)buf, size), 0);
   return *img;
}

/// - static method ------------------------------------------------------------
SDL_Surface* CSDL::get_img_rgb(const unsigned char* buf, const int size, SDL_Surface* img) {
   if (buf and size > 0) {
      ASSERT_ARGUMENT(img != 0, "get_img_rgb requires valid img!");
      ASSERT_ARGUMENT(img->format->BytesPerPixel == 3 or img->format->BytesPerPixel == 1, "get_img_rgb requires valid img with 1 or 3 bytes per pixel");
      const int l = img->w * img->h;
      ASSERT_ARGUMENT(l == size or l*3 == size or l == 3*size, "get_img_rgb buffer size does not much with the img size"); 
      if (img->format->BytesPerPixel == 3) {
	 if (size == l*3) { //copy bytes (*3 because img has 3 bytes per pixel)
	    for(int y = 0; y < img->h; ++y) {
	       for(int x = 0; x < img->w; ++x) {
		  const int idx = (y * img->w + x) * 3;
		  Uint8* s = (Uint8*)img->pixels + idx;
		  *(s + img->format->Rshift / 8) = buf[idx];
		  *(s + img->format->Gshift / 8) = buf[idx + 1];
		  *(s + img->format->Bshift / 8) = buf[idx + 2];
	       }
	    }
	 } else { //buf is grayscale image 
	    for(int y = 0; y < img->h; ++y) {
	       for(int x = 0; x < img->w; ++x) {
		  Uint8* s = (Uint8*)img->pixels + (y * img->w + x)*3;
		  const unsigned char c = buf[y * img->w + x];
		  for(int i = 0; i < 3; ++i) {
		     *s = c;
		     s += 1;
		  }
	       }
	    }
	 }
      } else { //img has 1 byte per pixel
	 Uint8* s = (Uint8*)img->pixels;
	 if (size == l) { //just copy 
	    for(int i = 0; i < size; i++) {
	       *s = buf[i];
	       s += 1;
	    }
	 } else { //convert to grayscale
	    for(int y = 0; y < img->h; ++y) {
	       for(int x = 0; x < img->w; ++x) {
		  const int idx = y * img->w + x;
		  const unsigned char* b = &buf[idx * 3];
		  *((Uint8*)img->pixels + idx) = rgb_to_gray(*b, *(b + 1), *(b + 2));
	       }
	    }
	    SDL_Color colors[256];
	    for(int i = 0; i < 256; i++){ colors[i].r = colors[i].g = colors[i].b = i; }
	    //ASSERT_ARGUMENT(SDL_SetColors(img, colors, 0, 256) == 1, "Cannot set color palette");
	    ASSERT_ARGUMENT(SDL_SetPaletteColors(img->format->palette, colors, 0, 256) == 1, "Cannot set color palette");
	 }
      }
   }
   return img;
}

/// - static method ------------------------------------------------------------
bool CSDL::save_img(const unsigned char* buf, const int size, const std::string& filename) {
   int fd = open(filename.c_str(), O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
   ASSERT_IO(fd != -1, "Cannot save image '" + filename + "'");
   ASSERT_IO(::write(fd, buf, size) == size, "Error during writting image to '" + filename + "'");
   return close(fd) == 0;
}

/// - static method ------------------------------------------------------------
bool CSDL::save_img_bmp(SDL_Surface* img, const std::string& filename) {
   ASSERT_IO(SDL_SaveBMP(img, filename.c_str()) == 0, "Cannot save image '" + filename + "'");
   return true;
}

/// - static method ------------------------------------------------------------
bool CSDL::save_img_jpeg(SDL_Surface* img, int quality, const std::string& filename) {
   bool ret = false;
   if (img) {
      const int size = img->w * img->h * 3;
      unsigned char* buf = new unsigned char[size];
      ASSERT_ARGUMENT(img->format->BytesPerPixel == 1 or img->format->BytesPerPixel == 3, "save_img_jpeg support only 1 or 3 bytes per pixel");
      if (img->format->BytesPerPixel == 3) {
	 for(int y = 0; y < img->h; y++) {
	    for(int x = 0; x < img->w; x++) {
	       Uint8* s = (Uint8*)img->pixels + (y * img->w + x)*3;
	       buf[(y*img->w + x)*3] = *(s + img->format->Rshift / 8);
	       buf[(y*img->w + x)*3 + 1] = *(s + img->format->Gshift / 8);
	       buf[(y*img->w + x)*3 + 2] = *(s + img->format->Bshift / 8);
	    }
	 }
      } else {
	 memcpy(&buf[0], img->pixels, size);
      }
      unsigned long jsize = 0;
      unsigned char* jbuf;
      jpeg_compress(buf, size, img->w, img->h, &jbuf, jsize, quality, img->format->BytesPerPixel);

      int fd = open(filename.c_str(), O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
      if (fd != -1 ) {
	 if (write(fd, &jbuf[0], jsize) != jsize) {
	    WARN("Error during writting image to " << filename);
	 } else {
	    ret = true;
	 }
	 ret = ret and close(fd) == 0;
      } else {
	 WARN("Cannot save image to " << filename);
      }
      free(jbuf);
      delete[] buf;
   }
   return ret;
}

/// - public method ------------------------------------------------------------
bool CSDL::save_img_png(int quality, const std::string& filename) {
   bool ret = false;
   SDL_RenderPresent(rnd);
   if (!offscreen) {
      SDL_RenderReadPixels(rnd, 0, SDL_PIXELFORMAT_RGB24, scr->pixels, scr->pitch);
   }
   ret = IMG_SavePNG(scr, filename.c_str());
   return ret;
}

/// - static method ------------------------------------------------------------
mre::SImage* CSDL::convert(const SDL_Surface* src, mre::SImage* img) {
   ASSERT_ARGUMENT(src and img, "mre::gui::convert images must be given!");
   ASSERT_ARGUMENT(src->w == img->w and src->h == img->h, "mre::gui::convert support only images of same size");
   const int bpp = src->format->BytesPerPixel;
   ASSERT_ARGUMENT((bpp == 3 or bpp == 1) and (img->bpp == 1 or img->bpp == 3), "mre::gui::convert only 3 or 1 bpp are supported"); 
   unsigned char* d = img->getImageBuffer();
   if (bpp == 3) {
      if (img->bpp == 3) {
	 for(int y = 0; y < img->h; ++y) {
	    for(int x = 0; x < img->w; ++x) {
	       const int idx = (y * img->w + x) * 3;
	       unsigned char* s = (Uint8*)src->pixels + idx;
	       d[idx] = *(s + src->format->Rshift / 8);
	       d[idx + 1] = *(s + src->format->Gshift / 8);
	       d[idx + 2] = *(s + src->format->Bshift / 8);
	    }
	 }
      } else { //img->bpp == 1 convert to grayscale 
	 for(int y = 0; y < img->h; ++y) {
	    for(int x = 0; x < img->w; ++x) {
	       const int idx = (y * img->w + x); //img->bpp is 1, so do not multiply by 3
	       unsigned char* s = (Uint8*)src->pixels + idx * 3;
	       d[idx] = rgb_to_gray(*(s + src->format->Rshift / 8), *(s + src->format->Gshift / 8), *(s + src->format->Bshift / 8));
	    }
	 }
      }
   } else { //bpp == 1 
      if (img->bpp == 3) {
	 for(int y = 0; y < img->h; ++y) {
	    for(int x = 0; x < img->w; ++x) {
	       const int idx = (y * img->w + x)*3;
	       const unsigned char s = *((Uint8*)src->pixels + (y * img->w + x));
	       for (int i = 0; i < 3; ++i) { d[idx + i] = s; }
	    }
	 }
      } else { // img->bpp == 1, just copy
	 memcpy(d, src->pixels, img->size);
      }
   }
   return img;
}

/// - static method ------------------------------------------------------------
void CSDL::draw(int x, int y, const SRGB& color, SDL_Surface* dst) {
   if (dst) {
      if (dst->format->BytesPerPixel == 1) {
	 *((Uint8*)dst->pixels + ( y * dst->w + x)) = rgb_to_gray(color.r, color.g, color.b);
      } else if (dst->format->BytesPerPixel == 3 or dst->format->BytesPerPixel == 4) {
	 Uint8* s = (Uint8*)dst->pixels + ( y * dst->w + x) * dst->format->BytesPerPixel;
	 *(s + dst->format->Rshift / 8) = color.r;
	 *(s + dst->format->Gshift / 8) = color.g;
	 *(s + dst->format->Bshift / 8) = color.b;
      } else {
	 ASSERT_ARGUMENT(false, "CSDL::draw support only surface with 1 or 3 bytes per pixel");
      }
   }
}

/// - static method ------------------------------------------------------------
void CSDL::draw(const IntVector& pixels, const SRGB& color, SDL_Surface* dst) {
   /*
http://wiki.libsdl.org/SDL_RenderPresent
SDL_RenderClear
SDL_RenderDrawLine
SDL_RenderDrawLines
SDL_RenderDrawPoint
SDL_RenderDrawPoints
SDL_RenderDrawRect
SDL_RenderDrawRects
SDL_RenderFillRect
SDL_RenderFillRects
SDL_SetRenderDrawBlendMode
SDL_SetRenderDrawColor
*/
   if (rnd) {
      SDL_SetRenderDrawColor(rnd, color.r, color.g, color.b, 255);
      for(int i = 0; i < pixels.size(); ++i) {
	 SDL_RenderDrawPoint(rnd, pixels[i]%dst->w, pixels[i]/dst->w);
      }
   }
}

/// - static method ------------------------------------------------------------
void CSDL::draw(const int idx, const SRGB& color, SDL_Surface* dst) {
   if (rnd) {
      SDL_SetRenderDrawColor(rnd, color.r, color.g, color.b, 255);
      SDL_RenderDrawPoint(rnd, idx%dst->w, idx/dst->w);
   }
}

/// - static method ------------------------------------------------------------
void CSDL::drawGrid255(const FloatMatrix& grid, SDL_Surface* dst) {
   SDL_SetRenderDrawColor(rnd, 255, 255, 255, 255);
   SDL_RenderClear(rnd);
   for(int i = 0; i < grid.SIZE; ++i) {
      const char c = round(grid[i] * 255);
      SDL_SetRenderDrawColor(rnd, c, c, c, 255);
      SDL_RenderDrawPoint(rnd, i%dst->w, i/dst->w);
   }
}

/// - static method ------------------------------------------------------------
void CSDL::drawGrid(const ByteMatrix& grid, SDL_Surface* dst) {

   SDL_SetRenderDrawColor(rnd, 255, 255, 255, 255);
   SDL_RenderClear(rnd);
   for(int i = 0; i < grid.SIZE; ++i) {
      Uint8 c = (grid[i] == 0 ? 0 : grid[i] == 1 or grid[i] == 2 ? 255 : 127);
      SDL_SetRenderDrawColor(rnd, c, c, c, 255);
      SDL_RenderDrawPoint(rnd, i%dst->w, i/dst->w);
   }
}

/// - static method ------------------------------------------------------------
mre::DoubleVector& CSDL::getIntegralImage(SDL_Surface* s, DoubleVector& img) {
   if (img.size() != s->w) { img.resize(s->w); }
   for (int i = 0; i < img.size(); i++) { img[i] = 0.0; }
   Uint8* pix = (Uint8*)s->pixels;
   for (int i = 0; i < s->h; i++) {
      for (int j = 0; j < s->w; j++) {
	 img[j] += 1.*rgb2gray(SRGB(
		  *(pix + s->format->Rshift / 8),
		  *(pix + s->format->Gshift / 8),
		  *(pix + s->format->Bshift / 8))
	       );
	 pix += 3;
      }
   }
   return img;
}

/// - static method ------------------------------------------------------------
SDL_Surface* CSDL::resize(SDL_Surface* s, double zoomx, double zoomy) {
   //SMOOTHING_OFF   0
   //SMOOTHING_ON    1
   return zoomSurface(s, zoomx, zoomy, 0);
}

/// - constructor --------------------------------------------------------------
CSDL::CSDL(bool init_video) : scr(0), win(0), txt(0), rnd(0) {
   if (!initialized) {
      if (init_video) {
	 SDL_Init(SDL_INIT_VIDEO);
	 videoInit = true;
      } else {
	 SDL_Init(0);
	 videoInit = false;
      }
      initialized = true;
   }
   sdl_init_counter++;
}

/// - destructor ---------------------------------------------------------------
CSDL::~CSDL() {
   if (scr) { SDL_FreeSurface(scr); }
   if (rnd) { SDL_DestroyRenderer(rnd); }
   if (win) { SDL_DestroyWindow(win); }
   if (txt) { SDL_DestroyTexture(txt); }
   sdl_init_counter--;
   if (sdl_init_counter == 0) {
      //TODO remove win, txt, rnd
      SDL_Quit();
   }
}

/// - public method ------------------------------------------------------------
void CSDL::init(int width, int height, const std::string& title, const std::string& icon, bool grayscale) {
   ASSERT_ARGUMENT(scr == 0, "mre::gui class instance already initialized");
   if (!grayscale) {
      win = SDL_CreateWindow(title.c_str(), 
	    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
	    width, height, 
	    SDL_WINDOW_SHOWN);
      //rnd = SDL_CreateRenderer(win, -1, 0); // SDL_RENDERER_ACCELERATED
      rnd = SDL_CreateRenderer(win, SDL_RENDERER_SOFTWARE, 0);
      // scr = SDL_SetVideoMode(width, height, 24, SDL_SWSURFACE | SDL_RESIZABLE);
      scr = SDL_CreateRGBSurface(0, width, height, 24,
	    rmask, gmask, bmask, amask);
      if (scr== NULL) {
	 fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
	 exit(1);
      }
      txt = SDL_CreateTexture(rnd,
	    //    SDL_PIXELFORMAT_ARGB8888,
	    SDL_PIXELFORMAT_RGB24,
	    SDL_TEXTUREACCESS_STREAMING,
	    width, height);

   } else {
      SDL_Color colors[256];
      for(int i = 0; i < 256; i++){
	 colors[i].r = colors[i].g = colors[i].b = i;
      }
      //      scr = SDL_SetVideoMode(width, height, 8, SDL_SWSURFACE | SDL_RESIZABLE);
      win = SDL_CreateWindow(title.c_str(), 
	    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
	    width, height, 
	    SDL_WINDOW_SHOWN);
      rnd = SDL_CreateRenderer(win, -1, 0); // SDL_RENDERER_ACCELERATED

      scr = SDL_CreateRGBSurface(0, width, height, 8,
	    rmask, gmask, bmask, amask);
      ASSERT_ARGUMENT(SDL_SetPaletteColors(scr->format->palette, colors, 0, 256) == 1, "Cannot set color palette");
      txt = SDL_CreateTexture(rnd,
	    SDL_PIXELFORMAT_INDEX8,
	    SDL_TEXTUREACCESS_STREAMING,
	    width, height);
   }
   // TODO  SDL_WM_SetCaption(title.c_str(), icon.c_str());
   ASSERT_ARGUMENT(scr and txt , "mre::gui init fail!");
   offscreen = false;
}

/// - public method ------------------------------------------------------------
void CSDL::initOffScreen(int width, int height, bool grayscale, bool rgba) {
   ASSERT_ARGUMENT(scr == 0, "mre::gui class instance already initialized");
   if (!grayscale) {
      scr = SDL_CreateRGBSurface(0, width, height, 32,
	    rmask, gmask, bmask, amask);
      rnd = SDL_CreateSoftwareRenderer(scr);
      /*
	 SDL_SetRenderDrawColor(rnd, 0, 255, 0, 255);
	 SDL_RenderClear(rnd);
	 SDL_RenderPresent(rnd);
	 IMG_SavePNG(scr, "TEST.png");
	 exit(0);*/
   } else {
      SDL_Color colors[256];
      for(int i = 0; i < 256; i++){
	 colors[i].r = colors[i].g = colors[i].b = i;
      }
      scr = SDL_CreateRGBSurface(0, width, height, 8, 0, 0, 0, 0);
      //ASSERT_ARGUMENT(SDL_SetColors(scr, colors, 0, 256) == 1, "Cannot set color palette");
      ASSERT_ARGUMENT(SDL_SetPaletteColors(scr->format->palette, colors, 0, 256) == 1, "Cannot set color palette");
      rnd = SDL_CreateSoftwareRenderer(scr);
   }
   ASSERT_ARGUMENT(scr and rnd, "mre::gui init offscreen fail!");
   offscreen = true;
}

/// - public method ------------------------------------------------------------
SDL_Surface* CSDL::create_surface(void) {
   ASSERT_ARGUMENT(scr, "mre::gui::create_surface fail, screen surface is not yet created!");
   SDL_Surface* ret = SDL_CreateRGBSurface(0, scr->w, scr->h, scr->format->BytesPerPixel*8, 0, 0, 0, 0);
   ASSERT_ARGUMENT(ret, "mre::gui::create_surface fail!");
   return ret;
}

/// - public method ------------------------------------------------------------
void CSDL::refresh(void) {
   if (scr and videoInit) {
      // SDL_Flip(scr);
      /*     SDL_UpdateTexture(txt, NULL, scr->pixels, scr->pitch);
	     SDL_RenderClear(rnd);
	     SDL_RenderCopy(rnd, txt, NULL, NULL);
	     */
      SDL_RenderPresent(rnd);
   }
}

/// - public method ------------------------------------------------------------
void CSDL::refresh(SDL_Surface* img) {
   if (img) {
      SDL_Rect rect;
      rect.x = 0; rect.y = 0;
      rect.w = scr->w < img->w ? scr->w : img->w;
      rect.h = scr->h < img->h ? scr->h : img->h;
      // DEBUG("img: " << (int)img->format->BytesPerPixel);
      // DEBUG("scr: " << (int)scr->format->BytesPerPixel);
      if (img->format->BytesPerPixel == 3 and scr->format->BytesPerPixel == 1) {
	 for(int y = rect.y; y < rect.h; ++y) {
	    for(int x = rect.x; x < rect.w; ++x) {
	       Uint8* s = (Uint8*)img->pixels + (y * img->w + x) * 3;
	       *((Uint8*)scr->pixels + (y * scr->w + x)) = rgb_to_gray(*(s + img->format->Rshift / 8), *(s + img->format->Gshift / 8), *(s + img->format->Bshift / 8));
	    }
	 }
      } else {
	 SDL_BlitSurface(img, &rect, scr, &rect); 
      }
      if (0 and videoInit) { //TODO 
	 //   SDL_Flip(scr);
	 /*SDL_UpdateTexture(txt, NULL, scr->pixels, scr->pitch);
	   SDL_RenderClear(rnd);
	   SDL_RenderCopy(rnd, txt, NULL, NULL);
	   */	 SDL_RenderPresent(rnd);
      } else {
	 SDL_RenderPresent(rnd);
      }
   }
}

/// - public method ------------------------------------------------------------
void CSDL::draw(const IntVector& pixels, const SRGB& color) {
   draw(pixels, color, scr);
}

/// - private method -----------------------------------------------------------


/* end of sdl_gui.cc */
