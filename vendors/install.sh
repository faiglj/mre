#!/bin/sh

lst="imr-h.lite imr-h.gui imr-h.algorithm"
for i in $lst
do
   cd $i; ./install.sh; cd ..
done

cd evg-thin
./install.sh
cd ..
cd libhungarian
./install.sh
cd ..
