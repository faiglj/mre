/*
 * File name: jpeg_memory.cc
 * Date:      2011/10/13 10:14
 * Author:    Jan Faigl
 */

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <jerror.h>

#include "jpeg_memory.h"

namespace mre {

   typedef struct {
      struct jpeg_destination_mgr pub; 

      unsigned char** outbuffer; 
      unsigned long* outsize;
      unsigned char* newbuffer; 
      JOCTET * buffer; 
      size_t bufsize;
   } mem_destination_mgr;

   typedef mem_destination_mgr * mem_dest_ptr;

// #define OUTPUT_BUF_SIZE 1024*1024
#define OUTPUT_BUF_SIZE 2048*2048

   /// ----------------------------------------------------------------------------
   void init_mem_destination (j_compress_ptr cinfo) { //nothing to do
   }

   boolean empty_mem_output_buffer (j_compress_ptr cinfo) {
      size_t nextsize;
      JOCTET * nextbuffer;
      mem_dest_ptr dest = (mem_dest_ptr) cinfo->dest;

      /* Try to allocate new buffer with double size */
      nextsize = dest->bufsize * 2;
      nextbuffer = (JOCTET *)malloc(nextsize);

      if (nextbuffer == NULL) {
	 ERREXIT1(cinfo, JERR_OUT_OF_MEMORY, 10);
      }

      memcpy(nextbuffer, dest->buffer, dest->bufsize);

      if (dest->newbuffer != NULL) {
	 free(dest->newbuffer);
      }

      dest->newbuffer = nextbuffer;

      dest->pub.next_output_byte = nextbuffer + dest->bufsize;
      dest->pub.free_in_buffer = dest->bufsize;

      dest->buffer = nextbuffer;
      dest->bufsize = nextsize;

      return TRUE;
   }

   /// ----------------------------------------------------------------------------
   void term_mem_destination (j_compress_ptr cinfo) {
      mem_dest_ptr dest = (mem_dest_ptr) cinfo->dest;

      *dest->outbuffer = dest->buffer;
      *dest->outsize = dest->bufsize - dest->pub.free_in_buffer;
   }

   /// ----------------------------------------------------------------------------
   void jpeg_mem_dest(j_compress_ptr cinfo, unsigned char** outbuffer, unsigned long* outsize) {
      mem_dest_ptr dest;

      if (outbuffer == NULL || outsize == NULL) /* sanity check */
	 ERREXIT(cinfo, JERR_BUFFER_SIZE);

      /* The destination object is made permanent so that multiple JPEG images
       * can be written to the same buffer without re-executing jpeg_mem_dest.
       */
      if (cinfo->dest == NULL) { /* first time for this JPEG object? */
	 cinfo->dest = (struct jpeg_destination_mgr *)
	    (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
		  sizeof(mem_destination_mgr));
      }
      dest = (mem_dest_ptr) cinfo->dest;
      dest->pub.init_destination = init_mem_destination;
      dest->pub.empty_output_buffer = empty_mem_output_buffer;
      dest->pub.term_destination = term_mem_destination;
      dest->outbuffer = outbuffer;
      dest->outsize = outsize;
      dest->newbuffer = NULL;

      if (*outbuffer == NULL || *outsize == 0) {
	 /* Allocate initial buffer */
	 dest->newbuffer = *outbuffer = (unsigned char*)malloc(OUTPUT_BUF_SIZE);
	 if (dest->newbuffer == NULL) {
	    ERREXIT1(cinfo, JERR_OUT_OF_MEMORY, 10);
	 }
	 *outsize = OUTPUT_BUF_SIZE;
      }

      dest->pub.next_output_byte = dest->buffer = *outbuffer;
      dest->pub.free_in_buffer = dest->bufsize = *outsize;
   }

} //end namespace mre

/* end of jpeg_memory.cc */
