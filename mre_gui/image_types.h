/*
 * File name: image_types.h
 * Date:      2010/06/19 07:33
 * Author:    Jan Faigl
 */

#ifndef __IMAGE_TYPES_H__
#define __IMAGE_TYPES_H__

#include <vector>
#include <inttypes.h>

#include "mre_types.h"

namespace mre {
   
   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct Coords { 
      int x; int y; 

      Coords() : x(0), y(0) {}
      Coords(int x, int y) : x(x), y(y) {}
   };

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SRGB {
      uint8_t r;
      uint8_t g;
      uint8_t b;
      SRGB() : r(0), g(0), b(0) {}
      SRGB(uint8_t r, uint8_t g, uint8_t b) : r(r), g(g), b(b) {}
      SRGB& operator=(const SRGB& a) {
         if (this != &a) {
            r = a.r; g = a.g; b = a.b;
         }
         return *this;
      }
   };

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SHSV {
      uint8_t h;
      uint8_t s;
      uint16_t v;
      SHSV() : h(0), s(0), v(0) {}
      SHSV(uint8_t h, uint8_t s, uint16_t v) : h(h), s(s), v(v) {}
      SHSV& operator=(const SHSV& a) {
	 if (this != &a) {
	    h = a.h; s = a.s; v = a.v;
	 }
	 return *this;
      }

      void clear(void) { h = 0; s = 0; v = 0; }
   };

   typedef std::vector<SRGB> RGBVector;
   typedef std::vector<SHSV> HSVVector;

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SRGBCube {
      const uint8_t dim;
      const uint8_t k;
      int nbr;
      typedef std::vector<bool> BoolVector;
      BoolVector values;

      SRGBCube(uint8_t v);
      int getNbr(void) const { return nbr; }
      void add(const SHSV& hsv, int dh, int ds, int dv);
      void add(const SRGB& rgb);
      inline int index(const SRGB& rgb) const {
	 return (rgb.r/k) * (dim*dim) + (rgb.g / k) * dim + rgb.b / k;
      }
      inline bool inRange(const SRGB& rgb) const {
	 return values[(rgb.r/k) * (dim*dim) + (rgb.g / k) * dim + rgb.b / k];
      }

      void clear(void);
   };

   /// ----------------------------------------------------------------------------
   /// @brief rgb2hsv
   /// 
   /// @param rgb 
   /// @param hsv 
   /// ----------------------------------------------------------------------------
   SHSV& rgb2hsv(const SRGB& rgb, SHSV& hsv);

   /// ----------------------------------------------------------------------------
   /// @brief hsv2rgb
   /// 
   /// @param hsv 
   /// @param rgb 
   /// ----------------------------------------------------------------------------
   SRGB& hsv2rgb(const SHSV& hsv, SRGB& rgb);

   /// ----------------------------------------------------------------------------
   /// @brief rgb2gray
   /// 
   /// @param rgb 
   /// 
   /// @return 
   /// ----------------------------------------------------------------------------
   uint8_t rgb2gray(const SRGB& rgb);

   /// ----------------------------------------------------------------------------
   /// @brief loadCube
   /// 
   /// @param filename 
   /// @param rgbCube 
   /// 
   /// @return 
   /// ----------------------------------------------------------------------------
   SRGBCube& loadCube(const std::string& filename, SRGBCube& rgbCube);

   /// ----------------------------------------------------------------------------
   /// @brief saveCube
   /// 
   /// @param rgbCube 
   /// @param filename 
   /// ----------------------------------------------------------------------------
   void saveCube(const SRGBCube& rgbCube, const std::string& filename);

   struct SImage {
      const int w;
      const int h;
      const int bpp;
      const int size;
      unsigned char* img;


      SImage(int iW, int iH, int iBpp = 1) : w(iW), h(iH), bpp(iBpp), size(w*h*bpp) {
	 img = new unsigned char[size];
      }

      const unsigned char* getImageBuffer(void) const { return img ? &img[0] : 0; }
      unsigned char* getImageBuffer(void) { return img ? &img[0] : 0; }

      ~SImage() {
	 if (img) { delete[] img; }
      }
   };

} // end namespace mre

#endif

/* end of image_types.h */
