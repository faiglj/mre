/*
 * File name: global_control.h
 * Date:      2012/02/08 07:00
 * Author:    Jan Faigl
 */

#ifndef __GLOBAL_CONTROL_H__
#define __GLOBAL_CONTROL_H__

#include <vector>

#include <libplayerc++/playerc++.h>

#include "imr_config.h"

#include "mre_types.h"
#include "mre_ranger_sim.h"

namespace mre {

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SPlayerClient {
      PlayerCc::PlayerClient* client;
      PlayerCc::LaserProxy* laser;
      const CRangerSim* rangerSim;
      SLaserData laserData;
      int id;
      std::string labelStr;
      char* label;

      SPlayerClient(const std::string& host, int port, int laser_idx, int id);
      SPlayerClient(const CRangerSim* s, int id);
      ~SPlayerClient();

      const SPosition& getPose(void) const { return laserData.robotPose; }

      void read(void);
   };

   typedef std::vector<SPlayerClient*> PtrPlayerClientVector;

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   class CGlobalControl {
      imr::CConfig& cfg;
      PlayerCc::SimulationProxy* sim;
      const double ROBOT_DISK_RADIUS;
      PtrPlayerClientVector clients;
      const CRangerSim* rangerSim;

      public:
      static imr::CConfig& getConfig(imr::CConfig& config);

      CGlobalControl(imr::CConfig& cfg);
      ~CGlobalControl();

      void setRangerSim(CRangerSim* s) { rangerSim = s; }

      void connect(void);

      const PtrPlayerClientVector& getRobots(void) const { return clients; }
      double getRobotDiskRadius(void) const { return ROBOT_DISK_RADIUS; }
      void read(void);
      void setRobotPose(int idx, const SPosition& pose);

      private:

   };

} //end namespace mre


#endif

/* end of global_control.h */
