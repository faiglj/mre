/*
 * File name: minmax_pairing.cc
 * Date:      23.2.2012
 * Author:    Miroslav Kulich
 *            Jan Faigl (cleanup) 
 */

#include <vector>
#include <limits>

#include <imr_assert.h>
#include <logging.h>

#include "minmax_pairing.h"

using imr::logger;

using namespace mre;

/// - constructor --------------------------------------------------------------
CMinMaxPairing::CMinMaxPairing(const DoubleMatrix& costMatrix, bool minmax) : costMatrix(costMatrix), MINMAX(minmax), N(costMatrix.NROWS) {
   ASSERT_ARGUMENT(costMatrix.NROWS == costMatrix.NCOLS, "CMinMaxPairing costMatrix must be squared");
   best = std::numeric_limits<int>::max();
   IntVector result(N);
   bestPermutation.resize(N, -1);
   c = 0;
   for(int i = 0; i < N; ++i) { result[i] = i; }
   permute(result, 0);
}

/// - destructor ---------------------------------------------------------------
CMinMaxPairing::~CMinMaxPairing() {
}

/// - public method ------------------------------------------------------------

/// - private method -----------------------------------------------------------
void CMinMaxPairing::permute(IntVector v, const int start) {
   if (start == N - 1) {
      MINMAX ? minmax(v) : minsum(v);
   } else {
      for (int i = start; i < N; i++) {
         const double d = costMatrix(start, v[i]);
         if (d > 0 and d < best) {
            int tmp = v[i];
            v[i] = v[start];
            v[start] = tmp;
            permute(v, start + 1);
            v[start] = v[i];
            v[i] = tmp;
         }
      }
   }
}

/// - private method -----------------------------------------------------------
void CMinMaxPairing::minmax(const IntVector& v) {
   double max = 0;
   std::stringstream ss;
   for(int i = 0; i < N; ++i) { ss << v[i] << " "; }
   c++;
   for (int i = 0; i < N; i++) {
      const double d = costMatrix(i, v[i]);
      if (d > 0 and d > max) {
         max = costMatrix(i, v[i]);
      }
      //  INFO("v[i]" << v[i] << " cost: " << costMatrix(i, v[i]));
   }
   DEBUG("Permutation[" << c << "]: " << ss.str() << " max: " << max);
   if (best > max) { best = max; bestPermutation = v; }
   //  INFO("max: " << max << " best: " << best);
}

/// - private method -----------------------------------------------------------
void CMinMaxPairing::minsum(const IntVector& v) {
   double sum = 0;
   std::stringstream ss;
   for(int i = 0; i < N; ++i) { ss << v[i] << " "; }
   c++;
   for (int i = 0; i < N; i++) {
      const double d = costMatrix(i, v[i]);
      if (d > 0) {
         sum += d;
      }
   }
   DEBUG("Permutation[" << c << "]: " << ss.str() << " sum: " << sum);
   if (best > sum) { best = sum; bestPermutation = v; }
}

