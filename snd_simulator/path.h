/*
 * File name: path.h
 * Date:      Tue Oct 23 15:02:50 +0200 2012
 * Author:    Miroslav Kulich
 */

#ifndef __PATH_H__
#define __PATH_H__

#include <queue>
#include "mre_types.h"

namespace mre {

   class CPath {
      public:
	 CPath();
	 ~CPath();

	 void reset(void);
	 void add(const SPosition& pos);
	 bool hasNextGoal();
	 bool shutSlowDown();
	 SPosition getGoal();
	 bool shouldSlowDown();
	 void nextGoal();

      private:
	 std::queue<SPosition> path;
   };

}

#endif

/* end of path.h */
