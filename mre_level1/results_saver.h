/*
 * File name: results_saver.h
 * Date:      2012/02/18 10:49
 * Author:    Jan Faigl
 */

#ifndef __RESULTS_SAVER_H__
#define __RESULTS_SAVER_H__

#include <string>

#include <imr_exceptions.h>
#include <imr_config.h>
#include <result_log.h>
#include <timerN.h>

#include "mre_types.h"

namespace mre {

   struct SResults {
      imr::CConfig& cfg;

      const bool SAVE_PIC;
      const bool SAVE_RESULTS;
      const std::string IMAGE_EXT;
      const std::string ITER;


      imr::CResultLog resultLog;
      std::string output;
      std::string picDir;

      bool saveResults;
      bool verboseLog;

      imr::CTimerN tm;


      int stepTotal;
      int step;
      GridCoordsVectorVector travelledPaths;
      DoubleVector travelledDistances;
      double expectedExplorationTimeSteps;

      static imr::CConfig& getConfig(imr::CConfig& config);

      SResults(imr::CConfig& config);
      ~SResults();

      std::string getVersion(void) { return "IMR MRE - Multi-Robot Exploration - 0.7"; }
      std::string getRevision(void);
      std::string getFinalPicFilename(void);

      void init(int numRobots, double laserRange);
      void fillResults(void);
      void defineResultLog(const std::string& method, const std::string& cost, double weight, int w_grid, int h_grid, double w_map, double h_map, double cell_size);
      void save(void);
      void appendToLog(void);
      void saveInfo(const std::string& file) throw(imr::io_error);
      void saveSettings(const std::string& file) throw(imr::io_error);

      private:
      std::string getOutputPath(const std::string filename, std::string& dir);
      std::string getOutputIterPath(const std::string filename, std::string& dir);

   };

} //end namespace mre

#endif

/* end of results_saver.h */
