/*
 * File name: colors.cc
 * Date:      2006/04/30 16:00
 * Author:    Jan Faigl
 */

#include "colors.h"

using namespace imr::gui;

/// --------------------------------------------------------------------------
CColors Colors::DEFAULT;

/// --------------------------------------------------------------------------
/// Class CSkeleton
/// --------------------------------------------------------------------------
CColors::CColors() {
   i = 0;
   colors.push_back(new std::string("red"));
   colors.push_back(new std::string("blue"));
   colors.push_back(new std::string("orange"));
   colors.push_back(new std::string("green"));
   colors.push_back(new std::string("seagreen"));
   colors.push_back(new std::string("yellow"));
   colors.push_back(new std::string("moccasin"));
   colors.push_back(new std::string("cyan"));
   colors.push_back(new std::string("RoyalBlue1"));
   colors.push_back(new std::string("SpringGreen2"));
   colors.push_back(new std::string("wheat1"));
   colors.push_back(new std::string("sienna2"));
   colors.push_back(new std::string("magenta"));
   colors.push_back(new std::string("gold"));
   // colors.push_back(new std::string("brown"));
}

/// --------------------------------------------------------------------------
CColors::~CColors() {
   for(int i = 0; i < colors.size(); i++) {
      delete colors[i];
   }
}

/// --------------------------------------------------------------------------
void CColors::reset(void) {
   i = 0;
}

/// --------------------------------------------------------------------------
const Color& CColors::cur(void) {
   return *(colors[i]);
}

/// --------------------------------------------------------------------------
const Color& CColors::next(void) {
   const Color& c = *(colors[i]);
   i = (i+1)%colors.size();
   return c;
}

/* end of colors.cc */
