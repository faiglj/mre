#!/usr/local/bin/ruby19

#
#  File name: load.rb
#  Date:      2012/10/02 22:52
#  Author:    Jan Faigl
#


fid = File.new("#{ARGV[0]}.txt", 'w')
File.new(ARGV[0]).each_line{|l|
   fid.puts l.strip.split('  ').collect{|x| sprintf("%.2f", x.to_f)}.join(' ')
}
fid.close
