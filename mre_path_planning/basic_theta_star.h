/*
 * File name: basic_theta_star.h
 * Date:      2012/02/16 16:19
 * Author:    Jan Faigl
 */

#ifndef __BASIC_THETA_STAR_H__
#define __BASIC_THETA_STAR_H__

#include "mre_types.h"

namespace mre {
   class CBasicThetaStar {
      const ByteMatrix& navGrid;
      const unsigned char FREESPACE;
      public:
      CBasicThetaStar(const ByteMatrix& navGrid, const unsigned char FREESPACE = 255);
      ~CBasicThetaStar();

      GridCoordsVector& getPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path) const;
      float getDistance(const GridCoords& start, const GridCoords& goal) const;

      private:
      inline int index(const GridCoords& pt) const { return pt.y * navGrid.NCOLS + pt.x; }
      bool lineOfSight(const GridCoords& s, const GridCoords& sc) const;
   };

} //end namespace mre

#endif

/* end of basic_theta_star.h */
