/*
 * File name: path_follower.cc
 * Date:      2009/08/06 14:28
 * Author:    Jan Faigl
 */

#include <cmath>

#include "path_follower.h"

using namespace mre;

/// ----------------------------------------------------------------------------
/// CPathFollower

/// - static method ------------------------------------------------------------
imr::CConfig& CPathFollower::getConfig(imr::CConfig& config) {
   config.add<double>("reach-goal-distance", "a distance to the current goal to ", 0.1);
   return config;
}

/// ----------------------------------------------------------------------------
CPathFollower::CPathFollower(imr::CConfig& cfg) {
   reachGoalDistance = cfg.get<double>("reach-goal-distance");
}

/// ----------------------------------------------------------------------------
CPathFollower::~CPathFollower() {
}

/// ----------------------------------------------------------------------------
void CPathFollower::reset(void) {
   while(!path.empty()) {
      path.pop();
   }
}

/// ----------------------------------------------------------------------------
void CPathFollower::add(const SPosition& pos) {
   path.push(pos);
}

/// ----------------------------------------------------------------------------
void CPathFollower::add(const PositionVector& newPlan) {
   for (PositionVector::const_iterator it = newPlan.begin(); it != newPlan.end(); it++) {
      path.push(*it);
   }
}

/// ----------------------------------------------------------------------------
bool CPathFollower::next(const SPosition& cur, SPosition& next, double& goalDistance) {
   bool ret = false;
   if (!path.empty()) {
      SPosition& pos = path.front();

      goalDistance = sqrt(cur.squared_distance(path.front()));
      if (goalDistance < reachGoalDistance) {
         path.pop();
      } 
      if (!path.empty()) {
         next = path.front();
         ret = true;
      }
   }
   return ret;
}

/// - public method -----------------------------------------------------------
double CPathFollower::currentRemainingDistance(const SPosition& cur) const {
   double ret = 0.0;
   if (!path.empty()) {
      ret = sqrt(cur.squared_distance(path.front()));
   }
   return ret;
}

/* end of path_follower.cc */
