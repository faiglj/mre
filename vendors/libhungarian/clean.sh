#!/bin/sh

if [ `uname` = FreeBSD ]
then
   gmake -C src clean
else
   make -C src clean
fi
