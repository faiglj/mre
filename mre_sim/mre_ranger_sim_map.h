/*
 * File name: mre_ranger_sim_map.h
 * Date:      2012/02/10 08:53
 * Author:    Jan Faigl
 */

#ifndef __MRE_RANGER_SIM_MAP_H__
#define __MRE_RANGER_SIM_MAP_H__

#include <string>

#include <imr_config.h>

#include "mre_types.h"

namespace mre {

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   class CRangerSimMap {
      private:
         imr::CConfig& cfg;
         const double MAP_W;
         const double MAP_H;
      public:
         const double CELL_SIZE;
         const int W;
         const int H;
      private:
         const double MIN_X;
         const double MAX_X;
         const double MIN_Y;
         const double MAX_Y;
         const double KX;
         const double KY;
      public:
         ByteMatrix grid;

      public:
         enum { FREESPACE = 0, OBSTACLE = 1};

      public:
         static imr::CConfig& getConfig(imr::CConfig& config);
         CRangerSimMap(imr::CConfig& cfg, const std::string& filename, double cell_size);
         ~CRangerSimMap();

         inline SPosition& grid2real(int r, int c, SPosition& pos) const {
            pos.x = c / KX + MIN_X;
            pos.y = MIN_Y - (r - H) / KY;
            return pos;
         }

         inline void real2grid(const SPosition& pos, int& r, int& c) const {
            c = KX * (pos.x - MIN_X);
            r = H - KY * (pos.y - MIN_Y);
         }

         inline double grid2realX(int c) const { return c / KX + MIN_X; }
         inline double grid2realY(int r) const { return MIN_Y - (r - H) / KY; }
         inline int real2gridX(double x) const { return KX * (x - MIN_X); }
         inline int real2gridY(double y) const { return H - KY * (y - MIN_Y); }

         const ByteMatrix& getGrid(void) const { return grid; }

         bool isCollisionFree(const GridCoordsVector& pts, GridCoords& collisionPoint) const;
      private:

   };

} //end namespace mre

#endif

/* end of mre_ranger_sim_map.h */
