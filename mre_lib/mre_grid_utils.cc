/*
 * File name: mre_grid_utils.cc
 * Date:      2012/02/10 09:21
 * Author:    Jan Faigl
 */

#include <cstdlib>
#include <algorithm>
#include <limits>

#include <logging.h>

#include "mre_grid_utils.h"

namespace mre {

#define SWAP(x, y) {int t = x; x = y; y = t; }

   /// ----------------------------------------------------------------------------
   class CompareInt {
      public:
         bool operator()(const int& a, const int& b) const {
            return a < b;
         }
   };

   /// ----------------------------------------------------------------------------
   IntVector& fillPolygon(const IntVector& vx, const IntVector& vy, const int W, IntVector& area) {
      area.clear();
      int minx = vx.front();
      int maxx = vx.front();
      int miny = vy.front();
      int maxy = vy.front();
      const int n = vx.size();
      for (int i = 1; i < n; i++) {
         if (vy[i] < miny) {
            miny = vy[i];
         } else if (vy[i] > maxy) {
            maxy = vy[i];
         }
         if (vx[i] < minx) {
            minx = vx[i];
         } else if (vx[i] > maxx) {
            maxx = vx[i];
         }
      }

      int x1, y1;
      int x2, y2;
      int ind1, ind2;
      // int cache[maxx - minx + 2];
      int cache[n];
      int ints;
      for (int y = miny; (y <= maxy); y++) {
         ints = 0;
         for (int i = 0; (i < n); i++) {
            if (!i) {
               ind1 = n - 1;
               ind2 = 0;
            } else {
               ind1 = i - 1;
               ind2 = i;
            }
            y1 = vy[ind1];
            y2 = vy[ind2];
            if (y1 < y2) {
               x1 = vx[ind1];
               x2 = vx[ind2];
            } else if (y1 > y2) {
               y2 = vy[ind1];
               y1 = vy[ind2];
               x2 = vx[ind1];
               x1 = vx[ind2];
            } else {
               continue;
            }
            if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
               cache[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
            } 	    
         }
         std::sort(cache, cache + ints, CompareInt());
         for (int i = 0; i < ints; i += 2) {
            int xa = cache[i] + 1;
            xa = (xa >> 16) + ((xa & 32768) >> 15);
            int xb = cache[i+1] - 1;
            xb = (xb >> 16) + ((xb & 32768) >> 15);

            int f, t;
            if (xa < xb) {
               f = xa;
               t = xb;
            } else {
               f = xb;
               t = xa;
            }
            const int dy = y*W;
            for (int i = f; i < t; i++) {
               area.push_back(dy + i);
            }
         }
      }
      return area;
   }

   /// ----------------------------------------------------------------------------
   GridCoordsVector& bresenham(const GridCoords& pt1, const GridCoords& pt2, GridCoordsVector& line, bool clearLine, bool addLastPoint) {
      // The pt2 point is not added into line
      int x0 = pt1.x; int y0 = pt1.y;
      int x1 = pt2.x; int y1 = pt2.y;
      if (clearLine) { line.clear(); }
      GridCoords p;
      int dx = x1 - x0;
      int dy = y1 - y0;
      int steep = (abs(dy) >= abs(dx));
      if (steep) {
         SWAP(x0, y0);
         SWAP(x1, y1);
         // recompute Dx, Dy after swap
         dx = x1 - x0;
         dy = y1 - y0;
      }
      int xstep = 1;
      if (dx < 0) {
         xstep = -1;
         dx = -dx;
      }
      int ystep = 1;
      if (dy < 0) {
         ystep = -1;
         dy = -dy;
      }
      int twoDy = 2 * dy;
      int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
      int e = twoDy - dx; //2*Dy - Dx
      int y = y0;
      int xDraw, yDraw;
      for (int x = x0; x != x1; x += xstep) {
         if (steep) {
            xDraw = y;
            yDraw = x;
         } else {
            xDraw = x;
            yDraw = y;
         }
         p.x = xDraw; //TODO? p.r = xDraw;
         p.y = yDraw; //TODO? p.c = yDraw;
         line.push_back(p);

         // next
         if (e > 0) {
            e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
            y = y + ystep;
         } else {
            e += twoDy; //E += 2*Dy;
         }
      }
      if (addLastPoint) { line.push_back(pt2); }
      return line;
   }

   /// ----------------------------------------------------------------------------
   GridCoordsVector& circle(const GridCoords& center, int rad, GridCoordsVector& circle) {
      circle.clear();
         int x = center.x;
         int y = center.y;
         int left, right, top, bottom;
         int result;
         int x1, y1, x2, y2;
         int cx = 0;
         int cy = rad;
         int ocx = (int) 0xffff;
         int ocy = (int) 0xffff;
         int df = 1 - rad;
         int d_e = 3;
         int d_se = -2 * rad + 5;
         int xpcx, xmcx, xpcy, xmcy;
         int ypcy, ymcy, ypcx, ymcx;

         do {
            ypcy = y + cy;
            ymcy = y - cy;
            if (cx > 0) {
               xpcx = x + cx;
               xmcx = x - cx;
               circle.push_back(GridCoords(xmcx, ypcy));
               circle.push_back(GridCoords(xpcx, ypcy));
               circle.push_back(GridCoords(xmcx, ymcy));
               circle.push_back(GridCoords(xpcx, ymcy));
            } else {
               circle.push_back(GridCoords(x, ymcy));
               circle.push_back(GridCoords(x, ypcy));
            }
            xpcy = x + cy;
            xmcy = x - cy;
            if ((cx > 0) && (cx != cy)) {
               ypcx = y + cx;
               ymcx = y - cx;
               circle.push_back(GridCoords(xmcy, ypcx));
               circle.push_back(GridCoords(xpcy, ypcx));
               circle.push_back(GridCoords(xmcy, ymcx));
               circle.push_back(GridCoords(xpcy, ymcx));
            } else if (cx == 0) {
               circle.push_back(GridCoords(xmcy, y));
               circle.push_back(GridCoords(xpcy, y));
            }
            /*
             * Update 
             */
            if (df < 0) {
               df += d_e;
               d_e += 2;
               d_se += 2;
            } else {
               df += d_se;
               d_e += 2;
               d_se += 4;
               cy--;
            }
            cx++;
         } while (cx <= cy);
         return circle;
   }

   /// ----------------------------------------------------------------------------
   bool isObstacleFree(const GridCoordsVector& pts, const ByteMatrix& map, unsigned char FREESPACE) {
      bool ret = true;
      for(int i = 0; i < pts.size(); ++i) {
         if (map(pts[i].y, pts[i].x) != FREESPACE) {
            ret = false;
            break;
         }
      }
      return ret;
   }

   /// ----------------------------------------------------------------------------
   GridCoordsVector& simplify(const GridCoordsVector& pts, const ByteMatrix& map, GridCoordsVector& ptsSimpl) {
      if (pts.size() > 2) {
         ptsSimpl.clear();
         ptsSimpl.push_back(pts.front());

         GridCoordsVector line;
         int last = 1;
         int curBest = last;
         bool done = false;
         for(int i = 2; i < pts.size(); ++i) {
            bresenham(ptsSimpl.back(), pts[i], line);
            if (isObstacleFree(line, map)) {
               last = i;
            } else {  //this shortcut collides with an obstacle, use the previous one
               ptsSimpl.push_back(pts[last]);
               line.clear();
               last = last + 1;
            }
         }
         if (last < pts.size()) {
            ptsSimpl.push_back(pts[last]);
         } 
      } else {
         ptsSimpl = pts;
      }
      return ptsSimpl;
   }

   /// ----------------------------------------------------------------------------
   GridCoordsVector& simplify2(const GridCoordsVector& pts, const ByteMatrix& map, GridCoordsVector& ptsSimpl, int maxFails, const unsigned char FREESPACE) {
      if (pts.size() > 2) {
         ptsSimpl.clear();
         GridCoordsVector ln;
         ptsSimpl.push_back(pts.front());
         int last = 1;
         while(ptsSimpl.back() != pts.back()) {
            const int MAX = maxFails == -1 ? pts.size() - last : maxFails;
            int e = 0;
            for(int i = (last + 1); i < pts.size(); ++i) {
               if (isObstacleFree(bresenham(ptsSimpl.back(), pts[i], ln), map, FREESPACE)) {
                  last = i;
               } else {
                  e++;
                  if (e > MAX) { break; } //parameters
               }
            }
            ptsSimpl.push_back(pts[last]);
         }
      } else {
         ptsSimpl = pts;
      }
      return ptsSimpl;
   }

   /// ----------------------------------------------------------------------------
   GridCoordsVector& fillPath(const GridCoordsVector& pts, GridCoordsVector& path) {
      path.clear();
      if (pts.size() > 2) {
         for(int i = 1; i < pts.size(); ++i) {
            GridCoordsVector p = path;
            bresenham(pts[i-1], pts[i], path, false, true);
         }
 //       path.push_back(pts.back());
      } else if (pts.size() > 1) {
         bresenham(pts.front(), pts.back(), path, false, true);
   //      path.push_back(pts.back()); //bresenham does not add the last point
      }
      return path;
   }

   /// ----------------------------------------------------------------------------
   int getPathLengthManhattan(const GridCoordsVector& path) {
      int ret = 0;
      for(int i = 1; i < path.size(); ++i) {
         ret += path[i-1].manhattan_distance(path[i]);
      }
      return ret;
   }

   /// ----------------------------------------------------------------------------
   double getPathLength(const GridCoordsVector& path) {
      double ret = 0;
      for(int i = 1; i < path.size(); ++i) {
         ret += sqrt(path[i-1].squared_distance(path[i]));
      }
      return ret;
   }

   /// ----------------------------------------------------------------------------
   bool isCollisionFree(const GridCoordsVector& pts, const ByteMatrix& grid, GridCoords& collisionPoint, GridCoordsVector& freePoints, unsigned char FREESPACE) {
      bool ret = true;
      collisionPoint = pts.back();
      freePoints.clear();
      const int W = grid.NCOLS;
      const int H = grid.NROWS;
      for(int i = 0; i < pts.size(); ++i) {
         const GridCoords& pt = pts[i];
         if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
            if (
                  grid(pt.y, pt.x) != FREESPACE
                  or (pt.x > 1 and grid(pt.y, pt.x - 1) != FREESPACE) or (pt.x < (W-1) and grid(pt.y, pt.x + 1) != FREESPACE) 
                  or (pt.y > 1 and grid(pt.y - 1, pt.x) != FREESPACE) or (pt.y < (H-1) and grid(pt.y+1, pt.x) != FREESPACE) 
               )
            {
               ret = false;
               collisionPoint = pt;
               break;
            } else {
               freePoints.push_back(pt);
            }
         } else {
            break; //ray goes out of the map
         }
      }
      return ret;
   }
} //end namespace mre

/* end of mre_grid_utils.cc */
