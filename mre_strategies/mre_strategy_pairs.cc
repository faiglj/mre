/*
 * File name: mre_strategy_pairs.cc
 * Date:      2012/02/08 15:48
 * Author:    Jan Faigl
 */

#include <vector>
#include <algorithm>

#include <logging.h>

#include "mre_strategy_pairs.h"

using imr::logger;

using namespace mre;

/// ----------------------------------------------------------------------------
/// @brief 
/// ----------------------------------------------------------------------------
struct SPair {
   int goalIDX;
   int robotIDX;
   double cost;

   SPair(int g, int r, double c) : goalIDX(g), robotIDX(r), cost(c) {}
   ~SPair() {}

   SPair& operator=(const SPair& a) {
      if (this != &a) {
         goalIDX = a.goalIDX;
         robotIDX = a.robotIDX;
         cost = a.cost;
      }
      return *this;
   }
};

typedef std::vector<SPair*> PairPtrVector;

/// ----------------------------------------------------------------------------
/// @brief 
/// ----------------------------------------------------------------------------
class Compare {
   public:
   bool operator()(const SPair* a, const SPair* b) const {
      return a->cost < b->cost;
   }
};

/// - constructor --------------------------------------------------------------
CPairs::CPairs() {
}

/// - destructor ---------------------------------------------------------------
CPairs::~CPairs() {
}

/// - public method ------------------------------------------------------------
IntVector& CPairs::assign(const DoubleMatrix& costMatrix, IntVector& goalsIDX) {
   //costMatrix rows - robots, columns - goals
   PairPtrVector pairs;
   DEBUG("CPairs: " << costMatrix.NROWS << "x" << costMatrix.NCOLS << " pairs: " << pairs.size());
   int count = 0;
   for(int r = 0; r < costMatrix.NROWS; ++r) {
      for(int g = 0; g < costMatrix.NCOLS; ++g) {
         if (costMatrix(r, g) >= 0) {
            pairs.push_back(new SPair(g, r, costMatrix(r, g)));
            count++;
            SPair& p = *pairs.back();
         }
      }
   }
   ASSERT_ARGUMENT(pairs.size() == count, "CPairs - unexpected size of pairs");
   std::sort(pairs.begin(), pairs.end(), Compare());
   goalsIDX.resize(costMatrix.NROWS); 
   for(int i = 0; i < goalsIDX.size(); ++i) { goalsIDX[i] = -1; }
   BoolVector done(costMatrix.NCOLS, false);
   for(int i = 0; i < pairs.size(); ++i) {
      const SPair& pair = *pairs[i];
      if (goalsIDX[pair.robotIDX] == -1 and !done[pair.goalIDX]) {
         goalsIDX[pair.robotIDX] = pair.goalIDX;
         done[pair.goalIDX] = true;
      }
   }
   for(int i = 0; i < pairs.size(); ++i) { delete pairs[i]; }
   return goalsIDX;
}

/// - private method -----------------------------------------------------------

/* end of mre_strategy_pairs.cc */
