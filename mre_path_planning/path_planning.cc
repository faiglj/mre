/*
 * File name: path_planning.cc
 * Date:      2012/02/08 12:14
 * Author:    Jan Faigl
 */

#include <cmath>
#include <limits>
#include <algorithm>

#include <boost/foreach.hpp>

#include <logging.h>
#include <imr_assert.h>

#include "distance_transform.h"
#include "basic_theta_star.h"
#include "mre_grid_utils.h"

#include "path_planning.h"

using imr::logger;

using namespace mre;

#define foreach BOOST_FOREACH

const char OCCUPIED_ROBOT = 1;

/// - constructor --------------------------------------------------------------
CPathPlanning::CPathPlanning(const CMap& map, bool use_robot_dt, bool omnimotion) : 
   map(map), grid(map.H, map.W), 
   USE_ROBOT_DT(use_robot_dt),
   ROBOT_OMNI_MOTION(omnimotion)
{
   robotGoalPaths = 0;
}

/// - destructor ---------------------------------------------------------------
CPathPlanning::~CPathPlanning() {
   releasePaths();
   foreach(CDistanceTransform* dt, dtRobots) { delete dt; }
}

/// - public method ------------------------------------------------------------
void CPathPlanning::updateMap() {
   const ByteMatrix& navGrid = map.getNavGrid();
   for(int i = 0; i < navGrid.SIZE; i++) {
      grid(i) = (navGrid(i) == map.FREESPACE_CELL ? CDistanceTransform::FREESPACE : CDistanceTransform::OBSTACLE);
   }
}

/// - public method ------------------------------------------------------------
void CPathPlanning::set(const RobotPtrVector& robots, const GridCoordsVector& goalsI) {
   goals = goalsI;
   releasePaths();
   robotShapes.clear();

   robotShapes.resize(robots.size());
   for(int r = 0; r < robots.size(); ++r) { //mark all robots in the grid 
      IntVector& robotShape = robotShapes[r];
      foreach(int i, robots[r]->shape) {
         if (grid(i) == CDistanceTransform::FREESPACE) {
      //      grid(i) = OCCUPIED_ROBOT; //TODO disable robots as obstacles
      //      robotShape.push_back(i);
         }
      }
   }

   robotGoalPaths = new GridCoordsVector*[robots.size()];
   for(int i = 0; i < robots.size(); ++i) {
      robotGoalPaths[i] = new GridCoordsVector[goals.size()];
   }
}

/// - public method ------------------------------------------------------------
void CPathPlanning::computeRobotDT(const RobotPtrVector& robots) {
   foreach(CDistanceTransform* dt, dtRobots) { delete dt; }
   dtRobots.clear();
   foreach(const SRobot* robot, robots) {
      IntVector backup;
      foreach(int i, robot->shape) { 
         if (grid(i) == OCCUPIED_ROBOT) {
            backup.push_back(i);
            grid(i) = CDistanceTransform::FREESPACE; //mark robot as freespace 
         }
      } //backup "obstacles" of the robot
      dtRobots.push_back(new CDistanceTransform(grid));
      dtRobots.back()->setGoal(robot->gridPose);
      foreach(int i, backup) { grid(i) = OCCUPIED_ROBOT; } // restore robot's shape
   } //end all robots
}

/// - public method ------------------------------------------------------------
void CPathPlanning::compute(const RobotPtrVector& robots) {
   CDistanceTransform* dt = new CDistanceTransform(grid);
   for(int r = 0; r < robots.size(); ++r) {
      const GridCoords& pose = robots[r]->gridPose;
      const IntVector& robotShape = robotShapes[r];
      IntVector backup;
      foreach(int i, robotShape) { 
         if (grid(i) == OCCUPIED_ROBOT) {
            backup.push_back(i);
            grid(i) = CDistanceTransform::FREESPACE; //mark robot as freespace 
         }
      } //backup "obstacles" of the robot
      //ASSERT_ARGUMENT(grid(pose.y, pose.x) == CDistanceTransform::FREESPACE, "Robot must be in freespace");
      ASSERT_ARGUMENT(grid(pose.y, pose.x) == CDistanceTransform::FREESPACE or grid(pose.y, pose.x) == OCCUPIED_ROBOT, "Robot must be in freespace");

      for(int g = 0; g < goals.size(); ++g) {
         if (!USE_ROBOT_DT) {
            dt->setGoal(goals[g]);
            ASSERT_ARGUMENT(grid(goals[g].y, goals[g].x) == CDistanceTransform::FREESPACE, "Goal must be in freespace");
            dt->getPath(pose, robotGoalPaths[r][g]);
         } else {
            dtRobots[r]->getPath(goals[g], robotGoalPaths[r][g]);
            std::reverse(robotGoalPaths[r][g].begin(), robotGoalPaths[r][g].end());
         }
      }
      foreach(int i, backup) { grid(i) = OCCUPIED_ROBOT; } // restore robot's shape
   } //end all robot
   delete dt;
}

/// - public method ------------------------------------------------------------
void CPathPlanning::computeThetaStar(const RobotPtrVector& robots) {
   CBasicThetaStar* bts = new CBasicThetaStar(grid);
   for(int r = 0; r < robots.size(); ++r) {
      const GridCoords& pose = robots[r]->gridPose;
      const IntVector& robotShape = robotShapes[r];
      IntVector backup;
      foreach(int i, robotShape) { 
         if (grid(i) == OCCUPIED_ROBOT) {
            backup.push_back(i);
            grid(i) = CDistanceTransform::FREESPACE; //mark robot as freespace 
         }
      } //backup "obstacles" of the robot
      ASSERT_ARGUMENT(grid(pose.y, pose.x) == CDistanceTransform::FREESPACE or grid(pose.y, pose.x) == OCCUPIED_ROBOT, "Robot must be in freespace");
      for(int g = 0; g < goals.size(); ++g) {
         ASSERT_ARGUMENT(grid(goals[g].y, goals[g].x) == CDistanceTransform::FREESPACE, "Goal must be in freespace");
         bts->getPath(pose, goals[g], robotGoalPaths[r][g]);
      }
      foreach(int i, backup) { grid(i) = OCCUPIED_ROBOT; } // restore robot's shape
   } //end all robot
   delete bts;
}


/// - public method ------------------------------------------------------------
IntVector& CPathPlanning::getPath(int robotIDX, int goalIDX, IntVector& path) const {
   path.clear();
   if (robotIDX >= 0 and robotIDX < robotShapes.size() and goalIDX >= 0 and goalIDX < goals.size()) {
      foreach(const GridCoords& pt, robotGoalPaths[robotIDX][goalIDX]) {
         path.push_back(pt.x + pt.y * grid.NCOLS);
      }
   }
   return path;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CPathPlanning::getPathSimple(int robotIDX, const GridCoords& goal, GridCoordsVector& path) {
   path.clear();
   if (robotIDX >= 0 and robotIDX < robotShapes.size() and dtRobots[robotIDX]) {
      GridCoordsVector pts;
      dtRobots[robotIDX]->getPath(goal, pts);
      std::reverse(pts.begin(), pts.end());
      IntVector backup;
      foreach(int i, robotShapes[robotIDX]) { 
         backup.push_back(grid[i]); 
         grid[i] = CDistanceTransform::FREESPACE; //mark robot as freespace 
      } //backup "obstacles" of the robot
      simplify2(pts, grid, path, 10);
      for(int i = 0; i < robotShapes[robotIDX].size(); ++i) {
         grid[robotShapes[robotIDX][i]] = backup[i]; //restore robot position 
      }
   }
   return path;
}


/// - public method ------------------------------------------------------------
/*
double CPathPlanning::getPathLength(int robotIDX, int goalIDX) const {
   double ret = -1.0;
   if (robotIDX >= 0 and robotIDX < robotShapes.size() and goalIDX >= 0 and goalIDX < goals.size()) {
      const GridCoordsVector& pts = robotGoalPaths[robotIDX][goalIDX];
      if (pts.size() > 1) {
         ret = 0.0;
         //DEBUG("CPathPlanning::getPathLength pts.size(): " << pts.size());
         for (int i = 1; i < pts.size(); i++) {
            ret += sqrt(pts[i-1].squared_distance(pts[i]));
         }
      }
   }
   return ret;
}
*/

/// - public method ------------------------------------------------------------
GridCoordsVector& CPathPlanning::getSimplifiedPath(int robotIDX, int goalIDX, GridCoordsVector& plan) {
   plan.clear();
   GridCoordsVector pts;
   if (robotIDX >= 0 and robotIDX < robotShapes.size() and goalIDX >= 0 and goalIDX < goals.size()) {
      foreach(const GridCoords& pt, robotGoalPaths[robotIDX][goalIDX]) {
         pts.push_back(pt);
      }
      //Having the rough estimation of the path, simplify
      IntVector backup;
      foreach(int i, robotShapes[robotIDX]) { 
         backup.push_back(grid[i]); 
         grid[i] = CDistanceTransform::FREESPACE; //mark robot as freespace 
      } //backup "obstacles" of the robot
      //simplify(pts, plan);
      simplify2(pts, grid, plan, 10);
      for(int i = 0; i < robotShapes[robotIDX].size(); ++i) {
         grid[robotShapes[robotIDX][i]] = backup[i]; //restore robot position 
      }
   }
   return plan;
}

/// - public method ------------------------------------------------------------
PathNodeVector& CPathPlanning::getPathPlan(const SPosition& pose, const GridCoordsVector& path, PathNodeVector& plan) {
   plan.clear();
   if (!path.empty()) {
      if (ROBOT_OMNI_MOTION) {
	 GridCoordsVector ptsFill;
	 fillPath(path, ptsFill);
	 SPathNode node;
	 for(int i = 1; i < ptsFill.size(); ++i) {
	    const GridCoords& pt = ptsFill[i];
	    node.coords = pt; 
	    node.pose.x = map.grid2realX(pt.x); 
	    node.pose.y = map.grid2realY(pt.y);
	    plan.push_back(node);
	 } //end all points of the path
      } else {
	 SPathNode node;
	 GridCoordsVector line;
	 SPosition cur = pose;
	 SPosition next(map.grid2realX(path[0].x), map.grid2realY(path[0].y));
	 if (fabs(cur.yaw - cur.azimuth(next)) > DEG2RAD(3)) { // allow 3 degrees tolerance
	    node.pose = cur; node.pose.yaw = next.yaw;
	    node.coords = GridCoords(map.real2gridX(node.pose.x), map.real2gridY(node.pose.y));
	    plan.push_back(node);
	 } //end turn at the spot
	 node.pose = next; node.coords = path[0]; plan.push_back(node);
	 for(int i = 1; i < path.size(); ++i) {
	    cur = next;
	    next.x = map.grid2realX(path[i].x);
	    next.y = map.grid2realY(path[i].y);
	    node.pose.yaw = cur.azimuth(next);
	    bresenham(path[i-1], path[i], line);
	    line.push_back(path[i]);
	    foreach(const GridCoords& pt, line) {
	       node.coords = pt; node.pose.x = map.grid2realX(pt.x); node.pose.y = map.grid2realY(pt.y);
	       //           ASSERT_ARGUMENT(map.
	       plan.push_back(node);
	    }
	 }
      }
   } //end path is not empty
   return plan;
}

/// - public method ------------------------------------------------------------
void CPathPlanning::checkPath(const SRobot& robot, const PathNodeVector& plan) {
   foreach(const SPathNode& pt, plan) {
      ASSERT_ARGUMENT(grid(pt.coords.y, pt.coords.x) != CDistanceTransform::OBSTACLE, "Collision detected");
   }
}

/// - public method ------------------------------------------------------------
FrontierVector& CPathPlanning::getReachableFrontiers(const FrontierVector& frontiers, FrontierVector& reachable) {
   reachable.clear();
   foreach(const SFrontier& f, frontiers) {
      foreach(CDistanceTransform* dt, dtRobots) {
         if (dt->isReachable(f.coords)) {
            reachable.push_back(f);
            break;
         }
      }
   }
   return reachable;
}

/// - public method ------------------------------------------------------------
bool CPathPlanning::isReachable(int robotIDX, const GridCoords& pt) {
   bool ret = false;
   if (robotIDX >= 0 and robotIDX < dtRobots.size()) {
      ret = dtRobots[robotIDX]->isReachable(pt);
   }
   return ret;
}

/// - public method ------------------------------------------------------------
int CPathPlanning::getRobotDTPathLength(int robotIDX, const GridCoords& pt) {
   int ret = std::numeric_limits<int>::max();
   if (robotIDX >= 0 and robotIDX < dtRobots.size()) {
      GridCoordsVector pathDT;
      GridCoordsVector pathDTSimple;
      dtRobots[robotIDX]->getPath(pt, pathDT);
      simplify2(pathDT, grid, pathDTSimple, 10);
      const double l1 = getPathLength(pathDTSimple);
      std::reverse(pathDT.begin(), pathDT.end());
      simplify2(pathDT, grid, pathDTSimple, 10);
      const double l2 = getPathLength(pathDTSimple);
      ret = l1 < l2 ? l1 : l2;
   }
   return ret;
}

/// - public method ------------------------------------------------------------
int CPathPlanning::getDTPathLength(const GridCoords& start, const GridCoords& goal) {
   int ret = std::numeric_limits<int>::max();
   CDistanceTransform* dt = new CDistanceTransform(grid);
   dt->setGoal(start);
   GridCoordsVector pathDT;
   GridCoordsVector pathDTSimple;
   dt->getPath(goal, pathDT);
   simplify2(pathDT, grid, pathDTSimple, 10);
   const double l1 = getPathLength(pathDTSimple);
   std::reverse(pathDT.begin(), pathDT.end());
   simplify2(pathDT, grid, pathDTSimple, 10);
   const double l2 = getPathLength(pathDTSimple);
   ret = l1 < l2 ? l1 : l2;
   delete dt;
   return ret;
}

/// - public method ------------------------------------------------------------
IntVector& CPathPlanning::getDTPathLength(const GridCoordsVector& starts, const GridCoords& goal, IntVector& distances) {
   CDistanceTransform* dt = new CDistanceTransform(grid);
   dt->setGoal(goal);
   distances.clear();
   for(int i = 0; i <starts.size(); ++i) {
      GridCoordsVector pathDT;
      GridCoordsVector pathDTSimple;
      dt->getPath(starts[i], pathDT);
      simplify2(pathDT, grid, pathDTSimple, 10);
      const double l1 = getPathLength(pathDTSimple);
      std::reverse(pathDT.begin(), pathDT.end());
      simplify2(pathDT, grid, pathDTSimple, 10);
      const double l2 = getPathLength(pathDTSimple);
      distances.push_back(l1 < l2 ? l1 : l2);
   }
   delete dt;
   return distances;
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CPathPlanning::getRobotDTPath(int robotIDX, const GridCoords& pt, GridCoordsVector& path) {
   path.clear();
   if (robotIDX >= 0 and robotIDX < dtRobots.size()) {
      GridCoordsVector pathDT;
      GridCoordsVector pathDTSimple1;
      GridCoordsVector pathDTSimple2;
      dtRobots[robotIDX]->getPath(pt, pathDT);
      simplify2(pathDT, grid, pathDTSimple1, 10);
      const double l1 = getPathLength(pathDTSimple1);
      std::reverse(pathDT.begin(), pathDT.end());
      simplify2(pathDT, grid, pathDTSimple2, 10);
      const double l2 = getPathLength(pathDTSimple2);
      if (l1 < l2) {
	 path = pathDTSimple1;
	 std::reverse(path.begin(), path.end());
 //        fillPath(pathDTSimple1, path);
      } else {
	 path = pathDTSimple2;
        // fillPath(pathDTSimple2, path);
      }
   }
   return path;
}

/// - private method -----------------------------------------------------------
void CPathPlanning::releasePaths(void) {
   if (robotGoalPaths) {
      for(int i = 0; i < robotShapes.size(); ++i) {
         delete[] robotGoalPaths[i];
      }
      delete[] robotGoalPaths;
      robotGoalPaths = 0;
   }
}

/* end of path_planning.cc */
