/*
 * File name: path_planning.h
 * Date:      2012/02/08 12:01
 * Author:    Jan Faigl
 */

#ifndef __PATH_PLANNING_H__
#define __PATH_PLANNING_H__

#include <vector>

#include "mre_types.h"
#include "map.h"

namespace mre {

   class CDistanceTransform;
   class CPathPlanning {
      private:
         const CMap& map;
         ByteMatrix grid;
         const bool USE_ROBOT_DT;
	 const bool ROBOT_OMNI_MOTION;
         typedef std::vector<CDistanceTransform*> DistanceTransformPtrVector;

         GridCoordsVector goals;
         IntVectorVector robotShapes;
         GridCoordsVector** robotGoalPaths;
         DistanceTransformPtrVector dtRobots;
         
      public:
         CPathPlanning(const CMap& map, bool use_robot_dt, bool omnimotion);
         ~CPathPlanning();

         const CMap& getMap(void) const { return map; }

         void updateMap();
         const ByteMatrix& getGrid(void) const { return grid; }
         void set(const RobotPtrVector& robots, const GridCoordsVector& goalsI);

         void computeRobotDT(const RobotPtrVector& robots);
         void compute(const RobotPtrVector& robots);

         void computeThetaStar(const RobotPtrVector& robots);

         IntVector& getPath(int robotIDX, int goalIDX, IntVector& path) const;
         GridCoordsVector& getPathSimple(int robotIDX, const GridCoords& goal, GridCoordsVector& path);
         const GridCoordsVector& getPath(int robotIDX, int goalIDX) const { return robotGoalPaths[robotIDX][goalIDX];}

 //        double getPathLength(int robotIDX, int goalIDX) const;

         GridCoordsVector& getSimplifiedPath(int robotIDX, int goalIDX, GridCoordsVector& plan);

         PathNodeVector& getPathPlan(const SPosition& pose, const GridCoordsVector& path, PathNodeVector& plan);

         void checkPath(const SRobot& robot, const PathNodeVector& plan);

         FrontierVector& getReachableFrontiers(const FrontierVector& frontiers, FrontierVector& reachable);

         bool isReachable(int robotIDX, const GridCoords& pt);

         int getRobotDTPathLength(int robotIDX, const GridCoords& pt);
         int getDTPathLength(const GridCoords& start, const GridCoords& goal);
         IntVector& getDTPathLength(const GridCoordsVector& starts, const GridCoords& goal, IntVector& distances);

         GridCoordsVector& getRobotDTPath(int robotIDX, const GridCoords& pt, GridCoordsVector& path);

      private:
         void releasePaths(void);
   };

} //end namespace mre

#endif

/* end of path_planning.h */
