/*
 * File name: goals.h
 * Date:      2012/08/06 13:40
 * Author:    Jan Faigl
 */

#ifndef __GOALS_H__
#define __GOALS_H__

#include <vector>

#include "mre_types.h"
#include "mre_sdl_gui.h" //debug
#include "map.h"
#include "distance_transform.h"
#include "basic_theta_star.h"


namespace mre {

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   struct SGoal {
      const GridCoords coords;
      const int label;
      const int globalLabel;
      const int index;
      const int goalSet;

      CDistanceTransform* dt;

      bool covered;

      IntVector coveringCells;

      SGoal(const GridCoords& coords, int label = -1, int globalLabel = -1, int index = -1, int goalSet = -1) : coords(coords), label(label), globalLabel(globalLabel), index(index), goalSet(goalSet), dt(0), covered(false) {}
      ~SGoal() { if (dt) { delete dt; } }
   };

   typedef std::vector<SGoal*> GoalPtrVector;
   struct SGoalSet {
      const int label;
      SGoal* representative;
      GoalPtrVector goals;
      SGoalSet(SGoal* representative, int label = -1) : representative(representative), label(label) {}
      ~SGoalSet() {}
   };

   struct SCell {
      const int index;
      SCell(int index) : index(index) {};
      GoalPtrVector coveredGoals;
   };

   typedef std::vector<SGoalSet*> GoalSetPtrVector;
   typedef std::vector<SCell*> CellPtrVector;

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   class CGoals {
      gui::CGui* gui;
      const CMap& map;
      ByteMatrix grid;
      const double RANGE_RATIO;
      const double LASER_RANGE;
      const int NUM_ROBOTS;
      GoalPtrVector allGoals;
      GoalPtrVector goals;
      GoalPtrVector representatives;
      GoalSetPtrVector goalSets;
      CellPtrVector cells;

      public:

      static ByteMatrix& getPlanningGrid(const CMap& map, ByteMatrix& grid);
      static GridCoordsVectorVector& findPath(const ByteMatrix& grid, const GridCoords& start, const GridCoordsVector& goals, GridCoordsVectorVector& paths);
      static GridCoordsVector& findPath(const ByteMatrix& grid, const GridCoords& start, const GridCoords& goal, GridCoordsVector& path);

      CGoals(const CMap& map, const GridCoordsVectorVector& goalSets, const RobotPtrVector& robots, double visibilityRangeRatio = 0.99, bool computeCoverage = true, gui::CGui* gui = 0);
      ~CGoals();

      void setGui(gui::CGui* g) { gui = g; }

      int size(void) const { return goals.size(); }
      int size_goal_sets(void) const { return goalSets.size(); }

      SGoal* goal(int i) const { return goals[i]; }
      SGoal* globalGoal(int i) const { return allGoals[i]; }
      const ByteMatrix& getGrid(void) const { return grid; }

      GridCoordsVector& getGoalsRepresentatives(GridCoordsVector& newGoals);
      GridCoordsVector& getGoalsGoalSetCoverage(GridCoordsVector& newGoals);
      GridCoordsVector& getGoalsCompleteCoverage(GridCoordsVector& newGoals, IntVector& goalsCoverage);
      GridCoordsVector& getGoalsCompleteCoverage2(GridCoordsVector& newGoals);
      GridCoordsVector& getGoalsCompleteCoverageCumulativeCoverage(GridCoordsVector& newGoals);
      GridCoordsVector& getGoalsCompleteCoverageFOV(GridCoordsVector& newGoals);
      GridCoordsVector& getGoalsCompleteCoverageFOV2(GridCoordsVector& newGoals);
      GridCoordsVector& getGoalsCompleteCoverageFOV3(GridCoordsVector& newGoals);
      GridCoordsVector& getCoveringCells(GridCoordsVector& newGoals, IntVector& goalsCoverage);
      GridCoordsVector& getCoveringCellsRandom(double samplesSigma, GridCoordsVector& newGoals, IntVector& goalsCoverage);

      GridCoordsVector& getAlternateGoals(const GridCoords& robotPose, const GridCoordsVector& goals, GridCoordsVector& alternateGoals) const;

      ByteMatrix& getUnknownSpacePartition(const RobotPtrVector& robots, ByteMatrix& partition);

      private:
      void computeCoveringCells(void);
   };

} //mre


#endif

/* end of goals.h */
