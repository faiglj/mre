#ifndef SND_DATA_H
#define SND_DATA_H


#include <vector>


#ifdef USE_ROS
#include <ros/ros.h>
#endif


typedef unsigned int uint32_t;


class SND_data
{
	private:
		
		double scanRes;
		double maxRange;
		uint32_t count;

		// robot posotion
		double x;
		double y;
		double yaw;
		
		// goal postition
		double goalX,goalY,goalA;
		
		std::vector<double> laser_ranges;
		
#ifdef USE_ROS
		ros::Publisher * publisher;
#else
        double turnSpeed;
        double driveSpeed;
#endif

	public:
		SND_data();
		double robot_radius;
		double min_gap_width;
		double obstacle_avoid_dist;
		double max_speed;
		double max_turn_rate;
		double goal_position_tol;
		double goal_angle_tol;
		std::vector< std::vector<double> > goal_vector;
		
		double   GetScanRes() { return scanRes; };
		double   GetMaxRange() { return maxRange; };
		uint32_t GetCount() { return count; };
		
		double   range(const int index) {return laser_ranges[index]; };

		double GetXPos() { return x; };
		double GetYPos() { return y; };
		double GetYaw()  { return yaw; };
		
		void setPos(double posX, double posY, double posA) {x=posX; y=posY, yaw=posA; };

		double getGoalX() { return goal_vector[0][0]; }
		double getGoalY() { return goal_vector[0][1]; }
		double getGoalA() { return goal_vector[0][2]; }
		
		void setGoal(double x, double y, double a) {goalX = x; goalY = y; goalA = a; };
		void setGoal(std::vector< std::vector<double> > goals) { goal_vector = goals; };
		
		bool hasNextGoal() { if (goal_vector.size() > 0) return true; else return false; };
		bool shutSlowDown() { if (goal_vector.size() == 1) return true; else return false; };
		
		void setLaserScan(double res, double range, std::vector<double> scans);
#ifdef USE_ROS
		void setPublisher(ros::Publisher * pub) { publisher = pub; };
#else
        double getTurnSpeed(void) { return this->turnSpeed; }
        double getDriveSpeed(void) { return this->driveSpeed; }
#endif
		
		void publishSpeed(double driveSpeed, double turnSpeed);
		void WaitForNextGoal();
		
		void exit();
	/*
		void   SetSpeed(double velocity_modulus,
							double velocity_angle);
		void   SetSpeed(double velocity_x,
							double velocity_y,
							double velocity_angle);
	*/


};

#endif //SND_DATA_H
