/*
 * File name: sdl_gui.h
 * Date:      2011/12/26 20:51
 * Author:    Jan Faigl
 */

#ifndef __SDL_GUI_H__
#define __SDL_GUI_H__

#include <SDL.h>

#include <matrix_utils.h>

#include "image_types.h"

namespace mre { namespace gui { 

   /*
   /// ----------------------------------------------------------------------------
   /// @brief draw
   /// 
   /// @param s 
   /// @param pt 
   /// @param rgb 
   /// ----------------------------------------------------------------------------
   template<class Coords, class RGB>
   void draw(SDL_Surface* s, const Coords& pt, const RGB& rgb) {
   Uint8* p = (Uint8*)s->pixels + pt.y * s->pitch + pt.x * 3;
    *(p + s->format->Rshift / 8) = rgb.r;
    *(p + s->format->Gshift / 8) = rgb.g;
    *(p + s->format->Bshift / 8) = rgb.b;
    }
    */

   template<class Coords, class RGB>
      RGB& getRGB(SDL_Surface* s, const Coords& pt, RGB& rgb) {
         Uint8* bufp = (Uint8*)s->pixels + pt.y * s->pitch + pt.x * 3;
         rgb.r = *(bufp + s->format->Rshift / 8);
         rgb.g = *(bufp + s->format->Gshift / 8);
         rgb.b = *(bufp + s->format->Bshift / 8);
         return rgb;
      }

   class CSDL {
      private:
         static bool initialized;
         static bool videoInit;
         static int sdl_init_counter;
         SDL_Surface* scr;
	 SDL_Window* win;
	 SDL_Texture* txt;
	 SDL_Renderer *rnd;
	 bool offscreen;
         int width;
         int height;
      public:

         static SDL_Surface* load_img(const std::string& filename, bool check = true);
         static SDL_Surface* load_img(const std::string& filename, SDL_Surface** img);
         static SDL_Surface* load_img_raw(const std::string& filename, SDL_Surface** img, bool force3bpp = false);
         static SDL_Surface* get_img(const unsigned char* buf, const int size, SDL_Surface** img);
         static SDL_Surface* get_img_rgb(const unsigned char* buf, const int size, SDL_Surface* img);

         static bool save_img(const unsigned char* buf, const int size, const std::string& filename);
         static bool save_img_bmp(SDL_Surface* img, const std::string& filename);

         static bool save_img_jpeg(SDL_Surface* img, int quality, const std::string& filename);
         bool save_img_png(int quality, const std::string& filename);

         static SImage* convert(const SDL_Surface* src, SImage* img);

         void draw(int x, int y, const SRGB& color, SDL_Surface* dst);
         void draw(const IntVector& pixels, const SRGB& color, SDL_Surface* dst);
         void draw(const int idx, const SRGB& color, SDL_Surface* dst);
         void drawGrid255(const FloatMatrix& grid, SDL_Surface* dst);
         void drawGrid(const ByteMatrix& grid, SDL_Surface* dst);

         static DoubleVector& getIntegralImage(SDL_Surface* s, DoubleVector& img);
         static SDL_Surface* resize(SDL_Surface* s, double zoomx, double zoomy);

         CSDL(bool init_video = true);
         ~CSDL();

         bool operator()(void) { return scr; }

         void init(int width, int height, const std::string& title = "mre::gui", const std::string& icon = "mre::gui", bool grayscale = false);
         void initOffScreen(int width, int height, bool grayscale = false, bool rgba = false);
         SDL_Surface* create_surface(void);
         SDL_Surface* getScreenSurface(void) { return scr; }
         SDL_Renderer* getScreenRenderer(void) { return rnd; }
         void refresh(void);
         void refresh(SDL_Surface* img);

         void draw(const IntVector& pixels, const SRGB& color);
   };

} } //end namespace mre::gui

#endif

/* end of sdl_gui.h */
