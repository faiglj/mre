/*
 * File name: mre_ranger_sim.cc
 * Date:      2012/02/10 08:41
 * Author:    Jan Faigl
 */

#include <boost/foreach.hpp>

#include <logging.h>

#include "mre_grid_utils.h"
#include "mre_ranger_sim.h"

#define foreach BOOST_FOREACH

using imr::logger;

using namespace mre;

/// - static method ------------------------------------------------------------
imr::CConfig& CRangerSim::getConfig(imr::CConfig& config) {
   config.add<std::string>("ranger-sim-map", "Bitmap map e.g., png, used for the simulation", "");
   CRangerSimMap::getConfig(config);
   config.add<int>("ranger-sim-scan-samples", "Number of samples", 180);
   config.add<double>("ranger-sim-scan-min-angle", "Minimal angle of the scan, in degrees", -120.0);
   config.add<double>("ranger-sim-scan-max-angle", "Maximal angle of the scan, in degrees", 120.0);
   config.add<double>("ranger-sim-scan-range", "Range of the single distanece measurement", 10.0);
   return config;
}

/// - constructor --------------------------------------------------------------
CRangerSim::CRangerSim(imr::CConfig& cfg, double cell_size) :
   cfg(cfg), map(cfg, cfg.get<std::string>("ranger-sim-map"), cell_size),
   enlargedMap(map.H, map.W),
   LASER_RANGE(cfg.get<double>("ranger-sim-scan-range"))

{
   laser.laserPose = SPosition(0.0, 0.0, 0.0); //Notice position of the laser should be placed regarding the robot body

   laser.resize(cfg.get<int>("ranger-sim-scan-samples"));
   laser.minAngle = DEG2RAD(cfg.get<double>("ranger-sim-scan-min-angle"));
   laser.maxAngle = DEG2RAD(cfg.get<double>("ranger-sim-scan-max-angle"));
   laser.resAngle = (laser.maxAngle - laser.minAngle) / laser.size;
   for(int i = 0; i < map.grid.SIZE; ++i) {
      enlargedMap[i] = map.grid[i];
   }
}

/// - destructor ---------------------------------------------------------------
CRangerSim::~CRangerSim() {
}

/// - public method ------------------------------------------------------------
void CRangerSim::enlargeMap(double r) {
   const int neigh = (ceil(r / map.CELL_SIZE) - 1); //2
   const int neigh2 = neigh*neigh;
   DEBUG("neigh: "  << neigh);
   for(int i = 0; i < enlargedMap.SIZE; ++i) { enlargedMap(i) = CRangerSimMap::FREESPACE; }
   for(int r = 0; r < enlargedMap.NROWS; ++r) {
      for(int c = 0; c < enlargedMap.NCOLS; ++c) {
         if (map.grid(r, c) == CRangerSimMap::OBSTACLE) {
            for (int i = -neigh; i < neigh + 1; i++) { //enlarge grid
               for (int j = -neigh; j < neigh + 1; j++) {
                  if (i*i + j*j <= neigh2) {
                     const int x = c + j;
                     const int y = r + i;
                     if (x >= 0 and x < map.W and y >= 0 and y < map.H) {
                        enlargedMap(y, x) = CRangerSimMap::OBSTACLE;
                     }
                  }
               }
            }
         } // end if obstacle
      }
   }
}

/// - public method ------------------------------------------------------------
void CRangerSim::setPose(const SPosition& pose, const GridCoords& gridPose, SRobot& robot) {
   robot.pose.yaw = pose.yaw;
   robot.gridPose = gridPose;
   robot.pose.x = map.grid2realX(gridPose.x);
   robot.pose.y = map.grid2realY(gridPose.y);
   ASSERT_ARGUMENT(enlargedMap(robot.gridPose.y, robot.gridPose.x) == CRangerSimMap::FREESPACE, "Collision detected");
}

/// - public method ------------------------------------------------------------
int CRangerSim::getScan(const SRobot& robot, double range, IntVector& vx, IntVector& vy, IntVector& obstacles) const {
   vx.resize(laser.size);
   vy.resize(laser.size);
   obstacles.clear();
   double a = laser.minAngle + robot.pose.yaw;
   for(int i = 0; i < vx.size(); ++i) {
      const GridCoords e(robot.gridPose.x + round((range / map.CELL_SIZE - 1) * cos(a)), robot.gridPose.y + round((range / map.CELL_SIZE - 1) * sin(a)));
      GridCoordsVector beam;
      GridCoords ptCol;
      bresenham(robot.gridPose, e, beam);
      if (map.isCollisionFree(beam, ptCol)) {
         vx[i] = e.x; vy[i] = e.y;
      } else {
         vx[i] = ptCol.x; vy[i] = ptCol.y;
         obstacles.push_back(ptCol.x + ptCol.y * map.W);
      }
      a += laser.resAngle;
   }
   return vx.size();
}

/// - public method ------------------------------------------------------------
SLaserData& CRangerSim::getScan(const SPosition& pos, SLaserData& scan) const {
   scan.minAngle = laser.minAngle;
   scan.maxAngle = laser.maxAngle;
   scan.resAngle = laser.resAngle;
   scan.resize(laser.size);
   scan.laserPose = laser.laserPose;
   scan.robotPose = pos;

   double a = laser.minAngle + pos.yaw;
   const GridCoords pt(map.real2gridX(pos.x), map.real2gridY(pos.y));
   ASSERT_ARGUMENT(enlargedMap(pt.y, pt.x) == CRangerSimMap::FREESPACE, "CRangerSimMap - Collision detected");
   for(int i = 0; i < scan.size; ++i) {
      const SPosition p(pos.x + LASER_RANGE * cos(a), pos.y + LASER_RANGE * sin(a));
      GridCoordsVector beam;
      GridCoords ptCol;
      bresenham(pt, GridCoords(map.real2gridX(p.x), map.real2gridY(p.y)), beam);
      if (map.isCollisionFree(beam, ptCol)) {
         scan.samples[i] = LASER_RANGE;
      } else {
         scan.samples[i] = sqrt(pos.squared_distance(SPosition(map.grid2realX(ptCol.x), map.grid2realY(ptCol.y))));
      }
      a += scan.resAngle;
   }
   return scan;
}

/// - public method ------------------------------------------------------------
int CRangerSim::getScanReduced(const SRobot& robot, double range, IntVector& vx, IntVector& vy, IntVector& obstacles) const{
   vx.clear();
   vy.clear();
   obstacles.clear();
   GridCoords prev;
   double a = laser.minAngle + robot.pose.yaw;
   for(int i = 0; i < laser.size; ++i) {
      const GridCoords e(robot.gridPose.x + round((range / map.CELL_SIZE - 1) * cos(a)), robot.gridPose.y + round((range / map.CELL_SIZE - 1) * sin(a)));
      GridCoordsVector beam;
      GridCoords ptCol;
      bresenham(robot.gridPose, e, beam);
      if (map.isCollisionFree(beam, ptCol)) {
         if (vx.empty() or prev.manhattan_distance(e) > 0) {
            vx.push_back(e.x); vy.push_back(e.y);
            prev = e;
         }
      } else if (vx.empty() or prev.manhattan_distance(ptCol) > 0) {
         vx.push_back(ptCol.x); vy.push_back(ptCol.y);
         obstacles.push_back(ptCol.x + ptCol.y * map.W);
         prev = ptCol;
      }
      a += laser.resAngle;
   }
   return vx.size();
}

/// - public method ------------------------------------------------------------
int CRangerSim::getScanOmni(const SRobot& robot, double range, IntVector& vx, IntVector& vy, IntVector& obstacles) const {
   vx.clear();
   vy.clear();
   GridCoords prev;
   GridCoords ct(map.W / 2, map.H / 2);

   foreach(const GridCoords& endPt, scanEndPoints) {
      GridCoordsVector beam;
      GridCoords ptCol;
      const GridCoords e(
            endPt.x - ct.x + robot.gridPose.x,
            endPt.y - ct.y + robot.gridPose.y
            );
      bresenham(robot.gridPose, e, beam);
      if (map.isCollisionFree(beam, ptCol)) {
         if (vx.empty() or prev.manhattan_distance(e) > 0) {
            vx.push_back(e.x); vy.push_back(e.y);
            prev = e;
         }
      } else if (vx.empty() or prev.manhattan_distance(ptCol) > 0) {
         vx.push_back(ptCol.x); vy.push_back(ptCol.y);
         obstacles.push_back(ptCol.x + ptCol.y * map.W);
         prev = ptCol;
      }

   }
   return vx.size();
}

/// - public method ------------------------------------------------------------
void CRangerSim::initScanOmni(const double LASER_RANGE) {
   scanEndPoints.clear();
   const int N = laser.size;
   const double da = 2 * M_PI / N; //ASSUME 360 degree laser scanner and 2*361 measurements
   SPosition ct(map.grid2realX(map.W / 2), map.grid2realY(map.H / 2));
   double a = 0;
   for(int i = 0; i < N; ++i) {
      SGridCoords endPt(GridCoords(
               map.real2gridX(ct.x + LASER_RANGE * cos(a)),
               map.real2gridY(ct.y + LASER_RANGE * sin(a))
               ));
      if (scanEndPoints.empty()) {
         scanEndPoints.push_back(endPt);
      } else {
         if (scanEndPoints.back().manhattan_distance(endPt) > 0) {
            scanEndPoints.push_back(endPt);
         }
      }
      a += da;
   }
   const int d = scanEndPoints.front().manhattan_distance(scanEndPoints.back());
   if (d >= 2) {
     scanEndPoints.push_back(GridCoords(scanEndPoints.back().x, scanEndPoints.front().y));
   }
   GridCoordsVector pts;
   bresenham(scanEndPoints.back(), scanEndPoints.front(), pts, true, false);
}

/* end of mre_ranger_sim.cc */
