/*
 * File name: voronoi_planning.h
 * Date:      2011/05/28 18:58
 * Author:    Jan Faigl
 */

#ifndef __VORONOI_PLANNING_H__
#define __VORONOI_PLANNING_H__

#include "mre_grid_utils.h"

namespace mre {

   class CVoronoiPlanning {
      public:
         CVoronoiPlanning(const ByteMatrix& map, const unsigned char freespace = 255);
         ~CVoronoiPlanning();

         void compute(bool saveDistances = false);

         GridCoordsVector& getPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path); 

         const GridCoordsVector& getVornoiCells(void) const { return nodes_; }

         void getVoronoiEdges(GridCoordsVector& s, GridCoordsVector& e) const;

         FloatVector& getDistances(const GridCoordsVector& path, GridCoordsVector& obstaclesO, FloatVector& distancesO);

      private:
         const ByteMatrix& grid; //source grid map
         const unsigned char FREESPACE;
         const int W;
         const int H;
         ByteMatrix sGrid;
         GridCoordsVector nodes_;
         IntVector gridCoordsIdxs_;
         FloatVector distances;
         IntVector obstacles;
   };

} //end namespace mre

#endif

/* end of voronoi_planning.h */
