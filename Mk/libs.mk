#
# Date:      2011/07/11 17:55
# Author:    Jan Faigl
#

CPPFLAGS+=-std=c++0x

OPSYS=$(shell uname)

PLATFORM=$(shell uname -p)
ARCH=.$(PLATFORM)

ROOT_DIR:=$(CURDIR)/$(root_dir)

ifeq ($(OPSYS),FreeBSD)
   include $(CURDIR)/$(root_dir)/Mk/libs.bsd.mk
else
   include $(CURDIR)/$(root_dir)/Mk/libs.lnx.mk
endif

LOG4CXX_CFLAGS:=$(shell pkg-config --cflags liblog4cxx)
LOG4CXX_LDFLAGS:=$(shell pkg-config --libs liblog4cxx)
LOG4CXX_LDFLAGS+=-llog4cxx 

CAIRO_CFLAGS:=$(shell pkg-config --cflags cairo)
CAIRO_LDFLAGS:=$(shell pkg-config --libs cairo) -lX11

SDL2_CFLAGS:=$(shell sdl2-config --cflags)
SDL2_LDFLAGS:=$(shell sdl2-config --libs) -lSDL2_gfx -lSDL2_image

local_dir:=/usr/local
BOOST_CFLAGS:=-I$(local_dir)/include
BOOST_LDFLAGS+=-L$(local_dir)/lib
BOOST_LDFLAGS+=-lboost_program_options -lboost_thread -lboost_filesystem -lboost_iostreams -lboost_system
BOOST_LDFLAGS:=$(BOOST_LDFLAGS)


CGAL_CPFLAGS:=-I/usr/local/include
CGAL_LDFLAGS:=-L/usr/local/lib/ -pthread -lCGAL -lCGAL_Core -lz -lmpfr -lgmpxx -lgmp -lboost_program_options -lm 

JPEG_CFLAGS:=-I$(local_dir)/include
JPEG_LDFLAGS:=-L$(local_dir)/lib -ljpeg

CONCORDE_CFLAGS:=-I$(local_dir)/include/concorde
CONCORDE_LDFLAGS:=-L$(local_dir)/lib -lconcorde

FOV_CFLAGS:=-I$(local_dir)/include
FOV_LDFLAGS:=-L$(local_dir)/lib -lfov

HDF5_CPPFLAGS=-I$(local_dir)/include
HDF5_LDFLAGS=-L$(local_dir)/lib -lhdf5_hl_cpp -lhdf5_hl -lhdf5_cpp -lhdf5

# local opt libraties
ANN_CFLAGS:=-I/usr/local/opt/ANN
ANN_LDFLAGS:=-L/usr/local/opt/ANN -lANN

# vendors libraties
imr-h_dir:=$(ROOT_DIR)
LOCAL_CFLAGS:=-I$(imr-h_dir)/include
LOCAL_LDFLAGS:=-L$(imr-h_dir)/lib

IMR-H_LDFLAGS=-limr-h
IMR-H-ALGORITHM_LDFLAGS=-limr-h-algorithm
IMR-H-GUI_LDFLAGS=-limr-h-gui

TRIANGLE_CFLAGS:=-I$(imr-h_dir)/include
TRIANGLE_LDFLAGS:=-L$(imr-h_dir)/lib -ltriangle

HUNGARIAN_CFLAGS:=-I$(imr-h_dir)/include
HUNGARIAN_LDFLAGS:=-L$(imr-h_dir)/lib -lhungarian

#PLAYER_CFLAGS:=$(shell pkg-config --cflags playerc++)
#PLAYER_LIBS:=$(shell pkg-config --libs playerc++) -pthread
PLAYER_CFLAGS:=-I/usr/local/opt/player3/include/player-3.0  -I/usr/local/include
PLAYER_LDFLAGS:=-L/usr/local/opt/player3/lib -lplayerc++ -L/usr/local/lib -lboost_thread -lboost_signals -lplayerc -lm -lz -lplayerinterface -lplayerwkb -lgeos -lgeos_c -lplayercommon -pthread
