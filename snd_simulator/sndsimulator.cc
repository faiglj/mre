/*
 * File name: sndsimulator.cc
 * Date:      Mon Oct 15 14:50:45 +0200 2012
 * Author:    Miroslav Kulich
 */

#include <unistd.h>
//#include <curses.h>
#include <boost/foreach.hpp>
#include <iostream>
#include <sstream>

#include <logging.h>

#include "sndsimulator.h"

#define foreach BOOST_FOREACH

using namespace mre;

using imr::logger;

/// Callbacks ------------------------------------------------------ 

/**
 * Function called by libfov to apply light to a cell.
 *
 * \param map Pointer to map data structure passed to function such as
 *            fov_circle.
 * \param x   Absolute x-axis position of cell.
 * \param y   Absolute x-axis position of cell.
 * \param dx  Offset of cell from source cell on x-axis.
 * \param dy  Offset of cell from source cell on y-axis.
 * \param src Pointer to source data structure passed to function such
 *            as fov_circle.
 */
void apply(void* map, int x, int y, int dx, int dy, void* src) {
  if (((CSndSimulator *)map)->onMap(x, y))
    ((CSndSimulator *)map)->setSeen(x, y);
}


/**
 * Function called by libfov to determine whether light can pass
 * through a cell. Return zero if light can pass though the cell at
 * (x,y), non-zero if it cannot.
 *
 * \param map Pointer to map data structure passed to function such as
 *            fov_circle.
 * \param x   Absolute x-axis position of cell.
 * \param y   Absolute x-axis position of cell.
 */
bool opaque(void *map, int x, int y) {
  return ((CSndSimulator *)map)->blockLOS(x, y);
}


/// - static -------------------------------------------------------------------
imr::CConfig& CSndSimulator::getConfig(imr::CConfig& config) {
//   config.add<double>("x", "help", 0.0);
   CSnd::getConfig(config);
   return config;
}

/// - constructor --------------------------------------------------------------
CSndSimulator::CSndSimulator(int mapW, int mapH, SLaserConfig &lconfig, imr::CConfig& config, gui::CGui* g) :
  W(mapW),
  H(mapH),
  grid(mapH,mapW),
  laserConf(lconfig),
  snd(lconfig,config),
  gui(g),
  cfg(config)
{
  fov_settings_init(&fov_settings);
  fov_settings_set_opacity_test_function(&fov_settings, opaque);
  fov_settings_set_apply_lighting_function(&fov_settings, apply);
  maxRange = cfg.get<double>("laser-max-range");
  cellSize = cfg.get<double>("grid-cell-size");
}

/// - destructor --------------------------------------------------------------
CSndSimulator::~CSndSimulator() {
  fov_settings_free(&fov_settings);
}

/// - public --------------------------------------------------------------
void CSndSimulator::setMap(CMap* m) {
  map = m;
  for(int i = 0; i < map->grid.SIZE; ++i) {
    if (map->grid(i) > 0.7) {
      grid(i) = CMap::OCCUPIED_CELL;
    } else if (map->grid(i) < 0.2) {
      grid(i) = CMap::FREESPACE_CELL;
    } else {
      grid(i) = CMap::UNKNOWN_CELL;
    }
  }
}

/// - public --------------------------------------------------------------
bool CSndSimulator::onMap(unsigned int x, unsigned int y) {
  return (x<W && y<H);
}

/// - public --------------------------------------------------------------
bool CSndSimulator::blockLOS(unsigned int x, unsigned int y) {
  if(!onMap(x,y)) return true;
  return (grid(y, x) != CMap::FREESPACE_CELL);
}

/// - private --------------------------------------------------------------
// dir is in radians
int CSndSimulator::getScanIndex(double dir, bool down) {
  const double density = double(numSamples)/(2*M_PI); // number of samples per degree
  double tmp = density*dir;
  int result = down ? ceil(tmp) : floor(tmp);
  if (result<0) { result+=numSamples; };
  if (result>=numSamples) { result-=numSamples; };
  return result;
}
  
/// - private --------------------------------------------------------------
double CSndSimulator::getAngle(double x, double y) {
  const double diagCellSize = cellSize*sqrt(2);
  double dx = map->grid2realX(x) - robot_x;
  double dy = map->grid2realY(y) - robot_y;
  double dist = sqrt(dx*dx + dy*dy);
  double angle = atan2(dy,dx);
  double delta = diagCellSize/(2*dist);
  int start = getScanIndex(angle-delta,true);
  int end = getScanIndex(angle+delta,false);
//   DEBUG("obst: " << x << " " << y << " robot: " << map->real2gridX(robot_x) << " " << map->real2gridY(robot_y));
//   DEBUG("obst: " << map->grid2realX(x) << " " << map->grid2realY(y) << " robot: " << robot_x << " " << robot_y);
//   DEBUG("dist: " << dist << " " << angle << " " << delta);
//   DEBUG(angle-delta << " " << angle+ delta);
//   DEBUG("start: " << start << " " << end);
  if (start < end) {
    for(int i=start;i<=end;i++) {
      if (scan[i] > dist) {
        scan[i] = dist;
      }
    }
  } else {
    for(int i=start;i<numSamples;i++) {
      if (scan[i] > dist) {
        scan[i] = dist;
      }
    }
    for(int i=0;i<=end;i++) {
      if (scan[i] > dist) {
        scan[i] = dist;
      }
    }
  }

  return angle;
}

/// - public --------------------------------------------------------------
void CSndSimulator::setSeen(unsigned int x, unsigned int y) {
  if(!onMap(x,y)) return;
  if (grid(y,x) != CMap::FREESPACE_CELL) {
//    std::cout << "[" << x << " " << y << "] " << (int) grid(y,x) << "]" << std::endl;
    getAngle(x, y);
  }
}

/// - public --------------------------------------------------------------
CSnd::SScan CSndSimulator::getScan(const SPosition& pose, bool draw) {
  CSnd::SScan result;
  numSamples = ceil(2*M_PI/laserConf.resolution);
  result.resize(laserConf.count);
//   DEBUG("laser: count " << lconf.count);
//   DEBUG("laser: maxRange " << lconf.maxRange);
//   DEBUG("laser: resolution " << lconf.resolution);
//   DEBUG("laser: minAngle " << lconf.minAngle);
//   DEBUG("laser: maxAngle " << lconf.maxAngle);
//   DEBUG("laser: numSamples " << numSamples);
  
  robot_x = pose.x;
  robot_y = pose.y;
  int radius = maxRange/cellSize;
  scan.resize(numSamples);
  for(int i=0;i<numSamples;i++) {
    scan[i] = maxRange;
  }
    
  fov_circle(&fov_settings, this, NULL, map->real2gridX(pose.x), map->real2gridY(pose.y), radius);
  
  double x, y;
  int start = getScanIndex(pose.yaw + laserConf.minAngle,true);
  int end  = getScanIndex(pose.yaw + laserConf.maxAngle,false);
//  std::stringstream ss;
  double a=pose.yaw + laserConf.minAngle;
  for(int n = 0, i=start;n<laserConf.count;a+=laserConf.resolution,n++) {
//    ss << "n: " << n << " " << i << " " << scan[i] << " ";
    result[n] = scan[i];
    x = pose.x + result[n]*cos(a);
    y = pose.y + result[n]*sin(a);
    if (draw) {
      gui->drawPoint(map->real2gridY(y),map->real2gridX(x),0, 50, 250);
    }
    i = i==numSamples-1 ? 0 : i+1;    
  }
//  DEBUG(ss.str());
  return result;
}

/// - public --------------------------------------------------------------
int CSndSimulator::getTrajectory(const SPosition& pose, CPath &path) {
  int numSteps = 0;
  bool draw;
  SPosition actPose(pose);
//  DEBUG("Act robot pose (" <<  map->real2gridX(pose.x) << ", " << map->real2gridY(pose.y));
//  gui->drawPoint(map->real2gridY(pose.y),map->real2gridX(pose.x),0,255,0);
  int xx = map->real2gridX(pose.x);
  int yy = map->real2gridY(pose.y);
   for(int iy=yy-1;iy<=yy+1;iy++) {
    for(int ix=xx-1;ix<=xx+1;ix++) {
       grid(iy,ix) = CMap::FREESPACE_CELL;
     }
   }
//  gui->drawGridLight(grid);
//  gui->refresh();
  while (path.hasNextGoal()) {
    draw = numSteps == -1;
    SPosition goal = path.getGoal();
//    DEBUG("Goal pose (" <<  map->real2gridX(goal.x) << ", " << map->real2gridY(goal.y) << " |  " << goal.x << ", " << goal.y );
//    gui->drawPoint(map->real2gridY(goal.y),map->real2gridX(goal.x),255,0,0);
    CSnd::SScan scan = getScan(actPose,draw);
    snd.oneStep(actPose, path,scan);

    /* This assumes we move at maximum speed. */
    double R = snd.getDriveSpeed()*0.2;
    double Phi = snd.getTurnSpeed()*0.2;

//    DEBUG("R: " << R << " Phi: " << Phi);

    actPose.x += R * cos(actPose.yaw);
    actPose.y += R * sin(actPose.yaw);
    actPose.yaw = normalize(actPose.yaw + Phi);
//    DEBUG("Actual pose (" <<  map->real2gridX(actPose.x) << ", " << map->real2gridY(actPose.y) << "  " << actPose.yaw);
//    if (draw) {
//      gui->drawPoint(map->real2gridY(actPose.y),map->real2gridX(actPose.x),0,255,0);
//    }
    numSteps++;
    if (numSteps > 300) break;
  }    
//    gui->refresh();

  INFO("Travel Time: " << numSteps);
  return numSteps;
}

/* end of sndsimulator.cc */
