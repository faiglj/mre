/*
 * File name: mre.h
 * Date:      2012/01/23 21:50
 * Author:    Jan Faigl
 */

#ifndef __MRE_H__
#define __MRE_H__

#include <vector>
#include <boost/thread/barrier.hpp>

#include <imr_config.h>
#include <concurrent/thread.h>

#include "sdl_gui.h"
#include "mre_sdl_gui.h"

#include "mre_types.h"
#include "mre_ranger_sim.h"

#include "map.h"

#include "global_control.h"
#include "robot_player.h"
#include "mre_events.h"
#include "results_saver.h"
#include "paths.h"
#include "path_planning.h"

namespace mre {

   class CMRE : public imr::concurrent::CThread {
      typedef imr::concurrent::CThread ThreadBase;
      typedef enum { GREEDY_ALLOC_RAND = 1, PAIRS = 2, HUNGARIAN = 3, MTSP_KMEANS = 5} TStrategy;
      typedef enum { DISTANCE_COST = 0, SND_COST = 1, ROT_PENALTY_COST = 2, TURN_PENALTY_COST = 3} TCost;
      typedef enum { ALL_FRONTIERS = 0, KMEANS_ICRA11 = 1, REPRESENTATIVE = 2, GOAL_SET_COVERAGE = 3, COMPLETE_COVERAGE = 4, COVERING_CELLS = 5, COVERING_CELLS_SAMPLES = 6} TGoals;
      private:
      imr::CConfig& cfg;
      CMap* map;
      SResults results;
      gui::CSDL sdl;
      gui::CGui* gui;

      CGlobalControl global;
      const double LASER_RANGE_MAX;
      const bool GLOBAL_CONTROL;
      const int IMAGE_QUALITY;
      const TStrategy STRATEGY;
      const TGoals GOALS_SELECTION;
      const TCost COST_FUNCTION;
      const int MIN_NUMBER_OF_FRONTIER_CELLS;
      const double GOAL_CLUSTER_RADIUS;
      EventQueue queue;
      RobotPlayerPtrVector robots;
      RobotPtrVector mreRobots;
      bool quit;
      Mutex mtx;
      boost::barrier *waitForDataBarrier;
      Condition cond;
      std::string output;
      std::string picDir;
      bool started;

      public:
      static imr::CConfig& getConfig(imr::CConfig& config);
      CMRE(imr::CConfig& cfg);
      ~CMRE();
      void stop();

      bool waitForExecution(void);

      protected:
      void threadBody(void);

      private:
      static TStrategy getStrategy(const std::string& str);
      static std::string getStrategyStr(const TStrategy);
      static TGoals getGoals(const std::string& str);
      static std::string getGoalsStr(const TGoals goals);
      static TCost getCost(const std::string& str);
      static std::string getCostStr(const TCost strategy);

      void individial_robots(void);
      //      void global_control(void);

      //      void updateMap(RobotPlayerPtrVector robots, double r);
      //GridCoordsVector& getGoals(const RobotPlayerPtrVector robots, FrontierVector& frontiers, GridCoordsVector& goalGridPts);
      GridCoordsVector& getGoals(const RobotPlayerPtrVector& robots, CPaths& paths, CMap& map, FrontierVector& frontiers, GridCoordsVector& goalGridPts, GridCoordsVectorVector& robotsGoalGridPts, IntVector& coveringCosts);
      //      void setRobotShape(SRobot& robot, double r) const;

      float addTurnPenalty(CPaths& paths, const GridCoords& gridPose, const SPosition& pose, const GridCoords& pt, float dist);

      void savePic(int iter);

   };
} //end namespace imr

#endif

/* end of mre.h */
