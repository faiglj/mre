/*
 * File name: voronoi_planning.cc
 * Date:      2011/05/28 18:58
 * Author:    Jan Faigl
 */

#include <cmath>
#include <limits>

#include <boost/foreach.hpp>
#include <evg-thin.hh>

#include <logging.h>
#include <imr_assert.h>
#include <dijkstra_lite.h>

#include "voronoi_planning.h"
#include "distance_transform.h"

#define foreach BOOST_FOREACH

using imr::logger;

using namespace mre; 

typedef imr::dijkstra::CDijkstraLite<double> Dijkstra;

/// ----------------------------------------------------------------------------
/// Class CVoronoiPlanning 

/// - static method ------------------------------------------------------------

/// - constructor --------------------------------------------------------------
CVoronoiPlanning::CVoronoiPlanning(const ByteMatrix& grid, const unsigned char freespace) : 
   grid(grid), FREESPACE(freespace), 
   H(grid.NROWS), W(grid.NCOLS),
   sGrid(grid.NROWS, grid.NCOLS)
{
}

/// - destructor ---------------------------------------------------------------
CVoronoiPlanning::~CVoronoiPlanning() {
}

/// - public method ------------------------------------------------------------
void CVoronoiPlanning::compute(bool saveDistances) {
   grid_type evgGrid;
   float distance_min = 0.0;
   float distance_max = std::numeric_limits<float>::max();
   bool pruning = false;
   evgGrid.resize(H);
   for (int y = 0; y < H; y++) {
      evgGrid[y].resize(W);
      for (int x = 0; x < W; x++) {
         evgGrid[y][x] = (grid(y,x) == 255 ? Free : Occupied);
      }
   }
   //evg_thin thin(evgGrid, distance_min, distance_max, false, false, rx, ry);
   evg_thin thin(evgGrid, distance_min, distance_max, false, false, -1, -1);
   skeleton_type skel = thin.generate_skeleton();
   thin.calculate_distances();
   if (saveDistances) { //store distance and closest obstacles
      obstacles.resize(W*H, -1);
      distances.resize(W*H, 0.0);
      for(int y = 0; y < H; ++y) {
         for(int x = 0; x < W; ++x) { 
            if (evgGrid[y][x] == Free) {
               const int pi = y*W + x;
               const int ix = thin.distance_grid[y][x].x;
               const int iy = thin.distance_grid[y][x].y;
               const int idx = iy*W + ix;
 /*              DEBUG("y,x: " << y << "," << x << " obs: " << iy << "," << ix);
               DEBUG("idx: " << idx << " restoration:" << (idx/w) << "," << (idx%w));
               */
               obstacles[pi] = idx;
               distances[pi] = thin.distance_grid[y][x].distance;
            }
         }  //end x
      } //end y
   } //end saveDistances
   thin.initialize();
   thin.thin();
   for (int r = 0; r < H; r++) {
      for (int c = 0; c < W; c++) {
         if (thin.getStep1Grid()[r][c] == evg_thin::processed){
            sGrid(r,c) = 1;
         }
      }
   }
   nodes_.clear();
   //IntVector gridCoordsIdxs(w*h, -1); //idxs is r*columns + c
   gridCoordsIdxs_.resize(W*H, -1);
   for (int y = 0; y < H; y++) {
      for (int x = 0; x < W; x++) {
         if (sGrid(y,x) == 1) { //graph cell
            int idx = y*W + x; 
            gridCoordsIdxs_[idx] = nodes_.size();
            nodes_.push_back(GridCoords(x, y));
         }
      }
   }
}

/// - public method ------------------------------------------------------------
GridCoordsVector& CVoronoiPlanning::getPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path) {
   ASSERT_ARGUMENT(
         start.y >= 0 and start.y < H
         and start.x >= 0 and start.x < W
         and goal.y >= 0 and goal.y < H
         and goal.x >= 0 and goal.x < W, "Start and goal must lies inside the map"
         );
   CDistanceTransform dt(grid);
   GridCoordsVector ls;
   GridCoordsVector lg;
   GridCoordsVector nodes = nodes_;
   IntVector gridCoordsIdxs = gridCoordsIdxs_;
   /*
   DEBUG("CVoronoiPlanning::getPath findPath start");
   DEBUG("nodes: " << nodes.size());
   for(int i = 0; i < nodes.size(); ++i) {
      DEBUG("i: " << nodes[i].x << " " << nodes[i].y);
   }*/
   dt.findPath(nodes, start, ls);
   DEBUG("CVoronoiPlanning::getPath findPath goal");
   dt.findPath(nodes, goal, lg);

   
   foreach(const GridCoords& pt, ls) { 
      const int idx = pt.y * W + pt.x;
      sGrid(pt.y, pt.x) = 1; 
      if (gridCoordsIdxs[idx] == -1) { //node not in the nodes
         gridCoordsIdxs[idx] = nodes.size();
         nodes.push_back(pt);
      }
   }
   foreach(const GridCoords& pt, lg) { 
      const int idx = pt.y * W + pt.x;
      sGrid(pt.y, pt.x) = 1; 
      if (gridCoordsIdxs[idx] == -1) { //node not in the nodes
         gridCoordsIdxs[idx] = nodes.size();
         nodes.push_back(pt);
      }
   }
   ///check 4neigh of each node and add edges to the dijkstra
   Dijkstra dijkstra;
   int nE = 0;
   foreach(const GridCoords& pt, nodes) {
      const int idx = pt.y * W + pt.x;
      for(int r = pt.y - 1; r <= pt.y + 1; r++) {
         for(int c = pt.x - 1; c <= pt.x + 1; c++) {
            if (r >= 0 and r < H and c >= 0 and c < W and (r != pt.y or c != pt.x)) {
               const int i = r * W + c;
               if (gridCoordsIdxs[i] != -1) { //cell is in the voronoi
                  /// add edges
                  const double dr = pt.y - r;
                  const double dc = pt.x - c;
                  dijkstra.addEdge(gridCoordsIdxs[idx], gridCoordsIdxs[i], sqrt(dr*dr + dc*dc));
                  nE++;
               }
            }
         }
      }
   }
   DEBUG("No. of voronoi nodes: " << nodes.size() << " dijkstra edges: " << nE);
   int iStart = start.y * W + start.x;
   int iGoal = goal.y * W + goal.x;
   ASSERT_ARGUMENT(gridCoordsIdxs[iStart] != -1, "Start is not in the list of nodes!");
   ASSERT_ARGUMENT(gridCoordsIdxs[iGoal] != -1, "Goal is not in the list of nodes!");
   IntVector pathIdx;
   double length; 
   DEBUG("iStart nodes idx: " << gridCoordsIdxs[iStart]);
   DEBUG("iGoal nodes idx: " << gridCoordsIdxs[iGoal]);
   dijkstra.solve(gridCoordsIdxs[iStart], gridCoordsIdxs[iGoal], length, pathIdx);
   DEBUG("dijkstra.solve: " << length << " idx: " << pathIdx.size());
   path.clear(); 
   foreach(int i, pathIdx) { path.push_back(nodes[i]); }
   return path;
}


/// - public method ------------------------------------------------------------
void CVoronoiPlanning::getVoronoiEdges(GridCoordsVector& s, GridCoordsVector& e) const {
   s.clear();
   e.clear();
   /*
   ByteMatrix sGrid;
   sGrid.clear();
   sGrid.resize(h, UCharVector(w, 0));

   grid_type evgGrid;
   float distance_min = 0.0;
   float distance_max = std::numeric_limits<float>::max();
   bool pruning = false;
   evgGrid.resize(map.size());
   for (int y = 0; y < map.size(); y++) {
      evgGrid[y].resize(map[y].size());
      for (int x = 0; x < map[y].size(); x++) {
         evgGrid[y][x] = (map[y][x] == 255 ? Free : Occupied);
      }
   }
   evg_thin thin(evgGrid, distance_min, distance_max, false, false, -1, -1);
   skeleton_type skel = thin.generate_skeleton();
   IntVector mark(skel.size(), 0);
   foreach(const node& n, skel) { //Note that grid is transposed (r,c)
      GridCoords pt1(n.x, n.y);
      foreach(int i, n.children) { 
         GridCoords pt2(skel[i].x, skel[i].y);
         s.push_back(pt1);
         e.push_back(pt2);
         break;
      }
   }
   */
   GridCoordsVector nodes = nodes_;
   IntVector gridCoordsIdxs = gridCoordsIdxs_;
   ///check 4neigh of each node and add edges to the dijkstra
   int nE = 0;
   foreach(const GridCoords& pt, nodes) {
      const int idx = pt.y * W + pt.x;
      for(int r = pt.y - 1; r <= pt.y + 1; r++) {
         for(int c = pt.x - 1; c <= pt.x + 1; c++) {
            if (r >= 0 and r < H and c >= 0 and c < W and (r != pt.y or c != pt.x)) {
               const int i = r * W + c;
               if (gridCoordsIdxs[i] != -1) { //cell is in the voronoi
                  /// add edges
                  const double dr = pt.y - r;
                  const double dc = pt.x - c;
                  s.push_back(nodes[gridCoordsIdxs[idx]]);
                  e.push_back(nodes[gridCoordsIdxs[i]]);
               }
            }
         }
      }
   }
}

/// - public method ------------------------------------------------------------
FloatVector& CVoronoiPlanning::getDistances(const GridCoordsVector& path, GridCoordsVector& obstaclesO, FloatVector& distancesO) {
   obstaclesO.clear();
   distancesO.clear();

   if (not distances.empty()) {
      for(int i = 0; i < path.size(); ++i) {
         const GridCoords& pt = path[i];
         const int idx = pt.y * W + pt.x;
         distancesO.push_back(distances[idx]);
         const int r = obstacles[idx]/W;
         const int c = obstacles[idx]%W;
 //        DEBUG("i: " << i << " pt: " << pt.r << "," << pt.c << " obs: " << r << "," << c << " d: " << distances[idx]);
         //obstaclesO.push_back(GridCoords(r, c));
         obstaclesO.push_back(GridCoords(c, r)); //GridCoords(x,y)
      } //end for
   } else {
      DEBUG("Compute voronoi to get distance of the path to obstacles");
      //1. compute voronoi
      grid_type evgGrid;
      float distance_min = 0.0;
      float distance_max = std::numeric_limits<float>::max();
      bool pruning = false;
      evgGrid.resize(H);
      for (int y = 0; y < H; y++) {
         evgGrid[y].resize(W);
         for (int x = 0; x < W; x++) {
            evgGrid[y][x] = (grid(y, x) == 255 ? Free : Occupied);
         }
      }
      evg_thin thin(evgGrid, distance_min, distance_max, false, false, -1, -1);
      skeleton_type skel = thin.generate_skeleton();
      thin.calculate_distances();

      //2. extract distances and obstacles
      for(int i = 0; i < path.size(); ++i) {
         const GridCoords& pt = path[i];
         const int r = thin.distance_grid[pt.y][pt.x].x;
         const int c = thin.distance_grid[pt.y][pt.x].y;
         const float d = thin.distance_grid[pt.y][pt.x].distance;
         //obstaclesO.push_back(GridCoords(r, c));
         obstaclesO.push_back(GridCoords(c, r)); //GridCoords(x,y)
         distancesO.push_back(d);
      }
   }
   return distancesO;
}

/* end of voronoi_planning.cc */
