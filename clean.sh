#!/bin/sh

if [ `uname` = FreeBSD ]
then
   cmd=gmake
else
   cmd=make
fi

for i in **/Makefile
do
   $cmd -C `dirname $i` clean
done

rm -rf include lib
