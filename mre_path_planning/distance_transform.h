/*
 * File name: distance_transform.h
 * Date:      2011/05/21 17:03
 * Author:    Jan Faigl
 */

#ifndef __DISTANCE_TRANSFORM_H__
#define __DISTANCE_TRANSFORM_H__

#include <vector>

#include "mre_types.h"

namespace mre {

   class CDistanceTransform {
      public:
         static const unsigned char FREESPACE;
         static const unsigned char OBSTACLE;

         CDistanceTransform(const ByteMatrix& map);
         ~CDistanceTransform();

         const ByteMatrix& getGrid(void) const { return map; }

         GridCoordsVector& findPath(const GridCoords& start, const GridCoords& goal, GridCoordsVector& path); 
         GridCoordsVector& findPath(const GridCoordsVector& starts, const GridCoords& goal, GridCoordsVector& path); 

	 void setGoal(const GridCoords& goalI);
	 const GridCoords& getGoal(void) const { return goal; }
	 GridCoordsVector& getPath(const GridCoords& start, GridCoordsVector& path) const;
	 double getDistance(const GridCoords& start) const;
	 int getDistanceManhattan(const GridCoords& start) const;

         bool isReachable(const GridCoords& start) const;

	 int getMaxValue(void) const { return 2 * grid.SIZE; }

         const DoubleMatrix& getDistanceGrid(void) const { return grid; }

      private:
         DoubleMatrix& compute(DoubleMatrix& grid) const;

      private:
         const ByteMatrix& map; //source map
         GridCoords goal;
         DoubleMatrix grid;
   };

} //end namespace mre

#endif

/* end of distance_transform.h */
