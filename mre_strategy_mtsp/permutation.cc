/*
 * File name: permutation.cc
 * Date:      2012/02/27 14:42
 * Author:    Jan Faigl
 */

#include <limits>
#include <sstream>

#include <imr_assert.h>
#include <logging.h>

#include "permutation.h"

using imr::logger;

using namespace mre;

/// - constructor --------------------------------------------------------------
CPermutation::CPermutation(const DoubleMatrix& cost) : cost(cost) {
   ASSERT_ARGUMENT(cost.NROWS == cost.NCOLS, "CPermutation cost matrix must be squared");
   best = std::numeric_limits<double>::max();
   const int N = cost.NROWS;
   IntVector result(N);
   for(int i = 0; i < N; ++i) { result[i] = i; }
   count = 0;
   permute(result, N);
}

/// - destructor ---------------------------------------------------------------
CPermutation::~CPermutation() {
}

/// - public method ------------------------------------------------------------

/// - private method -----------------------------------------------------------
void CPermutation::permute(IntVector& v, const int m) {
   for(int i = 0; i < m; ++i) {
      if(m > 1) {
         int s = v[m-1];
         for(int j = m - 1; j > 0; j--) {
            v[j] = v[j-1];
         }
         v[0] = s;
            permute(v, m - 1);
      } else {
         { //eval
            double max = 0;
            for (int j = 0; j < v.size(); j++) {
               if (cost(j, v[j]) > max) { max = cost(j, v[j]); }
            }
            if (best > max) {  best = max;  bestPermutation = v; }
            count++;
         }
         print(count, v);
      }
   }
}

/// - private method -----------------------------------------------------------
void CPermutation::print(int c, const IntVector& v) {
   double max = 0; 
   std::stringstream ss;
   ss << "Permutation[" << c << "]:";
   for(int i = 0; i < v.size(); ++i) {
      ss << " " << v[i];
      if (cost(i, v[i]) > max) { max = cost(i, v[i]); }
   }
   ss << " max: " << max;
   DEBUG(ss.str());
}

/* end of permutation.cc */
