/*
 * File name: mre_strategy_pairs.h
 * Date:      2012/02/08 15:44
 * Author:    Jan Faigl
 */

#ifndef __MRE_STRATEGY_PAIRS_H__
#define __MRE_STRATEGY_PAIRS_H__

#include "mre_types.h"

namespace mre {
   class CPairs {
      public:
      CPairs();
      ~CPairs();

      IntVector& assign(const DoubleMatrix& costMatrix, IntVector& goalsIDX);

   };

} //end namespace mre

#endif

/* end of mre_strategy_pairs.h */
