/*
 * File name: tmre.cc
 * Date:      2012/01/23 21:44
 * Author:    Jan Faigl
 */

#include <fstream>
#include <iostream>

#include <boost/program_options.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <logging.h>
#include <imr_config.h>
#include <boost_args_config.h>
#include <imr_exceptions.h>

#include "mre_sim.h"

using imr::logger;

namespace po = boost::program_options;
namespace fs = boost::filesystem;

const std::string MULTI_ROBOT_EXPLORATION_VERSION = "0.9";

/// ----------------------------------------------------------------------------
/// Program options variables
/// ----------------------------------------------------------------------------

/// ----------------------------------------------------------------------------
bool parseArgs(int argc, char * argv[], imr::CConfig& cfg) {
   bool ret = true;
   std::string configFile;
   std::string loggerCfg = ""; 

   po::options_description desc("General options"); 
   desc.add_options()
      ("help,h", "produce help message")
      ("logger-config,l", po::value<std::string>(&loggerCfg)->default_value(loggerCfg), "logger configuration file")
      ("config,c", po::value<std::string>(&configFile)->default_value(std::string(argv[0]) + ".cfg"), "configuration file")
      ;

   po::options_description mreOpt("Multi-Robot Exploration Options");
   boost_args_add_options(mre::CMRESim::getConfig(cfg), "", mreOpt);

   try {
      po::options_description cmdline_options; 
      cmdline_options.add(desc).add(mreOpt);

      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, cmdline_options), vm);
      po::notify(vm);
      std::ifstream ifs(configFile.c_str());
      store(parse_config_file(ifs, cmdline_options), vm);
      po::notify(vm);
      ifs.close();
      if (vm.count("help")) {
	 std::cerr << std::endl;
	 std::cerr << "Multi-Robot Exploration program ver. " << MULTI_ROBOT_EXPLORATION_VERSION << std::endl;
	 std::cerr << cmdline_options << std::endl;
	 ret = false;
      } 
      if (
	    ret && 
	    loggerCfg != "" && 
	    fs::exists(fs::path(loggerCfg))
	 )  {
	 imr::initLogger("tmre", loggerCfg.c_str());
      } else {
	 imr::initLogger("tmre");
      }
   } catch (std::exception & e) {
      std::cerr << std::endl;
      std::cerr << "Error in parsing arguments: " << e.what() << std::endl;
      ret = false;
   }
   return ret;
}

/// ----------------------------------------------------------------------------
/// Main PROGRAM
/// ----------------------------------------------------------------------------
int main(int argc, char **argv) {
   int ret = -1;
   imr::CConfig cfg;
   if (parseArgs(argc, argv, cfg)) {
      INFO("Start Logging");
      try {
	 mre::CMRESim* mre = new mre::CMRESim(cfg);
	 mre->execute();
	 delete mre;
	 //        INFO("Press ENTER to finish");
	 //        getchar();
	 ret = EXIT_SUCCESS;
      }  catch (imr::exception& e) {
	 ERROR("Error: " << e.what());
      }
      INFO("End Logging");
   }
   imr::shutdownLogger();
   return ret;
}

/* end of tmre.cc */
