/*
 * File name: permutation.h
 * Date:      2012/02/27 14:18
 * Author:    Jan Faigl
 */

#ifndef __PERMUTATION_H__
#define __PERMUTATION_H__

#include "mre_types.h"

namespace mre {
   
   class CPermutation {
      const DoubleMatrix& cost;
      double best;
      IntVector bestPermutation;
      int count;

      public:
      CPermutation(const DoubleMatrix& cost);
      ~CPermutation();

      const IntVector& getAssignment(void) const { return bestPermutation; }

      private:
      void permute(IntVector& v, const int m);
      void print(int c, const IntVector& v);
   };

} //end namespace mre

#endif

/* end of permutation.h */
