
define hokuyolaser laser
(
  # laser-specific properties
  # factory settings for LMS200	
  range_min 0.10
  range_max 3.5
  fov 360# 270.0
  samples 910 #682

  # generic model properties
  color "blue"
  size [ 0.05 0.05 0.07 ]
  alwayson 1
)



