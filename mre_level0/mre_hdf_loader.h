/*
 * File name: mre_hdf_loader.h
 * Date:      2011/08/11 11:32
 * Author:    Jan Faigl
 */

#ifndef __MRE_HDF_LOADER_H__
#define __MRE_HDF_LOADER_H__

#include <string>

#include <hdf5.h>

#include "mre_types.h"

namespace mre {

   namespace loader {
      template<class T, int t> 
         class CWrapper {
            enum { value = t };
            public:
            CWrapper() : v(0) {}
            CWrapper(T v) : v(v) {}
            const T& operator()(void) const { return v;}
            T v;
         };

      typedef enum {
         FILENAME,
         ATTRIBUTE,
         ATTRIBUTE_VALUE,
         GROUP,
         CLOSE_GROUP,
         DATASET,
         OPENREADONLY,
         OPENWRITETRUNC,
         CLOSE,
         FLOAT_MATRIX,
         INT_MATRIX
      } TParam;

      typedef CWrapper<const std::string, OPENREADONLY> OpenReadonly;
      typedef CWrapper<FloatMatrix*, FLOAT_MATRIX> FMatrixDataset;
      typedef CWrapper<IntMatrix*, INT_MATRIX> IMatrixDataset;

   } //end namespace loader

   /// ----------------------------------------------------------------------------
   /// @brief 
   /// ----------------------------------------------------------------------------
   class CHDFLoader {
      hid_t file_id;
      hid_t group_id;
      std::string filename;
      std::string datasetName;
      std::string attrName;
      loader::TParam strType;
      public:
      CHDFLoader();
      ~CHDFLoader();

      CHDFLoader& operator<<(const loader::TParam& p);
      CHDFLoader& operator<<(const std::string& str);
      CHDFLoader& operator<<(const loader::OpenReadonly& o);
      CHDFLoader& operator<<(const FloatMatrix& matrix);
      CHDFLoader& operator<<(const IntMatrix& matrix);
      CHDFLoader& operator<<(int v);

      CHDFLoader& operator>>(std::string& str);
      CHDFLoader& operator>>(loader::FMatrixDataset& c);
      CHDFLoader& operator>>(loader::IMatrixDataset& c);
      CHDFLoader& operator>>(int& v);


      /// ----------------------------------------------------------------------------
      /// A more conventional methods to deal with hdf
      void open(const std::string& filename, int flags);
      std::string& getRootAttribute(const std::string attrName, std::string& value);
      FloatMatrix* getFloatMatrixDataset(std::string& datasetName, FloatMatrix* m = 0);
      IntMatrix* getIntMatrixDataset(std::string& datasetName, IntMatrix* m = 0);

      void setRootAttribute(const std::string attrName, const std::string& value);
      void setFloatMatrixDataset(std::string& datasetName, const FloatMatrix& matrix);
      void setIntMatrixDataset(std::string& datasetName, const IntMatrix& matrix);
   };

} //end namespace mre

#endif

/* end of mre_hdf_loader.h */
