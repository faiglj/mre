/*
 * File name: paths.h
 * Date:      2013/04/02 07:41
 * Author:    Jan Faigl
 */

#ifndef __PATHS_H__
#define __PATHS_H__

#include <set>

#include <imr_config.h>
#include <matrix_utils.h>
#include <dijkstra_lite.h>

#include "basic_theta_star.h"
#include "distance_transform.h"

#include "map.h"
#include "mre_types.h"

namespace mre {

   class CPaths {
      typedef imr::dijkstra::CDijkstraLite<float> Dijkstra;
      typedef std::vector<int> IntVector;
      typedef std::vector<float> FloatVector;
      public:

     // typedef enum { FREESPACE_CELL = 0, OCCUPIED_CELL = 1 } TCellType;
      typedef enum { FREESPACE_CELL = 255, OCCUPIED_CELL = 0 } TCellType;
      private:

      struct SPath {
         int from;
         int to;
         GridCoordsVector* path;
         SPath(const SPath& p) : from(p.from), to(p.to) {
            if (p.path) {
               path = new GridCoordsVector;
               *path = *(p.path);
            }
         }
         SPath(int from, int to, GridCoordsVector* path = 0) : from(from), to(to), path(path) {
         } 
         ~SPath() {
            if (path) { delete path; }
         }
      };

      class CPathLess {
         public:
            bool operator()(const SPath& p1, const SPath& p2) const {
               if (p1.from < p2.from) {
                  return true;
               } else if (p1.from == p2.from and p1.to < p2.to) {
                  return true;
               } else {
                  return false;
               }
            }
      };

      typedef std::set<SPath, CPathLess> PathCache;

      public:
      static imr::CConfig& getConfig(imr::CConfig& config);

      CPaths(imr::CConfig& cfg, const ByteMatrix& iGrid, const unsigned char FREESPACE);
      ~CPaths();

      const ByteMatrix& getGrid(void) const { return occ; }
      float getDistance(const GridCoords& pt1, const GridCoords& pt2);
      bool getDistance(const GridCoords& pt1, const GridCoords& pt2, float &dist);
      GridCoordsVector& getPath(const GridCoords& pt1, const GridCoords& pt2, GridCoordsVector& path);
      GridCoordsVector& getPath2WaySimpl(const GridCoords& pt1, const GridCoords& pt2, GridCoordsVector& path);
      void updateMap(const ByteMatrix& iGrid);


      PathNodeVector& getPathPlan(const CMap& map, const SPosition& pose, const GridCoordsVector& path, PathNodeVector& plan);

      GridCoordsVector& fillPath(const GridCoordsVector& pts, GridCoordsVector& path);
      GridCoordsVector& simplify(const GridCoordsVector& pts, GridCoordsVector& path);

      CDistanceTransform* getDT(void) { return dt; }

      private:

      void createDijkstra(void);
      void release(void);
      bool isFree(const GridCoordsVector& pts);

      private:
      inline void updateDistance(int from, int to, float dist) {
         if (distances[from] == 0) {
            const int N = graphNodes;
            distances[from] = new float[N];
            for (int i = 0; i < N; ++i) { distances[from][i] = -1.0; }
         }
         distances[from][to] = dist;
      }

      inline void updateDistances(int from, const FloatVector& dists) {
         const int N = graphNodes;
         if (distances[from] == 0) { distances[from] = new float[N]; }
         for(int i = 0; i < N; ++i) {
            distances[from][i] = dists[i];
         }
      }


      private:
      const bool ENABLE_DISTANCE_CACHING;
      const bool ENABLE_PATHS_CACHING;
      const bool DISTANCE_USE_DIJKSTRA;
      const bool PATHS_USE_DIJKSTRA;
      const unsigned char FREESPACE;
      ByteMatrix occ;
      const int SIZE;

      IntVector tr; //translation of the grid idx to the graph idx
      IntVector gridIndexes; //translation of the graph idx to grid idx
      int graphNodes;
      Dijkstra dijkstra;
      CDistanceTransform* dt;
      CBasicThetaStar* bts;
      float** distances;
      PathCache pathCache;
      //     int** preds;
   };

} //end namespace mre


#endif

/* end of paths.h */
