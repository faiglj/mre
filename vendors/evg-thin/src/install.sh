#!/bin/sh

if [ `uname` = FreeBSD ]
then
   mcmd=gmake
else
   mcmd=make
fi

$mcmd clean
$mcmd 
rm -rf test.o libevg-thin.a ; ar rcs libevg-thin.a *.o 
$mcmd clean

LIBRARY_PREFIX=../../../lib
INCLUDE_PREFIX=../../../include

mkdir -p $LIBRARY_PREFIX $INCLUDE_PREFIX
cp libevg-thin.a $LIBRARY_PREFIX/
cp *.hh $INCLUDE_PREFIX/
