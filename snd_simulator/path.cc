/*
 * File name: path.cc
 * Date:      Tue Oct 23 15:02:50 +0200 2012
 * Author:    Miroslav Kulich
 */

#include "path.h"

using namespace mre;

/// - constructor --------------------------------------------------------------
CPath::CPath() {
}

/// - destructor --------------------------------------------------------------
CPath::~CPath() {
}

/// - public ---------------------------------------------------------------------------
void CPath::reset(void) {
   while(!path.empty()) {
      path.pop();
   }
}

/// - public ---------------------------------------------------------------------------
void CPath::add(const SPosition& pos) {
   path.push(pos);
}

/// - public method ---------------------------------------------------------------------------
bool CPath::hasNextGoal() {
   return  !path.empty();
}

/// - public method ---------------------------------------------------------------------------
bool CPath::shouldSlowDown() {
   return path.size() == 1;
}

/// - public method ---------------------------------------------------------------------------
SPosition CPath::getGoal() {
   return path.front();
}

/// - public method ---------------------------------------------------------------------------
void CPath::nextGoal() {
   path.pop();
}

/* end of path.cc */
