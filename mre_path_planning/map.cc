/*
 * File name: map.cc
 * Date:      2012/01/24 18:31
 * Author:    Jan Faigl
 */

#include <cmath>
#include <algorithm>
#include <limits>

#include <boost/foreach.hpp>

#include <logging.h>

#include "map.h"
#include "simple_intersection.h"

#define foreach BOOST_FOREACH

using imr::logger;

using namespace mre;

typedef mre::geom::CIntersection<SPosition, true> GeomImproper;

/// ----------------------------------------------------------------------------
class CompareInt {
   public:
   bool operator()(const int& a, const int& b) const {
      return a < b;
   }
};

/// ----------------------------------------------------------------------------
class CObjectLess {
   public:
      CObjectLess(const FrontierVectorVector& objects) : objects(objects) {}
      bool operator()(const int a, const int b) const {
	 return objects[a].size() < objects[b].size();
      }
      const FrontierVectorVector& objects;
};

/// - static -------------------------------------------------------------------
imr::CConfig& CMap::getConfig(imr::CConfig& config) {
   config.add<double>("map-width", "map width in meters", -1);
   config.add<double>("map-height", "map height in meters", -1);
   config.add<double>("grid-cell-size", "Size of the squared cell in meters", 0.05);
   config.add<float>("grid-init-probability", "Init probability", 0.5);
   config.add<float>("grid-obstacle-probability", "model of sensor", 0.9);
   config.add<float>("grid-freespace-probability", "model of sensor", 0.4);
   config.add<double>("laser-max-range", "Restrict laser range (in meters)", 0.5);
   config.add<double>("goals-representative-distance-factor", "Distance factor for determining the number of frontier's representatives", 1.8);
   return config;
}

/// - constructor --------------------------------------------------------------
CMap::CMap(imr::CConfig& cfg) : 
   cfg(cfg),
   MAP_W(cfg.get<double>("map-width")),
   MAP_H(cfg.get<double>("map-height")),
   CELL_SIZE(cfg.get<double>("grid-cell-size")),
   W(round(MAP_W / CELL_SIZE)),
   H(round(MAP_H / CELL_SIZE)),
   MIN_X(-MAP_W / 2), MAX_X(MAP_W / 2),
   MIN_Y(-MAP_H / 2), MAX_Y(MAP_H / 2),
   KX(W / MAP_W), KY(H / MAP_H),
   MAX_LASER_RANGE(cfg.get<double>("laser-max-range")),
   GOALS_REPRESENTATIVE_DISTANCE_FACTOR(cfg.get<double>("goals-representative-distance-factor")),
   INIT_PROB(cfg.get<float>("grid-init-probability")),
   OBSTACLE_PROB(cfg.get<float>("grid-obstacle-probability")),
   FREESPACE_PROB(cfg.get<float>("grid-freespace-probability")),
   grid(H, W),
   navGrid(H, W)
{
   ASSERT_ARGUMENT(MAP_W > 0 and MAP_H > 0, "CMap -- Map dimensions must be given");
   DEBUG("CMap grid size " << W << "x" << H);
   const float p = cfg.get<float>("grid-init-probability");
   for(int i = 0; i < grid.SIZE; ++i) {
      grid(i) = p;
   }
   gui = 0;
   border.push_back(SPosition(MIN_X, MAX_Y));
   border.push_back(SPosition(MIN_X, MIN_Y));
   border.push_back(SPosition(MAX_X, MIN_Y));
   border.push_back(SPosition(MAX_X, MAX_Y));
}

/// - destructor ---------------------------------------------------------------
CMap::~CMap() {
}

/// - public method ------------------------------------------------------------
void CMap::getCellsValues(const IntVector& cells, FloatVector& values) {
   values.resize(cells.size());
   for(int i = 0; i < cells.size(); ++i) {
      values[i] = grid(cells[i]);
   }
}

/// - public method ------------------------------------------------------------
void CMap::setCellsValues(const IntVector& cells, const FloatVector& values) {
   for(int i = 0; i < cells.size(); ++i) {
      grid(cells[i]) = values[i];
   }
}

/// - public method ------------------------------------------------------------
void CMap::setCellsValues(const IntVector& cells, float v) {
   for(int i = 0; i < cells.size(); ++i) {
      grid(cells[i]) = v;
   }
}

/// - public method ------------------------------------------------------------
void CMap::updateFreespace(const IntVector& cells) {
   foreach(int i, cells) { //update freespace
      const float dP = FREESPACE_PROB * grid(i);
      grid(i) = dP / (2 * dP - FREESPACE_PROB - grid(i) + 1);
   }
}

/// - public method ------------------------------------------------------------
void CMap::updateFreespaceFilter(const IntVector& cells, float threshold) {
   foreach(int i, cells) { //update freespace
      if (grid(i) < threshold) {
         const float dP = FREESPACE_PROB * grid(i);
         grid(i) = dP / (2 * dP - FREESPACE_PROB - grid(i) + 1);
      }
   }
}

/// - public method ------------------------------------------------------------
void CMap::updateObstacles(const IntVector& cells) {
   foreach(int i, cells) { //update obstacles
      const float dP = OBSTACLE_PROB * grid[i];
      grid(i) = dP / (2 * dP - OBSTACLE_PROB - grid[i] + 1);
   }
}

/// - public method ------------------------------------------------------------
void CMap::updateObstacles(const IntVector& cells, const FloatVector& obstSave) {
   ASSERT_ARGUMENT(cells.size() == obstSave.size(), "CMap::updateObstacles no. of items must be same");
   for(int i = 0; i < cells.size(); ++i) { //update obstacles
      const float dP = OBSTACLE_PROB * obstSave[i];
      grid(cells[i]) = dP / (2 * dP - OBSTACLE_PROB - obstSave[i] + 1);
   }
}
/// - public method ------------------------------------------------------------
void CMap::fillScan(SLaserData& scan) {
   //convert scan to grid idx
   IntVector freespace;
   IntVector obstacles;
   fillScan(scan, freespace, obstacles);
   FloatVector obstSave;


   //save obstacle values
//   foreach(int i, obstacles) { obstSave.push_back(grid[i]); }

   foreach(int i, freespace) { //update freespace
     if (grid[i] != OCCUPIED_CELL) { 
	grid[i] = FREESPACE_CELL;
	
     }
   }

   foreach(int i, obstacles) { //update freespace
     grid[i] = OCCUPIED_CELL;
   }
}



/// - public method ------------------------------------------------------------
void CMap::fillScan(const SLaserData& scan, IntVector& freespace, IntVector& obstacles) {
   freespace.clear();
   obstacles.clear();
   //laser origin in absolute values
   double lx = scan.robotPose.x + scan.laserPose.x * cos(scan.robotPose.yaw);
   double ly = scan.robotPose.y + scan.laserPose.y * sin(scan.robotPose.yaw);


   // derived from the sdl_gfx function filledPolygonColorMT
   typedef std::vector<int> IntVector;

   IntVector vx;
   IntVector vy;
   IntVector disInt;
   //std::vector<double> saveProb;

   vx.push_back(real2gridX(lx));
   vy.push_back(real2gridY(ly));
				 //DEBUG("border X: " << MIN_X << " " << MAX_X << " Y:  " << MIN_Y << " " << MAX_Y);
   for (int i = 0; i < scan.size; ++i) {
      const double angle = scan.robotPose.yaw + i * scan.resAngle + scan.minAngle;
      const bool res = scan.samples[i] > MAX_LASER_RANGE;
      if (res) { scan.samples[i] = MAX_LASER_RANGE; }
      const double x = scan.samples[i] * cos(angle) + lx;
      const double y = scan.samples[i] * sin(angle) + ly;
      int c, r;
      if (x >= MIN_X and x <= MAX_X and y >= MIN_Y and y <= MAX_Y) {
         c = real2gridX(x);
         r = real2gridY(y);
         if (!res) { obstacles.push_back(c + r * W); } //save obstacles
      } else {
         SPosition p;
//				 DEBUG("int: " << i << ": " << lx << " " << ly << "  " << x << " " << y);
         getBorderIntersection(SPosition(lx, ly), SPosition(x, y), p);
         c = real2gridX(p.x);
         r = real2gridY(p.y);
      }
      if (vx.back() != c || vy.back() != r) {
         vx.push_back(c);
         vy.push_back(r);
      }
   }

   int minx = vx.front();
   int maxx = vx.front();
   int miny = vy.front();
   int maxy = vy.front();
   const int n = vx.size();
   for (int i = 1; i < n; i++) {
      if (vy[i] < miny) {
         miny = vy[i];
      } else if (vy[i] > maxy) {
         maxy = vy[i];
      }
      if (vx[i] < minx) {
         minx = vx[i];
      } else if (vx[i] > maxx) {
         maxx = vx[i];
      }
   }

   int x1, y1;
   int x2, y2;
   int ind1, ind2;
  // int cache[maxx - minx + 2];
   int cache[n];
   int ints;
   for (int y = miny; y <= maxy; y++) {
      ints = 0;
      for (int i = 0; i < n; i++) {
         if (!i) {
            ind1 = n - 1;
            ind2 = 0;
         } else {
            ind1 = i - 1;
            ind2 = i;
         }
         y1 = vy[ind1];
         y2 = vy[ind2];
         if (y1 < y2) {
            x1 = vx[ind1];
            x2 = vx[ind2];
         } else if (y1 > y2) {
            y2 = vy[ind1];
            y1 = vy[ind2];
            x2 = vx[ind1];
            x1 = vx[ind2];
         } else {
            continue;
         }
         if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
            cache[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
         }
      }
      std::sort(cache, cache + ints, CompareInt());
      for (int i = 0; i < ints; i += 2) {
         int xa = cache[i] + 1;
         xa = (xa >> 16) + ((xa & 32768) >> 15);
         int xb = cache[i+1] - 1;
         xb = (xb >> 16) + ((xb & 32768) >> 15);

         int f, t;
         if (xa < xb) {
            f = xa;
            t = xb;
         } else {
            f = xb;
            t = xa;
         }
         const int dy = y*W;
         for (int i = f; i < t; i++) {
            freespace.push_back(dy + i);
         }
      }
   }
}

/// - public method ------------------------------------------------------------
void CMap::updateNavGrid(const int ROBOT_CELLS_RADIUS, const RobotPtrVector& robots) {
   for(int i = 0; i < navGrid.SIZE; ++i) { 
      if (grid(i) > 0.7) {
         navGrid(i) = OCCUPIED_CELL;
      } else if (grid(i) < 0.2) {
         navGrid(i) = FREESPACE_CELL;
      } else {
         navGrid(i) = UNKNOWN_CELL;
      }
   }

   //grow obstacles
   //const int neigh = (ceil(ROBOT_RADIUS / CELL_SIZE)) + 1; //2
   const int neigh = ROBOT_CELLS_RADIUS;
   const int neigh2 = neigh*neigh;
   for (int r = 0; r < H; r++) {
      for (int c = 0; c < W; c++) {
         if (navGrid(r, c) == OCCUPIED_CELL) {
            for (int i = -neigh; i < neigh + 1; i++) { //enlarge grid
               for (int j = -neigh; j < neigh + 1; j++) {
                  const int x = c + j;
                  const int y = r + i;
                  if (
                        (navGrid(y, x) == FREESPACE_CELL or navGrid(y, x) == UNKNOWN_CELL)
			and (i*i + j*j <= neigh2)
			and (x >= 0 and y >= 0 and x < W and y < H)
		     ) {
		     navGrid(y, x) = OCCUPIED_GROWN_CELL;
		  }
	       }
	    }
	 } // end occupied cell
      }
   }
   //robots must be in freespace
   foreach(const SRobot* robot, robots) {
      navGrid(robot->gridPose.y, robot->gridPose.x) = FREESPACE_CELL;
      foreach(int i, robot->shape) {  navGrid(i) = FREESPACE_CELL; }
   }
}

/// - public method ------------------------------------------------------------
void CMap::updateNavGrid(const double ROBOT_RADIUS, const RobotPtrVector& robots) {
   const int ROBOT_CELLS_RADIUS = (ceil(ROBOT_RADIUS / CELL_SIZE)) + 1; //2
   updateNavGrid(ROBOT_CELLS_RADIUS, robots); 
}

/// - public method ------------------------------------------------------------
int CMap::getNoOfFreespaceCells(void) const {
   int ret = 0;
   for (int i = 0; i < navGrid.SIZE; ++i) {
      if (grid(i) < 0.2) {
	 ret++;
      }
   }
   return ret;
}

static const int dr[] = { -1, -1, -1,  0,  0,  1,  1,  1};
static const int dc[] = { -1,  0,  1, -1,  1, -1,  0,  1};

/// - public method ------------------------------------------------------------
bool CMap::isFrontier(const GridCoords& pt) const {
   bool ret = false;
   const int r = pt.y;
   const int c = pt.x;

   if ((navGrid(r,c) == FREESPACE_CELL)) {
      for (int i = 0; i < 8; i++) { // check neighborhood cells
	// DEBUG("isFrontier neighborhood i: " << i << " state: " << grid( r + dr[i], c + dc[i]));
	 const float v =  grid( r + dr[i], c + dc[i]);
	 if (v >= 0.2 and v <= 0.7) {
	    ret = true;
	    break;
	 }
      } //end 8-neighborhood
   }
   return ret;
}

/// - public method ------------------------------------------------------------
int CMap::findFrontiers(const GridCoordsVector& robotPoses, FrontierVector& frontiers) const {
   const float threshold = 0.7;
   int buffer[W];
   unsigned int prev;
   const int EMPTY = -1;
   int color = -1;
   int act;
   bool found;
   std::map<int,int> mapping;
   for(unsigned int i = 0; i < W; ++i) {
      buffer[i] = EMPTY;
   }
   prev = EMPTY;

   frontiers.clear();
   for (int r = 1; r < H - 1; ++r) {
      for (int c = 1; c < W - 1; ++c) {
         found = false;
         if ((navGrid(r,c) == FREESPACE_CELL) && (!isRobotNearBy(robotPoses, r, c, 4.0)) ) {
            for (int i = 0; i < 8; i++) { // check neighborhood cells
               if (navGrid( r + dr[i], c + dc[i]) == UNKNOWN_CELL) {
                  found = true;
                  break;
               }
            }
         }
         if ( found ) {
            act = prev;
            for(int k = c - 1; k <= c + 1; ++k) {
               if ( buffer[k] != EMPTY ) {
                  if ( act == EMPTY ) {
                     act = buffer[k];
                  } else {
                     if ( act < buffer[k] ) {
                        mapping[buffer[k]] = act;
                     } else if ( act > buffer[k] ) {
                        mapping[act] = buffer[k];
                        act = buffer[k];
                     }
                  }
               }
            }

            if ( act == EMPTY ) {
               act = ++color;
            }
            frontiers.push_back(SFrontier(c, r, act));
            prev = act;
            buffer[c] = act;
         } else {
            prev = EMPTY;
            buffer[c] = EMPTY;
         }
      }
   }

   int num = 0;
   foreach(SFrontier& f, frontiers) {
      while ( mapping.find(f.object) != mapping.end() ) {
         f.object = mapping[f.object];
      }
      if (f.object > num) {
         num = f.object;
      }
   }
   return num + 1; //objects are indexed from 0
}

/// - public method ------------------------------------------------------------
FrontierVector& CMap::findAllGoals(const FrontierVector &frontiers, int max, int minNumOfFrontierCells, FrontierVector& result) const {
   result.clear();
   FrontierVector tmp;
   for(int i = 0; i <= max; i++) {
      findGoals(frontiers, i, minNumOfFrontierCells, tmp);
      result.insert(result.end(), tmp.begin(), tmp.end());
   }
   return result;
}

/// - public method ------------------------------------------------------------
FrontierVector& CMap::findAllGoalsARN(const FrontierVector &frontiers, int max, int minNumOfFrontierCells, int nRobots, FrontierVector& result) const {
   result.clear();
   FrontierVector tmp;
   FrontierVectorVector objects;
   for(int i = 0; i <= max; i++) {
      FrontierVector object;
      foreach(const SFrontier& f, frontiers) {
	 if (f.object == i) {
	    object.push_back(f);
	 }
      } //end all frontiers
      if (object.size() >= minNumOfFrontierCells) {
	 objects.push_back(object);
      }
   } //end filtration of all objects
   if (not objects.empty()) { 
      DEBUG("Num frontiers cells: " << frontiers.size() << " num objects: " << objects.size());
      IntVector num_repre; 
      IntVector objectsIDX;
      int num_candidates = 0;
      for(int i = 0; i < objects.size(); ++i) {
	 num_repre.push_back(round(1 + objects[i].size() / (GOALS_REPRESENTATIVE_DISTANCE_FACTOR * MAX_LASER_RANGE / CELL_SIZE)));
	 num_candidates += num_repre.back();
	 objectsIDX.push_back(i);
      }
      std::sort(objectsIDX.begin(), objectsIDX.end(), CObjectLess(objects));
      DEBUG("num_candidates: " << num_candidates << " nrobots: " << nRobots);
      for(int i = 0; i < objects.size(); ++i) {
	 DEBUG("Object[" << i << "]: " << objects[objectsIDX[i]].size());
      }
      int idx = 0;
      while(num_candidates < nRobots) {
	 DEBUG("num_candidates: " << num_candidates);
	 num_repre[objectsIDX[idx]]++;
	 num_candidates++;
	 idx = (idx + 1)%objectsIDX.size();
      }
      DEBUG("num_candidates: " << num_candidates << " nrobots: " << nRobots);
      for(int i = 0; i < objectsIDX.size(); ++i) {
	 rfe(objects[objectsIDX[i]], num_repre[objectsIDX[i]], result);
      }
   }
   return result;
}

/// - public method ------------------------------------------------------------
bool CMap::isCollisionFree(const GridCoordsVector& pts, GridCoords& collisionPoint) const {
   bool ret = true;
   collisionPoint = pts.back();
   foreach(const GridCoords& pt, pts) {
      if (pt.x >= 0 and pt.x < W and pt.y >= 0 and pt.y < H) {
         if (
               navGrid(pt.y, pt.x) != FREESPACE_CELL 
               or (pt.x > 1 and navGrid(pt.y, pt.x - 1) != FREESPACE_CELL) or (pt.x < (W-1) and navGrid(pt.y, pt.x + 1) != FREESPACE_CELL) 
               or (pt.y > 1 and navGrid(pt.y - 1, pt.x) != FREESPACE_CELL) or (pt.y < (H-1) and navGrid(pt.y+1, pt.x) != FREESPACE_CELL) 
            )
         {
            ret = false;
            collisionPoint = pt;
            break;
         }
      } else {
         break; //ray goes out of the map
      }
   }
   return ret;
}

/// - private method -----------------------------------------------------------
bool CMap::isRobotNearBy(const GridCoordsVector& robotPoses, int r, int c, double radius) const {
   foreach(const SGridCoords& pose, robotPoses) {
      const int rr = pose.y;
      const int cc = pose.x;
      if ( (r-rr)*(r-rr)  + (c-cc)*(c-cc) < radius ) {
	 return true;
      }
   }
   return false;
}

/// - private method -----------------------------------------------------------
FrontierVector& CMap::findGoals(const FrontierVector &frontiers, int number, int minNumOfFrontierCells, FrontierVector& result) const {
   FrontierVector object;
   result.clear();
   foreach(const SFrontier& f, frontiers) {
      if (f.object == number) {
	 object.push_back(f); //TODO push reference or points
      }
   }
   if ( !object.empty() and object.size() >= minNumOfFrontierCells) {
      const int num_repre = round(1 + object.size() / (GOALS_REPRESENTATIVE_DISTANCE_FACTOR * MAX_LASER_RANGE/CELL_SIZE)); //TODO: magic constant -- little bit smaller than 2*radius (in cells)
      rfe(object, num_repre, result);
   }
   return result;
}

/// - private method -----------------------------------------------------------
FrontierVector& CMap::rfe(const FrontierVector& object, const int num_repre, FrontierVector& result) const {
   if (num_repre > 0 && object.size() > 0) {
      FrontierVector mean;
      //   const int num_repre = round(1 + object.size() / (GOALS_REPRESENTATIVE_DISTANCE_FACTOR * MAX_LASER_RANGE/CELL_SIZE)); //TODO: magic constant -- little bit smaller than 2*radius (in cells)
      const int radius = object.size() / num_repre;
      for(int i = 0; i < num_repre; ++i) {
	 mean.push_back(object[(int)((i + 0.5) * radius)]);
      }
      IntVector group(object.size(), -1);
      GridCoordsVector center(num_repre);
      int imin;
      double min;
      int i;
      int step = 0;
      bool changed = true;
      while (changed) {
	 IntVector count(num_repre, 0);
	 step++;
	 changed = false;
	 int k = 0;
	 foreach(SGridCoords& c, center) {
	    c.x = 0; c.y = 0;
	 }
	 foreach(const SFrontier& f, object) {
	    min = std::numeric_limits<int>::max();
	    i = 0;
	    foreach(const SFrontier& m, mean) {
	       int xx = f.coords.x - m.coords.x;
	       int yy = f.coords.y - m.coords.y;
	       double dd = xx*xx + yy*yy;
	       if ( dd<min ) {
		  min = dd;
		  imin = i;
	       }
	       i++;
	    }
	    center[imin].y += f.coords.y;
	    center[imin].x += f.coords.x;
	    count[imin]++;
	    if (group[k] != imin) {
	       group[k] = imin;
	       changed = true;
	    }
	    k++;
	 }
	 for(int j = 0; j < center.size(); ++j) {
	    if (count[j] > 0) {
	       mean[j].coords.x = center[j].x / count[j];
	       mean[j].coords.y = center[j].y / count[j];
	    } else { 
	       mean[j].coords.x = mean[j].coords.y = -1;
	    }
	 }
	 if (step == 2) { changed = false; }
      }

      SFrontier repre;
      foreach(const SFrontier& m, mean) {
	 if (m.coords.x > -1 and m.coords.y > -1) {
	    min = std::numeric_limits<int>::max();
	    foreach(const SFrontier& f, object) {
	       int xx = f.coords.x - m.coords.x;
	       int yy = f.coords.y - m.coords.y;
	       double dd = xx*xx + yy*yy;
	       if ( dd < min ) {
		  min = dd;
		  repre = f;
	       }        
	    }
	    result.push_back(repre);
	 }
      }
   }
   return result;
}

/// - private method -----------------------------------------------------------
SPosition& CMap::getBorderIntersection(const SPosition& p1, const SPosition& p2, SPosition& p) const {
   bool found = false;
   for(int i = 0; i < border.size(); ++i) {
      const SPosition& a = border[i];
      const SPosition& b = border[(i+1)%border.size()];
      const char c = GeomImproper::segmentIntersection(a, b, p1, p2, p);
      if (c != '0') {
	 found = true;
	 break;
      }
   }
   ASSERT_ARGUMENT(found, "border intersection point not found");
   return p;
}

/* end of map.cc */
