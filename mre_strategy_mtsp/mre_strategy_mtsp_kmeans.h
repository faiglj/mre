/*
 * File name: mre_strategy_mtsp_kmeans.h
 * Date:      2012/02/21 14:11
 * Author:    Jan Faigl
 */

#ifndef __MRE_STRATEGY_MTSP_KMEANS_H__
#define __MRE_STRATEGY_MTSP_KMEANS_H__

#include <vector>

#include <imr_config.h>

#include "mre_sdl_gui.h"
#include "mre_types.h"
#include "path_planning.h"

#include "distance_transform.h"


namespace mre {

   class CMTSPKMeans {
      imr::CConfig& cfg;
      gui::CGui* gui;
      CPathPlanning& paths;
      ByteMatrix* navGrid;
      DoubleMatrix* distances;
      const int MAX_ITER;
      const bool EVAL_ALL_CLUSTERS_COST;
      const bool MINMAX_PAIRING;
      const bool RETURN_DEPOT;
      const bool FORCE_ASSIGN_SAME_GOALS;

      std::string concordeExec;
      GridCoordsVectorVector guiClusteredGoals;

      struct SVertex {
         GridCoords coords;
         int goalIDX;
         CDistanceTransform* dt;

         SVertex(GridCoords coords, int idx = -1) : coords(coords), goalIDX(idx), dt(0) {}
         ~SVertex() { if (dt) { delete dt; } }
      };

      typedef std::vector<SVertex*> VertexPtrVector;
      VertexPtrVector vertices;
      public:
      static imr::CConfig& getConfig(imr::CConfig& config);

      CMTSPKMeans(imr::CConfig& cfg, CPathPlanning& paths, bool returnDepot, bool forceAssignGoals);
      ~CMTSPKMeans();

      void setGui(gui::CGui* g) { gui = g; }

      IntVector& assign(const DoubleMatrix& costMatrix, const GridCoordsVector& goals, const RobotPtrVector& robots, IntVector& goalsIDX);
      const GridCoordsVectorVector& getClusteredGoals(void) const { return guiClusteredGoals; }

      private:
      void alloc_vertices(const GridCoordsVector& goals, const RobotPtrVector& robots);
      void release_vertices(void);
      IntVectorVector& kmeans(const GridCoordsVector& goals, GridCoordsVector& means, IntVectorVector& cluster);
      int solveLK(const int robotIDX, const RobotPtrVector& robots, const GridCoordsVector& goals, const IntVector& robotGoals, IntVector& newRoute, double& length);
      void fixAssignment(const IntVectorVector& routes, IntVector& goalsIDX);

      void drawAssignment(const GridCoordsVector& goals, const RobotPtrVector& robots, const IntVectorVector& cluster, const IntMatrix& firstGoals, const IntVector& assignment);
   };
} //end namespace mre

#endif

/* end of mre_strategy_mtsp_kmeans.h */
